function [ RFangle, RFphase ] = rot_mat2angles( R, prec )
%[ RFangle, RFphase ] = rot_mat2angle(  R, prec )
%  1x1xN vectors with flip angle (rad) and phase (rad) of RF pulse 
warning('rot_mat2angles:obsolete', 'Old function; use RFpulse2Flipangle as replacement with improved robustness and more convenient interface.')
warning('off', 'rot_mat2angles:obsolete');
if nargin < 2 || isempty( prec )    
    prec = 1 - 1e-9;
end 
RFangle = acos( max(-prec,min(prec, R(3,3,:) ) ) );
RFphase = angle( complex( R(1,3,:), R(2,3,:) ));

end %rot_mat2angles.m

function test


% R_phase = angle( R(3,2,:) - R(2,3,:) + ( R(1,3,:) - R(3,1,:) ) * 1i );  % phase angle (rad) of RF pulse in xy-plane
% R_fa    = acos( ( R(1,1,:) + R(2,2,:) + R(3,3,:) - 1 ) / 2 );           % flip angle  (rad) of RF pulse


%% check if conversion is precise enough for required flip angles
N = 2000;
theta0 = pi*10.^( -8*rand( 1, N) );
phi0 = eps-pi + 2*pi*rand( 1, N);
theta1 = theta0;
phi1 = phi0;

R = angles2rot_mat( theta0, phi0 );
[theta1(:), phi1(:)] = rot_mat2angles( R );

figure;
[~, idx] = sort( theta0, 'ascend' );
loglog( theta0( idx ), abs( ( theta1( idx ) - theta0( idx ) )./theta0( idx ) )  );
grid on
xlabel( 'flip angle [rad]' )
ylabel( 'relative error' );

end