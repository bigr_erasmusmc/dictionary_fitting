function fn = save_niiTgz( fn, img, T , tempdir)
% fngz = save_niiTgz( fn, nii [, [] , tempdir]);
% fngz = save_niiTgz( fn, img, T   [, tempdir]);
%
% Saves img to fn
% Convenience interface to immediately store the nifti. Calls make_niiT( img, T)
% Call gz on the stored nifti and remove the nii file.
% Don't include the .gz extension in fn; its generated automatically.
%
% INPUTS:
%   fn: path+filename of the nii image, including the .gz extension is optional. 
%   img : image data to be stored. 
%   T   : affine transformation specifying world coordinates (see affineTransformFromDicominfo)
%   nii : already prepared nifty structure as created by make_niiT
%   tempdir: optional argument containing a folder for temporary files. 
%
% OUTPUTS:
%  fngz : final filename, including the gz extension. 
%
% Created by Dirk Poot, Erasmus MC. 19-7-2012
%  D.Poot  30-1-2017: updated help and added output 

if ~isstruct(img)
    nii = make_niiT(img,  T);
else
    nii = img;
end;

if nargin>3
    [outdir, outfile, outext ] = fileparts(fn);
    if isequal(outext,'.gz')
        [outdir, outfile, outext ] = fileparts([outdir, outfile]);
    end;
    tempfile = fullfile( tempdir, [outfile outext]); 
    fprintf('saving: %s', tempfile );
    save_nii(nii, tempfile);
    fprintf('.gz');
    if 0
        % use gzip store in different directory argument
        gzip( tempfile, outdir );
    else
        % zip in place and move (seems to be MUCH faster over network)
        gzip( tempfile );
        fprintf('\nMove gz file to %s', outdir);
        movefile( [tempfile '.gz'] , outdir);
    end;
    delete( tempfile );
else
    [outdir, outfile, outext ] = fileparts(fn);
    if isequal(outext,'.gz')
        fn = fullfile(outdir, outfile) ;
    end;
    fprintf('saving: %s', fn );
    save_nii(nii, fn);
    fprintf('.gz');
    gzip( fn );
    delete( fn );
end;
fn = [fn '.gz'];
fprintf('\n');
