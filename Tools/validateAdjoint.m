function result = validateAdjoint( mulA, mulAt, size_x, numTests)
% result = validateAdjoint( A, AT, size_x, numTests);
%
% INPUTS:
% mulA  : function taking 1 argument. It should be a linear function; 
%         so  'y = mulA( x )' == 'y = reshape( A*x(:), size_y)' for the matrix 
%         A (implicitly) specified in mulA.
% mulAt : function taking 1 argument. It should be a linear function, implementing 
%         the adjoint of mulA, so 'r = mulAt( y )' == 'r = reshape( A'*y(:), size_x)'
% numTests : If numTests is a postive integer: 
%              Tests if At and A' are the same by multiplying both with a random 
%              matrices having numTests columns. This is (rather) thourough test. 
%              However, any differences cannot be localized easily onto parts of x or y. 
%            If numTests is a negative integer: 
%              Test if At really is A' at -numTests rows&columns, usefull for debugging 
%              when A and At are (different) functions. (As bugs might intoduce 
%              differences). The -numTests rows and columns are selected randomly, 
%              so negative numTests might not be appropriate for large sparse A (& At) 
%              unless numTests is also large (You might not hit any nonzero value)
% 
% OUTPUTS:
%  result : structure with the fields:
%    Areduced  : (Reduced) explicit A as obtained with mulA. 
%    ATreduced : (Reduced) explicit A as obtained with mulAt
%    reduce_x  : row vector with indices of tested elements of x, 
%			     or numel(x) x numTests random matrix with which mulA is tested. 
%    reduce_y  : row vector with indices of tested elements of y, 
%			     or numel(y) x numTests random matrix with which mulAt is tested. 
%    maxAcceptableErr = maxAcceptableErr;
% 
% 18-3-2017, Created by Dirk Poot, Erasmus MC; extracted test from cgiterLS. 


%% Testing A and At functions for consistency:
if numel(size_x)<2
    size_x(end+1:2)=1;
end;
numel_x = prod( size_x );
numTestsorig = numTests;
if numel_x/2<abs(numTests)
    % (almost) exhaustive test:
    reduce_x = round(linspace(1, numel_x, min(abs(numTests), numel_x)));
    numTests = numel(reduce_x);
elseif numTests>0
    % randomized tests:
    reduce_x = randn( numel_x , numTests);
else % numTests<0
    reduce_x = sort(ceil(rand(1,-numTests)*numel_x));
end;
ntests = abs( numTests );
storeAt = zeros( ntests, ntests );
storeA  = zeros( ntests, ntests);
progressbar('start',ntests);
for k = 1 : ntests
    if size(reduce_x,1)==1
        tst = zeros(size_x);
        tst( reduce_x(k) )=1;
    else
        tst = reshape( reduce_x(:,k) , size_x );
    end;
    r = mulA(tst);
    if k==1
        size_y = size(r);
        numel_y= prod( size_y );
        if numel(r)/2 < abs(numTestsorig) && (numTests>0)
            reduce_y = min((1:numTests),numel_y);
        elseif numTests>0
            reduce_y = sort(ceil(rand(1,numTests)*numel(r)));
        else % numTests<0
            reduce_y = randn( numel(r) , -numTests);
        end;
        %rx = find(abs(r(:))>.53); % tuned to be numTests
    elseif size_y ~= size(r)
        error('size not constant');
    end;
    if  size(reduce_y,1)==1
        storeAt(:,k)= r(reduce_y);
        
        tst = zeros( size_y );
        tst( reduce_y(k) )=1;
    else
        storeAt(:, k)= (r(:)'*reduce_y)';
        
        tst = reshape( reduce_y(:,k), size_y );
    end;
    r = mulAt(tst);
    if size(r)~=size_x
        error('Size of output of mulAt not equal to size_x.');
    end;
    if size(reduce_x,1)==1
        storeA( :, k ) = r( reduce_x );
    else
        storeA( :, k )= (r(:)'*reduce_x)';
    end;
    progressbar(k);
end;
progressbar('ready');
storeAt = storeAt';
% Acceptable error when < 1e-12 of maximum value in jacobian, or (when larger), 1e-8 of maximum value in row/column. 
maxAcceptableErr = max( 1e-12* max(abs(storeA(:))), 1e-8 * bsxfun(@max, max(abs(storeA),[],1), max(abs(storeA),[],2) ));
disp(['Maximum absolute difference: ' num2str(max(max(abs(storeA-storeAt)))) ' maximum absolute values : ' num2str([max(max(abs( storeA))) max(max(abs(storeAt)))])]);
if any( any( abs( storeA-storeAt)>maxAcceptableErr  ))
    warning('validateJacobian:Error','Difference between A and At seems to be too large.')
    imagebrowse(cat(3,storeA,storeAt,storeA-storeAt,abs(storeA)-abs(storeAt)),[],'description','The four images (third dimension) are (the same part of): [ A, At'', A-At'', abs(A)-abs(At'') ]' )
    % imagebrowse(reshape(abs(cat(3,storeA,storeAt',storeA-storeAt',storeA-storeAt')),[18 12 12 numTests 4]))    
    disp('Inspect image, to see if A and At are equal enough.');
    % keyboard;
else
    disp('validateAdjoint: difference between A and At seems to be small enough');
end;
if nargout>=1
    result = struct;
    result.Areduced = storeA;
    result.ATreduced = storeAt;
    result.reduce_x = reduce_x;
    result.reduce_y = reduce_y;
    result.maxAcceptableErr = maxAcceptableErr;
end;
%     storeA,storeAt
%     storeA-storeAt

% function [reduce] = make_reduce( size_in, numTests )
% 
% if prod(size_in) / 2 < abs(numTests)
%     % exhaustive test:
%     reduce = ( 1 : prod(size_in) );
%     
%     numTests = numel(ry);
% elseif numTests>0
%     reduce = unique( sort(ceil(rand(1,numTests)*numel_x)) );
% else % numTests<0
%     reduceM = randn( prod(size_in) , -numTests);
% end;
% 
% function [mat, size_out] = testfun( fun, size_in, reduce_in, reduce_out )
% 
% mat = zeros( size( reduce_out, 2) , size( reduce_in, 2));
% ntests = size( reduce_in, 2);
% progressbar('start',ntests);
% for testnr = 1 : ntests
%     if size( reduce_in , 1 )==1
%         z = zeros( size_in );
%         z( reduce_in( testnr) ) = 1;
%     else
%         z = reshape( reduce_in(:, testnr ), size_in );
%     end;
%     out = fun( z );
%     size_out = size(out);
%     if size( reduce_out, 1) ==1 
%         mat(:, testnr ) = out( reduce_out );
%     else
%         mat(:, testnr ) = (out(:)'*reduce_out)';
%     end;
% end;
function test()
%% Test with real matrix:

A = randn(20,10);
mulA = @(x) A*x;
mulAt = @(y) A'*y;
result = validateAdjoint( mulA, mulAt, size(A,2), 3)
result = validateAdjoint( mulA, mulAt, size(A,2), -5)
result = validateAdjoint( mulA, mulAt, size(A,2), 125)
%% Test with complex matrix:
A = complex( randn(23,83),randn(23,83)) ;
mulA = @(x) A*x;
mulAt = @(y) A'*y;
result = validateAdjoint( mulA, mulAt, size(A,2), 7)
result = validateAdjoint( mulA, mulAt, size(A,2), -8)
result = validateAdjoint( mulA, mulAt, size(A,2), 125)

%% test incorrect complex:
A = complex( randn(45,51),randn(45,51)) ;
mulA = @(x) A*x;
mulAt = @(y) A.'*y;
result = validateAdjoint( mulA, mulAt, size(A,2), 7)
result = validateAdjoint( mulA, mulAt, size(A,2), -8)
result = validateAdjoint( mulA, mulAt, size(A,2), 125)

