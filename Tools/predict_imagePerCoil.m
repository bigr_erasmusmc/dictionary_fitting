function [F, dF] = predict_imagePerCoil( theta, nImages, nCoils, outputCoilNorm)
% [F, G] = predict_imagePerCoil( theta, nImages, nCoils)
%
% Function for fitting images and coil sensitivities from initial
% reconstructions per coil. 
%
% imgRidx  = 0        + (1:nImages);
% imgIidx  = nImages  + (1:nImages);
% coilRidx = 2*nImages+ (1:nCoils);
% coilRidx = 2*nImages+nCoils+ (1:nCoils);
%
% images  = complex( theta(imgRidx, :), theta(imgIidx, :) );
% coils_in= complex( theta(coilRidx,:), theta(coilIidx,:) );
% coils_nrm = sqrt( sum(coils_in.*conj(coils_in) ) ); % norm of coils. 
% coils     = coils_in ./ coils_nrm( ones(nCoils,1), :) ;
%
% tmp  =  images(:,k) * coils(:,k).'  ; 
% F(:, k) = [real(tmp(:)) ; imag(tmp(:))];
%
% Created By Dirk Poot, Erasmus MC, 26-6-2015

if size(theta,1)~=2*nImages+2*nCoils
    error('wrong size of theta');
end;
imgRidx  = 0        + (1:nImages);
imgIidx  = nImages  + (1:nImages);
coilRidx = 2*nImages+ (1:nCoils);
coilIidx = 2*nImages+nCoils+ (1:nCoils);

images  = complex( theta(imgRidx, :), theta(imgIidx, :) );
coils_in= complex( theta(coilRidx,:), theta(coilIidx,:) );
coils_nrm = sqrt( sum(coils_in.*conj(coils_in) ,1 ) ); % norm of coils. 
coils     = coils_in ./ coils_nrm( ones(nCoils,1), :) ; % normalize coils. 

[imgrep, coilrep] = ndgrid(1:nImages, 1:nCoils);
Fc  =  images(imgrep,:) .* coils(coilrep,:) ; 
F = [real(Fc);imag(Fc)];
if outputCoilNorm~=0
    F(end+1,:) = (coils_nrm -1) * outputCoilNorm;
end;

if nargout>1
    %Mathematica:
    % coilNrm = Sqrt[(CR1^2 + CI1^2 + CR2^2 + CI2^2)]
    % F1 = (imR1 + I imI1) * (CR1 + I CI1)/coilNrm
    % D[F1, imR1]
    % D[F1, imI1]
    % D[F1, CR1]
    % D[F1, CI1]
    % D[F1, CR2]
    % D[F1, CI2]
    dFc = cell(1,size(theta,1));
    for k=1:nImages
        dFk = coils(coilrep,:);
        dFk( imgrep~=k ,: )=0;
        dFc{imgRidx(k)} =    dFk;
        dFc{imgIidx(k)} = 1i*dFk;
    end;
    
    for k=1:nCoils
        dFk = images(imgrep,:)./coils_nrm( ones(numel(imgrep),1), :);
        dFk( coilrep~=k ,: )=0;
        dFc{coilRidx(k)} =     dFk - Fc .* repmat( theta(coilRidx(k),:) ./ coils_nrm.^2 ,[numel(imgrep) 1] );
        dFc{coilIidx(k)} = 1i* dFk - Fc .* repmat( theta(coilIidx(k),:) ./ coils_nrm.^2 ,[numel(imgrep) 1] );
    end;
    dFc = cat(3,dFc{:});
    dF = [real(dFc);imag(dFc)];
    if outputCoilNorm~=0
        thetaCoil = zeros(size(theta));
        thetaCoil([coilRidx;coilIidx],:) =  theta([coilRidx;coilIidx],:);
        dF(end+1,:,:) = bsxfun(@times, outputCoilNorm ./ coils_nrm, permute(thetaCoil,[3 2 1]));
    end;
end;

function test()
%%
nImages = 2;
nCoils  = 3;
tht = randn(nImages*2+nCoils*2,5);
[F, dF] = predict_imagePerCoil( tht, nImages, nCoils, 0);
[F, dF] = predict_imagePerCoil( tht, nImages, nCoils, 123);
validateJacobian( @(t) predict_imagePerCoil( t, nImages, nCoils, 0) , tht);
validateJacobian( @(t) predict_imagePerCoil( t, nImages, nCoils, 123) , tht);