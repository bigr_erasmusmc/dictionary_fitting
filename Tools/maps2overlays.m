function [overlays] = maps2overlays( maps, mask, includeCRLB)
% [overlays] = maps2overlays( maps, mask)
% Converts the 'maps' structure as used in the mapping scripts of DPoot
% (e.g. T2_SE.m, T1_MOLLI.m,..) into an 'overlays' structure that can be
% used with imagebrowse to overlay on the images used for fitting.
%
% INPUTS:
%  maps : a structure array with (at least) the fields:
%     - name : string containg the name of the variable in this map
%     - unit : string containg the unit of the values in 'map'
%     - ROI  : optional; string containg the name of the ROI of this map. 
%     - map  : an up to 3D numeric array with the actual map of the variable 'name' 
%              all (up to) 3 dimensions should be spatial dimensions. (It is assumed 
%              that the first dimension of the images used to fit on contains the different 
%              contrasts/time points)
%     - sCRLB : optional standard deviation estimate of each value in map.
%               (typically provided as square root of the Cramer Rao lower bound)        
%     - plotrange : 2 element vector with the minimum and maximum value of
%                   the range to be plotted. 
%     - colormap : optional; specifies the colormap used to display this
%                   map. Default = Jet. 
%     - mask : optional; The mask, which in nonzero in the locations where the map
%              should be displayed. Values between 0 and 1 specify
%              opacity (0 = fully transparant, 1 = fully opaque)
%              if not provided and also no mask argument is provided, all
%              finite values are shown fully opaque. 
%  mask : ND array with the default mask that is used for the entries in which 
%         maps does not specify a mask. 
%  includeCRLB : default = true; If true, create separate overlays for the CRLB (even when sCRLB field exists)
%                when false: CRLB is not included in overlays. 
%
% Created by Dirk Poot, Erasmus MC, 30-6-2016

if nargin<3 || isempty(includeCRLB)
    includeCRLB = true;
end;
porder = [5 1 2 3 4];
overlays = struct('image',cell(numel(maps),1));
overlayidx = 1;
if nargin>1
    tmpmask = permute( logical(mask), porder );
else
    tmpmask = [];
end;
for k = 1 : numel(maps)
    if ~isreal( maps(k).map )
        error('maps(%d).map is not real, cannot create as overlay',k);
    end;
    overlays(overlayidx).image = permute( maps(k).map, porder );
    if isfield(maps, 'mask') && ~isempty( maps(k).mask )
        % maps.mask has priority:
        curmask = permute( maps(k).mask, porder );
    else
        if ~isempty(tmpmask)
            % if maps.mask does not exist and mask argument is provided, that is used:
            curmask = tmpmask;
        else
            % else use finite values in image:
            curmask = isfinite(overlays(overlayidx).image);
        end;
    end;
    overlays(overlayidx).mask = curmask ;
    if isfield(maps,'colormap') && ~isempty(maps(k).colormap)
        curmap = maps(k).colormap;
    else
        curmap = colormap('Jet');
    end;
    overlays(overlayidx).colormap = curmap;
    overlays(overlayidx).min = maps(k).plotrange(1);
    overlays(overlayidx).max = maps(k).plotrange(2);
    if isfield(maps,'ROI') && ~isempty(maps(k).ROI)
        curname = sprintf('%s %s (%s)', maps(k).name, maps(k).ROI, maps(k).unit );
    else
        curname = sprintf('%s (%s)', maps(k).name, maps(k).unit );
    end;
    overlays(overlayidx).name = curname;
    overlayidx = overlayidx +1;
    if isfield(maps,'sCRLB') && ~isempty(maps(k).sCRLB) && includeCRLB
        overlays(overlayidx).image = permute( maps(k).sCRLB, porder );
        overlays(overlayidx).mask = curmask;
        overlays(overlayidx).colormap = colormap('Copper');
        overlays(overlayidx).min = 0;
        overlays(overlayidx).max = diff(maps(k).plotrange);
        overlays(overlayidx).name = sprintf('sCRLB %s (%s)', maps(k).name, maps(k).unit );
        overlayidx = overlayidx+1;
    end;
end;