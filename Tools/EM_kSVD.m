function [u,s,v] =  EM_kSVD( calmat , k, nonzeropatt, maxiter) 
% [u,s,v] =  EM_kSVD( mat, k, correctdatamask, maxiter) 
% Simple implementation of the EM svd algorithm for missing data mentioned
% in Kurucz_KDDCup2007_Methods for large scale SVD with missing values.pdf
%
% Iterates SVD and filling in missing data by current SVD truncated to k elements. 
%
% Computes the EM algorithm for the missing data svd.
% INPUTS:
%  mat : a matrix of which the SVD should be computed
%  k   : truncation order in the iterations; default .3*min(size(mat))
%  correctdatamask: index or mask into mat that selects the correct values.
%        By default all nonzero, finite values. 
%  maxiter: (maximum) number of EM iterations
%
% Created by Dirk Poot, 14-7-2016, Erasmus MC

if nargin<2 || isempty(k)
    k = max(1,round(min(size(calmat))*.3));
end;
if nargin<3 || isempty( nonzeropatt )
    nonzeropatt = calmat~=0 && isfinite(calmat); 
end;
if nargin<4 || isempty(maxiter)
    maxiter = 30;
end;
%%
cur = zeros(size(calmat));
lp = true;  % TODO: add check for convergence. 
iter = 0;
if all(nonzeropatt(:))
    maxiter = 1;
end;
diags = cell(1,maxiter);
while lp && iter < maxiter
    iter = iter + 1;
    cur(nonzeropatt) = calmat(nonzeropatt);
    [u,s,v]=svd(cur,'econ');
    cur = u(:,1:k)*s(1:k,1:k)*v(:,1:k)';
    diags{iter} = diag(s);
end;