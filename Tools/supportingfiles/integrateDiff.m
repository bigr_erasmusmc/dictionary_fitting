function [V] = integrateDiff( G , initialV,  varargin)
% [V] = integrateDiff( {G1, G2, [G3,...]}, initialV [, option1 , value1 ] )
% Integrates the gradients in the first, second ,.. dimension to a field V.
% If the gradients are not exactly integratable, the least squares solution 
% is found.
% points and values specify the value of some points. (Implicitly) specifies 
% the integration constant.
% Also see integrateGradient that uses central differences instead of the forward differences used here. 
%
% This function solves (pseudocode):
% V = arg min_V   sum( for each dim : (diff(V,[],dim)- G{dim}).^2 ) 
%                   + (V(points)-values)'* weights * (V(points)-values) 
%
% nan values in the gradients: do not add error for that gradient.
%
% E.g:
% V_in = randn(20,19,18);
% [G1] = diff(V_in,[],1)
% [G2] = diff(V_in,[],2)
% [G3] = diff(V_in,[],3)
% V = integrateGradient({G1,G2,G3}, [1 1 1], V_in(1,1,1) );
% max(abs(V(:)-V_in(:)))      % is very small, (roundoff errors only)
%
% INPUTS:
% G : cell array with N almost equal sized N dimensional elements for N dimensional V
%     Only difference in size: G{dim} is 1 smaller than V in dimension dim (due to diff)
% initialV   : Optional initial image for V. If the provided image is close to the actual 
%              solution, convergence speed might be improved. Since the problem that is solved
%              is convex, the same solution is always found (to within convergence tolerances).
% options:
% points : k x N  matrix with integer indexes of the k points that you want 
%                 to specify. 2 points per dimension is sufficient (even &odd), but you can use more to 
%                 fix V (approximately) in several places.
%          k x 1 : linear index into V is assumed.
% values : k element column vector with the target values of V at points
% weights: scalar, k element weight vector, or k x k weight matrix. 
%          Default = [] (=> 1 ) : error in value is weighted equal to error in gradient.
% periodic : default = false; if true assumes 2*pi periodicity in gradients (i.e. they are angles) 
% If the points & value arguments are omitted, an arbitrary offset is introduced in V.
% 
% Created by Dirk Poot, Erasmus MC, 25-2-2016
% Based on integrateGradient by Dirk Poot. 

opt = struct;
opt.periodic = 0;
% opt.period = 2*pi;
opt.points = [];
opt.values = [];
opt.weights = [];
if nargin>2
    opt = parse_defaults_optionvaluepairs( opt, varargin{:});
end;

% Parse G:
% Check size:
ndim = numel(G);
sz = size(G{1});sz(1)=sz(1)+1;
if numel(sz)<ndim || any(sz(ndim+1:end)~=1)
    error('integrateGradients: Number of inputs should match the dimension of each input.');
end;
for k=1:ndim
    szk = sz;szk(k) = sz(k)-1;
    if ~isequal( szk, size(G{k}));
        error('integrateGradients: Size of inputs should be equal except that G{k} should be 1 smaller in dimension k.')
    end;
end;
% Check for NaN values: (these positions are not taken into account)
Gnan = cell(numel(G),1);
anyGnan = false;
for k=1:numel(G)
    Gknan =isnan(G{k}); 
    if any(Gknan(:))
        anyGnan  = true;
        mnG = mean(G{k}(~Gknan));
%         w=1./(1+[-10:2:10].^2);w = w/sum(w); 
%         Gknanw = separableConvN_c( 1-Gknan, repmat({w},1,ndim) );
        if nnz(Gknan)<.2*numel(Gknan)
            Gknan = find(Gknan); % only use linear indexing with few nan values, as logical indexing is faster when selecting many values.
        end;
        Gnan{k} = Gknan;
        G{k}(Gknan) = 0 ; % remove nan values. 
    else
        Gnan{k} = [];
    end;
end;
finiteV = isfinite(initialV);
if ~all(finiteV(:))
    initialV(~finiteV)=0; % remove infinites in input. 
end;

setV = @(V) reshape(V,sz);
selV = @(V) V;

% Least squares solve:
% V = arg min_V   sum( (gradient(V)-G{:}).^2 ) + (V(pnt)-values)'*weights * (V(pnt)-values) ) 
%   = arg min_V   sum( (A * V(:) - G{:}).^2 )  + (V(pnt)-values)'*weights * (V(pnt)-values) ) 
%   = arg min_V   (A * V(:) - G{:})' *(A * V(:) - G{:}) + (V(pnt)-values)'*weights * (V(pnt)-values) 
%   = arg min_V    V(:)'*A'*A*V(:)  - 2 *G{:}' *A * V(:) + G{:}'*G{:} + V(pnt)'*weights * V(pnt) - 2*V(pnt)'*weights*values + values'*weights * values 
% => 2A'*A*V +  2*weights * V(pnt) = 2A'*G + 2*weights*values
% => V = (A'*A + weights(inv_pnt) +laplacianweight* L'*L )^(-1) * (A'*G + weights*values)

% nonlinear least square solve (periodic):
% V = arg min_V   sum( ( for all dim (diff(V,[],dim)-G{dim}).^2 ) + (1-cos(V(pnt)-values)).*weight ) 
% V = arg min_V   sum( V(:)'*A'*A*V(:) - 2 *G{:}' *A * V(:) + G{:}'*G{:} + (1-cos(V(pnt)-values)).*weight ) 
%
% D[C,V] = 2A'*A*V - 2 *G{:}' *A + weight.*sin(V(pnt)-values) 

if ~opt.periodic
    y = mulWithAt(G);
%     if anyGnan
        mulAL = @(V) selV( mulWithAt(mulWithA(setV(V), Gnan)) );
%     else
%         mulAL = @(V) reshape(gradientMulND(reshape(V,sz),1),[],1);
%     end;

    if ~isempty(opt.points)
        if size(opt.points,2)==1
            plin = opt.points;
        else
            cumsz = cumprod([1 sz(1:end-1)])';
            plin = (opt.points-1)*cumsz + 1; % errors if size(points) incorrect.
        end;
        weights = opt.weights;
        y = V_add_mulBt(y, opt.values, plin, weights);
        mulALB = @(V) V_add_mulBt( mulAL(V) , mulB(V, plin) , plin, weights );
    else
        mulALB = mulAL;
    end;
    if nargin<2 
        initialV = [];
    else
        initialV = initialV(:);
    end;

    [V ,crit, y_fit,trace]= cgiterLS( @(V) 0, @(V) V, y, initialV, mulALB ,1000);
else
    % nonlinear/periodic constraint:
    funcs = struct;
    mulA  = @(V) mulWithA( setV(V), Gnan) ;
    mulAT = @mulWithAt;
    
    funcs(1).fun = @(V) leastSquaresCostFunction( V, mulA, mulAT, G, false, @minG, @dotG );
    funcs(1).hessmul = funcs(1).fun('hessmulfun');
    funcs(2).fun = @(V) periodicCost(V, opt.values, opt.weights);
    funcs(2).hessmul = @periodicCost_hessmul;
    [preconinfo, preconfun] = spatialRegularizerMakePreconditioner(struct('sizeTheta',[1 size(initialV)]),[1 -.5]);
    if 0
        fun = @(x) addCostFunctions( x, {funcs(1).fun, funcs(2).fun}, {funcs(1).hessmul, funcs(2).hessmul});
        [f,g,hinfo]=fun(initialV);
        hmulfun = fun('hessmulfun')
        hmul = @(x) hmulfun(hinfo,x);
        [condest, maxeigv, mineigv, approxmaxeigv, approxmineigv] = inverse_free_condest( hmul , numel(initialV) , 1, 1000);
        Mfun = @(x) preconfun( preconinfo, x);
        [condest2, maxeigv2, mineigv2, approxmaxeigv2, approxmineigv2] = inverse_free_condest( hmul , numel(initialV) , 1, 1000, Mfun);
        imagebrowse(reshape( cat(3,approxmaxeigv, approxmineigv, approxmaxeigv2, approxmineigv2), [size(initialV) 4]))
    end;
    precon_mul = @(dummy, x) preconfun( preconinfo, x);
    V = conjgrad_nonlin(funcs, initialV ,'max_iter',400,'Preconditioner_Multiply',precon_mul,'linesearch_method',2,'max_iter_linesearch',10);
    if 0 
        V2 = conjgrad_nonlin(funcs, 0*initialV ,'max_iter',400,'Preconditioner_Multiply',precon_mul,'linesearch_method',2,'max_iter_linesearch',10);
        V3 = conjgrad_nonlin(funcs, 0*initialV ,'max_iter',400,'linesearch_method',2,'max_iter_linesearch',10);
        V4 = conjgrad_nonlin(funcs, 0*initialV ,'max_iter',400);
        %%
        imagebrowse(cat(4,V,V2,V3,V4))
    end;
end;
V = setV(V);


function G = mulWithA(V, Gnan )
% Define
%    A*V(:) =  grad(V) 
% Compute
%   A'*A*V(:)
ndim = ndims(V);
G = cell(ndim,1);
for dim = 1 : numel(G)
    G{dim} = diff(V,[],dim);
end;
if nargin>1
    for k=1:numel(Gnan)
        if ~isempty(Gnan{k})
            G{k}(Gnan{k})=G{k}(Gnan{k})*.1;
        end;
    end;
end;
% G = cat(ndim+1, G{:});

function V = mulWithAt(G)
% Define
%    A*V(:) =  grad(V) 
% Compute
%   A'*A*V(:)

% ndim = ndims(G{1});
szV = size(G{1});szV(1) = szV(1)+1;
if any(szV(1:numel(G))<4)
    error('Currently should have at least 4 elements in each dim');
end;
V =0;
f = [-1 1];
for dim = 1 : numel(G)
    V = V + convn(reshape(f,[ones(1,dim-1) 2 1]), G{dim});
end;
V = V(:);

function PntW = mulB(V, plin)
% define
%  B * V(:) = V(plin) .* sqrt( weights )
% Compute 
%  B'*B * V(:)

PntW = V(plin);

function V = V_add_mulBt(V, pnt, plin, weights)
% define
%  B * V(:) = V(plin) .* sqrt(weights)
% Compute 
%  B'*B * V(:)
if isempty(weights)
    pntW = pnt;
elseif numel(weights)==numel(pnt)
    pntW = pnt.*weights;  % vector weight.
else
    pntW = weights * pnt; % scalar or matrix weight;
end;
    
V(plin) = V(plin) +  pntW;

function [A] = minG(A,B)
for k=1:numel(A)
    A{k} = A{k} - B{k};
end;
function [n] = dotG(A)
n = 0;
for k=1:numel(A)
    n = n + dot(A{k}(:),A{k}(:));
end;
function [C , dC, hinfo] = periodicCost( V, value, weight)
resid = V-value;
cresid = cos(resid);
C = (1-cresid).*weight;
C = sum(C(:));
if nargout>1
    dC = weight.*sin(resid);
    if nargout>2
        hinfo.H = reshape( cresid.*weight ,[],1);
    end;
end;
function [Hx] = periodicCost_hessmul( hinfo, X)
Hx = hinfo.H .* X;