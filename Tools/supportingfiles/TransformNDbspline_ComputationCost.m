function [flops, addressing, memory, str]  = TransformNDbspline_ComputationCost( in, T, mapT, size_out_arg, filterbank, bsplineorder, AR, clip_o)
% [flops, addressing, memory, str]  = TransformNDbspline_ComputationCost( in, T, mapT, size_out_arg, filterbank, bsplineorder, AR, clip_o)
%
% Returns the (theoretical) computational complexity of TransformNDbspline with the given arguments.
%
% 
% 

% first part (argument parsing) is copy from TransformNDbspline_ReferenceImplementation
if nargin~=8
    error('8 inputs required');
end;
if numel(size_out_arg) ~= ndims( in )
    error('newSz should be a vector of length ndims(in)');
end;
doDoutDT = nargout>1;

tagdims = find( isnan( size_out_arg ) );
procdims = find( ~isnan( size_out_arg ) );
doAdjoint = all( size_out_arg(procdims) < 0 );
if doAdjoint
    size_out_arg( procdims ) = -size_out_arg( procdims );
    if doDoutDT
        error('cannot compute DoutDT of adjoint transform.');
    end;
end;
if any( size_out_arg < 0 )
    error('newSz should be positive (or all negative to specify adjoint)');
end;

size_in = size(in);
size_out = size_out_arg;
size_out(tagdims) = size_in( tagdims );
if doAdjoint
    size_i = size_out;
    size_o = size_in;
else
    size_i = size_in;
    size_o = size_out;
end;
size_Tfull = [numel(procdims) size_o]; size_Tfull(tagdims+1) = 1;
size_T = size(T);
size_T(end+1:numel(size_Tfull)) = 1; % extend with scalar dimensions. 

isAffineTransform = (ndims(T)==2) && (size(T,1)==numel(procdims)) && (size(T,2)==size(T,1)+1) && isempty(mapT);

if isempty(bsplineorder) || numel(bsplineorder) > 1+(~isAffineTransform) 
    error('bsplineorder should be scalar or up to 2 element vector when T is not specifying an affine transform');
end;
bsplineorderT = bsplineorder(end);
bsplineorder = bsplineorder(1);

nprocdims = numel(procdims);

bsplineops = [0 1 7 17] ; % # flop for evaluating bspline coefficients   bsplineops( bsplineord+1 ); obtained by inspecting bspline_proc.cpp (implementation bSplineCoeffs)
flopbspline = bsplineops( bsplineorder+1 )*nprocdims;
flopbsplinestr = sprintf('%d*%d (=%d, evaluating bspline)',bsplineops( bsplineorder+1 ), nprocdims, flopbspline);
if isempty(filterbank)
    filtlen = bsplineorder+1;
    flopfilterbank = 0;
    flopfilterbankstr = '';
else
    filtlen = size(filterbank,1);
    flopfilterbank = filtlen*(bsplineorder+1)*nprocdims;
    flopfilterbankstr = sprintf('+ %d*%d*%d (=%d, filters from filterbank ) ', filtlen, (bsplineorder+1), nprocdims, flopfilterbank);
end;
flopsamplingstr = '';
flopsampling = 0;
for dim = nprocdims:-1:1
    flopsamplingstr = [flopsamplingstr sprintf('%d^%d + ',filtlen,dim)];
    flopsampling = flopsampling + filtlen^dim;
end;
num_tag  = prod( size_o( tagdims) );
if num_tag~=1
    flopsampling = flopsampling + (num_tag-1)*filtlen^nprocdims;
    flopsamplingstr = [sprintf('%d * ', num_tag) flopsamplingstr];
end;
flopsamplingstr = sprintf('%s (=%d, sampling)', flopsamplingstr(1:end-2), flopsampling);

num_samp = prod( size_o( procdims) );
floptotal = flopsampling + flopfilterbank + flopbspline;
flops = [floptotal floptotal*num_samp];
addressing =[];
memory = [];
str = sprintf('#flop per sample : %s %s+ %s = %d \n', ...
                            flopsamplingstr, flopfilterbankstr, flopbsplinestr, floptotal);
