/*
 * This is a MEX-file for MATLAB.
 * Compile with:
 *     mex MI_ND_dirk.cpp 
 *  Add -g for debugging and then add -o for optimizing code and debugging. add -v for verbose mode.
 *
 * First I like to appologize for the huge piece of code below. It might look overwhelmingly complicated at first.
 * However, I think it is no more complicated than what is needed, and when looked at more carefully, it is not 
 * too difficult to understand. Part of the complication is the combination of extreme flexibility and high performance of this routine.
 * 
 * The matlab interface & input/output arguments are described at the start of 'mexFunction'.
 * 
 * This routine computes the mutual information histogram. To provide optimal performance I use the preprocessor a lot
 * to remove code that is not needed (in special cases). 
 * But for this to work, each function needs a unique name; so I first build the function name, and input arguments.
 * Where possible, if's with (possibly) constant arguments are used in the routine to select the required parts.
 * (I assume the compiler to remove brances of the if's that are never taken.)
 * When if's cant (easily) be used, the same preprocessor directives are used to remove peaces of the code.
 * The different versions are created by repeatively including this file with BUILDMIFUN and options defined.
 * The routine is a template to allow the instanciation for different data types.
 * A few versions are implemented separately for extreme performance. 
 * (e.g.: 5 CPU cycles per voxel for single precision images, without transform, smoothing or derivatives. Performance limit: main memory bandwidth)
 * 
 * When you want to create your own specialized routine (for speed):
 * - First : Find the list of includes below and add one where you declare all options you want to fix 
 *           (especially fixing interplen & filterlen are important for performance. 
 *			  When these are fixed the compiler should unroll all loops (but it does not))
 *           Then add a call to that special instance in the appropriate location of the calling tree at last part of 
 *			 'MexFunction' (end of this file)
 * - When you are still not satisfied with the performance; and you know it should be possible to make it faster:
 *   copy the entire routine; remove all unneeded parts (for your version). Unroll all loops & optimize. Test:
 *   Still to slow: fix datatypes and manually optimize with SSE instructions. (and prepare for a few days of coding)
 *   (At the Intel Core 2 quad, the most annoying performance limitting factor was loads that stalled due to write to 'not yet known' adress.)
 * 
 * Created by Dirk Poot, University of Antwerp.
 */

// If building function: (not defined at first)
#ifdef BUILDMIFUN


// Start building function name and function arguments, based on the options:
// TRANSFORM_IMG : apply affine transform to all images, except the first one (the fixed image)
// SMOOTHH       : Smooth the histogram
// DERIVATIVE	 : Compute derivative of histogram wrt transform parameters, only allowed in conjunction with smoothing and transform.
// TRANSFORMONLY : only transform image (and store into S0); don't compute histogram
// NOSCALEMINMAX : don't scale and clamp source images; WARNING: if set, image values outside bounds create segfaults!

// Construct arguments of main function:
#ifdef SMOOTHH
	#ifdef DERIVATIVE
		#define HISTARGS histtype * __restrict hist , histtype * __restrict dhdTransf,  const mwSize * __restrict sizeh, const filttype * __restrict hfilt, const filttype * __restrict dhfilt, size_t hfilterlen, size_t hfiltersteps,
		#define HISTARGSs hist, dhdTransf,  sizeh, hfilt, dhfilt, hfilterlen, hfiltersteps,
    #else
		#define HISTARGS histtype * __restrict hist, const mwSize * __restrict sizeh, const filttype * __restrict hfilt, size_t hfilterlen, size_t hfiltersteps,
		#define HISTARGSs hist,  sizeh, hfilt, hfilterlen, hfiltersteps,
    #endif
#else
//    #define HISTARGS histtype * __restrict hist, mwSize sizeh[Nimgs], 
    #define HISTARGS histtype * __restrict hist, const mwSize * __restrict sizeh, 
    #define HISTARGSs hist, sizeh, 
#endif

#define IMGSARGS const imgObjType * __restrict imgs, const mwSize * __restrict sizeIMG
//#define IMGSARGS imgObjType * imgs, mwSize sizeIMG[]
#define IMGSARGSs imgs, sizeIMG

#ifdef TRANSFORM_IMG
//	#define TRANSFORMARGS , const postype transform[Ndims*(Ndims+1)*Nimgs]
    #define TRANSFORMARGS , const postype * __restrict transform
	#define TRANSFORMARGSs , transform
#else
	#define TRANSFORMARGS 
	#define TRANSFORMARGSs 
#endif 	

#ifdef NOSCALEMINMAX
	#define SCALEANDOFFSETARGS
	#define SCALEANDOFFSETARGSs
#else
//	#define SCALEANDOFFSETARGS , imgtype offsetS[Nimgs] , imgtype scaleS[Nimgs]
    #define SCALEANDOFFSETARGS , const imgtype * __restrict offsetS , const imgtype * __restrict scaleS
	#define SCALEANDOFFSETARGSs , offsetS , scaleS
#endif

// specifiy correct function name:
#ifdef TRANSFORMONLY
    #define FUNNM imTransf
#else
    #define FUNNM MIhist
#endif

// construct extra arguments for image sample function
#ifdef DERIVATIVE
    #define SAMPLEDERIVATIVEARGS ,  postype * __restrict dSdPos
    #define SAMPLEDERIVATIVEARGSs , dSdPos
//    #define SAMPLEDERIVATIVEARGSs2 , dSdPos+Ndims*(imgindx-1)
#else
    #define SAMPLEDERIVATIVEARGS
    #define SAMPLEDERIVATIVEARGSs
//    #define SAMPLEDERIVATIVEARGSs2 
#endif

// construct image arguments for updHist function
#ifdef DERIVATIVE
    #define UPDHISTIMGARGS  imgtype S[Nimgs], imgtype dSdTransf[Ndims*(Ndims+1)*(Nimgs-1)]
    #define UPDHISTIMGARGSs  S, dSdTransf
#else
    #define UPDHISTIMGARGS  imgtype S[Nimgs]
    #define UPDHISTIMGARGSs  S
#endif

#ifdef SMOOTHH
    #define UPDHISTTEMPARGS , filttype * __restrict tempspace
    #define UPDHISTTEMPARGSs , tempspace
#else
    #define UPDHISTTEMPARGS 
    #define UPDHISTTEMPARGSs 
#endif

#pragma optimize( "t", on )
// First declare updater of histogram, as (huge) inline function:
#ifndef CREATEDUPDHISTFUN
#define CREATEDUPDHISTFUN
template <	 int Ndims, int Nimgs, size_t filterlen_, 
              typename histtype , 
			  typename imgtype
#ifdef SMOOTHH 
  			, typename filttype
#endif
            >
__inline void updHist ( HISTARGS histtype weight, UPDHISTIMGARGS SCALEANDOFFSETARGS UPDHISTTEMPARGS)
{
	imgtype t[Nimgs];		// processed image intensities.
	int i[Nimgs];			// truncated image intensities/histogram index.
    int hbase=0;
#ifdef SMOOTHH
	int f[Nimgs];
    const size_t filterlen = (filterlen_==-1 ? hfilterlen : filterlen_); 
    const size_t border = filterlen;
    filttype * __restrict curfilt = tempspace;  // use Nimgs * filterlen elements
    #ifdef DERIVATIVE
        filttype * __restrict dcurfiltdS = tempspace + Nimgs * filterlen; // use filterlen*(Nimgs-1) elements
    #endif
#else 
    size_t border = 1;
#endif	

    for(int imindx=Nimgs-1;imindx>=0;imindx--) {
        // Load/Scale image intensities 
#ifdef NOSCALEMINMAX
		t[imindx] = S[imindx];
#else
		t[imindx] = (S[imindx] + offsetS[imindx]) * scaleS[imindx];
		t[imindx] = minmax<imgtype>(0 , t[imindx] , (imgtype) (sizeh[imindx] - border));
#ifdef SMOOTHH
        filttype clampt = t[imindx];
#endif
#endif;
        // compute histogram index (and update total position)
        i[imindx] = (int) t[imindx];        // position in h
        hbase = hbase * sizeh[imindx] +i[imindx];
#ifdef SMOOTHH
        // if smoothing histogram: load the correct filter samples:
        t[imindx] = (t[imindx]-((imgtype) i[imindx]))*hfiltersteps; // fraction in h.
        f[imindx] = (int) t[imindx];                 // column index in filter.
        t[imindx] = t[imindx]-((imgtype) f[imindx]); // fraction in filter
        for (int delay = 0;delay<filterlen; delay++) {
            curfilt[ filterlen * imindx + delay ]                =   hfilt[ f[imindx]*filterlen + delay]*((filttype)(1-t[imindx])) +  hfilt[(f[imindx]+1)*filterlen + delay] * ((filttype)t[imindx]);
#ifdef DERIVATIVE
            if (imindx>0) {
                // Compute derivative of filter wrt image intensities:
                if ( (clampt==(sizeh[imindx] - border)) | (clampt==0) )   {
                    dcurfiltdS[ filterlen * imindx + delay ] = 0;
                } else {
                    if (dhfilt==NULL) {
                        dcurfiltdS[ filterlen * (imindx-1) + delay ] = ( hfilt[(f[imindx]+1)*filterlen + delay] - hfilt[f[imindx]*filterlen + delay]) * hfiltersteps ;
                    } else {	
                        dcurfiltdS[ filterlen * (imindx-1) + delay ] =  dhfilt[ f[imindx]*filterlen + delay]*((filttype)(1-t[imindx])) + dhfilt[(f[imindx]+1)*filterlen + delay] * ((filttype)t[imindx]);
                    }
#ifndef NOSCALEMINMAX                    
                    dcurfiltdS[ filterlen * (imindx-1) + delay ] *= scaleS[imindx];
#endif
                }
            }
#endif            
        }
#endif
    }
#ifdef SMOOTHH
    // Add the (properly shifted) Nimgs-D smooth kernel to the histogram:
    // Assume compiler removes loops that are not needed (MSVC does typically not do that completely, unfortunately)
    if (Nimgs>5) {
        mexErrMsgTxt("More than 5 images for smoothed histogram currently not supported.");
    }
    int allk[Nimgs];
    filttype filttmp[Nimgs+1];
    filttmp[Nimgs] = weight;
    for(int k4 = 0; k4 < (Nimgs>4?filterlen:1) ; k4++){
        if (Nimgs>4) {
            allk[4] = k4;
            filttmp[4] = filttmp[5] *  curfilt[ filterlen * 4 + k4 ];
        }
        int hoff4 = (      k4) * (Nimgs>4?sizeh[3]:0);
        for(int k3 = 0; k3 < (Nimgs>3?filterlen:1) ; k3++){
            if (Nimgs>3) {
                allk[3] = k3;
                filttmp[3] = filttmp[4] * curfilt[ filterlen * 3 + k3 ];
            }
            int hoff3 = (hoff4 + k3) * (Nimgs>3?sizeh[2]:0);
            for(int k2 = 0; k2 < (Nimgs>2?filterlen:1) ; k2++){
                if (Nimgs>2) {
                    allk[2] = k2;
                    filttmp[2] = filttmp[3] * curfilt[ filterlen * 2 + k2 ];
                }
                int hoff2 = (hoff3 + k2) * (Nimgs>2?sizeh[1]:0);
                for(int k1 = 0; k1 < (Nimgs>1?filterlen:1) ; k1++){
                    if (Nimgs>1) {
                        allk[1] = k1;
                        filttmp[1] = filttmp[2] * curfilt[ filterlen * 1 + k1 ];
                    }
                    int hoff1 = (hoff2 + k1) * (Nimgs>1?sizeh[0]:0);
                    for(int k0 = 0; k0 < (Nimgs>0?filterlen:1) ; k0++){
                        int hoff0 = hoff1 + k0;
                        filttmp[0] = filttmp[1] * curfilt[ filterlen * 0 + k0 ];
    					hist[ hbase + hoff0 ] += (histtype) filttmp[0];
#ifdef DERIVATIVE
                        // derivative of dHist_dTransf = dHist_dfilti * dfilti_dSi * dSi_dTransf
						if (abs(filttmp[0]) > 32*std::numeric_limits<filttype>::epsilon( ) ) {
							for( int transfindx = 0; transfindx<Ndims*(Ndims+1); transfindx++) {
								for(int imindx=1;imindx < Nimgs; imindx++) {
									dhdTransf[transfindx + (imindx-1)*Ndims*(Ndims+1) + Ndims*(Ndims+1)*(Nimgs-1) * (hbase + hoff0) ] += 
										filttmp[0]/filttmp[imindx] * dcurfiltdS[  filterlen * (imindx-1) + allk[imindx]] * dSdTransf[transfindx + (imindx-1)*Ndims*(Ndims+1)];
								}
							}
						}
#endif
                    }
				}
			}
		} 
    }
#else // of ifdef SMOOTHH       
    // No smoothing: just add to the histogram.
    hist[hbase] += weight;
#ifdef DERIVATIVE
    mexErrMsgTxt("error cannot compute derivative when not smoothing histogram.");
#endif 
#endif// of ifdef SMOOTHH
        
}
#endif // of ifndef CREATEDUPDHISTFUN

#ifndef CREATEDINTERPFUN
#define CREATEDINTERPFUN
#pragma optimize( "t", on )
/*#ifndef CREATEDIMGSTRUCT
template <int Ndims, typename imgtype, typename postype> struct linInterp {
    public:
    static  val(const imgtype * im, const int * sizeIMG, const postype * pos );
    static __inline imgtype val(const imgtype * im, const int * sizeIMG, const postype * pos , postype * __restrict dSdPos);
};
#endif */

template <typename imgtype, typename postype> __inline imgtype linInterp0(const imgtype * im, const int * sizeIMG, const postype * pos SAMPLEDERIVATIVEARGS)
{
    return im[0];
}

template <typename imgtype, typename postype> __inline imgtype linInterp1(const imgtype * im, const int * sizeIMG, const postype * pos SAMPLEDERIVATIVEARGS)
{
        const int Ndims = 1;
        int indx = (int) floor(pos[Ndims-1]);
        postype frac = pos[Ndims-1]-indx;
        int cumsz =1;
        for (int dim=0;dim<Ndims-1;dim++) {
            cumsz *= sizeIMG[dim];
        }
        imgtype left  = linInterp0(im + cumsz*indx, sizeIMG, pos SAMPLEDERIVATIVEARGSs);
        imgtype right = linInterp0(im + cumsz*(indx+1), sizeIMG, pos SAMPLEDERIVATIVEARGSs);
#ifdef DERIVATIVE
        dSdPos[Ndims-1] = right-left;
#endif
        return (imgtype) ((left)*(1-frac) + (right)*frac);
}

template <typename imgtype, typename postype> __inline imgtype linInterp2(const imgtype * im, const int * sizeIMG, const postype * pos SAMPLEDERIVATIVEARGS)
{
    const int Ndims = 2;
        int indx = (int) floor(pos[Ndims-1]);
        postype frac = pos[Ndims-1]-indx;
        int cumsz =1;
        for (int dim=0;dim<Ndims-1;dim++) {
            cumsz *= sizeIMG[dim];
        }
        imgtype left  = linInterp1(im + cumsz*indx, sizeIMG, pos SAMPLEDERIVATIVEARGSs);
#ifdef DERIVATIVE
        postype tmpderiv[ 1 ]; // max(1,Ndims-1) ];
        for (int dim = 0; dim<Ndims-1; dim++){
             tmpderiv[dim] = dSdPos[dim];
        }
#endif
        imgtype right = linInterp1(im + cumsz*(indx+1), sizeIMG, pos SAMPLEDERIVATIVEARGSs);
#ifdef DERIVATIVE
        for (int dim = 0; dim<Ndims-1; dim++){
            dSdPos[dim] = tmpderiv[dim] * (1-frac) + dSdPos[dim] * frac ;
        }
        dSdPos[Ndims-1] = right-left;
#endif
        return (imgtype) ((left)*(1-frac) + (right)*frac);
}

template <typename imgtype, typename postype> __inline imgtype linInterp3(const imgtype * im, const int * sizeIMG, const postype * pos SAMPLEDERIVATIVEARGS)
{
    const int Ndims = 3;
        int indx = (int) floor(pos[Ndims-1]);
        postype frac = pos[Ndims-1]-indx;
        int cumsz =1;
        for (int dim=0;dim<Ndims-1;dim++) {
            cumsz *= sizeIMG[dim];
        }
        imgtype left  = linInterp2(im + cumsz*indx, sizeIMG, pos SAMPLEDERIVATIVEARGSs);
#ifdef DERIVATIVE
        postype tmpderiv[ 2 ]; //max(1,Ndims-1)];
        for (int dim = 0; dim<Ndims-1; dim++){
             tmpderiv[dim] = dSdPos[dim];
        }
#endif
        imgtype right = linInterp2(im + cumsz*(indx+1), sizeIMG, pos SAMPLEDERIVATIVEARGSs);
#ifdef DERIVATIVE
        for (int dim = 0; dim<Ndims-1; dim++){
            dSdPos[dim] = tmpderiv[dim] * (1-frac) + dSdPos[dim] * frac ;
        }
        dSdPos[Ndims-1] = right-left;
#endif
        return (imgtype) ((left)*(1-frac) + (right)*frac);
}

#ifndef CREATEDIMGSTRUCT
#define CREATEDIMGSTRUCT
template<int Ndims, typename imgtype> struct imgStruct { 
    imgtype* im; 
    int sizeIMG[Ndims]; 
    int filterlen; /*unused, but should be present for compilation*/};
template<int Ndims, typename imgtype, typename filttype> struct imgFiltInterpolateStruct { 
    imgtype* im; 
    int sizeIMG[Ndims]; 
    const filttype * filterbank; 
    const filttype * dfilterbank;
    filttype * curfilt;
    filttype * curdfilt;
    int filterlen; 
    int nsteps; };
#endif

//template<int Ndims, typename imgtype, typename postype> __inline imgtype linearInterpolateImg(imgStruct<Ndims, imgtype> img,  postype pos[Ndims] SAMPLEDERIVATIVEARGS) {
template<int Ndims, typename imgtype, typename postype> __inline imgtype linearInterpolateImg(imgStruct<Ndims, imgtype> img,  postype pos[] SAMPLEDERIVATIVEARGS) {
    if (Ndims==1)
    	return linInterp1(img.im, img.sizeIMG, pos SAMPLEDERIVATIVEARGSs);
    else if (Ndims==2)
    	return linInterp2(img.im, img.sizeIMG, pos SAMPLEDERIVATIVEARGSs);
    else if (Ndims==3)
    	return linInterp3(img.im, img.sizeIMG, pos SAMPLEDERIVATIVEARGSs);
    else 
        mexErrMsgTxt("More than 3 dimensions for linear interpolating image currently not supported.");
};
template<int Ndims, typename imgtype, typename postype> __inline imgtype nearestNeighborInterpolateImg(const imgStruct<Ndims, imgtype> img, const postype * pos SAMPLEDERIVATIVEARGS) {
#ifdef DERIVATIVE
    Ndims = "Cant compute derivative of nearest neighbor interpolation."; // create compile error on instantiation.
#endif
    int indx = 0;
    for (int dim=Ndims-1;dim>=0; dim-- ){
        indx = (indx*img.sizeIMG[dim]) + round(pos[dim]);
    }
	return img.im[indx];
};

template<int Ndims, int filtlen_, typename imgtype, typename postype, typename filttype> __inline imgtype filterbankInterpolateImg( imgFiltInterpolateStruct<Ndims, imgtype, filttype> img, postype pos[Ndims] SAMPLEDERIVATIVEARGS) {
    int cumsz[Ndims+1];
    const int filterlen = (filtlen_==-1 ? img.filterlen : filtlen_);
    int baseindx=0;
    cumsz[0]=1;
    for (int dim=0;dim<Ndims; dim++ ){
        cumsz[dim+1] = cumsz[dim] * img.sizeIMG[dim];
        int ipos = (int) pos[dim];
        baseindx += ipos * cumsz[dim];
        postype fracpos = (pos[dim]-ipos)*img.nsteps;
        int filtcol = (int) fracpos;
        fracpos -= filtcol;
        for (int filtindx=0; filtindx<filterlen; filtindx++ ) {
            img.curfilt[filtindx + filterlen * dim] = (filttype) (img.filterbank[ filtcol * filterlen + filtindx ]*(1-fracpos) + img.filterbank[ (filtcol+1) * filterlen + filtindx ]*fracpos);
#ifdef DERIVATIVE    
            if (img.dfilterbank==NULL){ // compute derivative from difference:
                img.curdfilt[filtindx + filterlen * dim] =(-img.filterbank[ filtcol * filterlen + filtindx ]       +  img.filterbank[ (filtcol+1) * filterlen + filtindx ])*img.nsteps;
            } else { // compute derivative from dfilterbank:
                img.curdfilt[filtindx + filterlen * dim] = (filttype) (img.dfilterbank[ filtcol * filterlen + filtindx ]*(1-fracpos) + img.dfilterbank[ (filtcol+1) * filterlen + filtindx ]*fracpos);
            }
#endif
        }
    }
    if ( Ndims>3) {
        mexErrMsgTxt("More than 3 dimensions for filtered image sampling currently not supported.");
    }
    imgtype sample =0;
    int allk[Ndims];
    filttype filttmp[Ndims];
    // Actually sample the 
    // When filterlen is known and small, all of the following loops should be unroled:
    for(int k2 = 0; k2 < (Ndims>2?filterlen:1) ; k2++){
        if (Ndims>2) {
            allk[2] = k2;
            filttmp[2] = img.curfilt[ filterlen * 2 + k2 ];
        }
        int hoff2 = (0    + k2) * (Ndims>2?img.sizeIMG[1]:0);
        for(int k1 = 0; k1 < (Ndims>1?filterlen:1) ; k1++){
            if (Ndims>1) {
                allk[1] = k1;
                filttmp[1] = ((Ndims>2) ? filttmp[2] : 1) * img.curfilt[ filterlen * 1 + k1 ];
            }
            int hoff1 = (hoff2 + k1) * (Ndims>1?img.sizeIMG[0]:0);
            for(int k0 = 0; k0 < (Ndims>0?filterlen:1) ; k0++){
                int hoff0 = hoff1 + k0;
                filttmp[0] = ((Ndims>1) ? filttmp[1] :1 ) * img.curfilt[ filterlen * 0 + k0 ];
                sample += (imgtype) (img.im[ baseindx + hoff0 ]  * filttmp[0]);
#ifdef DERIVATIVE
                // derivative of sample:  dsample_dpos = dsample_dfilt * dfilt_dpos 
                // THE FOLLOWING LOOP SHOULD BE UNROLLED (but MSVC refuses to!!!!)
                for(int dim=0;dim < Ndims; dim++) {
                    dSdPos[ dim ] += img.im[ baseindx + hoff0 ] * filttmp[0]/img.curfilt[ filterlen * dim + allk[dim] ] * img.curdfilt[allk[dim] + filterlen * dim];
                }
#endif
            }
        }
    }
    return sample;
}

#endif	// of #ifndef CREATEDINTERPFUN			
                    
                    
template <	int Ndims,          // number of dimensions of each of the images
            int Nimgs,          // number of images, equals number of histogram dimensions.
            typename imgtype    // internal datatype of all images (output by reader, when not transforming,  imgSampler not used and then it should also be the image storage type).
           ,typename imgObjType// structure/class containing image information that also has pointer field 'im' that references the data storage 
#ifdef TRANSFORM_IMG
           ,typename postype,   // datatype in which positions are calculated and in which the transformation is stored.
//            imgtype imgSampler(const imgObjType, const postype [Ndims] SAMPLEDERIVATIVEARGS), // image sampling function (only used in conjunction with image transformation)
            imgtype imgSampler(imgObjType, postype [] SAMPLEDERIVATIVEARGS), // image sampling function (only used in conjunction with image transformation)
            bool dontSampleOutsideIMG, // verify that all samples are taken inside all images (0<=pos<size-interpborder), if false, imgSampler should handle out of bound values correctly.
            int interpborder_          // border taken into account for bound checking (see dontSampleOutsideIMG)
                                           // if interpborder_==-1,  interpborder =img[0].filterlen, else interpborder = interpborder_;
#endif
			, typename histtype // datatype of the histogram (or stored images)
#ifdef SMOOTHH 
  			, typename filttype // datatype of the histogram filter.
#endif
            >
void FUNNM ( HISTARGS IMGSARGS TRANSFORMARGS SCALEANDOFFSETARGS )
{
	// Declare all variables:
	imgtype S[Nimgs];		// holds current value of images
#ifdef SMOOTHH 
    filttype * tempspace = (filttype *) mxMalloc( (2*Nimgs-1) * hfilterlen * sizeof(filttype));
#endif
#ifdef TRANSFORM_IMG
#ifdef DERIVATIVE
	imgtype dSdTransf[Ndims*(Ndims+1)*(Nimgs-1)];
    postype dSdPos[Ndims];
#endif
	int S0indx[Ndims+1];
	postype partsumindx[Ndims*(Ndims+1)*(Nimgs-1)];
    for(int dimindx=1; dimindx<Ndims; dimindx++) {
        S0indx[dimindx] = 0;
    }
    S0indx[Ndims] = 1;
	S0indx[0] = -1;
    for(int dimindx=0; dimindx<Ndims+1; dimindx++) {
        for(int dimindx1=0; dimindx1<Ndims; dimindx1++) {
            postype transfShouldBe = (dimindx1 == dimindx ? 1 : 0);
            if (transform[dimindx1 + dimindx* Ndims] !=transfShouldBe) {
                mexErrMsgTxt("Fixed image should not be transformed.");
            }
        }
    }
    for( int imgindx =1; imgindx<Nimgs; imgindx++ ) {
		for (int dimindx2 = 1; dimindx2<Ndims+1; dimindx2++) {
			for (int dimindx=0; dimindx<Ndims; dimindx++) {
				// copy last column of transform to all (but the first) columns of partsumindx, for all images.
				partsumindx[dimindx + Ndims * (dimindx2 + (Ndims+1)*(imgindx-1))] = transform[dimindx + Ndims * (Ndims + (Ndims+1)*(imgindx))];
			}
		}
	}; 
    const int interpborder = (interpborder_==-1 ? imgs[0].filterlen : interpborder_);
#endif
    size_t lenS0 = 1;
	for(int dimindx=0; dimindx<Ndims; dimindx++) {
		lenS0 *= sizeIMG[dimindx + 0 * Ndims];
	}

// START MAIN LOOP:
	for(int indxS0=0; indxS0 < lenS0; indxS0++){
#ifdef TRANSFORM_IMG
        // Update current position in first image:
        int S0dim=0;
        for( ; ; S0dim++) {
            S0indx[S0dim] += 1; // goto next element
            if (S0indx[S0dim] >= sizeIMG[S0dim + 0 * Ndims]) { 
                S0indx[S0dim] =0; // clear current position and proceed to update next dim.
                if (S0dim >= Ndims-1) {
                    break; // break before increasing S0dim to Ndims.
                }
            } else {
                break;
            }
        }
        // Find transformed image locations:
        for( ;S0dim>=0; S0dim--){
            for( int imgindx =1; imgindx<Nimgs; imgindx++ ) {
                for( int dimindx=0; dimindx<Ndims  ; dimindx++){
                     partsumindx[dimindx + Ndims *(S0dim + (Ndims+1) * (imgindx-1))] = partsumindx[dimindx + Ndims *(S0dim+1 + (Ndims+1) * (imgindx-1))] +  S0indx[S0dim] * transform[dimindx + Ndims *(S0dim + (Ndims+1)*(imgindx))];
                }
            }
        }	 
        if (dontSampleOutsideIMG) {
            bool insideAll = true;
            for( int imgindx =1; imgindx<Nimgs; imgindx++ ) {
                for( int dimindx=0; dimindx<Ndims  ; dimindx++){
                    insideAll = insideAll & (partsumindx[dimindx + Ndims *( 0 +  (Ndims+1) * (imgindx-1))]>=0) & (partsumindx[dimindx + Ndims *( 0 +  (Ndims+1) * (imgindx-1))]< sizeIMG[dimindx + Ndims * imgindx] - interpborder);
                }
                if (!insideAll) break;
            }
            if (!insideAll) {
                continue;   // don't add to the histogram when outside any of the images. 
            }
        }
        // Read image values:
        S[0] = imgs[0].im[indxS0];
        for( int imgindx =1; imgindx<Nimgs; imgindx++ ) {
            S[imgindx] = imgSampler(imgs[imgindx], &partsumindx[ Ndims *(0 + (Ndims+1) * (imgindx-1))] SAMPLEDERIVATIVEARGSs);
#ifdef DERIVATIVE
            for( int dimindx1=0; dimindx1<Ndims; dimindx1++ ) {
                for( int dimindx2=0; dimindx2<Ndims; dimindx2++ ) {
                    dSdTransf[dimindx1 + Ndims * (dimindx2 + (Ndims+1) * (imgindx-1))] = (imgtype) dSdPos[dimindx1] * S0indx[dimindx2] ;
                }
            }
#endif
        }
        
#else // of #ifdef TRANSFORM_IMG
        for( int imgindx =0; imgindx<Nimgs; imgindx++ ) {
            S[imgindx] = imgs[imgindx][indxS0];
        }
#endif // of #ifdef TRANSFORM_IMG
        
#ifdef TRANSFORMONLY
		// if only transforming the image, just store the transformed images and continue:
		hist[indxS0] += S[1];
#else
		// update histogram
		updHist<Ndims, Nimgs, -1 /*filterlen_*/, histtype , imgtype
#ifdef SMOOTHH 
  			, filttype
#endif
            > ( HISTARGSs 1, UPDHISTIMGARGSs SCALEANDOFFSETARGSs UPDHISTTEMPARGSs);
#endif // endif of else of ifdef TRANSFORMONLY
	} // end main loop. 
#ifdef SMOOTHH 
    mxFree(tempspace);
#endif
}
// Clear definitions used in function (building):
#undef HISTARGS 
#undef HISTARGSs
#undef IMGSARGS
#undef IMGSARGSs
#undef TRANSFORMARGS
#undef TRANSFORMARGSs
#undef SCALEANDOFFSETARGS
#undef SCALEANDOFFSETARGSs
#undef SAMPLEDERIVATIVEARGS
#undef SAMPLEDERIVATIVEARGSs
#undef SAMPLEDERIVATIVEARGSs2
#undef UPDHISTIMGARGS  
#undef UPDHISTIMGARGSs
#undef UPDHISTTEMPARGS
#undef UPDHISTTEMPARGSs
#undef FUNNM

#else // ifdef BUILDMIFUN

#include "mex.h"
#include "math.h"
//#include "Windows.h"
#include <limits>
#include <algorithm> 
//#include "intrin.h"
//#include "emmintrin.h"
// Declare that we are busy building all functions:
#define BUILDMIFUN
// datatype of image: 
// #define imgtype  double    
// # define TYPESTR  // string added to function name; specifying image type
using std::max;
using std::min;

template <typename T> T minmax(T llim, T val, T ulim) { 
	return max(llim, min(ulim,val));
}


// Create different versions of the main function: (with/without derivative, smoothing, transforming)

// Without transform, no smoothing:
#include __FILE__
// Without transform, smoothing of histogram:
#define SMOOTHH
#undef CREATEDUPDHISTFUN    // updhist function has different parameters when smoothing histogram
#include __FILE__
#undef SMOOTHH

// functions with transform:
#define TRANSFORM_IMG   //apply affine transform to img2
#include __FILE__
// transform, smoothing
#define SMOOTHH
#include __FILE__
// transform, smoothing, derivative 
#define DERIVATIVE
#undef CREATEDUPDHISTFUN    // updhist function has different parameters when smoothing histogram and computing derivative
#undef CREATEDINTERPFUN     // Interpolation function have different parameters when computing derivative.
#include __FILE__

#undef DERIVATIVE
#undef SMOOTHH

// transform image, no histogram computations. (only with TRANSFORM_IMG enabled, as otherwise we are only stupidly copiing images)
#define TRANSFORMONLY  
#include __FILE__
#undef TRANSFORM_IMG

// datatype of position in images & histogram:
//#define postype double
// datatype of filters:
//#define filttype histtype
// datatype of histogram:
//#define histtype  double   


#define HISTARGS histtype * __restrict hist , histtype * __restrict dhdTransf, const mwSize * __restrict sizeh, const filttype * __restrict hfilt, const filttype * __restrict dhfilt, size_t hfilterlen, size_t hfiltersteps,
#define HISTARGSs hist, dhdTransf,  sizeh, hfilt, dhfilt, hfilterlen, hfiltersteps,
#define HISTARGSsNoDeriv hist,  sizeh, hfilt, hfilterlen, hfiltersteps,
#define HISTARGSsNoSmooth hist, sizeh,

#define IMGSARGS const imgtype * __restrict const * imgs, const mwSize * __restrict sizeIMG
//#define IMGSARGS imgtype * * imgs, mwSize sizeIMG[]
#define IMGSARGSs imgs, sizeIMG
#define IMGSARGSs2 imgStr, sizeIMG
//#define TRANSFORMARGS , const double transform[Ndims*(Ndims+1)*Nimgs]
#define TRANSFORMARGS , const postype * __restrict transform
#define TRANSFORMARGSs , transform
//#define SCALEANDOFFSETARGS , imgtype offsetS[Nimgs] , imgtype scaleS[Nimgs]
#define SCALEANDOFFSETARGS , const imgtype * __restrict offsetS , const imgtype * __restrict scaleS
#define SCALEANDOFFSETARGSs , offsetS , scaleS
#define SCALEANDOFFSETARGSsf , offsetSf , scaleSf
#define IMAGESMOOTHARGS , const filttype * __restrict imageInterpFilter, const filttype * __restrict imagedInterpFilter, int imageInterpfiltlen, int imageInterpfiltSteps
#define IMAGESMOOTHARGSs , imageInterpFilter, imagedInterpFilter, imageInterpfiltlen, imageInterpfiltSteps
template <	int Ndims,          // number of dimensions of each of the images
            int Nimgs,          // number of images, equals number of histogram dimensions.
            typename imgtype,    // datatype of all images.
            typename postype,   // datatype in which positions are calculated and in which the transformation is stored.
			typename histtype, // datatype of the histogram
  			typename filttype // datatype of the histogram filter.
            >
__inline void MIhist_delegate ( HISTARGS IMGSARGS TRANSFORMARGS SCALEANDOFFSETARGS IMAGESMOOTHARGS)
{
    // Unfortunatly, the splitting code below is a bit large, but only special combinations of the following parameters are allowed
    // Parameters checked in the if tree:
    //     imageInterpFilter    : interpolation filter for the images (only allowed when transform!=NULL).
    //     transform            : if !=NULL: apply affine transformation
    //     dhdTransf            : if !=NULL: compute derivative (only allowed in combination with transform and hfilt)
    //     hfilt                : if !=NULL: filter with which the histogram is smoothed.
    
    if (imageInterpFilter==NULL) {
        // No interpolation filter for images
        if (transform==NULL) {
            // Do not transform images
            if (dhdTransf!=NULL) 
                 mexErrMsgTxt("Cannot compute derivative when not transforming images");
            // if not transforming, images should have the same size, but that is checked in the calling function.
            if (hfilt==NULL) { 
                // Not smooth histogram 
                MIhist<Ndims, Nimgs, imgtype, const imgtype*, histtype> ( HISTARGSsNoSmooth IMGSARGSs SCALEANDOFFSETARGSs );
            } else {
                // Smooth histogram 
                MIhist<Ndims, Nimgs, imgtype, const imgtype*, histtype, filttype> ( HISTARGSsNoDeriv IMGSARGSs SCALEANDOFFSETARGSs );
            }
        } else {
            // Transform images, not image interpolation filter, so use linear interpolation:
            
            // Define linear interpolation struct:
            imgStruct<Ndims, imgtype> imgStr[Nimgs];
            for (int imgindx = 0; imgindx<Nimgs; imgindx++) {
                imgStr[imgindx].im = (imgtype *) imgs[imgindx];
                for (int dim=0; dim<Ndims ; dim++) {
                    imgStr[imgindx].sizeIMG[dim] = sizeIMG[imgindx*Ndims + dim];
                }
            }

            if (hfilt==NULL) {
                // No histogram filtering
                if (dhdTransf!=NULL) 
                    mexErrMsgTxt("Cannot compute derivative when not filtering the histogram.");
                MIhist<Ndims, Nimgs, imgtype, imgStruct<Ndims, imgtype>, postype, linearInterpolateImg<Ndims, imgtype, postype>, true, 1, histtype> ( HISTARGSsNoSmooth IMGSARGSs2 TRANSFORMARGSs SCALEANDOFFSETARGSs );
                                                                                                                                                    // hist, sizeh,imgStr, sizeIMG,   transform, offsetS , scaleS
            } else {
                // Histogram filtering
                if (dhdTransf==NULL) {
                    // No derivative
                    MIhist<Ndims, Nimgs, imgtype, imgStruct<Ndims, imgtype>, postype, linearInterpolateImg<Ndims, imgtype, postype>, true, 1, histtype, filttype> ( HISTARGSsNoDeriv IMGSARGSs2 TRANSFORMARGSs SCALEANDOFFSETARGSs );
                } else {
                    // Compute derivative.
                    MIhist<Ndims, Nimgs, imgtype, imgStruct<Ndims, imgtype>, postype, linearInterpolateImg<Ndims, imgtype, postype>, true, 1, histtype, filttype> ( HISTARGSs IMGSARGSs2 TRANSFORMARGSs SCALEANDOFFSETARGSs );
                }
            }
        }
    } else {
        // image interpolation filter present.
        if (transform==NULL) 
            mexErrMsgTxt("Image smoothing filter not used when not transforming images");

        // Define image filter interpolation struct:
        imgFiltInterpolateStruct<Ndims, imgtype, filttype> imgStr[Nimgs];
        for (int imgindx = 0; imgindx<Nimgs; imgindx++) {
            imgStr[imgindx].im = (imgtype *) imgs[imgindx];
            for (int dim=0; dim<Ndims ; dim++) {
                imgStr[imgindx].sizeIMG[dim] = sizeIMG[imgindx*Ndims + dim];
            }
            imgStr[imgindx].filterbank  = imageInterpFilter;
            imgStr[imgindx].dfilterbank = imagedInterpFilter;
            imgStr[imgindx].filterlen   = imageInterpfiltlen; 
            imgStr[imgindx].nsteps      = imageInterpfiltSteps;
            imgStr[imgindx].curfilt     = (filttype *) mxMalloc(sizeof(filttype) * imageInterpfiltlen * Ndims);
            if (dhdTransf==NULL) {
                imgStr[imgindx].curdfilt= NULL;
            } else {
                imgStr[imgindx].curdfilt= (filttype *) mxMalloc(sizeof(filttype) * imageInterpfiltlen * Ndims);
            }
        }
        if (hfilt==NULL) {
            // No histogram filtering
            if (dhdTransf!=NULL) 
                mexErrMsgTxt("Cannot compute derivative when not filtering the histogram.");
            MIhist<Ndims, Nimgs, imgtype, imgFiltInterpolateStruct<Ndims, imgtype, filttype>, postype, filterbankInterpolateImg<Ndims, -1, imgtype, postype, filttype>, true, 1, histtype> ( HISTARGSsNoSmooth IMGSARGSs2 TRANSFORMARGSs SCALEANDOFFSETARGSs );
        } else {
            // Histogram filtering
            if (dhdTransf==NULL) {
                // No derivative
                MIhist<Ndims, Nimgs, imgtype, imgFiltInterpolateStruct<Ndims, imgtype, filttype>, postype, filterbankInterpolateImg<Ndims, -1, imgtype, postype, filttype>, true, 1, histtype, filttype> ( HISTARGSsNoDeriv IMGSARGSs2 TRANSFORMARGSs SCALEANDOFFSETARGSs );
            } else {
                // Compute derivative.
                MIhist<Ndims, Nimgs, imgtype, imgFiltInterpolateStruct<Ndims, imgtype, filttype>, postype, filterbankInterpolateImg<Ndims, -1, imgtype, postype, filttype>, true, 1, histtype, filttype> ( HISTARGSs IMGSARGSs2 TRANSFORMARGSs SCALEANDOFFSETARGSs );
            }
        }
        for (int imgindx = 0; imgindx<Nimgs; imgindx++) {
            mxFree(imgStr[imgindx].curfilt);
			if (dhdTransf!=NULL) {
				mxFree(imgStr[imgindx].curdfilt); // mxFree (should) test for NULL pointer, so if not needed.
			}
        }
    }
}

template <int Nimgs, typename imgtype, typename postype, typename histtype, typename filttype>
__inline void MIhist_delegate_Ndims ( HISTARGS IMGSARGS TRANSFORMARGS SCALEANDOFFSETARGS IMAGESMOOTHARGS , int Ndims ) {
    if (transform==NULL)  {
        mwSize sizeIMG2[Nimgs];
        for( int imgindx = 0; imgindx<Nimgs; imgindx++) {
            sizeIMG2[imgindx] = 1;
            for( int dim =0; dim<Ndims; dim++ ) {
                sizeIMG2[imgindx] *= sizeIMG[imgindx*Ndims + dim];
                if (sizeIMG[imgindx*Ndims + dim] !=sizeIMG[0*Ndims + dim]) {
                    mexErrMsgTxt("If not transforming images, the sizes should be equal.");
                }
            }
        }
        MIhist_delegate<1, Nimgs, imgtype, postype, histtype, filttype> ( HISTARGSs imgs, sizeIMG2 TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs );
    } else /*if (Ndims==1) 
        MIhist_delegate<1, Nimgs, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs);
    else */if (Ndims==2) 
        MIhist_delegate<2, Nimgs, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs);
    else if (Ndims==3) 
        MIhist_delegate<3, Nimgs, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs);
    else {
        mexErrMsgTxt("Unsupported number of image dimensions.");
    }
}

template <typename imgtype, typename postype, typename histtype, typename filttype>
__inline void MIhist_delegate_NdimsNimgs ( HISTARGS IMGSARGS TRANSFORMARGS SCALEANDOFFSETARGS IMAGESMOOTHARGS , int Ndims, int Nimgs ) {
  	/*if (Nimgs==1) 
        MIhist_delegate_Ndims<1, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims);
    else*/ if (Nimgs==2) 
        MIhist_delegate_Ndims<2, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims);
    else if (Nimgs==3) 
        MIhist_delegate_Ndims<3, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims);
    else/* if (Nimgs==4) 
        MIhist_delegate_Ndims<4, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims);
    else if (Nimgs==5) 
        MIhist_delegate_Ndims<5, imgtype, postype, histtype, filttype> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims);
    else*/ {
        mexErrMsgTxt("Unsupported number of images.");
    }
}

#undef HISTARGS
//#define HISTARGS histtype * __restrict hist , histtype * __restrict dhdTransf,  mwSize sizeh[Nimgs], double hfilt[], double dhfilt[], size_t hfilterlen, size_t hfiltersteps,
#define HISTARGS double * __restrict hist , double * __restrict dhdTransf,  const mwSize * __restrict sizeh, const double * __restrict hfilt, const double * __restrict dhfilt, size_t hfilterlen, size_t hfiltersteps,
#define HISTARGSsf   (const float *) hist ,     (const float *) dhdTransf,                            sizeh,           (const float *) hfilt,                 (float *) dhfilt,        hfilterlen,        hfiltersteps,
#undef IMGSARGS
//#define IMGSARGS const double* imgs[Nimgs], const mwSize sizeIMG[Ndims*Nimgs]
#define IMGSARGS const double * __restrict const * imgs, const mwSize * __restrict sizeIMG
//#define IMGSARGS const double ** imgs, const mwSize sizeIMG[]
#undef TRANSFORMARGS
#define TRANSFORMARGS , const double * __restrict transform
#undef SCALEANDOFFSETARGS
#define SCALEANDOFFSETARGS , const double * __restrict offsetS , const double * __restrict scaleS
#undef IMAGESMOOTHARGS
#define IMAGESMOOTHARGS , const double * __restrict imageInterpFilter, const double * __restrict imagedInterpFilter , int imageInterpfiltlen, int imageInterpfiltSteps
#define IMAGESMOOTHARGSsf , (float *) imageInterpFilter, (float *) imagedInterpFilter, imageInterpfiltlen, imageInterpfiltSteps

const int maxNumIMG = 5;
const int maxNumDims = 3;
    
template <typename postype>
__inline void MIhist_delegate_NdimsNimgsImFiltTypes ( HISTARGS IMGSARGS TRANSFORMARGS SCALEANDOFFSETARGS IMAGESMOOTHARGS , int Ndims, int Nimgs, mxClassID imagetype, mxClassID filtertype ) {
  	if (imagetype == mxDOUBLE_CLASS) {
        if (filtertype == mxDOUBLE_CLASS) {
            MIhist_delegate_NdimsNimgs<double, postype, double, double> ( HISTARGSs IMGSARGSs TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims, Nimgs);
        } else 
            mexErrMsgTxt("Currently unsupported combination of types.");
    } /*else if (imagetype ==  mxSINGLE_CLASS) {
        float offsetSf[maxNumIMG];
        float scaleSf[maxNumIMG];
        for (int imgindx = 0;imgindx<Nimgs; imgindx++) {
            offsetSf[imgindx] = (float) offsetS[imgindx];
            scaleSf[imgindx] = (float) scaleS[imgindx];
        }
        if (filtertype == mxDOUBLE_CLASS) {
            MIhist_delegate_NdimsNimgs<float, postype, double, double> ( HISTARGSs (const float* const *) imgs , sizeIMG TRANSFORMARGSs SCALEANDOFFSETARGSsf IMAGESMOOTHARGSs , Ndims, Nimgs);
        } else if (filtertype == mxSINGLE_CLASS) {
            MIhist_delegate_NdimsNimgs<float, postype, float, float> ( HISTARGSsf (const float* const *) imgs , sizeIMG TRANSFORMARGSs SCALEANDOFFSETARGSsf IMAGESMOOTHARGSsf  , Ndims, Nimgs); 
        }*/ else 
            mexErrMsgTxt("Currently unsupported combination of types.");
    //}
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
/* [hist, dhdTransf] =  MI_ND_dirk( img , sizesandscale, transform, histsmoothfilter, dhistsmoothfilter, imgInterpFilter, dimgInterpFilter)
Computes the mutual information histogram of the set of images in the cell array img
Optionally: - the images 2..end can be transformed with an affine transform 
			- the histogram can be smoothed
			- the derivative of the histogram can be computed (only when transforming & smoothing). 
Internally, specialized code for the given inputs and requested outputs is called to ensure
optimal performance in all cases.
Inputs:
	img : k element cell arrau with nD images, single or double
	sizesandscale : k x 3 element matrix; each row i contains
					  sizeh_i  			: number of bins in the 2D histogram in the direction for image i
                      offset_i			: value added to the intensity of img i to obtain the index in the histogram
                      scale_i			: multiplication factor of (img i +offset i), to obtain the index in the histogram
	transform : optional; n x (n+1) x k  matrix which specifies the affine transform applied to the images before 
                computing the MI histogram. Transform of first image should be the identity transform (since that is the fixed image) 
                For each point x, which is an n-element column vector index into the first image, the following 
                point in image i is sampled:
    				transform(:,:,i)*[x;1] 
				Histogram computation: 
					H( img{1}(x), img{2}( transform(:,:,2)*[x;1] ), ... , img{k}( transform(:,:,k)*[x;1] ) ) += 1, for all valid x
    histsmoothfilter : optional smoothing filterbank, with which each point added to the histogram is smoothed. 
				The data type of the histogram is set equal to the datatype of histsmoothfilter.
				Each column specifies a filter with a specific delay. 
				The delay should increase linearly for each column and the first and last column should have a 
				difference in delay of 1. The filter length should be short to evaluate fast. 
				The filters are linearly interpolated to obtain intermediate delays.
				Example:
				    rfilt= exp(-linspace(-2,2,3*20+1)'.^2);
					filt = zeros(3,21);
					filt(1,end:-1:1) = rfilt(1:size(filt,2));
					filt(2,end:-1:1) = rfilt(size(filt,2):2*size(filt,2)-1);
					filt(3,end:-1:1) = rfilt(2*size(filt,2)-1:end,1);
					filt = filt./([1;1;1]*sum(filt)); % normalize.
				Or a much longer filter:
					rfilt = exp(-linspace(-3.5,3.5,8*300+1)'.^2);
					filt = fliplr(reshape(rfilt(2:end),300,8)');
					filt(:,end+1) = [rfilt(1);filt(1:end-1,1)];
					filt = filt./(ones(size(filt,1),1)*sum(filt)); % normalize.
	dhistsmoothfilter : optional derivative of the smoothing filter. Should be of the same type and size as smoothfilter.
					When not provided I use the numerical derivative of smoothfilter, 
					which is not continuous, but piecewise constant :
						(smoothfilter(:,2:end)-smoothfilter(:,1:end-1))*(size(smoothfilter,2)-1)
					When dsmoothfilter is provided I linearly interpolate on that to get the derivative of smoothfilter.
	img2InterpFilter : Optional filterbank with which Img2 is interpolated to apply the transform.
					It should be constructed in the same way as histsmoothfilter, but is different in general. 
					It should be of the same type as the images.
					Note that the delay of the filter is not compensated, so you typically want to adjust transform with the 
					delay of the first column in the filterbank:
					transform(:,end) -= delay(img2InterpFilter(:,1));
					To interpolate on a 2x upsampled (and filter-compensated) image, an accurate filter (and compensation) 
					can be obtained with:
					[rintpfilt, compensation]=designfilter(3,12,.5 / 2,1);
					intpfilt = fliplr(reshape(rintpfilt(2:end),12,3)');
					intpfilt(:,end+1) = [rintpfilt(1);intpfilt(1:end-1,1)];
	dimg2InterpFilter : derivative of img2InterpFilter, used to compute the gradient of the histogram. 
					When not provided while img2InterpFilter is, the numerical derivative of img2InterpFilter is computed.
					However, this numerical derivative is, just as with dhistsmoothfilter, piecewize constant and thus not continuous.
Outputs:
	hist	: the (sizeh_1 x .. x sizeh_k) Mutual information histogram.
	dhdTransf   : if transform & smoothing : derivative of hist wrt to transform parameters; size(Dh) = [size(transform) size(hist)];
*/ 
	const double ** imgs;
    //double *sizesandscale;
	double *transform =NULL , *hfilt =NULL, *dhfilt=NULL, *hist=NULL, *dhdTransf=NULL, *imageInterpFilter=NULL, *imagedInterpFilter=NULL;
	mwSize sizeIMG[maxNumDims*maxNumIMG];
	int     Ndims;
	int  hfilterlen=0, hfiltersteps=0;
	int  imageInterpfiltlen=0, imageInterpfiltSteps=0;

	mxClassID imageClass ;
	mxClassID filterClass = mxDOUBLE_CLASS;

	/* Check for proper number of arguments */
	if((nrhs<2) | (nrhs>7)) {
		mexErrMsgTxt("2 to 7 inputs required. (imgs, sizesandscale, transform, histsmoothfilter, dhistsmoothfilter, img2InterpFilter, dimg2InterpFilter) ");
	} else if (!(nlhs==1) & !(nlhs==2) ) {
		mexErrMsgTxt("1 or 2 outputs required. (hist, dhdTransf)");
	}
	
    bool hasderiv =  (nlhs>=2);

	/* check input arguments, their sizes and types and get pointers. */
	// S1 & S2:
    int Nimgs = (int) mxGetNumberOfElements(prhs[0]);
    if ( !((mxGetClassID(prhs[0]) == mxCELL_CLASS) & (Nimgs>=1) & (Nimgs<=maxNumIMG) )) {
		mexErrMsgTxt("imgs should be a cell array with 1 to 5 elements.");
	}
    imgs = (const double**) mxMalloc( Nimgs * sizeof(double*));
    for (int imgindx=0; imgindx<Nimgs; imgindx++) {
        mxArray * curimg = mxGetCell( prhs[0], imgindx );
        if (imgindx==0) {
            Ndims = mxGetNumberOfDimensions( curimg );
            imageClass = mxGetClassID(curimg);
        } else {
            if (Ndims != mxGetNumberOfDimensions( curimg )) 
                mexErrMsgTxt("All images should have the same number of dimensions.");
            if (imageClass !=mxGetClassID(curimg)) 
                mexErrMsgTxt("All images should be of the same class.");
        }
        if( mxIsComplex( curimg ) ) {
            mexErrMsgTxt("Images should be non complex.");
    	}
        const mwSize * cursize =  mxGetDimensions( curimg );
        for (int dim=0;dim<Ndims; dim++ ) {
            sizeIMG[imgindx * Ndims + dim ] = cursize[dim];
        }
        imgs[imgindx] = mxGetPr( curimg );
    }
	
	//sizesandscale:
	if( (mxGetM(prhs[1])!=Nimgs) | (mxGetN(prhs[1])!=3) | !mxIsDouble(prhs[1]) | mxIsComplex(prhs[1]) ) {
		mexErrMsgTxt("sizesandscale should be a (Nimgs x 3) non complex double matrix.");
	} 
    double * sizehd = mxGetPr(prhs[1]);
    mwSize sizeh[ maxNumIMG ];
    for (int imgindx = 0; imgindx<Nimgs; imgindx++) {
        sizeh[imgindx] = (mwSize) sizehd[imgindx];
        if ((sizeh[imgindx]<0) | (sizeh[imgindx]>10000)) {
            mexErrMsgTxt("size of h should be between 0 and 10000 in each dimension.");
        }
    }
    double * offsetS = sizehd + Nimgs;
    double * scaleS = sizehd + 2*Nimgs;
    
	// check transform
	if( nrhs>=3) {
		bool hasTransf = (mxGetNumberOfElements( prhs[2] )!=0);
        const mwSize * tS =  mxGetDimensions( prhs[2] );
		if( hasTransf & !((mxGetNumberOfDimensions( prhs[2] )==3) & (tS[0]==Ndims) & (tS[1]==Ndims+1) & (tS[2]==Nimgs) & mxIsDouble(prhs[2]) & !mxIsComplex(prhs[2]))) {
			mexErrMsgTxt("Transform should be a Ndims x Ndims+1 x Nimgs non complex double matrix.");
		} 
		if(hasTransf) {
			transform = mxGetPr(prhs[2]); 
		}
	}

	// check histogram smoothing filter:
	if( nrhs>=4) {
		bool hasSmooth  = (mxGetNumberOfElements(prhs[3])!=0); // if not empty
		if( hasSmooth) {
			filterClass = mxGetClassID(prhs[3]);
			if ( ((filterClass!= mxSINGLE_CLASS) & (filterClass != mxDOUBLE_CLASS)) | mxIsComplex(prhs[3]) | (mxGetNumberOfDimensions(prhs[3])!=2) ) {
				mexErrMsgTxt("histogram smoothing filter should be a suitably constructed non complex single or double matrix.");
			}
			hfilt = mxGetPr(prhs[3]); 
			hfilterlen = (int) mxGetM(prhs[3]);
			hfiltersteps = (int) mxGetN(prhs[3])-1;
			if (nrhs>=5) {
				bool hasdSmooth = (mxGetNumberOfElements(prhs[4])!=0);
				if( hasdSmooth & ( (filterClass!=mxGetClassID(prhs[4])) | mxIsComplex(prhs[4]) | (mxGetM(prhs[4])!=mxGetM(prhs[3])) |( mxGetN(prhs[4])!= mxGetN(prhs[3]))  | (mxGetNumberOfDimensions(prhs[4])!=2) ) ) {
					mexErrMsgTxt("derivative of histogram smoothing filter should be a suitably constructed non complex single or double  matrix.");
				} 
				if (hasdSmooth & hasderiv ) {
					// we only need derivative of smoothing kernel when computing derivative of hist.
					dhfilt = mxGetPr(prhs[4]); 
				}
			}
		}
	} 

	// check image interpolation filter:
	if( nrhs>=6) {
		bool hasInterp  = (mxGetNumberOfElements(prhs[5])!=0); // if not empty
		if( hasInterp) {
			if ( (imageClass!= mxGetClassID(prhs[5])) | mxIsComplex(prhs[5]) | (mxGetNumberOfDimensions(prhs[5])!=2) ) {
				mexErrMsgTxt("interpolation filter should be a suitably constructed non complex matrix (same type as Img).");
			}
			imageInterpFilter = mxGetPr(prhs[5]); 
			imageInterpfiltlen = (int) mxGetM(prhs[5]);
			imageInterpfiltSteps = (int) mxGetN(prhs[5])-1;
			if (nrhs>=7) {
				bool hasdSmooth = (mxGetNumberOfElements(prhs[6])!=0);
				if( hasdSmooth & ( (imageClass!=mxGetClassID(prhs[6])) | mxIsComplex(prhs[6]) | (mxGetM(prhs[6])!=mxGetM(prhs[5])) |( mxGetN(prhs[6])!= mxGetN(prhs[5]))  | (mxGetNumberOfDimensions(prhs[6])!=2) ) ) {
					mexErrMsgTxt("derivative of histogram smoothing filter should be a suitably constructed non complex single or double  matrix.");
				} 
				if (hasdSmooth & hasderiv ) {
					// we only need derivative of smoothing kernel when computing derivative of hist.
					imagedInterpFilter = mxGetPr(prhs[6]); 
				}
			}
		}
	} 	
	
	/* Transform image when only that is requested: */
/*	if ((sizeh1==0) & (sizeh2==0)) {
		// Special request: transform input image2 to hist, which has size of image1
		if ((hfilt!=NULL) | hasderiv | (transform==NULL)) {
			mexErrMsgTxt("Can only transform image without smoothing or derivatives and when a transform is supplied.");
		}
		plhs[0] = mxCreateNumericArray(Ndims, sizeS1,  imageClass,   mxREAL);
		if (imageClass == mxSINGLE_CLASS) {
			if(Ndims==2){
				imTransf_2DTfi((float *)S1d, (float *)S2d, transform, sizeS1, sizeS2, (float *) imageInterpFilter, imageInterpfiltlen, imageInterpfiltSteps);
			} else if(Ndims==3){
				imTransf_3DTfi((float *)S1d, (float *)S2d, transform, sizeS1, sizeS2, (float *) imageInterpFilter, imageInterpfiltlen, imageInterpfiltSteps);
			} else {
				mexErrMsgTxt("Not implemented yet");
			}
		} else if (imageClass == mxDOUBLE_CLASS) {
			S1d = mxGetPr(plhs[0]);
			if(Ndims==2){
				imTransf_2DTfi(S1d, S2d, transform, sizeS1, sizeS2, imageInterpFilter, imageInterpfiltlen, imageInterpfiltSteps);
			} else if(Ndims==3){
				imTransf_3DTfi(S1d, S2d, transform, sizeS1, sizeS2, imageInterpFilter, imageInterpfiltlen, imageInterpfiltSteps);
			} else {
				mexErrMsgTxt("Not implemented yet");
			}
		} else 
			mexErrMsgTxt("Wrong image class for transform.");
		return;
	} */

	// Create normal output matrices for histogram:
	plhs[0] = mxCreateNumericArray((int) Nimgs, sizeh, filterClass, mxREAL);
	hist = mxGetPr(plhs[0]);

	if( hasderiv) {
		mwSize dHsize[ maxNumIMG + 3];
		if( (hfilt==NULL) | (transform==NULL)) {
			mexErrMsgTxt("Derivatives not implemented when not smoothing or when no affine transformation is supplied.");
		}
		dHsize[0] = Ndims;
		dHsize[1] = (Ndims+1);
		dHsize[2] = (mwSize) (Nimgs-1);
        for (int imgindx = 0; imgindx<Nimgs; imgindx++) {
            dHsize[imgindx+3] = sizeh[imgindx];
        }
		plhs[1] = mxCreateNumericArray((int) Nimgs+3, dHsize,  filterClass,   mxREAL);
		dhdTransf =  mxGetPr(plhs[1]);
	}
	/* Call the normal subroutine. */
    MIhist_delegate_NdimsNimgsImFiltTypes<double> ( HISTARGSs  imgs, sizeIMG TRANSFORMARGSs SCALEANDOFFSETARGSs IMAGESMOOTHARGSs , Ndims, Nimgs, imageClass, filterClass);
    mxFree(imgs); imgs = NULL;
}
#endif // end of else of ifdef BUILDMIFUN