#ifndef BSPLINE_PROC_CPP
#define BSPLINE_PROC_CPP
/* This file provides a set of classes dealing with b-splines
 * 
 * Test code by building: mex bspline_proc.cpp -DBUILDTESTFUN_BSPLINE_PROC
 *           and call   : a = bspline_proc();
 *			 Inspect a for correct results.
 *           don't define 'BUILDTESTFUN_BSPLINE_PROC' when including this from external code.
 * 
 * class bSplineCoeffs is a static function class, that computes bSpline coefficients.
 * Typically you want to use a wrapped version, such as described below.
 * 
 * class bSplineCoefficients is a wrapper class around bSplineCoeffs that allows 
 *   preparing coefficients and sampling source data with these coefficients. Still a quite
 *   raw class templated over b-spline length and coefficientType. If b-spline length ==-1, 
 *   the length can be set at run-time.
 *
 * class bSplineSampleVect allows to b-spline interpolate vectors.
 *   bSplineSampleVect<[bsplineorder=-1,] datatype>( source , step , length, stride, nvectors [, bsplineorder] )
 *   The method sample( * out, position )
 *   samples the input vectors around the column specified by position.
 *   inputs:
 *		 step   : the step (in number of elements) from 1 element of a vector to the next.
 *		 length : Number of elements in each vector.
 *		 stride : the step (in number of elements) from 1 vector to the next.
 *		 nvectors: number of vectors given.
 *       bsplineorder: order of the b-spline that is used.
 *   If you don't want to store the entire interpolated vector, you can use the version templated over bsplineorder.
 *   get subtype() and use specialization. where '.' is the value of subtype.
 *	 and call prepare( position ) 
 *		before calling sample( i ) to get element i of the interpolated vector.
 * 
 *  class filter: a filterbank that can be interpolated with bsplines.
 *      determine filter.subtype() to determine which specialization should be called:
 *      e.g. for subtype()==2:
 *      filter::specialization2 spec = filter::specialization2(filter);
 *      spec.prepare( loc ) , spec.prepareDerivative( loc ), spec.prepareValueAndDerivative( loc )
 *      note important!!:  0 <= loc < 1
 *      and then: spec.sample[Derivative]( i ) to get interpolated [partial derivative w.r.t. loc of] element i.
 *      with 0 <= i < filter.length()
 *
 *  class NDimage: image type, templated over data type and dimensionality. Can be sampled by a sampler e.g. a bspline sampler.
 *  
 *  See  function 'self_test_mex' for example usage.
 * 
 * Created by Dirk Poot, Erasmus MC
 */

/* B-spline interpolation in columns of F. 
*/
#include "math.h"
#include "emm_vec.hxx"
#include <algorithm>

template <int bsplineord, typename filtertype> class bSplineCoeffs;

template <typename filtertype> class bSplineCoeffs<0,filtertype> {
public:
	static inline void get( filtertype * bsplcoeffs, filtertype location ) {
		bsplcoeffs[0]=1;
	}
	static inline void getD( filtertype * bsplcoeffs, filtertype location , filtertype scale) {
		bsplcoeffs[0]=1;
		mexErrMsgTxt("Cant compute derivative of 0 order b-spline.");
	}
};
template <typename filtertype> class bSplineCoeffs<1,filtertype> {
public:
	static inline void get( filtertype * bsplcoeffs, filtertype location ) {
		bsplcoeffs[0] =1-location;
		bsplcoeffs[1] =  location;
	}
	static inline void getD( filtertype * bsplcoeffs, filtertype location , filtertype scale) {
		bsplcoeffs[0] = - scale;
		bsplcoeffs[1] =  scale;
	}
};
template <typename filtertype> class bSplineCoeffs<2,filtertype> {
public:
	static inline void get( filtertype * bsplcoeffs, filtertype location ) {
		filtertype locationr = 1-location;
		bsplcoeffs[0] = .5*locationr*locationr;
		bsplcoeffs[1] = .5+ location *locationr;
		bsplcoeffs[2] =  .5* location*location;
	}
	static inline void getD( filtertype * bsplcoeffs, filtertype location , filtertype scale) {
		filtertype locationr = 1-location;
		bsplcoeffs[0] = - locationr * scale;
		bsplcoeffs[1] =  (locationr -location)* scale;
		bsplcoeffs[2] =  location * scale;
	}
};
template <typename filtertype> class bSplineCoeffs<3,filtertype> {
public:
	static inline void get( filtertype * bsplcoeffs, filtertype location ) {
		filtertype locationr = 1-location;
		filtertype sqr_location  = location *location;
		filtertype sqr_locationr = locationr*locationr;

		bsplcoeffs[0] = (1. /6.) * sqr_locationr * locationr;
		bsplcoeffs[1] = (1. /6.) * (4-3*sqr_location *(1 + locationr) );
		bsplcoeffs[2] = (1. /6.) * (4-3*sqr_locationr*(1 + location ) );
		bsplcoeffs[3] = (1. /6.) * sqr_location  * location ;
	}
	static inline void getD( filtertype * bsplcoeffs, filtertype location , filtertype scale) {
		filtertype locationr = 1-location;
		filtertype sqr_location  = location *location;
		filtertype sqr_locationr = locationr*locationr;
		scale *= 0.5;

		bsplcoeffs[0] = -scale * sqr_locationr ;
		bsplcoeffs[1] = -scale * (4 *location  - 3 * sqr_location );
		bsplcoeffs[2] =  scale * (4 *locationr - 3 * sqr_locationr);
		bsplcoeffs[3] =  scale * sqr_location  ;
	}
};
/*
// general template used for not explicitly implemented b-spline orders:
template <int bsplineord, typename filtertype> class bSplineCoeffs<bsplineord,filtertype> {
public:
	static inline void get( filtertype * bsplcoeffs, filtertype location ) {
        // Use the Boor's algorithm (http://en.wikipedia.org/wiki/De_Boor_algorithm)
        // which is:
        // d^0 = eye( bsplineord +1 );
        // alpha_i^k = (location-i)/(i+n+1-k-i)
        // d_i^k = (1-alpha) * d_{i-1}^{k-1} + alpha * d_i^{k-1}  , with k=1..n, i = l :-1: l-n+k, with l=bsplineord
        // bsplcoeffs = d_bsplineord^bsplineord
        //
        // Actually this algorithm would be substantially more efficient when evaluated on 
        // scalar vectors immediately. However for multi-dimensional images, first calculating the 
        // actual coefficients is beneficial.
       
        
        filtertype D[ (bsplineord+1)*(bsplineord+1) ]; // declare temporary space.
                             // note that only the upper triangle is used at all, 
                             // and at most (exactly) 1+bsplineord+.25*(bsplineord^2-mod(bsplineord,2) )
                             // elements are used at any one time. However, it would be 
                             // very difficult write the algorithm when declaring only that many. 
                                                    
        // specialize for k==1, to avoid the need to explicitly initialize D to identity:
        { int k==1;
            for (int i = bsplineord, i>=bsplineord-bsplineord+k, --i ) {
                filtertype alpha = (location+bsplineord-i)/(bsplineord + 1 - k);
                {int j = i-k ; // d[j,i] not yet set 
                    d[ j + i*(bsplineord+1)] = (1-alpha) ;
                {int j = i ; // d[j,i-1] not yet set 
                    d[ j + i*(bsplineord+1)] = alpha ;
                }
            }
        }
        // remaining k
        for (int k = 2 ; k <= bsplineord; ++k ) {
            for (int i = bsplineord, i>=bsplineord-bsplineord+k, --i ) {
                filtertype alpha = (location+bsplineord-i)/(bsplineord + 1 - k);
                {int j = i-k ; // d[j,i] not yet set 
                    d[ j + i*(bsplineord+1)] = (1-alpha)*d[ j + (i-1)*(bsplineord+1)] ;
                }
                for (int j = i-k+1 , j<=i-1, ++j) {
                    d[ j + i*(bsplineord+1)] = (1-alpha)*d[ j + (i-1)*(bsplineord+1)] + alpha * d[ j + i*(bsplineord+1)];
                }
                {int j = i ; // d[j,i-1] not set 
                    d[ j + i*(bsplineord+1)] =                                          alpha * d[ j + i*(bsplineord+1)];
                }
            }
        }
        for (int j = 0 , j<=bsplineord, ++j, ++bsplcoeffs ) {
            *bsplcoeffs = d[ j + bsplineord*(bsplineord+1)];
        }
    }
};
  // */      
        
template <int bsplineord, typename filtertype> class bSplineCoefficients {
protected:
	filtertype bsplcoeffs[ bsplineord + 1 ];
public:
	typedef filtertype value_type;
	enum {fixedlength = bsplineord+1};

//	bSplineCoefficients() {}; // default constructor, call prepare_coefficients[Derivative] to initialize coefficients.
	inline void prepare_coefficients( filtertype location ) {
		bSplineCoeffs<bsplineord, filtertype>::get( bsplcoeffs, location );
	}
	inline void prepare_coefficientsDerivative( filtertype location , filtertype scale ) {
		bSplineCoeffs<bsplineord, filtertype>::getD( bsplcoeffs, location , scale);
	}
	inline filtertype operator[]( int i ) {
		return bsplcoeffs[i];
	}
	inline int length() {
		return fixedlength;
	}
	inline int getOrder() {
		return bsplineord;
	}
	inline filtertype delay() {
		return ((filtertype) bsplineord-1) / 2.;
	}
	template <typename proctype> inline proctype sample( proctype * samplelocation, ptrdiff_t stride ) {
		// Sample single value with prepared bspline coefficients from samplelocation, samples separated by stride.
		proctype ret = (*samplelocation) * bsplcoeffs[0];//Numeric<filtertype>.Zero ;
		// compiler should!!! unroll loop (but MSVC probably does not)
		for (int k=1;k<=bsplineord ;k++) {
			samplelocation += stride;
			ret += (*samplelocation) * bsplcoeffs[k];
		}
		return ret;
	}
	template <typename proctype> void sample( proctype * in, proctype * out, ptrdiff_t length, ptrdiff_t stepin, ptrdiff_t stepout, ptrdiff_t stride) {
		// sample a vector of values, each with the same prepared coefficients. 
		// elements of both in and out vector separated by step.
		// elements for different b-spline coefficients in 'in' separated by stride. 
		// length specifies number of elements of vector out to compute.
		for (; length>0; length--,out+=stepout,in+=stepin) {
			*out = sample( in, stride );
		}
	}
	template <typename proctype> void multisample( proctype * in, proctype * out, ptrdiff_t length, ptrdiff_t stepin, ptrdiff_t stepout, filtertype origin, filtertype step) {
		// sample a vector of values, at the starts:  origin + k * step  (k=0..length-1)
		// length specifies number of elements of vector out to compute.
		for (int k=0; k<length; out+=stepout, k++) {
			filtertype position = origin + step * k; 
			ptrdiff_t column = (ptrdiff_t) fastfloor( position );
			position -= column;
			prepare_coefficients( position );
			*out = sample( in + column * stepin, stepin );
		}
	}
	template <typename proctype> inline void sampleAdjoint( proctype * samplelocation, ptrdiff_t stride , proctype value) {
		// Sample single value with prepared bspline coefficients from samplelocation, samples separated by stride.
		// compiler should!!! unroll loop (but MSVC probably does not)
		for (int k=0;k<=bsplineord ;k++, samplelocation += stride) {
			(*samplelocation) += value * bsplcoeffs[k];
		}
	}
	template <typename proctype> void sampleAdjoint( proctype * i, proctype * o, ptrdiff_t length, ptrdiff_t stepi, ptrdiff_t stepo, ptrdiff_t stride) {
		// sample a vector of values, each with the same prepared coefficients. 
		// elements of both in and out vector separated by step.
		// elements for different b-spline coefficients in 'out' separated by stride. 
		// length specifies number of elements of vector out to compute.
		for (; length>0; length--,o+=stepo,i+=stepi) {
			sampleAdjoint( i , stride , *o );
		}
	}
};


template <typename filtertype> class bSplineCoefficients<-1,filtertype> {
private:
	int order;
	union {
		bSplineCoefficients<0, filtertype> ord0;
		bSplineCoefficients<1, filtertype> ord1;
		bSplineCoefficients<2, filtertype> ord2;
		bSplineCoefficients<3, filtertype> ord3;
	} bspl;
public:
	typedef filtertype value_type;
	enum {fixedlength = -1};

	bSplineCoefficients( int order_ ) {
		order = order_;
		if ( (order<0) || (order>3) ) {
			mexErrMsgTxt("Only b-spline order 0,1,2 and 3 are supported.");
		}
	}
	int getOrder() {
		return order;
	}
	inline filtertype operator[]( int i ) {
		if (order==0) {
			bspl.ord0[ i ];
		} else if (order==1) {
			bspl.ord1[ i ];
		} else if (order==2) {
			bspl.ord2[ i ];
		} else if (order==3) {
			bspl.ord3[ i ];
		} 
	}
	inline int length() {
		return order+1;
	}
	inline void prepare_coefficients( filtertype location ) {
		if (order==0) {
			bspl.ord0.prepare_coefficients( location );
		} else if (order==1) {
			bspl.ord1.prepare_coefficients( location );
		} else if (order==2) {
			bspl.ord2.prepare_coefficients( location );
		} else if (order==3) {
			bspl.ord3.prepare_coefficients( location );
		} 
	}
	inline void prepare_coefficientsDerivative( filtertype location , filtertype scale ) {
		if (order==0) {
			bspl.ord0.prepare_coefficientsDerivative( location , scale);
		} else if (order==1) {
			bspl.ord1.prepare_coefficientsDerivative( location , scale);
		} else if (order==2) {
			bspl.ord2.prepare_coefficientsDerivative( location , scale);
		} else if (order==3) {
			bspl.ord3.prepare_coefficientsDerivative( location , scale);
		} 
	}
	template <typename proctype> proctype sample( proctype * samplelocation, ptrdiff_t stride ) {
		if (order==0) {
			return bspl.ord0.sample( samplelocation, stride);
		} else if (order==1) {
			return bspl.ord1.sample( samplelocation, stride);
		} else if (order==2) {
			return bspl.ord2.sample( samplelocation, stride);
		} else if (order==3) {
			return bspl.ord3.sample( samplelocation, stride);
		} 
	}
	template <typename proctype> void sample( proctype * in, proctype * out, ptrdiff_t length, ptrdiff_t stepin, ptrdiff_t stepout, ptrdiff_t stride) {
		if (order==0) {
			bspl.ord0.sample( in, out, length, stepin, stepout, stride);
		} else if (order==1) {
			bspl.ord1.sample( in, out, length, stepin, stepout, stride);
		} else if (order==2) {
			bspl.ord2.sample( in, out, length, stepin, stepout, stride);
		} else if (order==3) {
			bspl.ord3.sample( in, out, length, stepin, stepout, stride);
		} 
	}
	template <typename proctype> void multisample( proctype * in, proctype * out, ptrdiff_t length, ptrdiff_t stepin, ptrdiff_t stepout, filtertype origin, filtertype step) {
		if (order==0) {
			bspl.ord0.multisample( in, out, length, stepin, stepout, origin, step);
		} else if (order==1) {
			bspl.ord1.multisample( in, out, length, stepin, stepout, origin, step);
		} else if (order==2) {
			bspl.ord2.multisample( in, out, length, stepin, stepout, origin, step);
		} else if (order==3) {
			bspl.ord3.multisample( in, out, length, stepin, stepout, origin, step);
		} 
	}
	template <typename proctype> proctype sampleAdjoint( proctype * samplelocation, ptrdiff_t stride , proctype value) {
		if (order==0) {
			return bspl.ord0.sampleAdjoint( samplelocation, stride, value);
		} else if (order==1) {
			return bspl.ord1.sampleAdjoint( samplelocation, stride, value);
		} else if (order==2) {
			return bspl.ord2.sampleAdjoint( samplelocation, stride, value);
		} else if (order==3) {
			return bspl.ord3.sampleAdjoint( samplelocation, stride, value);
		} 
	}
	template <typename proctype> void sampleAdjoint( proctype * i, proctype * o, ptrdiff_t length, ptrdiff_t stepi, ptrdiff_t stepo, ptrdiff_t stride) {
		// stores in i, reads from o
		if (order==0) {
			bspl.ord0.sampleAdjoint( i, o, length, stepi, stepo, stride);
		} else if (order==1) {
			bspl.ord1.sampleAdjoint( i, o, length, stepi, stepo, stride);
		} else if (order==2) {
			bspl.ord2.sampleAdjoint( i, o, length, stepi, stepo, stride);
		} else if (order==3) {
			bspl.ord3.sampleAdjoint( i, o, length, stepi, stepo, stride);
		} 
	}
};

template <int bsplineord, typename filtertype> class bSplineSampleVect; // define fbSplineSampleVect

template <typename filtertype> class bSplineSampleVect<-1, filtertype> : private bSplineCoefficients<-1,filtertype> {
protected:
	friend class bSplineSampleVect<2, filtertype>;
	filtertype * source;
	ptrdiff_t step;
	ptrdiff_t stride;
	ptrdiff_t length;
	ptrdiff_t ncolumns;
public:
	bSplineSampleVect(): bSplineCoefficients<-1,filtertype>(0) { // default constructor;
		source = NULL;
		step =0;
		stride =0;
		length = 0;
		ncolumns = 0;
	}
	bSplineSampleVect( filtertype * source_, ptrdiff_t step_, ptrdiff_t length_, ptrdiff_t stride_ , ptrdiff_t ncolumns_, int order_): bSplineCoefficients<-1,filtertype>(order_) {
		source	= source_;
		step	= step_;
		length	= length_;
		stride	= stride_;
		ncolumns = ncolumns_;
	}

		#pragma intrinsic( floor )
	template <typename proctype> void sample( proctype * out, filtertype position ) {
		//ptrdiff_t column = (ptrdiff_t) floor( position );
		ptrdiff_t column = (ptrdiff_t) fastfloor( position );
		if ( (column<0) || (column + this->getOrder() > ncolumns) ) {
			mexErrMsgTxt("Sampling out of bounds.");
		}
		position -= column;
		prepare_coefficients( position );
		((bSplineCoefficients<-1, filtertype>) (*this)).sample<proctype>( source + column * stride, out , length, step, step, stride );
	}

	template <typename proctype> void sample_range( proctype * out, filtertype origin, filtertype step_ , int numsamples ) {
		//ptrdiff_t column = (ptrdiff_t) floor( position );
		if (length!=1) {
			mexErrMsgTxt("Can only sample range when length ==1.");
		}
		if ( !(origin>=0) || !(origin+step_*(numsamples-1)>=0) || !(origin+step_*(numsamples-1) + this->getOrder() < ncolumns) ) {
			mexErrMsgTxt("Sampling out of bounds.");
		}
		((bSplineCoefficients<-1, filtertype>) (*this)).multisample( source, out, numsamples, step, step, origin, step_);
	}

	template <typename proctype> void sampleAdjoint( proctype * o, filtertype position , bool clear_o) {
		ptrdiff_t column = (ptrdiff_t) floor( position );
		if ( (column<0) || (column+ this->getOrder() > ncolumns) ) {
			mexErrMsgTxt("Sampling out of bounds.");
		}
		position -= column;
		prepare_coefficients( position );
		((bSplineCoefficients<-1, filtertype>) (*this)).sampleAdjoint<proctype>( source + column * stride, o , length, step, step, stride );
		if (clear_o)
			for ( int k=0; k<length ; k++, o+=step) {
				*o =0;
			}

	}
	inline int subtype() {
		return this->getOrder();
	}
	typedef bSplineSampleVect<0, filtertype> specialization0 ;
	typedef bSplineSampleVect<1, filtertype> specialization1 ;
	typedef bSplineSampleVect<2, filtertype> specialization2 ;
	typedef bSplineSampleVect<3, filtertype> specialization3 ;
};

template <int bsplineord, typename filtertype> class bSplineSampleVect:  private bSplineCoefficients<bsplineord, filtertype> {
private:
	filtertype * source;
	filtertype * cursourcecolumn;
	ptrdiff_t step;
	ptrdiff_t stride;
	ptrdiff_t length;
	ptrdiff_t ncolumns;
public:
	bSplineSampleVect( filtertype * source_, ptrdiff_t step_, ptrdiff_t length_, ptrdiff_t stride_ , ptrdiff_t ncolumns_) {
		source	= source_;
		step	= step_;
		length	= length_;
		stride	= stride_;
		ncolumns = ncolumns_;
		cursourcecolumn = source;
	}
	bSplineSampleVect( bSplineSampleVect<-1 , filtertype> src ) {
		source  = src.source;
		step    = src.step;
		length  = src.length;
		stride  = src.stride;
		ncolumns= src.ncolumns;
		cursourcecolumn = source;
	}
	void prepare( filtertype position ) {
		ptrdiff_t column = (ptrdiff_t) floor( position );
		if ( (column<0) || (column + this->getOrder() > ncolumns) ) {
			mexErrMsgTxt("Sampling out of bounds.");
		}
		position -= column;
		cursourcecolumn += column * stride;
		prepare_coefficients( position );
	}
	filtertype sample( ptrdiff_t i  ) {
		return ((bSplineCoefficients<bsplineord, filtertype>) (*this)).sample( cursourcecolumn + i * step, stride );
	}
	/*template< int vlen >  vec<filtertype, vlen> sample( ptrdiff_t i ) {
		return ((bSplineCoefficients<bsplineord, filtertype>) (*this)).sample<vlen>( cursourcecolumn + i * step, stride );
	}*/
};


template <typename filtertype, int bsplineord> class sampleFilterBspline; // define samplefilter

template <typename filtertype> class filter { 
protected:
	int bsplineorder;
	filtertype * F;
	int stride;
	const int nsteps;
public:
	filter( filtertype * F_, int stride_, int nsteps_, int bsplineord_): F(F_), stride(stride_), nsteps(nsteps_), bsplineorder(bsplineord_) {
		
	};
	inline int length() {
		return stride;
	}
	inline int subtype() {
		return bsplineorder;
	}

	typedef sampleFilterBspline<filtertype, 0> specialization0 ;
	typedef sampleFilterBspline<filtertype, 1> specialization1 ;
	typedef sampleFilterBspline<filtertype, 2> specialization2 ;
	typedef sampleFilterBspline<filtertype, 3> specialization3 ;
};

//template <typename sampleFilterBspline, typename filtertype, int bsplineord> class sampleFilterBspline_Base {
template <typename filtertype, int bsplineord> class sampleFilterBspline : public filter<filtertype> {
private:
	typedef bSplineCoefficients<bsplineord, filtertype> bSplineT;
	filtertype * curColF;
	bSplineT bspl;
	bSplineT bsplDerivative;
	inline filtertype setcolumn( filtertype location ) {
		location *= (filtertype ) this->nsteps;
		int curcol = (int) location;
		curColF = this->F + curcol * this->stride;
		return location - curcol;
	}
public:
	sampleFilterBspline( filter<filtertype> &  F_ ) : filter<filtertype>( F_ ) {
	}
	inline void prepare(filtertype location) {
		//assert( (location>=0) && (location<=1) );
		bspl.prepare_coefficients( setcolumn( location ) );
	}
	inline void prepareDerivative(filtertype location) {
		bsplDerivative.prepare_coefficientsDerivative( setcolumn( location ) , this->nsteps );
	}
	inline void prepareValueAndDerivative(filtertype location) {
		filtertype fracLocation = setcolumn( location );
		bspl.prepare_coefficients( fracLocation );
		bsplDerivative.prepare_coefficientsDerivative( fracLocation , this->nsteps  );
	}
	inline filtertype sample( int i ) {
		return bspl.sample( curColF + i, this->stride ) ;
	}
	template< int vlen >  vec<filtertype, vlen> sample( ptrdiff_t i ) {
		return bspl.sample(  vecTptr<filtertype *, vlen>( curColF + i) , this->stride );
	}
	inline filtertype sampleDerivative( int i ) {
		return bsplDerivative.sample( curColF + i, this->stride ) ;
	}
};

// ND-IMAGE sampling function, compile time known length coefficients. 
template <int N, int ncoeffsperdim, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		proctype ret = 0;//Numeric<filtertype>.Zero ;
		// compiler should!!! unroll loop (but MSVC probably does not)
		for (int k=0; k < ncoeffsperdim ;k++, source += steps[N-1]) {
			ret += sampleNDimagefun<N-1, ncoeffsperdim, imgtype, proctype, filttype>::sample(source , steps, coeffs) * coeffs[ N-1 ][ k ];
		}
		return ret;
	}
};
// end case: sample source image and convert to processing type.
template <int ncoeffsperdim, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<0, ncoeffsperdim, imgtype, proctype, filttype> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		return (proctype) (*source); 
	}
};
// ND-IMAGE sampling function, runtime known length of coefficients. 
template <int N, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<N,-1,imgtype, proctype, filttype> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		proctype ret = 0;//Numeric<filtertype>.Zero ;
		for (int k=0; k < coeffs[N-1].length() ; k++, source += steps[N-1]) {
			ret += sampleNDimagefun<N-1, -1, imgtype, proctype, filttype>::sample(source , steps, coeffs) * coeffs[ N-1 ][ k ];
		}
		return ret;
	}
};
// end case: sample source image and convert to processing type.
template <typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<0, -1, imgtype, proctype, filttype> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		return (proctype) (*source); 
	}
};

template <int N, typename imgtype> class NDimage {
private:
	imgtype * origin;
	ptrdiff_t size[N];
	ptrdiff_t stride[N];

	template< typename samplerType > class sample_range_helper {
	public: 
		samplerType * original;
		int offset;
		int newlength;
		sample_range_helper(){}; // default constructor.
		sample_range_helper(samplerType * orig, int off, int len) {
			original = orig;offset=off;newlength = len;
		}
		inline int length() {
			return newlength;
		}
		inline typename samplerType::value_type operator[](int i) {
			return (*original)[i+offset];
		}
	};

public:
	NDimage( imgtype * origin_, const ptrdiff_t * size_, const ptrdiff_t * stride_) {
		origin = origin_;
		for (int i=0;i<N;i++) {
			size[i]=size_[i];
		}
		for (int i=0;i<N;i++) {
			stride[i]=stride_[i];
		}

	}
	template< typename procType, typename locationType, typename samplerType> procType sample( locationType point[], samplerType samplers[]) {
		imgtype * sampleorigin = origin;
		bool allinrange = true;
		ptrdiff_t loc[N];
		for (int i=0;i<N;i++) {
			locationType pti = point[i] - samplers[i].delay();
			loc[i] = (ptrdiff_t) floor(pti);
			pti -= loc[i];
			samplers[i].prepare_coefficients( pti );
			allinrange &= (loc[i]>=0) && (loc[i]+samplers[i].length() <= size[i]);
			sampleorigin += loc[i] * stride[i];
		}
		if (!allinrange) {
			//mexErrMsgTxt("Trying to sample (partly) outside of image, not supported (yet).");
			typedef sample_range_helper<samplerType> subsampT;
			subsampT subsamp[N];
			for (int i=0;i<N;i++) {
			    using std::min;
			    using std::max;

				int offset    = (int) max( (ptrdiff_t) 0, -loc[i]);
				int newlength = (int) min( (ptrdiff_t) samplers[i].length(), size[i]-loc[i] ) - offset;
				if (newlength<=0) {
					return 0;
				}
				sampleorigin += offset * stride[i];
				subsamp[i] = subsampT(&samplers[i], offset, newlength);
			}
			return sampleNDimagefun<N, -1 , imgtype, procType, subsampT>::sample(  sampleorigin, stride , subsamp );
		} else {
			return sampleNDimagefun<N, samplerType::fixedlength , imgtype, procType, samplerType>::sample(  sampleorigin, stride , samplers );
		}
	}
};

#ifdef BUILDTESTFUN_BSPLINE_PROC
#include "mex.h"

void self_test_mex( mxArray * out ) {
	// Perform some tests to check the code above as thouroughly as possible.
	// Return computed values as we can't independently check them.
	// Out should be an initialized 2 element struct array.
	typedef bSplineCoefficients<-1, double> bSplT;

	char * fieldnames[4] = {"test_bspline0","test_bspline1","test_bspline2","test_bspline3"};
	for (int bsplineord=0;bsplineord<4;bsplineord++){
		mxAddField(out , fieldnames[bsplineord]);
	}
	// Test explicit bspline order 0..3
	double tstsignal[4] = {0,0,0,0};
	for( int bsplineord = 0; bsplineord<4; bsplineord++) {
		const int nsteps = 100;
		bSplT bspl = bSplT( bsplineord );
		int doderiv=2;
		if (bsplineord==0) doderiv=1;

		for (int doderivative =0; doderivative<doderiv; doderivative++ ) {
			mxArray * testbspl = mxCreateDoubleMatrix( bsplineord+1 , nsteps, mxREAL);
			mxSetField(out, doderivative, fieldnames[bsplineord], testbspl);
			double * testbsplp = mxGetPr( testbspl );
			for (int i =0 ; i<nsteps; i++ ) {
				if (doderivative==0) {
					bspl.prepare_coefficients( ((double) i) / ((double) nsteps-1) );
				} else {
					bspl.prepare_coefficientsDerivative( ((double) i) / ((double) nsteps-1) , 1);
				}
				for (int j =0; j<=bsplineord ;j++ ){
					if (j>0) {
						tstsignal[j-1] = 0;
					}
					tstsignal[j] = 1;
					*testbsplp = bspl.sample( tstsignal, 1 );
					testbsplp++;
				}
				tstsignal[bsplineord] = 0;
			}
		}
	}
	// create test image
	mxArray * testimg = mxCreateDoubleMatrix( 10 , 10, mxREAL);
	double * testimgp = mxGetPr( testimg );
	ptrdiff_t imgsz[2] = {10,10};
	ptrdiff_t stride[2] = {1,10};
	testimgp[0 + 0* 10] = 1;

	NDimage<2, double> img = NDimage<2,double>(testimgp, imgsz,stride);

	// test interpolation of vectors (Note: step ==1, bsplineorder =2):
	typedef bSplineSampleVect<-1,double> sampleVectT;
	sampleVectT v = sampleVectT(testimgp, 1, imgsz[0]-4, imgsz[0], imgsz[1], 2);
	const int interpszv = 20;
	mxArray * testimgoutv = mxCreateDoubleMatrix( imgsz[0] , interpszv, mxREAL);
	double * testimgoutvp = mxGetPr( testimgoutv );
	for (int k=0;k<interpszv; k++) {
		v.sample(testimgoutvp + k * imgsz[0], k*0.1);
	}
	if (v.subtype()==2) {

		sampleVectT::specialization2 vs = sampleVectT::specialization2( v );
		for (int k=0;k<interpszv; k++) {
			vs.prepare( k*0.1 );
			for (int j = 0 ; j<4; j++ ) {
				testimgoutvp[ k * imgsz[0] + imgsz[0]-4 + j] = 
						vs.sample( j );
			}
		}
	} else {
		mexErrMsgTxt("Subtype 2 expected.");
	}
	mxAddField(out , "testinterpolvec");
	mxSetField(out, 0, "testinterpolvec", testimgoutv);		 

// test sampling of filterbank:
	typedef filter<double> filterT;
	const int nfsteps = imgsz[1]-2;
	filterT f = filterT(testimgp, imgsz[0], nfsteps, 2);
	// enforce filterbank periodicity constraint:
	for (int k=0;k<2; k++) {
		testimgp[ (k + nfsteps) * imgsz[0] + 0  ] = 0;
		for (int j=0;j<f.length()-1;j++) {
			testimgp[ (k + nfsteps) * imgsz[0] + j +1 ] = testimgp[ (k ) * imgsz[0] + j  ];
		}
	}
	const int interpszf = 100;
	mxArray * testfsamp = mxCreateDoubleMatrix( imgsz[0] , interpszf, mxREAL);
	double * testfsampp = mxGetPr( testfsamp );
	mxArray * testdfsamp = mxCreateDoubleMatrix( imgsz[0] , interpszf, mxREAL);
	double * testdfsampp = mxGetPr( testdfsamp );
	if (f.subtype()==2) {
		filterT::specialization2 fs = filterT::specialization2( f );
		for (int k=0;k<interpszf; k++) {
			fs.prepareValueAndDerivative( (double) k / (double) interpszf );
			for (int j = 0 ; j<fs.length(); j++ ) {
				testfsampp[ k * imgsz[0] + j]  = fs.sample( j );
				testdfsampp[ k * imgsz[0] + j] = fs.sampleDerivative( j );
			}
		}
	} else {
		mexErrMsgTxt("filter subtype 2 expected.");
	}
	mxAddField(out , "testfiltsamp");
	mxSetField(out, 0, "testfiltsamp", testfsamp);		 
	mxSetField(out, 1, "testfiltsamp", testdfsamp);		 

	
	const int interpsz = 100;
	const double interpsc = 1.0/30;
	const double offset = -1;
	typedef bSplineCoefficients<2,double> bSplTi;
	bSplTi bspli[2];
	mxArray * testimgout = mxCreateDoubleMatrix( interpsz , interpsz, mxREAL);
	double * testimgoutp = mxGetPr( testimgout );
	double pos[2];
	for (int i = 0; i<interpsz; i++ ) {
		pos[1] = i*interpsc+offset;
		for (int j =0 ; j<interpsz; j++ ) {
			pos[0] = j*interpsc+offset;

			*testimgoutp = img.sample<double>(pos, bspli);
			testimgoutp++;
		}
	}
	mxAddField(out , "testimg");
	mxAddField(out , "testimginterpolated");
	mxSetField(out, 0, "testimg", testimg);
	mxSetField(out, 0, "testimginterpolated", testimgout);


}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
	if (nlhs!=1) {
		mexErrMsgTxt("One output required.");
	}
	plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
	self_test_mex( plhs[0] );
}
#endif // BUILDTESTFUN_BSPLINE_PROC

#endif