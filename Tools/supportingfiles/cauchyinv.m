function [invP] = cauchyinv(x, cent, gam)
% [c] = CAUCHYINV(X, CENT, GAM)
% returns the inverse cumulative cauchy probability density of the values X, with cauchy center
% paramter CENT and width GAM. Default: CENT=0 and GAM = 1
%
% INPUTS
% X    : N-D matrix; points at which the cauchy cdf should be sampled
%        Only 0 < X(k)  < 1 for all k make sense. 
% CENT : (default = 0) center paramter of the cauchy distribution
% GAM  : (default = 1) gamma (widht) of the cauchy distribution
%
% OUTPUTS:
% c    : inverse cumulutative cauchy PDF:
%        solve c from  X = int( cauchyPDF( x_i, CENT, GAM) , x_i = -inf... c )
%        (like norminv for the normal distribution)
%        Note that this inverse is analytic (tan function)
%
% Created by Dirk Poot, Erasmus MC
% 17-3-2011

invP = tan( (x-.5)*pi) ;
if nargin>=3 && ~isempty(gam)
    invP  = bsxfun(@times, invP , gam ) ;
end;
if nargin>=2 && ~isempty( cent) 
    invP = bsxfun(@plus, invP , cent );
end;




