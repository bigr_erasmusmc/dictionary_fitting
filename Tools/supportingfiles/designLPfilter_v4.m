function [filts] = designLPfilter_v4(reference, filtlen, ARorder, varargin)
% [filter] = designLPfilter_v4(cutofffreq, filtlen, ARlen, option1, value1,...)
% [filter] = designLPfilter_v4(reference,  filtlen, ARlen, option1, value1,...)
% Designs a filter to be used with interpolate_with_filterbank
% or in affine transform.
%
% Specifically a filter that transitions from unit transfer to zero transfer within the transition band,
% or follows 'reference'. 
% The filter can use an AR prefilter with which images should be filtered prior to the interpolation with
% the resulting filterbank. This prefiltering allows substantially improved filter response with the same filterbank length. 
% 
%
% INPUTS:
%  cutofffreq : Scalar with the cutoff frequency of the filter (0.5 = nyquist frequency)
%  reference  : function that accepts 1 (vector) argument with the
%               frequency of which the magnitude of the transfer at each frequency 
%               is computed.
%               (to avoid aliasing, 0 should be returned for frequencies>.5)
%  filtlen : scalar. number of samples in each of the subfilters with specific delay. When
%             it's non integer the designed filterbank starts and ends with
%             approximately    (ceil(filtlen)-filtlen)*steps/2       zeros. 
%            set to 'FromInitial' to use the filtlen of the initial filter.
%  ARorder : scalar or 2 element integer vector. 
%            ARorder > 0 : 
%               An AR model with this order is used as compensation.
%               First element specifies the pre-filter, 2nd element (if
%               present) specifies post filter. Speficy resampling rate in samplingstep
%            ARorder == 0 : no prefiltering is to be used. Note prefiltering is 
%               (almost always) strongly beneficial to reach the desired response.
%            ARorder == -1 : compute explicit frequency domain prefiltering.
%               Typically only usefull when the prefiltering is already followed by an 
%               explicit Fourier transform which in its turn is followed by sampling. (i.e. non uniform FFT)
%               Using autoregressive prefiltering usually is faster when the fft is not needed. 
%            ARorder == 'FromInitial': Use the ARorder of the initial filter.
% Option-value pairs:
%  filterSteps : number of delay steps in the filterbank, default= 20
%  weight  : a function that computes the weight that should be given to a
%            specific frequency. Default: 1/(f+.1) source spectrum assumed
%            and ignoring a small part around the cutofffrequency:
%               weight = @(f) 1./sqrt((f-round(f)).^2+.01) ;
%            NOTE: except for the ignored frequencies around the
%                  cutoff frequency, this weight is periodic with period 1.
%            When you are only interested in the first derivative, multiply the above weight by f.
%  doplot  : false  : (default) no plotting.
%            true   : plot diagnostic information and frequency responce of resulting filter.
%  filterbank_bsplineorder : default = 2; 
%            order of b-spline interpolation for which the filterbank is designed.
%            Only specify 1 (linear interpolation), when derivatives are
%            not needed.
%  ForceExactZeroFreqTransfer : default = true; 
%            if true, the zero spatial frequency is treated specially to
%               prevent any aliasing or distortion of it.
%  transitionBands : n x 2 matrix with [start end] frequency of the n
%                    transition bands. Default: [] if reference is
%                    provided, [.9 1.1]*cutofffreq if cutofffreq is
%                    provided.
%  transitionBandTolerance : default = .01;
%            Absolute error tolerated at the edges of the transition bands
%            if empty, no hard constraints are used.
%  filterInitialisation : if non empty, a (array of) correct filter(s)
%            Used as initial value from which the
%            optimization starts. Providing an initial value should not
%            have an significant effect on the final filter (this is not guaranteed), 
%            though computation time might be strongly influenced.
%            if an vector of initial filters is providided, the filters should be 
%            ordered such that the cutoff frequency monotonically increases. 
%            The filter clostest to the currently requested is used as starting point. 
%            If possible, the initial filter is interpolated from the (closest) provided filters.
%            You probably want to set filtlen and ARorder to 'FromInitial'
%            When the provided initial filters are obtained with all settings
%            exactly equal, set 'MayReturnInitial' to true
%            You may also want to set 'skipUnconstrainedOptimization' to true.
%  MayReturnInitial : default = false; If true and 'filterInitialisation' is non-empty:
%            if cutofffrequency is (almost) equal to (one of) the inial filter(s), that filter is
%            returned without any optimization. If no matching filter is found, optimization is performed.
%            cutofffrequency difference should be <min(.001, .01*diff(transitionBands))
%  skipUnconstrainedOptimization: default = false
%            Allows to proceed immediately to the constrained minimization
%            step. Only usefull when a good inital filter is provided.
%  tolFunCon : default: default of fmincon. Function tolerance for
%             constrained optimization
%  samplingStep : step between samples with which this filter is used. 
%                 size should be equal to ARorder
%                 Only used for postcompensation AR computations. 
%                 default = [1 .5/cutofffreq].
%
% OUTPUTS:
%  filter : structure with the designed interpolating filter
%         Can be used with 'interpolate_with_filterbank'
% 
% Created by Dirk Poot (dirk.poot@ua.ac.be), Copyright University of Antwerp
%   Creation date: 15-7-2011, based on designLPfilter
%   Modified 9-10-2013: Dirk Poot, TUDelft.

if ~isa(reference,'function_handle')
    cutofffreq = reference;
    if ~isscalar(cutofffreq) %|| reference<=.1/filtlen 
        error('cutofffreq should be a scalar >=.1/filtlen. (and typically is <= .5 = Nyquist frequency)');
    end;
    reference = [];
else
    cutofffreq = [];
end;
% defaults: 
opts.filterSteps = [];
opts.weight = @(f) 1./sqrt((f-round(f)).^2+.01) ;
opts.weight_prefilter = .01 ; % weight as function of frequency
opts.doPlot = false;
opts.filterbank_bsplineorder = 2; % bspline interpolation order for filterbank
opts.ForceExactZeroFreqTransfer =true ;% if true: the zero spatial frequency (i.e. constant)
                                   % should be transfered exactly and have
                                   % an exactly zero contribution to the
                                   % aliasing.
                                   % if false: zero spatial frequency is
                                   % not treated specially.
opts.transitionBands = [];  
opts.samplingStep = [1 1];
if ~isempty(cutofffreq)
    opts.transitionBands = [.9 1.1]*cutofffreq;
    opts.samplingStep = [1 .5/cutofffreq];
end;
opts.transitionBandTolerance = .01; % tolerance 
opts.filterInitialisation = [];
opts.skipUnconstrainedOptimization = false;
opts.MayReturnInitial = false;
opts.tolFunCon = [];
opts.maxNmodes = 40;  % maximum number of modes to consider in optimization. Increasing may significantly increase computation time with little benefit of filter quality. 
opts.hardConstrain = []; % if true: apply hard constraints at edges of transition bands. Else no hard constraints are applied.
                         % default = ~isempty(otps.transitionBands);
% Update options with provided arguments:
opts = parse_defaults_optionvaluepairs( opts, varargin{:} );

% Initialize filter:
if ~isempty(opts.filterInitialisation)
    if ~isempty(cutofffreq) && numel(opts.filterInitialisation)>1
        cutofffreq_v = [opts.filterInitialisation.cutoffFrequency];
        cnt = max(1,sum( cutofffreq_v <= cutofffreq ));
        initialfilt = opts.filterInitialisation(cnt);
        if numel(opts.filterInitialisation)<cnt 
            frac = ( cutofffreq - cutofffreq_v(cnt) )/( cutofffreq_v(cnt+1)-cutofffreq_v(cnt) );
            if isequal(size(opts.filterInitialisation(cnt).prefilterAR), size(opts.filterInitialisation(cnt+1).prefilterAR)) && isequal(size(opts.filterInitialisation(cnt).filterbank ), size(opts.filterInitialisation(cnt+1).filterbank ))
                % we can interpolate:
                rti = [roots(opts.filterInitialisation(cnt).prefilterAR) roots(opts.filterInitialisation(cnt+1).prefilterAR)]*[1-frac; frac];
                initialfilt.prefilterAR = poly(rti);
                initialfilt.filterbank = opts.filterInitialisation(cnt).filterbank * (1-frac) + opts.filterInitialisation(cnt+1).filterbank*frac;
            elseif frac>.5
                % can't interpolate, but next filter is closer to requested filter:
                initialfilt = opts.filterInitialisation(cnt+1);
            end;
        end;
        if opts.MayReturnInitial && abs(initialfilt.cutoffFrequency-cutofffreq)<min(.001,.01*min(opts.transitionBands*[-1;1]))
            filts = initialfilt;
            return;
        end;
    else
        % no cutofffrequency provided, or just a single filter provided => we cant select.
        initialfilt = opts.filterInitialisation(1);
    end;
    opts.filterSteps = size( initialfilt.filterbank, 2 )-initialfilt.filterbank_bsplineorder;
    if isequal(filtlen,'FromInitial')
        filtlen = size( initialfilt.filterbank, 1 );
    end;
    if isequal(ARorder,'FromInitial')
        ARorder = numel( initialfilt.prefilterAR )-1;
    end;
else
    initialfilt = [];
    if isempty(opts.filterSteps)
        opts.filterSteps = 20;
    end;
end;
if isempty( opts.hardConstrain )
    opts.hardConstrain = ~isempty( opts.transitionBands );
end;
% opts.hardConstrain =false; % DEBUG, hardConstrain code should be fixed. 
if ~isa(opts.weight,'function_handle')
    error('weight should be a function');
end;
if numel(opts.filterSteps)~=1 || opts.filterSteps~=round(opts.filterSteps)
    error('nsteps should be scalar integer');
end;
nsteps = opts.filterSteps;
if (numel(filtlen)~=1 || filtlen<1) && ~isequal(filtlen,'fromInitial')
    error('filtlen should be scalar value >=1');
end;
if isempty( reference ) % == ~isempty(cutofffreq)
    % slowly transition in the transition band:
    if ~isempty( opts.transitionBands ) 
        reference = @(f) .5+.5*cos(max(0, min(pi, pi*(f-opts.transitionBands(1,1))/(opts.transitionBands(1,2)-opts.transitionBands(1,1)))));
    else
%         reference = @(f) f<cutofffreq;
%       Need 'slow' transition to appropriately handle the discrete frequency samples that are used. 
        reference = @(f) .5+.5*cos(max(0, min(pi, pi*(f-cutofffreq*.9)/(.2*cutofffreq))));
    end;
end;

nmodes = min(opts.maxNmodes, max(2,round(nsteps/2)+1)); % number of modes to consider in optimization.

% nfreqsteps = ceil( 10*filtlen*(1+ARorder) );
if ARorder(1)>=0 
    nfreqsteps = ceil( 5*filtlen*(1+ARorder(1)) );
else
    nfreqsteps = ceil( 5*filtlen *( 1 + 3*(ARorder(1)<0) ) );
end;
filtlen = filtlen - ( opts.filterbank_bsplineorder-1) /nsteps; % internally used filtlen should be slightly smaller due to b-spline interpolation of the resulting filterbank.

% build list of frequency samples
% and corresponding indices for the aliasing of precompensation 
freq = cell(1,nmodes);
precomensateindx = cell(size(freq));
freq{1} = (0.5:nfreqsteps)'/(2*nfreqsteps); % freq between 0 and .5
precomensatelen = numel(freq{1});
precomensateindx{1} = (1:precomensatelen)';
freqad = [-freq{1}(end:-1:1);freq{1}];      % between -.5 and .5
precomensateindxad =  [precomensateindx{1}(end:-1:1); precomensateindx{1}];
for k=2:numel(freq);
    freq{k} = k-1 + freqad;
    precomensateindx{k} = precomensateindxad;
end;
freq = vertcat(freq{:});                    % complete freq vector for all nmodes considered; 0 .. (nmodes-1)+.5 
precomensateindx = vertcat(precomensateindx{:});

% set weight; reduce weight in transition bands:
w = sqrt(opts.weight(freq));
for k=1:size(opts.transitionBands,1)
    w(freq>opts.transitionBands(k,1)& freq<opts.transitionBands(k,2)) = .01*w(freq>opts.transitionBands(k,1)& freq<opts.transitionBands(k,2));
end;

% Compute reference samples, with pre-compensation for b-spline
% interpolation of the filterbank. So ref is what we aim to acchieve with the fourier transform of the filterbank vector. 
filtbinpcompf = @(freq) 1./sinc(min(0.9,freq/nsteps)).^(1+opts.filterbank_bsplineorder);
ref = reference(freq) .* filtbinpcompf(freq);

if opts.hardConstrain
    % Setup:
    %  freqCon : frequencies at which hard constraints are applied 
    %  wCon    : weights at these frequencies
    %  refCon  : ?
    %  limsH   : ?
    %  limsL   : ?
    if isempty(cutofffreq)
        freqCon =  opts.transitionBands ;
    else
        freqCon =  opts.transitionBands *[1 0;.5 .5; 0 1]';
    end;
    wCon = {ones(numel(freqCon),1)};
    precomensateindxCon = (1:numel(freqCon))';
    precomensatelenCon = numel(freqCon);
    refCon = reference(freqCon) .* filtbinpcompf(freqCon) ;
    limsH = filtbinpcompf(freqCon)*opts.transitionBandTolerance;
    limsL = -limsH;
    if ~isempty(cutofffreq)
        limsH(:,2) = 0;
        limsL(:,2) = -refCon(:,2)*(.2/.5);
    end;
    
    freqCon = freqCon(:);
    refCon = refCon(:);
    limsH = limsH(:);
    limsL = limsL(:);
end;

% Basis functions used for constructing filter
% f_k(x) = cos(x * k * 2*pi / (filterlen*nsteps) ) * (abs(x)<=(filterlen*nsteps)/2)
%  make sure the cos part of f is zero at the cutoff to make sure its as continuous as possible. 
%  Use  cos( pi/2 + (int) * pi ) == 0 
%   => (filterlen*nsteps)/2 * k * 2*pi / (filterlen*nsteps) == pi/2 + i * pi
%   => k*pi == pi/2 + i * pi
%   => k-0.5 == i
% with integer (k+.5) so that f_k(x) is symmetric and exactly zero at abs(x)=(filterlen*nsteps)/2
% with x = 0 ... (filterlen*nsteps)/2 :  sample location in long filter, ofsset to center (negative x are not jet wrapped in filterbank)
%
% Fourier transform of this (note x is scaled wrt eq. above):
% F_k( f ) = int_x f_k( x*nsteps ) e^(-2*pi*i* f * x) 
%          = |(filterlen)| * (sinc( ((f-(k / (filterlen*nsteps)*nsteps)) * (filterlen) ) +sinc( (f+(k / (filterlen*nsteps)*nsteps)) * (filterlen) )) /2
%          = (filterlen)/2 * (sinc( f* (filterlen) -k ) + sinc( f*(filterlen) + k ) )/2
% maximum frequency in freq = nmodes-.5 
% k = 0.5 ... min( reasonable limit, filterlen *(nmodes-.5 ) )

k = 0.5:floor( filtlen*(nmodes-.5 )/2 );
filt_x =  (1:(ceil(filtlen)*nsteps+opts.filterbank_bsplineorder))' - (ceil(filtlen)*nsteps+1+opts.filterbank_bsplineorder)/2;
filt_base =  cos(filt_x * (k * 2*pi / (filtlen*nsteps)) );
filt_x_zmask = abs(filt_x)>(filtlen*nsteps/2);
filt_x_zmask([1:opts.filterbank_bsplineorder end-opts.filterbank_bsplineorder+1:end])=true;
filt_base( filt_x_zmask , :) =0; % set coefficients outside requested interval to zero.

FTfun = @(freq, k) sinc(bsxfun(@plus, freq*(filtlen) , - k)) + sinc(bsxfun(@plus, freq*(filtlen) , k));
FT = FTfun(freq, k);

if opts.hardConstrain
    FTCon = FTfun(freqCon, k);
end;
if opts.ForceExactZeroFreqTransfer
    % For exact transfer of the zero frequency, the transfer at 
    % the zero frequency should be exactly 1.
    % and at integer multiples of the sampling frequency it should be exactly zero.
    % Therefore, find the subspace of FT that is zero at fzero
    % and a FTfixed part that satisfies the constraints. 
    % Only optimize in the subspace. 
    %
    % TODO: add description/proof/explanation of algorithm.

    fzero = (0: k(end)/ filtlen)'; % integer frequencies (integer multiple of sampling frequency). At these frequencies transfer should be exact.  
    refAtaliased0 = reference( fzero ) .* filtbinpcompf(fzero);
    FT_fzero_allk = FTfun( fzero, k );
    [dum , kzeroidx ] = max(abs(FT_fzero_allk),[],2); %index of the base functions with transfer closest to each fzero.
    if any(diff(sort(kzeroidx))==0)
        error('Unexpected error; To few steps requested?');
    end;
    keep_k_mask = true(1,numel(k));
    keep_k_mask(kzeroidx) = false;
    k_zero = k( kzeroidx );
    k = k ( keep_k_mask ) ;
    FTfixed = FT(:,kzeroidx)/FT_fzero_allk(:,kzeroidx); % column i of FTfixed has transfer of 1 at zeroidx(i) and zero at all others.
%     FTfixed = FT(:,1)/2;
    FTfixed = FTfixed*refAtaliased0; % fix fixed points to reference.
    % Remove all components to the (aliased) zero frequency from FT:
    FT = FT(:,keep_k_mask) - FT(:,kzeroidx) * (FT_fzero_allk(:,kzeroidx) \ FT_fzero_allk(:, keep_k_mask));
%     FTfixed = sinc(bsxfun(@plus, freq*(filtlen) , 0)) ;
    if opts.hardConstrain
        FTfixedCon = FTCon(:,kzeroidx)/FT_fzero_allk(:,kzeroidx);
        FTfixedCon = FTfixedCon * refAtaliased0;
        FTCon = FTCon(:,keep_k_mask) - FTCon(:,kzeroidx) * (FT_fzero_allk(:,kzeroidx) \ FT_fzero_allk(:, keep_k_mask));
    end;
    
    filt_basez = filt_base(:,kzeroidx)/FT_fzero_allk(:,kzeroidx);
    filt_base = filt_base(:,keep_k_mask) - filt_base(:,kzeroidx) * (FT_fzero_allk(:,kzeroidx) \ FT_fzero_allk(:, keep_k_mask));
else
    FTfixed = [];
end;
warg = {w};
% Set and initialize prefiltering. Also initialize filt based on prefiltering option: 
filt = zeros(size(FT,2),1);
% See designLPfilter_errorfunction for interpretation of 
%  FTs, compensateindx
FTs = cell(1,numel(ARorder)+1);
FTs{1} = FT;
if sum(ARorder)>0
    % AR prefiltering :
    compensateindx = cell(size( FTs));
    for k=1:numel(ARorder)
        if opts.samplingStep(k) ==1
            FTs{k+1} = exp(2*pi*1i*freq(1:precomensatelen)*(0:ARorder(k)));
            compensateindx{k+1} = precomensateindx;
        else
            FTs{k+1} = exp((2*pi*opts.samplingStep(k)*1i)*freq*(0:ARorder(k)));
            compensateindx{k+1}=':';
        end;
    end;
    if opts.hardConstrain    
        compensateindxCon = compensateindx;
        FTsCon = FTs;
        FTsCon{1} = FTCon;
        for k=1:numel(ARorder)
            FTsCon{k+1} = exp((2*pi*opts.samplingStep(k)*1i)*freqCon*(0:ARorder(k)));
            compensateindxCon{k+1}=':';
        end;
    end;
    if isempty(initialfilt)
        % specify poles of AR compensation model (i.e. the resonance frequency) at cutoff frequency:
        if isempty(cutofffreq)
            pole = exp(1i*2*pi*.5)/2;
        else
            pole = exp(1i*2*pi*cutofffreq);
        end;
        pole = [pole;conj(pole)];
        rc = [1 2];lpidx = 1; 
        while any(abs(rc(2:end))>=1)
            ar = 1e-3*randn(ARorder(1)+1,1);%zeros(ARorder+1,1);
            if ARorder(1)>=2
                if ARorder(1)>=4
                    if abs(cutofffreq-.5)<100*eps
                        ar(1:5) = poly([0.934612687248351  -0.923218681977696  -0.638405939397468  -0.072608645234912]);
                    else
                        ar(1:5) = poly([pole*.933;pole*.690]);
                    end;
                else
                    ar(1:3) = poly(pole*.893);
                end;
            end;
            rc = fastar2rc(ar');
            lpidx = lpidx +1 ; if lpidx>20, error('cannot find a correct initial precompensation AR filter. Update initialization code.');end;
        end;
    else
        ar = initialfilt.prefilterAR';
    end;
elseif ARorder<0 
    % NOTE: error if more than 1 AR order is provided. Only 1 AR order is allowed (Except with AR pre and post filtering above)
    if numel(ARorder)~=1
        error('invalid; ARorder should be scalar except when AR pre and post filtering');
    end;
    compensateindx{2} = precomensateindx;
    FTs{2} = zeros(precomensatelen,0);
    filt = reference( k'/filtlen );
    if isa(opts.weight_prefilter ,'function_handle')
        warg{2} = opts.weight_prefilter ( freq(1:precomensatelen) );
    else
        warg{2} = opts.weight_prefilter;
    end;
else % ARorder ==0 => no precompensation. 
    FTs(2:end)=[]; % remove. 
    compensateindx = {[],[]};
    if opts.hardConstrain    
        FTsCon = FTs;
        FTsCon{1} = FTCon;
        compensateindxCon = compensateindx;
    end;
end;

if ~isempty(initialfilt)
    finit = (filtlen/2)*[reshape(initialfilt.filterbank(:,end:-1:1+opts.filterbank_bsplineorder)',[],1);initialfilt.filterbank(end,1:opts.filterbank_bsplineorder)']; 
    if opts.ForceExactZeroFreqTransfer
        finit = finit/sum(ar).^2 - filt_basez * refAtaliased0;
    end;    
    filt = filt_base\finit;
end;

% Add AR precompensation parameters:
if ARorder(1)>0
    filt = [filt;  ar(2:end)];
end;
% add parameters for post compensation AR models:
if numel(ARorder)>1
    filt = [filt;.001*randn(sum(ARorder(2:end)),1)];
end;

% perform optimization:
if ~opts.skipUnconstrainedOptimization
    % evaluate function to initialize filter part with existing AR model:
    fitfunlsq = @(x) designLPfilter_errorfunction(x, ref, warg, FTs, compensateindx, FTfixed,true);
%     [f,G] = fitfunlsq(filt);
%     filt(1:size(FT,2)) = -(G(:,1:size(FT,2))\f); % initialize filter part.

    if numel(FTs)>1
        % Optimize all parametrs
        optlsq = optimset('Jacobian','on','display','off','TolFun',1e-12,'TolX',1e-10,'MaxFunEvals',1000, 'MaxIter',1100, 'PrecondBandWidth',inf, 'Preconditioner',@aprecon_full);
        % optlsq = optimset('Jacobian','on','display','iter','TolFun',1e-12,'TolX',1e-10,'MaxFunEvals',1000, 'MaxIter',1100, 'PrecondBandWidth',inf, 'Preconditioner',@aprecon_full);
        init = filt;
        [filt , fval,residu, exflag,outp] = lsqnonlin(fitfunlsq ,filt,[],[],optlsq);
    end;
end;
%%
if opts.hardConstrain
    % set to zero weight in the transition band:
    for k=1:size(opts.transitionBands,1)
        w(freq>opts.transitionBands(k,1)& freq<opts.transitionBands(k,2)) = 0;
    end;

    fitfunlsq = @(x) designLPfilter_errorfunction(x, ref,    warg,    FTs,    compensateindx,    FTfixed,    true);
    fitfunCon = @(x) designLPfilter_errorfunction(x, refCon, wCon, FTsCon, compensateindxCon, FTfixedCon, true);
    % parameter mapping:
    % filt = M * par
    % d f( M * par) / d par = d f( filt ) / d filt * M
    % d2 f( M * par)/ d par d par' = M' * d2 f( filt) /d filt dfilt' * M
    [dum1,dum2,H0] = lsqwrapper(filt, fitfunlsq, eye(numel(filt)));
    R = chol(H0+eye(numel(filt)));
    Ri = inverse(R);
    optfuncon = @(x0) lsqwrapper(x0, fitfunlsq , Ri);
    confun = @(x0) evaluatecon( x0, fitfunCon, limsH, limsL, Ri);
    hessianfuncon = @(x, lambda) hessianwrapper(x, lambda, fitfunlsq, fitfunCon, Ri);
    optsCon = optimset('fmincon');
%     optsCon = optimset(optsCon,'GradObj','on','GradConstr','on','maxiter',1000,'Algorithm','interior-point');
    optsCon = optimset(optsCon,'GradObj','on','GradConstr','on','maxiter',1000,'Algorithm','interior-point','Hessian','user-supplied', 'HessFcn', hessianfuncon,'display','iter');
    if ~isempty(opts.tolFunCon)
        optsCon = optimset(optsCon,'tolFun',opts.tolFunCon);
    end;
    par = R* filt;
    [filt_con, fval_con, exflg, outp_con] = fmincon( optfuncon, par, [],[],[],[],[],[], confun ,optsCon);
    filt = Ri * filt_con;
%     filt = filt_con;
end;
nextidx = size(FT,2)+1;
ar = cell(1,numel(ARorder));
for k=1:numel(ARorder)
    ar{k} = [1 filt(nextidx:nextidx+ARorder(k)-1)'];
    nextidx = nextidx+ARorder(k);
    if ARorder(k)>1
        runout(k) = round(log(1000)/(1-max(abs(roots(ar{k}))))); % set AR runoutlength equal to slowest decaying root decaying by a factor 1000.
    else
        runout(k) = 0;
    end;
end;

filt = filt(1:size(FT,2));
% compute linear filter
filtf = filt_base * filt;
if opts.ForceExactZeroFreqTransfer
    totalgaincompensation = 1;
    for k = 1:numel(ar)
        totalgaincompensation = totalgaincompensation * sum(ar{k}).^2;
    end;
    filtf = (filtf + filt_basez * refAtaliased0 ) * totalgaincompensation;
end;

% put output in filter struct:
filts.cutoffFrequency = cutofffreq;
filts.filterbank_bsplineorder = opts.filterbank_bsplineorder;
[filts.filterbank, delay] = filterbank( filtf/(filtlen/2) , ceil(filtlen) ,nsteps , (ceil(filtlen)*nsteps-1)/2, opts.filterbank_bsplineorder);
filts.delay = ceil(filtlen)/2;%- opts.filterbank_bsplineorder/(2*nsteps);
filts.prefilterAR = ar{1};
% filts.postfilterAR = ar{2};
filts.ARrunout = runout(1);


if opts.doPlot
    nout = 200000;
    stepout = .0020164798731354;
    in = [1; zeros(ceil(nout*stepout),1)];
    xsmpls2 = 1+(0:nout-1)*stepout;
    [out] = interpolate_with_filterbank(in, xsmpls2, filts);
    out(1,:)=out(1,:)/2;
    pred2 = real(fft(out))*stepout;pred2 = 2*pred2(1:ceil(end/2),:);
    freq2 = (0:size(pred2,1)-1)'/nout/stepout;
    if numel(ARorder)>1
        % explicitly predict frequency responce of postfilter
        % since the filter only really can be applied after the 
        % sampling part of the resampler and this plot part does not go that
        % far.
        postfilter = exp((2*pi*1i*opts.samplingStep(2))*freq2 * (0:ARorder(2))) * ar{2}';
        postfilter = 1./real(postfilter.*conj(postfilter));
        pred2 = pred2.*postfilter ;
    end;
    if isempty(cutofffreq)
        ref2  = reference(freq2);
    else
        ref2  = (freq2<cutofffreq);
    end;
    w2    = sqrt(opts.weight(freq2));

    fval2 =  w2 .* (pred2-ref2);
    interpmode = abs(freq2 - round(freq2/nsteps)*nsteps)<=.5 & freq2>.5;
    if isempty(cutofffreq)
        fNy = .5; 
        errnrm2 = sqrt([sum(fval2(freq2<=fNy).^2) sum(fval2(freq2>fNy & freq2<=.5).^2) sum(fval2(freq2>.5 & ~interpmode).^2) sum(fval2(interpmode).^2)]/sum(w2(freq2<=fNy).^2));
    else
        errnrm2 = sqrt([sum(fval2(freq2<=cutofffreq).^2) sum(fval2(freq2>cutofffreq & freq2<=.5).^2) sum(fval2(freq2>.5 & ~interpmode).^2) sum(fval2(interpmode).^2)]/sum(w2(freq2<=cutofffreq).^2));
    end;
    disp(sprintf('Relative weighted error norms check: below cutoff: %f, above cutoff: %f, above Nyquist: %f, due to interpolation in filterbank: %f',errnrm2));

%%
    % plot ar-compensated responce:
    lh = plot(freq2, [ref2 log(abs([max(eps,w2) pred2 ])) pred2]);
    legend('reference','log weight','log filter' ,'filter');
    set(lh(2),'LineStyle',':');
%     set(lh(end),'color',get(lh(2),'color'));
end;


%%
function [f,g,H] = lsqwrapper( x, fun , R)
x = R*x;
if nargout == 1
    fv = fun(x);
else
    if nargout>2
        [fv, G] = fun(x);
%         hifo = G;
        H = 2*(G'*G);
%         tmp1 = permute(2*sum(bsxfun(@times, fv(1:size(H1,1)), H1),1), [2 3 1]);
%         tmp2 = permute(2*sum(bsxfun(@times, fv(1:size(H2,1)), H2),1), [2 3 1]);
%         H(:,size(tmp1,1)+1:end) = H(:,size(tmp1,1)+1:end) + [tmp1;tmp2];
%         H( size(tmp1,1)+1:end,1:size(tmp1,1)) = H( size(tmp1,1)+1:end,1:size(tmp1,1)) + tmp1';
        H = R'*H*R;
    else
        [fv, G] = fun(x);
    end;
    % d sum_i f_i(x)^2 /dx = 
    % sum_i 2* f_i(x) d f_i(x)/dx
    g = (2*fv(:)'*G)*R;
end
f = fv(:)'*fv(:);
if ~isfinite(f)
%     f = 1e40;
end;

function HY = hessmulWrapper(G, Y)
% d2 sum_i f_i(x)^2 /d x dx = d/dx sum_i 2* f_i(x) d f_i(x)/dx
%  = sum_i 2 df_i/dx *df_i/dx + f_i * d2f_i/dx
% ignore last term
HY = 2*(G'*(G*Y));

function H = hessianwrapper( x, lambda, fun, funCon, R)
x = R * x;
if 0
    [fv, G , H1, H2] = fun(x);
    [fvc,Gc,H1c, H2c] = funCon(x);
    % Gcf = [Gc;-Gc];
    H = 2*(G'*G);
    tmp1 = permute(2*sum(bsxfun(@times, fv(1:size(H1,1)), H1),1), [2 3 1]);
    tmp2 = permute(2*sum(bsxfun(@times, fv(1:size(H2,1)), H2),1), [2 3 1]);
    tmp1(:)=0;
    tmp2(:)=0;
    
    lambdm = lambda.ineqnonlin(1:end/2)-lambda.ineqnonlin(end/2+1:end);
    tmp1c = permute(sum(bsxfun(@times, lambdm, H1c),1), [2 3 1]);
    tmp2c = permute(sum(bsxfun(@times, lambdm, H2c),1), [2 3 1]);
    tmp1c(:)=0;
    tmp2c(:)=0;
    H(:,size(tmp1,1)+1:end) = H(:,size(tmp1,1)+1:end) + [tmp1+tmp1c;tmp2+tmp2c] ;
    H( size(tmp1,1)+1:end,1:size(tmp1,1)) = H( size(tmp1,1)+1:end,1:size(tmp1,1)) + tmp1'+tmp1c';
else
    [dummy, G] = fun(x);
    H = 2*(G'*G);
end;
H = R'*H*R;
function [c, ceq, gradc, gradceq] =  evaluatecon( x, fun, limsH, limsL, R)
x = R*x;
ceq = [];
if nargout>2
    [fv, gfv] = fun(x);
    gfv = gfv( 1:numel(limsH), : );
    gradc = [gfv;-gfv]';
    gradceq =[];
    gradc = (gradc' *R)';
else
    fv = fun(x);
end;
fv = fv(1:numel(limsH)); % remove AR gain as it has no constraint.
c = [fv(:)-limsH;-fv(:)+limsL];
c(~isfinite(c )) = 1e30*sign(c(~isfinite(c )));

function test()
%% simple test for low pass filter, no prefilter:
cutofffreq = .3;
[filts] = designLPfilter_v4( cutofffreq , 4, 0 ,'doPlot', true);
%% simple test for low pass filter, AR prefilter:
cutofffreq = .3;
[filts] = designLPfilter_v4( cutofffreq , 4, 3 ,'doPlot', true);
%%
cutofffreq = .3;
[filts] = designLPfilter_v4( cutofffreq , 4, 3 ,'filterbank_bsplineorder',1);
%%
cutofffreq = .3;
[filts] = designLPfilter_v4( cutofffreq , 4, 3 ,'filterbank_bsplineorder',3);
%% nuFFT interpolation filter:
cutofffreq = .3;
wfun = @(f) double( abs(f-round(f))<cutofffreq ) ;
[filts] = designLPfilter_v4( cutofffreq , 4, -1 ,'filterbank_bsplineorder',1, 'transitionBands',[cutofffreq, 1-cutofffreq],'weight',wfun ,'doPlot', true,'hardConstrain',false,'ForceExactZeroFreqTransfer',false);
%%
cutofffreq = .3;
wfun = @(f) double( abs(f-round(f))<cutofffreq ) ;
wfun_reg = @(f) .01 * exp(- max(0,f-cutofffreq).^2/(2*.01.^2) ); 
[filts] = designLPfilter_v4( cutofffreq , 4, -1 ,'filterbank_bsplineorder',1, 'transitionBands',[cutofffreq, 1-cutofffreq],'weight',wfun ,'doPlot', true,'hardConstrain',false,'ForceExactZeroFreqTransfer',false, 'weight_prefilter',wfun_reg );
