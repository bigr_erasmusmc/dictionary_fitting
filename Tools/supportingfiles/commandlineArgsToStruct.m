function [s] = commandlineArgsToStruct( varargin )
% [s] = argsToStruct( varargin )
%
% Converts a list of argument value pairs to a structure in which the
% arguments are the fields. Arguments should start with a '-', which is
% removed in the fieldname. 
%
% Created by Dirk Poot, Erasmus MC, 6-10-2015

if mod(nargin,2)~=0
    disp(['There are ' num2str(nargin) ' arguments provided : '])
    for k = 1 : nargin
        disp(varargin{k});
    end;
    error('Arguments should come in option value pairs and hence there should be have an even number of them');
end;

s = struct;
nfields = nargin/2;

for i = 1 : nfields;
    nmi = varargin{i*2-1};
    vali = varargin{i*2};
    
    if ~ischar(nmi) || numel(nmi)<2 || nmi(1)~='-' 
        disp('Provided arguments:');
        for k = 1 : nfields
            disp( [num2str(k) ':  ' varargin{k*2-1} ' = ' varargin{k*2}]);
        end;
        error(['Argument ' num2str(i) ' should be string starting with -']);
    end;
    nmi = nmi(2:end);
    if isfield(s,nmi)
        error('each argument should be defined only once');
    end;
    s.(nmi) = vali;
end;