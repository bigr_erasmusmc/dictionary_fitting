function run_job_$FUNCTION$( taskFullName )
% run_job_$FUNCTION$( funname, args )
%
% Script to run $FUNCTION$, created from a base function stored in run_job_base.m
% Edit the base function if any general modification needs to be made. 
% all other run_job_*.m are automatically created by make_run_job.m and can be 
% deleted.
%
% This function does:
% - if the the task output file does not exist:
%    - Load the task file
%    - Call $FUNCTION$ with the arguments provided in the task file
%    - Store the requested number of outputs in the task output file
% - Display some run time statistics. 
%
% 13-2-2018, D.Poot, Erasmus MC : created function 



% General lead-in
id = getenv('SGE_TASK_ID');
if isempty(id) 
    id = 1; 
    %error('Provide an id, either as input argument or through the environment variable SGE_TASK_ID');
end;
if ischar(id)
    id = str2double(id);
end;
pause(mod(id,150)*.02); % small pause to avoid crashing file systems and initialise RNG differently.
rng('shuffle'); % do initialise RNG after pause to initialise all jobs differently
progressbar('hasGUI','no');

% start the different timing measurements:
tic;
cpustarttime = cputime;
starttime = clock;

disp(sprintf('Job script started at:  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)',  floor( starttime([3 2 1 4 5 6]))));

[resultFullName] = ResultFullNameFromTaskFullName( taskFullName );
if exist( resultFullName, 'file')
    fprintf( 'Result file for current task already exist. Not computing again.  : %s\n',resultFullName);
else
    % load task file:
    ld = load( taskFullName );
    args = ld.args;
    taskPath = fileparts( taskFullName );
    for k = find( ld.arg_isstored )
        ldk = load( fullfile( taskPath , args{ k }));
        args{ k } = ldk.arg;
        clear ldk
    end;
    if ~isequal( ld.funname, '$FUNCTION$')
        error('Task file "%s" specifies function %s while this executable executes $FUNCTION$', taskFullName, ld.funname, '$FUNCTION$');
    end;
    %%% ACUTAL COMPUTATIONAL BLOCK. KEEP FUNCTIONALLY IN SYNC WITH clusterconfig.executionMode == 2 PART OF run_on_cluster
    errors = [];
    out = cell( 1, ld.numout);
    clear ld;
    try 
        functionstarttime = clock; 
        disp(sprintf('Calling $FUNCTION$ at :  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)',  floor( functionstarttime([3 2 1 4 5 6]))));
        [out{:}] = $FUNCTION$( args{:} );
        functionendtime = clock; 
        disp(sprintf('$FUNCTION$ finished at:  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)',  floor( functionendtime([3 2 1 4 5 6]))));
    catch errors
        functionendtime = clock; 
        disp(sprintf('$FUNCTION$ finished with error at:  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)',  floor( functionendtime([3 2 1 4 5 6]))));
    end;
    save( resultFullName, 'out' ,'errors', 'functionstarttime', 'functionendtime');
    %%% END OF ACUTAL COMPUTATIONAL BLOCK. 
end;
% delete task file when completed. In addition to saving space, this signals the task is ready. 
if exist( taskFullName ,'file')
    delete( taskFullName );
end;
% finish and display the resulting timing info:
endtime = clock;
tused = toc;
cpuendtime = cputime;
elapsedwalltime = etime( endtime, starttime);
disp(' ');
disp(['Time used tic-toc: ' num2str( tused) ' seconds.']);
disp(['CPU-time used    : ' num2str( cpuendtime-cpustarttime ) ' seconds (from ' num2str(cpustarttime) ' to ' num2str(cpuendtime) ')' ]);
disp(['Wall time used   : ' num2str( elapsedwalltime ) ' seconds'])
disp(sprintf('     from  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)',  floor( starttime([3 2 1 4 5 6]))));
disp(sprintf('       to  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)',  floor( endtime([  3 2 1 4 5 6]))));
