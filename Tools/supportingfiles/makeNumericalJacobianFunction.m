function funJ = makeNumericalJacobianFunction( fun, varargin )
% funJ = makeNumericalJacobianFunction( fun, opt )
% Creates a function handle that adds a numerically computed jacobian to the
% prediction function fun. 
% This to enable creating a fit_MRI or CramerRaoLowerBound_MRI function without
% having to analytically derive the jacobian. Note that for performance reasons
% it usually is preferred to compute the jacobian analytically. 
%
% INPUTS:
%   fun : function handle with signature:
%           [S] = fun( theta ), 
%              where
%                 size( theta ) = n x m1, and fun evaluates S for each of the m1  theta(:,j) separately.
%                 size( S )     = p x m1, real or complex valued predicted intensities. 
%           theta : n x m matrix with for each of the m voxels the parameters
%                   at which S and dS should be evaluted. 
% options:
%   absStep  : n x 1 vector with the step size for each parameter. 
%              Note that the step is at least this large; it might be larger (due to relStep)
%              nan values indicate no absolute step is provided (specify relStep and/or minAbsTheta for those parameters)
%   relStep  : n x 1 vector with relative step size; step = relStep * theta
%              nan values indicate default (1e-4 or  1e-6 for parameters for which absStep is provided)
%   minAbsTheta : n x 1 vector with minimum absolute value of each parameter that is used to compute step size. 
%              Can be used instead of absStep. If absStep is provided and not nan relStep has precedence over minAbsTheta. 
%              step = max( abs(theta), minAbsTheta ) * relStep  
%              default = 1
%
% OUTPUTS:
%    funJ : function handle with signature:
%           [S, dS] = funJ( theta ) 
%           where S = fun( theta)
%           and dS = [size(S) x n] numerically evaluated partial derivatives
%           of each S w.r.t. each element of theta. 
%
% 12-3-2018, D.Poot, Erasmus MC : Created 

opt = struct;
opt.absStep = [];
opt.relStep = [];
opt.minAbsTheta = [];
if nargin>1
    opt = parse_defaults_optionvaluepairs( opt, varargin{:} );
end;

n = max( [ numel( opt.absStep), numel( opt.relStep), numel( opt.minAbsTheta ) ] );

optFun = struct;
optFun.minAbsTheta = opt.minAbsTheta;
if isempty( optFun.minAbsTheta  )
    optFun.minAbsTheta = ones(n,1);
end;
optFun.relstep = nan(n,1);
if ~isempty( opt.relStep )
    optFun.relstep = opt.relStep;
end;
if ~isempty( opt.absStep )
    % We dont actually set a really absolute step size. 
    %   (To avoid rounding the step to zero for theta with excessive magnitude as well 
    %    as for simplifying implementation as it has only needs single step computation)
    % Instead we set minAbsTheta and relstep such that for 
    %   abs(theta)< minAbsTheta : steps = absStep
    % => minAbsTheta * relstep == absStep
    % => relstep is some arbitrary small value
    %    and minAbsTheta == absStep/relstep
    %   
    setAbsStep = isfinite( opt.absStep );
    optFun.relstep( setAbsStep & ~isfinite(optFun.relstep ) ) = 1e-6; % if relstep has not yet been set, set it to a small value. 
    optFun.minAbsTheta( setAbsStep ) = opt.absStep( setAbsStep ) ./optFun.relstep( setAbsStep );
end;
optFun.relstep( ~isfinite(optFun.relstep ) ) = 1e-4;
funJ = @(theta ) addNumericalJacobianCentDiff( fun , theta, optFun );

function [S, dS] = addNumericalJacobianCentDiff( fun , theta, opt )
% [S, dS] = addNumericalJacobianCentDiff( fun , theta, opt )
% central difference implemenation. 
if nargout==1
    % shortcut evaluation when the numerical derivative is not needed:
    S = fun( theta );
    return;
end;
szTheta = size( theta ) ;
if szTheta( 1 )~=numel(opt.relstep )
    error('theta has wrong number of rows');
end;
steps = bsxfun(@times, bsxfun(@max, abs(theta(:,:)), opt.minAbsTheta), opt.relstep );
thetaS = repmat( theta(:,:),[1 1 1+2*size(steps,1)]);
for parid = 1 : size( thetaS,1 )
    thetaS(parid , :, 2*parid   ) = thetaS(parid , :, 2*parid   ) - steps( parid, : );
    thetaS(parid , :, 2*parid+1 ) = thetaS(parid , :, 2*parid+1 ) + steps( parid, : );
end;
S_full = fun( thetaS);S_full = reshape( S_full, [size(S_full,1) size(thetaS,2) size(thetaS,3)]);
S = reshape( S_full(:,:,1) , [size(S_full,1) szTheta(2:end)]);
dS = bsxfun(@rdivide, (S_full(:,:,3:2:end)-S_full(:,:,2:2:end)), permute( 2*steps,[3 2 1])) ;


function test()
%%
TE = rand(10,1)*.1; % TE in seconds. 
fun = @(tht) predict_SET2( tht, TE); % function has analytical gradients, used here just to compare. 
thttst = [ 5 10 40 100 20  40;
           1 2  3   4  100 1000];
funJnum = makeNumericalJacobianFunction( fun, 'absStep',[.1;.1] );

tst = validateJacobian( funJnum, thttst );
[Sref, Jref] = fun( thttst );
tst
Jref