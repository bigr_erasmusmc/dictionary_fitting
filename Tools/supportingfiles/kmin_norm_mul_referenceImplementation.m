function [out, indx ] = kmin_norm_mul_referenceImplementation( A, B, k )
% see kmin_norm_mul_c
    Bp = permute(B,[2 1 3]);
    dotprod = reshape( A * Bp(:,:) , [size(A,1) size(B,1), size( B,3)]);
    sumnorm2 = sum( dotprod.*conj(dotprod) , 3) ;
    if nargin>=3 && ~isempty(k)
        if numel(k)~=1
            error('k should be scalar');
        end;
        % general version: 
        [ dst_sort, idx_sort ] = sort( sumnorm2 ,  2 , 'descend' ); % sort over initial vectors first indices have largest norm
        out = dst_sort(:,1:k)';
        indx = idx_sort(:,1:k)';
    else
        out = sumnorm2;
    end;
    end  