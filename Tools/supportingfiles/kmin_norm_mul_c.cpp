/* This is a MATLAB mex file that computes point distances. 
 * See help text in mexFunction for MATLAB help.
 *
 * Compile with:
 *	 mex kmin_norm_mul_c.cpp -v -I".\Tools\supportingfiles\" 
 * Or with OMP support:
 *   mex kmin_norm_mul_c.cpp -v -I".\Tools\supportingfiles\" CXXFLAGS="\$CXXFLAGS -fopenmp -mssse3 -mtune=core2" LDFLAGS="\$LDFLAGS -fopenmp"
 *
 * On Linux:
 *   mex kmin_norm_mul_c.cpp -v -I"./Tools/supportingfiles/" -DMEX
 *   mex kmin_norm_mul_c.cpp -v -I"./Tools/supportingfiles/" CXXFLAGS="\$CXXFLAGS -fopenmp -mssse3 -mtune=core2" LDFLAGS="\$LDFLAGS -fopenmp"
 *
 * optional arguments : -v (verbose) , -g (debug symbols) (add -O for optimizations while debugging)
 * NOTE: this function is only efficient on machines with more than 8 XMM registers, so 64-bit x86; NOT on 32-bit x86.
 *
 * NOTE: this function is derived from point_dist_nn_c.cpp; but with improved variable naming and code comments. 
 * Created by Dirk Poot, Erasmus MC, 11-3-2017
 * */

#include "mex.h"
#include <algorithm>
#include <limits>
#include <iterator>
#include "emm_vecptr.hxx"
#include "step_iterators.cpp" // for complex_pointer

//#define USEOMP
#ifdef _OPENMP
#define USEOMP
#endif

#ifdef USEOMP
#pragma message("Compiling with OMP support.")
#include <omp.h>
#else
#pragma message("Compiling without OMP support.")
#endif

#ifndef SUGGESTVLEN
  #define SUGGESTVLEN 4
#endif

struct matIndex{
    int i,j;
    matIndex(int i_, int j_): i(i_), j(j_) {};
};

// Some helper templates:
// remove_complex<T > : iff T is a 'complex' type returns the base type that is made complex. 
template < typename T > class remove_complex {
public:
  typedef T type;
};
template < typename T > class remove_complex< complex< T > > {
public:
  typedef T type;
};

// norm< T > : compute the norm squared; differentiated for real and complex.
template < typename T> class norm{
public: inline static T squared( T value ) {
  return value * value;  
}};
template < typename T> class norm< complex<T> > {
public: inline static T squared( complex<T> value ) {
  T tmpR =  value.real() ; 
  T tmpI =  value.imag() ; 
  return tmpR * tmpR + tmpI * tmpI ;  
}};

// zero< T> : return zero of any class.
template < typename T > struct zero {
    static inline T get() {
        return T::zero();
    }
};
template < > struct zero< double > {
    static inline double get() {
        return 0.;
    }
};
template < > struct zero< float > {
    static inline float get() {
        return 0.;
    }
};
template < typename T > struct zero< complex< T > > {
    static inline complex<T> get() {
        T z = zero<T>::get();
        return complex<T>( z, z );
    }
};

// repeat: helper class to repeat elements of a vec (even when complex)
template < typename T > struct repeat { 
    static inline T index( T value, const int i ) {
        return value.rep( i ) ;
    }
};
template < typename T > struct repeat< complex< T > > { 
    typedef complex< T > Tc;
    static inline Tc index( Tc value, const int i ) {
        return Tc( (value.real() ).rep( i ) , ( value.imag() ).rep( i ) );
    }
};


//simplemat< Tptr > : simple matrix type implemented over an pointer like iterator type. 
// Only allows addition (shift of origin) and (indexed) assignment. 
template< typename base > class simplematStoreHelper {
public:
  typedef typename base::value_type value_type;
  typedef typename base::array_type ptrT;
protected:
   ptrT valuePtr; 
public:
  simplematStoreHelper( ptrT valuePtr_) : valuePtr(valuePtr_) {};  
  inline void operator=(value_type newval) {
    *valuePtr = newval;
  }
  template<int vlen> inline void operator=( vec<value_type, vlen > v ) {
      v.store( &valuePtr[0] );
  }
};

using std::iterator_traits;

template <typename Tptr>  class simplemat {
	Tptr ptr;
	int stride;
public: 
    typedef simplemat<Tptr> Self;
    typedef typename iterator_traits<Tptr>::value_type value_type;
    typedef Tptr array_type;
    typedef simplematStoreHelper<Self>  reference;
    
    simplemat( Tptr ptr_, int stride_): ptr(ptr_), stride(stride_){};
    inline reference operator()(int a, int b) {
        return reference( ptr+( a + b * stride ) ) ;
    }
    inline Self operator+( matIndex step ) {
        return Self( ptr + (step.i + step.j *stride) , stride );
    }
};


/* sortMatStoreHelper 
 * class that is returned when referencing an element of simpleSortMat
 * Only allows assinging. 
 * Upon assignment the provided value is sorted and the index stored. 
 * */
template< typename base > class sortMatStoreHelper {
public:
  typedef typename base::value_type value_type;
  typedef typename base::array_type ptrT;
protected:
   ptrT valuePtr; 
   int * idxPtr;
   int stride;
   int curj;        
public:
  sortMatStoreHelper( ptrT valuePtr_, int * idxPtr_, int stride_, int curj_) : valuePtr(valuePtr_), idxPtr(idxPtr_), stride(stride_) , curj(curj_) {};  
  inline void operator=(value_type newval) {
    int idx = stride-1;
	if (valuePtr[idx] < newval) {
		// insertion sort new value
		while ( (idx>0) && (valuePtr[idx-1]<newval) ) {
			// if new distance is less than idx-1, move element idx-1 one up and continue searching downwards.
			valuePtr[idx] = valuePtr[idx-1];
			idxPtr[idx] = idxPtr[idx-1];
			idx--;
		}
		valuePtr[idx] = newval;
		idxPtr[idx] = curj;
	}
  }
  template<int vlen> inline void operator=( vec<value_type, vlen > v ) {
    value_type tempstore[vlen];
	v.store(&tempstore[0]);
	ptrT curResVec = valuePtr;
    int * curResIdx = idxPtr;
	for (int s=0; s<vlen; s++ ){
		int idx = stride-1;
		value_type newdist = tempstore[s];
		if (curResVec[idx]<newdist) {
			// insertion sort new value
			while ( (idx>0) && (curResVec[idx-1]<newdist) ) {
				// if new distance is less than idx-1, move element idx-1 one up and continue searching downwards.
				curResVec[idx] = curResVec[idx-1];
				curResIdx[idx] = curResIdx[idx-1];
				idx--;
			}
			curResVec[idx] = newdist;
			curResIdx[idx] = curj ;
		}
		curResVec += stride;
        curResIdx += stride;
	}
  }

};
/* simpleSortMat
 * class that implements sorting and selecting while assigning values. 
 * Only allows assinging and sorts incomming values as provided. 
 * The assignment order is arbitrary, but you should assign each element exactly once. (or you get duplicates in the output)
 * In MATLAB notation this does:
 * for all i and j (in aribitrary order):
 *   a(i,j) = v_ij
 * b = sort(a, 2);
 * out = b(:, 1: stride) ;
 * */
template <typename Tptr>  class simpleSortMat {
	Tptr  valuePtr;
	int * idxPtr;
	int stride;
	int idxoffset;
public:
    typedef simpleSortMat<Tptr> Self;
    typedef typename iterator_traits< Tptr >::value_type value_type;
    typedef Tptr array_type;
    typedef sortMatStoreHelper<Self>  reference;
    simpleSortMat( Tptr valuePtr_, int * idxPtr_, int stride_, int idxoffset_): valuePtr( valuePtr_), idxPtr(idxPtr_), stride(stride_), idxoffset(idxoffset_) {};
    inline reference operator()(int i, int j) {
        return reference( valuePtr +        i * stride , idxPtr +      i * stride, stride, idxoffset +      j );
    }
    inline Self operator+( matIndex step ) {
        return Self( valuePtr + step.i * stride , idxPtr + step.i * stride, stride, idxoffset + step.j );
    }
    
};


/* mul_norm_core
 * template class that performs the low level operation between A and B
 * Performs:
 *   tmp = A(i,:) * permute( B(j,:,:),[2 3 1]);    % tmp is row vector. 
 *   out(i,j) = tmp*tmp';
 *
 * A and B are provided as pointer types. 
 * A( i, k ) = Ain + i + strideA * k
 * lenA * vlen = size(A,1) (or lower when only part of A is processed)
 * B( j, k , l ) = Bin + j + strideB * (k + ndims * l )
 * lenB * vlen = size(B,1) (or lower when only part of B is processed)
 * ndims == size(A,2)==size(B,2)
 * ncolB == size(B,3)
 *
 * It performs the computation in vlen x vlen size blocks (in i and j)
 * Keeping all vlen x vlen x 2 intermediate values during the loop over k and l
 * in registers (at least, that is assumed and required for optimal performance. )
 *
 * Use by 'vectorizing' A and B with vecTptr for vlen > 1
 * (use vecTptr instead of vecptr as stride might not be an integer multiple of vlen)
 * for vlen==1 : ptrTypeA should be 'const double*' (or similar single, complex,...)
 * for vlen>1  : ptrTypeA should be 'vecTptr<const double*, vlen>' (or similar single, complex,...)
 *
 * The loop over A is inside the loop over B, so split large A in blocks to 
 * keep that block of A in cache during the loop over B. 
 **/
template <int vlen, typename ptrTypeA, typename ptrTypeB, typename mattypeS > class mul_norm_core;

template <typename ptrTypeA, typename ptrTypeB, typename mattypeS> class mul_norm_core<1, ptrTypeA, ptrTypeB, mattypeS> {
public: static void mul(ptrTypeA Ain,  ptrTypeB Bin, mattypeS out, int lenA, int lenB, int ndims, int ncolB, int strideA, int strideB) 
{
	const int vlen = 1;
    typedef typename iterator_traits< ptrTypeA >::value_type  T;
    //typedef typename remove_complex< T >::type Tnorm;
    typedef typename mattypeS::value_type Tnorm;
    
    ptrTypeB Bk = Bin;
    for (int kb = 0; kb < lenB; kb++, Bk += vlen ) {
        ptrTypeA Ak = Ain;
        
		for (int ka = 0; ka < lenA; ka++, Ak += vlen) {
			Tnorm nrmtmp0 = zero< Tnorm >::get();
            ptrTypeB Bkc = Bk;

            for (int col = 0 ; col < ncolB ; col++ ) {
                T dottmp0 = zero< Tnorm >::get();
                ptrTypeA Akc = Ak;
                
                for (int i=0 ; i<ndims ; i++, Akc += strideA, Bkc += strideB ) {
                    T ldA = *Akc;
                    T ldB = *Bkc;
                    dottmp0 += ldA * ldB;
                    // NOTE: dottmp0 += (*Akc) * (*Bkc); does not compile to the same and produces wrong output for complex T (using visual studio . 
                };
                nrmtmp0 += norm<T>::squared( (dottmp0) );
            }
            out( ka, kb ) = nrmtmp0;
		};
	};
} // end of mul
}; // End of class

template <typename ptrTypeA, typename ptrTypeB, typename mattypeS> class mul_norm_core<4, ptrTypeA, ptrTypeB, mattypeS> {
public: static void mul(ptrTypeA Ain, ptrTypeB Bin, mattypeS out, int lenA, int lenB, int ndims, int ncolB, int strideA, int strideB) 
{
	const int vlen = 4;
    typedef typename iterator_traits< ptrTypeA >::value_type  T;
    //typedef typename remove_complex< T >::type Tnorm;
    typedef vec<typename mattypeS::value_type, vlen> Tnorm;
    
    ptrTypeB Bk = Bin;
    for (int kb = 0; kb < lenB; kb++, Bk += vlen ) {
        ptrTypeA Ak = Ain;
        
		for (int ka = 0; ka < lenA; ka++, Ak += vlen) {
            Tnorm nrmtmp0 = Tnorm::zero();
            Tnorm nrmtmp1 = Tnorm::zero();
            Tnorm nrmtmp2 = Tnorm::zero();
            Tnorm nrmtmp3 = Tnorm::zero();
            ptrTypeB Bkc = Bk;

            for (int col = 0 ; col < ncolB ; col++ ) {
                
                T dottmp0 = zero< T >::get();
                T dottmp1 = zero< T >::get();
                T dottmp2 = zero< T >::get();
                T dottmp3 = zero< T >::get();
                
                ptrTypeA Akc = Ak;

                for (int i=0 ; i<ndims ; i++, Akc += strideA, Bkc += strideB) {
                    T ldA = *Akc;
                    T ldB = *Bkc;
                    dottmp0 += ldA * repeat< T >::index(ldB, 0) ;
                    dottmp1 += ldA * repeat< T >::index(ldB, 1) ;
                    dottmp2 += ldA * repeat< T >::index(ldB, 2) ;
                    dottmp3 += ldA * repeat< T >::index(ldB, 3) ;
                };
                nrmtmp0 += norm<T>::squared( dottmp0 );
                nrmtmp1 += norm<T>::squared( dottmp1 );
                nrmtmp2 += norm<T>::squared( dottmp2 );
                nrmtmp3 += norm<T>::squared( dottmp3 );
            }
			out( ka * vlen, kb * vlen + 0 ) = nrmtmp0;
			out( ka * vlen, kb * vlen + 1 ) = nrmtmp1;
			out( ka * vlen, kb * vlen + 2 ) = nrmtmp2;
			out( ka * vlen, kb * vlen + 3 ) = nrmtmp3;
		};
	};
} // End of mul
};// End of class


// mul_norm_vectorize_helper : helper class to 'vec'torize a pointer type
//                            ( returns vec< Tptr:value_type, vlen> elements upon dereferencing)
// 'penetrates' 'complex', so that complex< double* > is vectorized into complex< vecTptr< double*, vlen > >
// allowing the use of normal complex logic. 
template < typename Tptr , int vlen > struct mul_norm_vectorize_helper { 
    typedef vecTptr< Tptr, vlen > RET;
   /* static inline RET vectorize( T val ) {
        return RET( val );
    }*/
};
template < typename Tptr , int vlen > struct mul_norm_vectorize_helper< complex_pointer< Tptr > , vlen > { 
    typedef complex_pointer< Tptr > pointer_type_in;
    typedef vecTptr< Tptr, vlen > pointer_type_out;
    typedef complex_pointer< pointer_type_out > RET;
/*    static inline RET vectorize( type_in val ) {
        return RET( val );
    }    */
};

/* mul_norm
 * template class that performs the operation between A and B
 * Performs:
 *   tmp = A(i,:) * permute( B(j,:,:),[2 3 1]);    % tmp is row vector. 
 *   out(i,j) = tmp*tmp';
 *
 * A and B are provided as pointer types. 
 * A( i, k ) = Ain + i + strideA * k
 * lenA * vlen = size(A,1) (or lower when only part of A is processed)
 * B( j, k , l ) = Bin + j + strideB * (k + ndims * l )
 * lenB * vlen = size(B,1) (or lower when only part of B is processed)
 * ndims == size(A,2)==size(B,2)
 * ncolB == size(B,3)
 *
 * It performs the main part of the computation in vlen x vlen size blocks (in i and j)
 * Keeping all vlen x vlen x 2 intermediate values during the loop over k and l
 * in registers (at least, that is assumed and required for optimal performance. )
 *
 * Done by 'vectorizing' A and B with vecTptr for vlen > 1
 * (use vecTptr instead of vecptr as stride might not be an integer multiple of vlen)
 * for vlen==1 : ptrTypeA should be 'const double*' (or similar single, complex,...)
 * for vlen>1  : ptrTypeA should be 'vecTptr<const double*, vlen>' (or similar single, complex,...)
 *
 * Tptr: a pointer type ('const double *')
 **/
template <typename Tptr, int vlen, typename typeOut> void mul_norm(Tptr Ain, Tptr Bin, typeOut out, int strideA, int strideB, int lenA, int lenB, int ndims, int ncolB) 
{   /* This interface routine computes the full result for any matrix size by 
     * delegating the computations to the core routines. Using the 4x4 core for as much as possible. 
     * Also delegates over multiple threads if A is long. 
     * */
    //typedef vecTptr< Tptr, vlen> vTptr;
    typedef typename IF<vlen==1, Tptr, mul_norm_vectorize_helper< Tptr, vlen > >::RET  vTptr;
	const int maxBlk = 512; // vlen should divide maxBlk.
    
#ifdef USEOMP
    int olddynamic =  omp_get_dynamic();
    omp_set_dynamic(1);
    omp_set_num_threads(omp_get_num_procs());	
	int maxthreadsuse = omp_get_max_threads();
#pragma omp parallel for default(none) shared(a,b,c,strideA,strideB,lenA,lenB,ndims) //private()        
#endif
    // loop over blocks of A: (parallel process these blocks)
	for ( int ka = 0; ka < lenA ; ka += maxBlk ) {
		int nInBlockA = ( lenA-ka < maxBlk ? lenA-ka : maxBlk );
		int nVecInBlockA = nInBlockA/vlen;
        vTptr vA( Ain + ka );
		//for ( int kb =0; kb<lenB ; kb+=maxBlk ) 
		int kb =0; // do all of B at once (inner loop in core is over A). 
		{
			int nInBlockB = lenB - kb;//( lenB-kb<maxBlk ? lenB-kb : maxBlk );
			int nVecInBlockB = nInBlockB/vlen;
            if (vlen>1) {
                vTptr vB( Bin + kb );
                typeOut out_part = out + matIndex( ka, kb );
                mul_norm_core<vlen, vTptr, vTptr, typeOut>::mul( vA, vB, out_part, nVecInBlockA, nVecInBlockB, ndims, ncolB, strideA, strideB);
            } else {
                nVecInBlockB = 0; 
            }
			if ( nVecInBlockB * vlen < nInBlockB ) {
				// wrap up final part of B:
				Tptr Ablk = Ain + ka;
                Tptr Bblk = Bin + kb + nVecInBlockB * vlen;
                typeOut out_rempart = out + matIndex( ka, kb + nVecInBlockB * vlen );
                int nRemInBlockB = nInBlockB - nVecInBlockB*vlen;
                mul_norm_core<1, Tptr , Tptr, typeOut>::mul( Ablk, Bblk, out_rempart, nVecInBlockA*vlen, nRemInBlockB , ndims, ncolB, strideA, strideB);
			}
		}
		if ( nVecInBlockA * vlen < nInBlockA ) {
			// wrap up final part of A:
            
            Tptr Ablk = Ain + ka + nVecInBlockA * vlen;
            Tptr Bblk = Bin ;
            typeOut out_rempart = out + matIndex( ka + nVecInBlockA * vlen, kb );
			mul_norm_core<1, Tptr , Tptr, typeOut>::mul(Ablk, Bblk, out_rempart, nInBlockA - nVecInBlockA*vlen, lenB , ndims, ncolB, strideA, strideB);
		}
	}
#ifdef USEOMP
	omp_set_dynamic(olddynamic);     
#endif
}


template <typename T, int vlenR, int vlenC> void mul_norm_delegator(const T * A, const T * Ai, const T * B, const T * Bi, T * y, int * indx, int lenA, int lenB, int ndims, int ncolB, bool computeKnn, int numNeighbors, bool isComplexAB) {
    using namespace std;
    if ( computeKnn ) {
        for (int k=0;k<numNeighbors* lenA;k++) {
            y[k]= -numeric_limits<T>::infinity( );
        }
        typedef simpleSortMat< T * > typeOut;
        typeOut out( y, indx, numNeighbors, 1);
        if (isComplexAB) {
            // complex
            typedef complex_pointer< const T * > typeInPtr;
            mul_norm<typeInPtr, vlenC, typeOut>( typeInPtr( A, Ai), typeInPtr( B, Bi), out, lenA, lenB, lenA, lenB, ndims, ncolB);
        } else {
            // non complex
            typedef const T * typeInPtr;
            mul_norm<typeInPtr, vlenR, typeOut>( (typeInPtr) A, (typeInPtr) B, out, lenA, lenB, lenA, lenB, ndims, ncolB);
        }
	} else {
        typedef simplemat< T * > typeOut;
        typeOut out( (T *) y, lenA ) ;
        if (isComplexAB) {
            typedef complex_pointer< const T * > typeInPtr;
            mul_norm<typeInPtr, vlenC, typeOut>( typeInPtr( A, Ai), typeInPtr( B, Bi), out, lenA, lenB, lenA, lenB, ndims, ncolB);
        } else {
            typedef const T * typeInPtr;
            mul_norm<typeInPtr, vlenR, typeOut>( (typeInPtr)  A, (typeInPtr) B, out, lenA, lenB, lenA, lenB, ndims, ncolB);
        }
	}
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
/* out [,ind] = kmin_norm_mul_c(A, B [,k])
 * see help kmin_norm_mul_c.m 
*/
	
    const mxArray *Ain, *Bin, *kin ;
    const double *A,*B, *Ai, *Bi;
    double  *y;
    const mwSize *dims_B;
    
	int lenA, lenB, ndims, ncolB ;
	bool computeKnn = (nrhs==3);
	/* Check for proper number of arguments. */
	if ( ((nrhs!=2)&(nrhs!=3)) || ( ((nlhs!=1)&& !computeKnn) || ( (nlhs!=2) && computeKnn )) ) {
        // If adjusting text below; also update help text in: kmin_norm_mul_c.m 
		mexErrMsgTxt("Two or Three inputs required: [out ind] = kmin_norm_mul_c(A, B [,k]). \n" 
                     " A, B = points : npntA x ndim ; npntB x ndim x ncol\n"
                     " k    = number of closest neighbors : scalar integer\n"
                     " dst(i,j) =  sum(  abs( A(i, :) * permute( B(j, :, :) ,[2 3 1]) ).^2 );\n"
                     " if k is provided: \n"
                     "     [dst_sort ,index]  = sort( dst , 2 , 'descend')\n"
                     "     dst_out = dst_sort(:, 1:k )';\n"
                     "     index_out = index( :, 1:k )';\n"
                     "     -> Two outputs are required (dst_out, index_out).\n"
                     " else\n"
                     "     -> One output is required (dst).\n\n"
                     " ****  Created by Dirk Poot, Erasmus MC, 11-3-2017  ****");
	}

	/* parse inputs */
    Ain = prhs[0];
    Bin = prhs[1];
    if (computeKnn ) {
        kin = prhs[2];
    }
    
    /* check inputs and get pointers: */
	/* // A: */ 
	mxClassID pointclass = mxGetClassID( Ain );
    bool isComplexAB = mxIsComplex( Ain );
	if ((mxGetNumberOfDimensions( Ain )!=2) | !((pointclass==mxDOUBLE_CLASS) | (pointclass==mxSINGLE_CLASS)) ) { 
		mexErrMsgTxt("incorrect Input A");
	}
	ndims  = mxGetN( Ain );
	lenA   = mxGetM( Ain );
	A = mxGetPr( Ain );

    /* // B: */ 
	if (((mxGetNumberOfDimensions( Bin )!=2)& (mxGetNumberOfDimensions( Bin )!=3)) | (mxGetClassID( Bin )!=pointclass) | (mxIsComplex( Bin )!=isComplexAB) ) { 
		mexErrMsgTxt("incorrect Input B; required: size(A,2)==size(B,2), iscomplex(A)==iscomplex(B), class A == class B");
	}
    dims_B = mxGetDimensions( Bin );
    if (ndims!=dims_B[1] ) {
        mexErrMsgTxt("incorrect Input B");
    }
	lenB  = dims_B[ 0 ];
    if (mxGetNumberOfDimensions( Bin ) == 2 ) {
        ncolB = 1;
    } else {
        ncolB = dims_B[ 2 ];
    }
	B = mxGetPr( Bin );
	if (isComplexAB) {
        Ai = mxGetPi( Ain );
        Bi = mxGetPi( Bin );
    }
        
    /* // get k and create outputs: */ 
	int numNeighbors = 0;
	int * indx = NULL;
	if ( computeKnn )  {
		if ( mxGetNumberOfElements( kin ) !=1 ) {
			mexErrMsgTxt("k should be scalar");
		}
		numNeighbors = mxGetScalar( kin );
		plhs[0] = mxCreateNumericMatrix(numNeighbors, lenA, pointclass,  mxREAL);
		plhs[1] = mxCreateNumericMatrix(numNeighbors, lenA, mxINT32_CLASS,  mxREAL);
		indx = (int*) mxGetPr( plhs[1] );
	} else {
		plhs[0] = mxCreateNumericMatrix(lenA, lenB, pointclass,  mxREAL);
	}

    /* do actual computations : */
	y = mxGetPr( plhs[0] );
    if (pointclass==mxDOUBLE_CLASS) {
        typedef double T; 
        mul_norm_delegator<T,SUGGESTVLEN,SUGGESTVLEN>( (const T *) A, (const T *) Ai, (const T *) B, (const T *) Bi, (T *) y, indx, lenA, lenB, ndims, ncolB, computeKnn, numNeighbors, isComplexAB);
    } else {
        typedef float T; 
        mul_norm_delegator<T,SUGGESTVLEN,SUGGESTVLEN>( (const T *) A, (const T *) Ai, (const T *) B, (const T *) Bi, (T *) y, indx, lenA, lenB, ndims, ncolB, computeKnn, numNeighbors, isComplexAB);
    }
    /*
	if ( computeKnn ) {
		using namespace std;
		if (pointclass==mxDOUBLE_CLASS) {
			for (int k=0;k<numNeighbors* lenA;k++) {
				y[k]= -numeric_limits<double>::infinity( );
			}
            typedef simpleSortMat< double * > typeOut;
            typeOut out( y, indx, numNeighbors, 1);
            if (isComplexAB) {
                // complex
                typedef complex_pointer< const double * > typeInPtr;
                mul_norm<typeInPtr, 4, typeOut>( typeInPtr( A, Ai), typeInPtr( B, Bi), out, lenA, lenB, lenA, lenB, ndims, ncolB);
            } else {
                // non complex
                typedef const double * typeInPtr;
                mul_norm<typeInPtr, 4, typeOut>((typeInPtr) A, (typeInPtr) B, out, lenA, lenB, lenA, lenB, ndims, ncolB);
            }
		} else {
			float * ys = (float *) y;
			for (int k=0;k<numNeighbors* lenA;k++) {
				ys[k]=-numeric_limits<float>::infinity( );
			}
            typedef simpleSortMat< float * > typeOut;
            typedef const float * typeInPtr;
            typeOut out( ys, indx, numNeighbors, 1);
			mul_norm<typeInPtr, 4, typeOut>((typeInPtr) A, (typeInPtr) B, out, lenA, lenB, lenA, lenB, ndims, ncolB);
		}
	} else {
		if (pointclass==mxDOUBLE_CLASS) {
            typedef simplemat< double * > typeOut;
            typeOut out( (double *) y, lenA ) ;
            if (isComplexAB) {
                typedef complex_pointer< const double * > typeInPtr;
                mul_norm<typeInPtr, 4, typeOut>( typeInPtr( A, Ai), typeInPtr( B, Bi), out, lenA, lenB, lenA, lenB, ndims, ncolB);
            } else {
                typedef const double * typeInPtr;
                mul_norm<typeInPtr, 4, typeOut>(            A,             B, out, lenA, lenB, lenA, lenB, ndims, ncolB);
            }
		} else {
            typedef simplemat< float * > typeOut;
            typedef const float * typeInPtr;
            typeOut out( (float *) y, lenA ) ;
			mul_norm<typeInPtr, 4, typeOut>((typeInPtr) A, (typeInPtr) B, out, lenA, lenB, lenA, lenB, ndims, ncolB);
		}
	}*/
}

