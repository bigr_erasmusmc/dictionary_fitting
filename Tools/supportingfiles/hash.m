function h = hash( val )
% h = hash( val )
% Hash returns a MD5 hash for a given MATLAB object val.
% The hash h is very likely to be different for different objects. 
% I.e. it should be exceedingly hard to construct two different objects that have 
% the same hash.
%
% INPUTS:
%  val : any MATLAB object (though currently classes are not supported)
%
% OUTPUT:
%  h   : 32 long string with MD5 has of val; 
% 
% Based on rptgen, hash by Mathworks
% 13-2-2018, D. Poot, Erasmus MC: created initial version 


if isempty(val)
    h = '';
    return
end


% Create hash using Java Security Message Digest Object
mdobj = java.security.MessageDigest.getInstance('MD5');

addval(val);

% Convert into uint8 format
h = typecast(mdobj.digest, 'uint8');

% Convert into HEX format
h = dec2hex(h)';
h = lower(h(:)');

    function addval( val )
% addval( val)
% adds the content of val to the hash stream. 
% NOTE TO PROGRAMMERS:
%  As existing hashes are based on this code. Any update of the code 
%  should not modify the hash of objects for which a valid hash was already computed. 
%  (unless serious bugs are found that compromise the integrity of the hash)
        addtypesize( val )
        if isnumeric(val) 
            addnumeric( val );
        elseif islogical( val )
            addnumeric( uint8( val ) );
        elseif iscell( val )
            for k=1:numel(val)
                addval( val{k} );
            end;
        elseif isstruct( val )
            f = fieldnames( val );
            f = sprintf( '%s;', f{:} );  % add separator that is invalid in fieldname to avoid creating clashes. 
            f(end) = '`'; % Identifier for end of fieldnames is not really needed but added as security
            mdobj.update(typecast(uint16(f),'uint8'));
            addval( struct2cell( val ));
        elseif ischar( val )
            addnumeric( uint16( val ) );
        else
%             % custom class. Only if all properties are public readable we can compute a (reliable) hash:
%             meta = metaclass( val );
%             f = {meta.PropertyList.Name}; 
%             f = sprintf( '%s;', f{:} ); 
%             f(end) = '`';
%             mdobj.update(typecast(uint16(f),'uint8'));
%             for k = 1 : numel(
%             error( 'Currently unsuported type for the hash function.');
            % custom class. Cast to struct and hash that. 
            % Turn MATLAB:structOnObject warning off (temporarily)
            warnstate = warning('off', 'MATLAB:structOnObject');
            val = struct( val ); % error if not convertable to struct. 
            warning( warnstate )
            addval( val );
        end;
    end
    function addnumeric( val )
        if isempty(val)
            return;
        end;
        if ~isreal( val )
            addnumeric( real( val ) );
            val = imag( val );
        end;
        val = typecast( val(:) ,'uint8');
        if numel(val)<1e7
            mdobj.update(val(:));
        else
            f = factor(numel(val));
            p = cumprod( f(end:-1:1));
            colsz = p( max(1,nnz(p<1e7)));
            val = reshape(val, colsz,[]);
            for col = 1 : size(val,2);
                mdobj.update(val(:,col));
            end;
        end;
    end
    function addtypesize( val )
         t = [class(val) iscomplex_str(val) sprintf('%dx',size(val)) '.']; % add '.' as marker of end of size. 
         mdobj.update( uint8( t ) );
    end
    function str = iscomplex_str( val )
        if isreal( val )
            str = '' ;
        else
            str = '_cplx';
        end;
    end
end