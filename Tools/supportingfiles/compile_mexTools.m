% function compile_mexTools()
% This function compiles all mex routines in the Tools of D.Poot. 
%
% Just call 
%   compile_mexTools
% when it is on your path. 
% NOTE: compilation is needed only once on each system and in most cases the 
% binaries are pre-compiled. 
%
% 22-6-2017: Created by D.Poot

oldpath = pwd;
topath = fileparts(which('dot_c.cpp'))
if isempty(topath)
    error('the cpp files of the Tools folder of D.Poot are not found on path; add them to the path or CD to /Tools/supportingfiles/');
end;
if ispc
    systemspecificargs = {'-v'};
else
    systemspecificargs = {'-v','-DMEX', 'CXXFLAGS="\$CXXFLAGS -mssse3 -mtune=core2"'};
end;
if 0 
    openMPargs = {'CXXFLAGS="\$CXXFLAGS -fopenmp -mssse3 -mtune=core2"','LDFLAGS="\$LDFLAGS -fopenmp"'};
else
    openMPargs = {};
end;
%%
cd(topath)
disp(['Starting compilation : ' datestr( now) ])
if ismac
    mex('kmin_norm_mul_c.cpp',  systemspecificargs{:},'-DSUGGESTVLEN=1')
else
    mex('kmin_norm_mul_c.cpp',  systemspecificargs{:})
end;
%%
mex('accurate_dotprod_c.cpp', systemspecificargs{:} );

if verLessThan('MATLAB','8.2')
    warning('A bug with the compiler under MATLAB 2.11b creates wrong code for the adjoint of bspline_interpolate_regular. Compile under a newer version of MATLAB');
end;
if ispc
    mex('bspline_interpolate_regular.cpp','-D__SSE4__', systemspecificargs{:});
else
    mex('bspline_interpolate_regular.cpp',systemspecificargs{:});
end
%%
mex('compactedHessianMultiply_c.cpp',systemspecificargs{:}) 

if ispc
    % performance routine. 
    mex('dot_c.cpp','-DINCLUDE_SSE3',systemspecificargs{:})
    dotpath = fullfile(topath,'@double');
    if ~exist(dotpath ,'dir')
        mkdir(dotpath);
    end;
    copyfile( fullfile(topath,['dot_c.' mexext]), fullfile( dotpath ,['dot.' mexext]) );
    dotpath = fullfile(topath,'@single');
    if ~exist(dotpath ,'dir')
        mkdir(dotpath);
    end;
    movefile( fullfile(topath,['dot_c.' mexext]), fullfile( dotpath ,['dot.' mexext]) );
end;
mex('gradientMulND_c.cpp',systemspecificargs{:})

mex('laplaceMulND_c.cpp',systemspecificargs{:});
pause(1); 
mex('laplaceMulND_v2.cpp',systemspecificargs{:});

if ispc
    mex('MI_ND_dirk.cpp');
    mex('rice_besseli_funs_c.cpp',systemspecificargs{:}); % compiles with gcc, but maybe wrong (warnings)
end;

pause(1); 
mex('separableConvN_c.cpp',systemspecificargs{:})
pause(1);
mex('TotalVariation_Vector_c.cpp','-largeArrayDims');
mex('TotalVariation_Vector_fc.cpp','-largeArrayDims');


disp(['Finished compilation : ' datestr( now) ])
cd(oldpath)