function [P,dP,hP] = lognormpdf_logsigma( x, A, logsigma, selGrHess)
% [P,dP,hP] = lognormpdf_logsigma( x, A, logsigma, selGrHess)
% Same as lognorm pdf function (which this function calls)
% but with a different parameterisation of sigma.
%
% lognormpdf_logsigma( x, a , log(sigma) ) == lognormpdf( x , a, sigma)
%
% Using the logarithm of sigma makes regularisation better. 
% (relative changes in sigma should be penalised).
%
% Use in fit_MRI by specifying options:
%       'logPDFfun', @lognormpdf_logsigma
%       'imageType','custom'
%
% Created by Dirk Poot, Erasmus MC, 6-12-2012

sigma = exp(logsigma);
if nargout==1
    [P] = lognormpdf(x, A, sigma, selGrHess);
elseif nargout>=2
    if nargout>=3
        [P,dP,hP] = lognormpdf(x, A, sigma, selGrHess);
    else
        [P,dP] = lognormpdf(x, A, sigma, selGrHess);
    end;
    % d/dx f( exp( x) ) = exp(x) *( (d/da f(a)) | a=exp(x))
    if selGrHess(3)
        repf = size(P)./size(sigma);
        if all(repf==1)
            sigma = sigma(:);
        else
            sigma = reshape( repmat(sigma, repf) ,[],1);
        end;
        dP = reshape(dP, [], nnz(selGrHess));
        dP(:,end) = dP(:,end) .* sigma;
        if nargout>=3
            % hessian terms w.r.t. sigma are all grouped last
            szhP = size(hP);
            hP = reshape(hP,[],szhP(end));
            hP(:,end-nnz(selGrHess)+1:end) = bsxfun(@times, hP(:,end-nnz(selGrHess)+1:end) , sigma);
            hP(:,end) = hP(:,end) .* sigma + dP(:,end);

        end;   
    end;
end;