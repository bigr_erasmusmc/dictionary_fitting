function [dictionary , boundary_condition_int]= prefilter_spline( dictionary, bsplineorder, boundary_condition, dimensions )
% prefilteredDictionary = prefilter_spline( dictionary, bsplineorder, boundary_condition, dimensions )
%
% This function performs prefiltering required for subsequent b-spline interpolation
% with order 'bsplineorder' in the dimensions 'dimensions'. 
%
% INPUTS:
%  dictionary : ND array that should be prefiltered. (image or dictionary,...)
%  bsplineorder : scalar integer [0..3] specifying the bspline order with which 
%                 subsequent interpolation will be performed (by TransformNDbspline). 
%                 Note: 1 = linear interpolation (and that does not require pre-filtering)  
%  boundary_condition : Corresponds to boundary_condition in TransformNDbspline:
%                           if 0 : zero extension interpolation is assumed. 
%                           if 1 : mirror boundary condition. 
%                           if 2 : periodic domain boundary condition. 
%                       Plus a value that specifies an prefilter option: 
%                           0 (0-2) : interpolate at all points
%                           4 (4-6) : 'clamped boundary conditions' 
%                                     Set gradient of interpolant at point 2 and N-1 to numerical 
%                                     derivative between point (3 and 1)  and (N-2, N). 
%                           8 (8-10): Set second derivative of interpolant at point 2 and N-1 to numerical 
%                                     second derivative between point (3 and 1)  and (N-2, N).   
%                           12      : reserved (for 'natural boundary conditions')
%                                     second derivative at point 2 and N-1(?) is zero.     
%  dimensions : vector specifying dimensions for which the pre-filtering should
%               be performed. default = find( size(dictionary)>1 )
%               
% OUTPUTS:
%  prefilteredDictionary : dictionary prefiltered in all dimensions specified by the dimensions input.
%               Can be used in spline_predict_function
%               (preferably through make_spline_predict_function )
% 
% Requires : 'Inverse' from the toolbox 'Factorize' by Timothy A. Davis 
%            https://nl.mathworks.com/matlabcentral/fileexchange/24119-don-t-let-that-inv-go-past-your-eyes--to-solve-that-system--factorize-
%
% 3-5-2018, D.Poot, Erasmus MC : created first version based on code dictionary_fitting_interpolation by W. van Valenberg.
% 29-1-2019, D.Poot, Erasmus MC: Added prefilter options. 

maxSize = 10^6; % maximum number of columns in a block 

if nargin < 4 || isempty( dimensions )
    sz = size( dictionary ); 
    dimensions = find( sz > 1 );
end;
progressbar('start', numel( dimensions ), 'Prefiltering dictionary. Please wait...', 'EstTimeLeft', 'on');
size_dict  = size( dictionary ) ;    

boundary_condition_int = mod( bsplineorder, 4) ; % initialize bspline. 
% define spline
xval = floor(-bsplineorder/2) : ceil(bsplineorder/2);
[bspline , dbsplinedx, d2dbsplinedx] = bsplinefunction( bsplineorder, xval );

for curdim = dimensions

    % create matrix for deconvolution of dictionary to b-spline coefficients:
    % dictToObs * dictionary(k,:)' = conv_matrix * coefficients(k,:)'. 
    % => coefficients(k,:)' = inv( conv_matrix) * dictToObs * dictionary(k,:)'
    % => coefficients(k,:)' = inv( dictToObs \ conv_matrix  )* dictionary(k,:)'
    % => coefficients(k,:) = dictionary(k,:) * inv( (dictToObs\ conv_matrix)' );
    %
    % dictToObs: N x N matrix; mostly identity; but for edge atoms (first&last rows) may be derivative computation etc. 
    % conv_matrix : N x N matrix with b-spline blurring applied to coefficients 
    %               For edge atoms (first&last row) maybe evaluates derivative of the bspline interpolant. 
    % conv_matrixE : N x (N+length(xval)-1) : expanded conv_matrix including edge atoms. 
    %                in a second step the boundary condition 0,1,2 are applied to get conv_matrix. 

    % TODO: speed up construction of conv_matrixE
    conv_matrixE = zeros( size_dict( curdim ), size_dict( curdim )+numel(xval)-1);
    for k = 1 : size_dict( curdim ); % Also construct edges; may need to be cleared lated. 
        conv_matrixE( k, k-1+(1:numel(xval)) ) = bspline;
    end;
    dictToObs = []; % [] : signalling identity. 
%     conv_matrix = toeplitz( [ bspline( ceil(numel(xval)/2):end ) zeros(1, size_dict( curdim ) - ceil(numel(xval)/2))], [bspline(ceil(numel(xval)/2):end) zeros(1, size_dict(curdim)-ceil(numel(xval)/2))]);

    boundary_condition_pre = floor( boundary_condition /4)*4;   % prefilter boundary condition
    boundary_condition_int = boundary_condition- boundary_condition_pre; % interpolation phase boundary condition. 
    switch boundary_condition_pre
        case 0
            % interplating => handled above. 
        case 4
            % 'clamped boundary conditions' 
            %  Set gradient of interpolant at point 2 and N-1 to numerical 
            %  derivative between point (3 and 1)  and (N-2, N). 
            ddict = [-.5 0 .5];
            dictToObs = eye( size_dict( curdim ) );
            dictToObs(1,1:3) = ddict;
            dictToObs(end,end-2:end) = ddict;
            conv_matrixE([1 end],:)=0;
        
            conv_matrixE(1,1+(1:numel(dbsplinedx))) = -dbsplinedx;
            conv_matrixE(end,end-numel(dbsplinedx):end-1) = -dbsplinedx;
        case 8
            %  Set second derivative of interpolant at point 2 and N-1 to numerical 
            %  second derivative between point (3 and 1)  and (N-2, N).   
            ddict = [1 -2 1];
            dictToObs = eye( size_dict( curdim ) );
            dictToObs(1,1:3) = ddict;
            dictToObs(end,end-2:end) = ddict;
            conv_matrixE([1 end],:)=0;
            conv_matrixE(1,1+(1:numel(dbsplinedx))) = d2dbsplinedx;
            conv_matrixE(end,end-numel(dbsplinedx):end-1) = d2dbsplinedx;
        otherwise
            error('unsupported value for boundary_condition provided.');
    end;
    if ~isempty( dictToObs )
        conv_matrixE = dictToObs\conv_matrixE ;
    end;
    nn = nnz( xval<0 ); %number of negative xval; == number of boundary columns at the start of conv_matrixE.
    np = nnz( xval>0 ); %number of positive xval; == number of boundary columns at the end of conv_matrixE.
    
    switch boundary_condition_int
        case 0 % zero extend
            % chop off boundary
            conv_matrix = conv_matrixE( :, (1+nnz(xval<0)) : (end - nnz(xval>0) ));
        case 1 % mirror boundary conditions
            if nn>size_dict( curdim ) || np > size_dict( curdim )
                error('Boundary exceeds size of the dictionary. This is not supported');
            end;
            conv_matrixE( :, 1: nn*2 ) = [zeros(size_dict( curdim ), nn) conv_matrixE( :, nn:-1:1 )+conv_matrixE( :, nn+1:nn*2 )];
            conv_matrixE( :, end-(0: np*2-1) ) = [zeros(size_dict( curdim ), np) conv_matrixE( :, end-np+1 : end )+conv_matrixE( :, end-(np:np*2-1) )];
            conv_matrix = conv_matrixE( :, (1+nnz(xval<0)) : (end - nnz(xval>0) ));
        case 2 % periodic domain:
            conv_matrixE( :, nn+(1:np) ) = conv_matrixE( :, nn+(1:np) )+conv_matrixE( :, end-np+1:end );
            conv_matrixE( :, end-np-nn+(1:nn) ) = conv_matrixE( :, end-np-nn+(1:nn) ) + conv_matrixE( :, 1:nn);
            conv_matrix = conv_matrixE( :, (1+nnz(xval<0)) : (end - nnz(xval>0) ));
        otherwise
            error('unsupported value for boundary_condition provided.');
    end;

    % determine number of atoms in other dimensions

    if 0 
        maxAtoms = floor( maxSize / size_dict( curdim ) );
        Nsteps   = ceil(  numel(dictionary) / ( size( dictionary, curdim ) * maxAtoms ) );
        idx_sub = cell( ndims(dictionary), 1);
        deconv_matrix = inverse( conv_matrix, 'symmetric' );
        progressbar('start', [], 'Prefiltering dictionary. Please wait...', 'EstTimeLeft', 'on');
        for k = 1:Nsteps

            lin_idx            = 1 + (k-1)*maxAtoms : min( k*maxAtoms, numel(dict_comp) /  dim_dict(d) );
            [idx_sub{dim_fix}] = ind2sub( dim_dict( dim_fix ), repmat( lin_idx, dim_dict(d), 1 ) );
            idx_sub{d}         = repmat( [1:dim_dict(d)]', 1, numel(lin_idx) );
            idx_ind            = sub2ind( dim_dict, idx_sub{:} );

            dict_comp( idx_ind ) = deconv_matrix * dict_comp( idx_ind );
            %dict_comp( idx_ind ) = deconv_matrix \ dict_comp( idx_ind );
            progressbar(k / Nsteps);
        end
        progressbar('ready');
    else
        % assume time dimension (=size(dict_comp,1)) is long enough for vectorization. 
        idx1 = repmat( {':'}, 1, numel( size_dict ) );
        itdims = [2:curdim-1 curdim+1 : numel( size_dict ) ];
        if isempty( itdims )
            % avoid error for  2D dictionary, so only 1D interpolation: (else error as ind2sub below returns one output even when size(itsize)==[1 0] 
            itdims = numel( size_dict ) +1;
            itsize = 1;
        else
            itsize = size_dict( itdims );
        end;
        deconv_matrixT = inverse( conv_matrix' ); %;, 'symmetric' );
        blocksz = size_dict( [1 curdim] );
        progressbar('start', prod( itsize ), 'Prefiltering dictionary. Please wait...', 'EstTimeLeft', 'on');
        target_el_per_block = 1e6; 
        if curdim==2 && prod( blocksz) < target_el_per_block/2
            % optimization to vectorize :
            ncols_per_block = max(1, floor( target_el_per_block / prod( blocksz) ));
            sted = [1 : ncols_per_block : max(1, prod( size_dict( curdim+1:end ) )-floor(ncols_per_block/5)) prod( size_dict( curdim+1:end ) )+1]; % assumes curdim==2
            idx2 = {':',':',[]};
            for k = 1 : numel(sted)-1
                idx2{3} = sted(k) : sted(k+1)-1;
                blocksz2 = [blocksz numel( idx2{3} )];
                % Steps on next line:
                %   - take block of dictionary
                %   - permute &reshape to 'attach' 3rd dimension of this block to first dimension 
                %   - multiply with deconv_matrixT
                %   - reshape and inverse permute to get 3rd dimension back in correct form. 
                %   - store back into dictionary. 
                dictionary( idx2{:} ) = permute( reshape(  reshape( permute( dictionary( idx2{:} ), [1 3 2]),  blocksz2(1)*blocksz2(3), blocksz2(2) ) * deconv_matrixT, blocksz2([1 3 2])), [1 3 2]);
                progressbar( sted( k+1)-1 );
            end;
        else % curdim>=3 or block size is so large that no vectorization is needed. 
            dictionary = reshape( dictionary, [size_dict(1) prod( size_dict(2:curdim-1) ) size_dict( curdim ) prod( size_dict( curdim+1:end) )] );
            ncols_per_block = max(1, floor( target_el_per_block / prod( blocksz) ));
            
            sted = [1 : ncols_per_block :max(1, prod( size_dict( 2: curdim-1 ) )-floor(ncols_per_block/5))  prod( size_dict( 2: curdim-1 ) )+1]; % assumes curdim==2
            progbupdstep_l = ceil( 2*target_el_per_block / (prod( blocksz) * sted(end)) ); % ceil to avoid rounding towards zero. 
            idx2 = {':',[],':',[]};
            for l = 1 : prod( size_dict( curdim+1:end) )
                idx2{4} = l;
                for k = 1 : numel(sted)-1
                    idx2{2} = sted(k) : sted(k+1)-1;
                    blocksz2 = [blocksz(1) numel( idx2{2} ) blocksz(2)];
                    dictionary( idx2{:} ) = reshape( reshape( dictionary( idx2{:}), blocksz2(1)*blocksz2(2), blocksz2(3) ) * deconv_matrixT, blocksz2);
                end;
                if mod(l, progbupdstep_l)==0
                    progressbar( sted(k+1)-1 + l * (sted(end)-1));
                end;
            end;
            dictionary = reshape( dictionary, size_dict );
        end;
        progressbar('ready');
    end;

    progressbar( find( dimensions==curdim ) ); % find is unnececarily slow here, but still fine as time is negligable compared to computational work. 
end
progressbar('ready');


function test()
%% test if function runs:
A = randn(10,123);
bsplineorder = 1; 
boundary_condition = 1;
Af = prefilter_spline( A, bsplineorder, boundary_condition );
assert( isequal( A, Af ) ); %bspline order 1 does not need prefiltering. 

%% check if interpolation of prefiltered dictionary returns original values. 
A = randn(10,123);
bsplineorder = 2; 
boundary_condition = 1;
Af = prefilter_spline( A, bsplineorder, boundary_condition );

interpol = spline_predict_function([ones(1,size(A,2));(1:size(A,2))], Af, [], [], [], bsplineorder, [], [], boundary_condition);
assert( all(all( abs(A-interpol )<1e-14 )) )

%% check if interpolation of prefiltered dictionary returns original values. 
A = randn(9,123);
bsplineorder = 3; 
boundary_condition = 1;
Af = prefilter_spline( A, bsplineorder, boundary_condition );

interpol = spline_predict_function([ones(1,size(A,2));(1:size(A,2))], Af, [], [], [], bsplineorder, [], [], boundary_condition);
assert( all(all( abs(A-interpol )<1e-14 )) )

%% check complex if interpolation of prefiltered dictionary returns original values. 
A = complex( randn(9,123), randn(9,123)) ;
bsplineorder = 3; 
boundary_condition = 1;
Af = prefilter_spline( A, bsplineorder, boundary_condition );
Af2 = -1i*prefilter_spline( 1i*A, bsplineorder, boundary_condition );

interpol = spline_predict_functionC([[1;0]*ones(1,size(A,2));(1:size(A,2))], Af, [], [], [], bsplineorder, [], [], boundary_condition);
imagebrowse(catz(3,A, Af,Af2,Af-Af2,interpol, A(:,1:end)-interpol))
assert( all(all( abs(A-interpol )<1e-14 )) ) % when assertion fails, a wrong version of TransformNDbspline.mex* might be used. 

