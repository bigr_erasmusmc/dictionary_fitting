function [bvalues] = read_bval_file( filename )
% [bvalues] = read_bval_file( filename )
%
% Reads a bvalue file as created by FSL (?)
% A text file with a tab delimitted list of bvalues.  
%
% 22-3-2017: Created by Dirk Poot, Erasmus MC. 

fid = fopen( filename ,'r') ;
if fid == -1
    error(['cannot open ' filename ]);
end;    
try
    bvalues = fscanf(fid, '%d');
    if ~feof(fid) 
        error('%d bvalues read, but there seems to be more in the file', numel(bvalues));
    end;
catch ME
    ME
end;
fclose( fid );