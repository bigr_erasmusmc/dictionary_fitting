function [out]= interpolate_with_filterbank( vec, locations, filterbank, ar, bsplineorder)
% [out]= interpolate_with_filterbank( vec, locations, filterbank, ar, bsplineorder )
% OR:
% [out]= interpolate_with_filterbank( vec, locations, filter)
% Full matlab implementation of interpolation of a vector with filterbank.
% optional prefiltering with ar filter.
%
% INPUTS:
% vec : column vector or multiple column vectors
% locations: a single column vector with the sampling points.
% filterbank  : filterbank as for example designed with designLPfilter
% ar          : AR prefilter of vec, as for example designed with designLPfilter
% bsplineorder: default = 1; order of b-spline interpolation between columns of filterbank.
%                            nsteps = size(filterbank,2)-bsplineorder 
%               0 : neirest neighbor.
%               1 : linear interpolation.
%               2 : 2nd order b-spline.
%               3 : cubic b-spline interpolation (make sure filterbank is constructed with taking this b-spline order into account!).
% filter  : filter designed with designLPfilter_v3
%
% OUTPUTS:
% out : numel(locations) x number_of_columns_in_vec matrix with interpolated values.
%
% Created by Dirk Poot, Erasmus MC, 7-7-2011

if nargin<4
    ar = [];
end;
if nargin<5 
    bsplineorder = 1;
end;
if isstruct(filterbank)
    ar = filterbank.prefilterAR;
    ar(1) = filterbank.ARrunout;
    bsplineorder = filterbank.filterbank_bsplineorder;
    locations = locations - filterbank.delay;
    filterbank = filterbank.filterbank;
end;

mayUseMex = true;
if mayUseMex && exist('TransformCoreGeneral_c2','file')==3
    locationsmx = locations(:);
    if size(vec(:,:),2)>1
        dimorder  =  [1 2;1 2;1 2];
    else
        dimorder  =  [1;1;1];
    end;
%     out = TransformCoreGeneral_c2( vec(:,:), locationsmx, [], size(locationsmx,1), dimorder, filterbank, bsplineorder,  ar, [], []);
    out = TransformCoreGeneral_c2( vec(:,:), locationsmx, [], size(locationsmx,1), dimorder, filterbank, bsplineorder,  ar, [], []);
else
    warning('interpolate_with_filterbank:notUsingMex','Not using the mex file TransformCoreGeneral_c2, this causes substantial loss of performance');
    warning('off','interpolate_with_filterbank:notUsingMex'); % silence this warning for further calls. 
if ~isempty(ar)
    ext = ar(1);
    ar(1)= 1;
    vec(end+ext,1)=0; % expand vec.
    vec = flipud( filter(1, ar, vec(:,:))); % need to flip to filter in reverse direction
    vec(end+ext,1)=0; % expand vec.
    vec = flipud( filter(1, ar, vec)); % flip again to reset to correct direction.
    locations = locations + ext;
end;

[veclen,nveccols] = size(vec);
nsteps = size(filterbank,2)-bsplineorder;
filterlength = size(filterbank,1);
locationsel = find( (locations>=1-filterlength) & (locations<veclen+1) );

locationss = locations(locationsel); % only compute for valid locations
iloc = floor( locationss );
rem = locationss - iloc;
col = rem * nsteps;
coli = floor(col)+1;
colfrac = (col(:)-(coli(:)-1))';
out = zeros(nveccols,numel(locations));
if bsplineorder == 0
    effectivefilters = filterbank(:,coli) ;
elseif bsplineorder == 1
    diffbank = diff(filterbank,[],2);
    effectivefilters = filterbank(:,coli) + bsxfun(@times, colfrac, diffbank(:,coli));
elseif bsplineorder == 2
    effectivefilters = bsxfun(@times, .5*(1-colfrac).^2     , filterbank(:, coli     )) + ...
                       bsxfun(@times, .5+colfrac-colfrac.^2 , filterbank(:, coli + 1 )) + ...
                       bsxfun(@times, .5*(colfrac).^2      , filterbank(:, coli + 2 ));
elseif bsplineorder == 3
    one_mfrac = 1-colfrac;
    sqr_frac = colfrac.*colfrac;
    sqr_onemfr = one_mfrac.*one_mfrac;
    effectivefilters =(bsxfun(@times,     sqr_onemfr.*    one_mfrac   , filterbank(:, coli     )) + ...
                       bsxfun(@times, 4-3*sqr_frac  .*( 2-colfrac   ) , filterbank(:, coli + 1 )) + ...
                       bsxfun(@times, 4-3*sqr_onemfr.*( 2-one_mfrac ) , filterbank(:, coli + 2 )) + ...
                       bsxfun(@times,     sqr_frac  .*    colfrac     , filterbank(:, coli + 3 )) ) *(1/6);
else
    error('invalid or unsuported bsplineorder; valid: {0, 1, 2, 3}');
end;

for k=1:numel(locationsel)
    curindvec = iloc(k)+(1:filterlength);
    sel = (curindvec>=1) & (curindvec<=veclen);
    out(:,locationsel(k)) = effectivefilters(sel,k)'*vec(curindvec(sel),:);
end;
out = out';

% if ~isequal(outmx,out)
%     error('mex gives different result');
% end;
end;