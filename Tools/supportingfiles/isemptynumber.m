function tf = isemptynumber( value ) 
% tf = isemptynumber( value ) 
% Returns a boolean output with the same size as value. 
% True if value is 'empty' as indicated by the special NaN value created by
% emptynumber.
%
% Created by Dirk Poot, ErasmusMC, 28-4-2014

tf = isnan(value);
if any(tf(:))
    sel = double( value(tf));
    e = emptynumber;
    % typecast to integer to be able to test for specific nan values:
    seli = typecast( sel, 'uint64');
    ei = typecast( e, 'uint64');
    
    % assert:
    if size(sel)~=size(ei) 
        error('double representation is not 64 bit');
    end;
    tf( tf ) = (seli==ei); % set to true only if value is the empty nan.
end;