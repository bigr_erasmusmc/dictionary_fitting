/*
* This bspline_interpolate_regular.cpp is a MEX-file for MATLAB.
* Created by Dirk Poot, Erasmus Medical Center 
* (Windows) Build with: (add -g for debugging (add -O for optimizations while debugging); -v for verbose mode )
* mex bspline_interpolate_regular.cpp -I".\Tools\supportingfiles\" -g -DMEX -D__SSE4__
*
* NOTE: compiler under Matlab 2011b creates incorrect code for the adjoint when optimizing! (test 'adjoint' with test_bspline_interpolate_regular)
%       So compile under MATLAB 2013 (or newer?)
*
* Or on Linux:
* mex TransformNDbspline.cpp -I"./" -I"./Tools/supportingfiles/" -v CXXFLAGS="\$CXXFLAGS -march=barcelona" LDFLAGS="\$LDFLAGS "
*
*
* TODO: enable vectorization in inline bspline_filter.cpp line 920, in:
*       void get_coefficients( dataType coeffs, location_type location ) 
*/


#ifdef _OPENMP
//#define USEOMP
#endif

//#define ADDGUARDS

#include <assert.h>
#include "mex.h"
#include <cmath>
typedef mwSize IndexType; 
//#include <cmath>
#include <complex>
#include <iterator>
#include <algorithm>
#include <typeinfo> 
//#include "emm_wrap.cpp"
#include "emm_vecptr.hxx"
#include "tempSpaceManaging.cpp"
//#include "lineReadWriters.cpp"
#include "step_iterators.cpp"
#include "NDimage_new.cpp"
#include "subspaceIterator.cpp"
#include "SeparableTransform_core_c.cpp"
//#include "bspline_filter.cpp"
//#include <bitset>


#ifdef USEOMP
#include <omp.h>
#endif

//#define PERFORMANCECOUNTING // if defined: return some counts in the first (double) elements of the output. SO FOR DEBUG ONLY!!!
// Compile TransformNDbspline_timing.cpp to get this functionality.
#ifdef PERFORMANCECOUNTING
#include <Windows.h>
typedef volatile __int64 timeCntT;
timeCntT time_used[5];
#pragma intrinsic(__rdtsc)
#endif

#define MaxNrOfDims 6

const int n_rowscompOutArea = 5;
#define ERROR mexErrMsgTxt

#if defined _MSC_VER && _MSC_VER < 1900
// isnan should be included in cmath, but is not for Visual Studio: 
bool isnan(double v) {
  return v!=v;
} 
#endif

/*template < typename int_t, typename readType, typename TransfType >
void mulWithTransformDerivativeAdjoint( readType inR, int_t n_Tag, 
							TransfType & T,
							range< int_t> range_o, int_t stepIn_Tdim, int_t stepIn_Tag )
{
	//T.setAdjointPos( in[ currentIndex_o[0] ] );
	if (n_Tag==1) {
		for ( int_t m = range_o.first; m <= range_o.last; m++ ) {
			T.writeAdjoint( m , inR[  m * stepIn_Tdim ] );
		}
	} else {
		for ( int_t m = range_o.first; m <= range_o.last; m++ ) {
			for ( int_t l=0; l<n_Tag; l++) {
				T.adjoint( m , inR[  m * stepIn_Tdim + stepIn_Tag * l ] );
			}
		}
	}
};*/

template< typename clipT, typename int_t > vector< clipT > build_clipranges( const mxArray * clip, int_t * size, int ndimsT, int ndims_skip = 0 ) {
  vector< clipT > clips( ndimsT - ndims_skip );
  bool doClip = !mxIsEmpty(clip);
  if (doClip) {
    if ((mxGetNumberOfElements( clip )!=ndimsT) | (!mxIsCell( clip ) ) ) {
      mexErrMsgTxt("clip_i/o should be a ndims(T) element cell array, with each element Q_k  a ( nplanes_k  x  (ndims(T)-k+2) )  double matrix, where the subset of all allowable coordinate vectors x is given by  Q*[x;-1] <=0 (if Q = [A b] =>  A*x <= b, which is the standard linear program form in MATLAB).");
    }
    for (int k = ndims_skip ; k<ndimsT ; k++ ) {
      const mxArray * clipel = mxGetCell(clip, k);
      const double * ptr = NULL;
      int nplanes = 0; 
      if (clipel != NULL) {
        if ((mxGetNumberOfDimensions( clipel )!=2) | (mxGetClassID( clipel ) != mxDOUBLE_CLASS) | mxIsComplex( clipel ) | !((mxGetN(clipel)==ndimsT-k+1)|| mxIsEmpty(clipel)) ) {
          mexErrMsgTxt("Element k of clip_i should be a nplanes(k) x (ndims(T)-k+2) non complex double matrix.");
        }

        ptr = mxGetPr(clipel);
        nplanes = (int) mxGetM(clipel);
      }
      range<double> rng ={0, (double) size[k]-1};
      //				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.   
      clips[k-ndims_skip] = clipT(    ptr,   ptr + nplanes * (ndimsT-k),      nplanes,        ndimsT-k,         rng       );
    }
  } else {
    for (int k = ndims_skip ; k<ndimsT ; k++ ) {
      const double * ptr = NULL;
      int nplanes = 0;
      range<double> rng ={ 0, (double) size[k] -1 };
      //				    pointer to A  ,     pointer to b                      ,  number of half spaces,  dimensionality , outer limits.   
      clips[k-ndims_skip] = clipT(    ptr,   ptr + nplanes * (ndimsT-k),      nplanes,        ndimsT-k,         rng       );
    }
  }
  return clips;
}

/*template< typename T , typename int_t> T* adp( T* p, int_t step) {
  if (p==NULL) {
    return p;
  } else {
    return p+step;
  }
}*/
typedef ptrdiff_t int_t ;

template  <typename dataT, typename interpolateT , typename subRitT> void bspline_interpolate_Adjoint(dataT * C_r, double ** transformed_skip, interpolateT ** interpolators_skip, int ndims_proc , double * map_position_skipped, int_t vec_len ,subRitT subrit, ptrdiff_t step_o_dimskip ) {
  typedef separableTransformAdjoint<dataT, int_t> transformAdjointType;
  typedef typename subRitT::iterator_lineT line_itT;
	transformAdjointType * transformAdjointptr = NULL;
        
    transformAdjointptr = new transformGeneral1DinterpAdjoint<MaxNrOfDims,  interpolateT *, double ,double ,int_t>( transformed_skip, interpolators_skip, ndims_proc, map_position_skipped, vec_len);
    transformAdjointType & transformAdjoint = transformAdjointptr[0];
    vector<int> prevpos(  ndims_proc );
    for (int dim = 0 ; dim < ndims_proc; dim++ ) {
        prevpos[dim] = 0;
    }
    double * out_lp = C_r;
    line_itT lineit = subrit.begin_lines();
    subrit.highestdimupdated=0;
    for(  ; lineit != subrit.end() ; ++lineit, out_lp += step_o_dimskip) {
      const vector<int> & curpos = lineit.curPos();
      range<int > range_o = lineit.linerange();

			for (int dim =1 ; dim <= subrit.highestDimUpdatedLastStep(); ++dim ) { // maxDimDifferent is maximum dimension in which point i (might) differs from point i-1
				transformAdjoint.updateAdjoint( dim , prevpos[dim] );
				prevpos[ dim ] = curpos[dim];
			}
      for ( int_t m = range_o.first; m <= range_o.last; ++m ) {
        for (int_t i = 0; i< vec_len; ++i ) {
          transformed_skip[ 0 ][i] = out_lp[ m *vec_len + i] ;
        }
		  	transformAdjoint.updateAdjoint( 0 , m );
		  }
    }
    for (int dim =1 ; dim < ndims_proc ; dim++ ) { // maxDimDifferent is maximum dimension in which point i (might) differs from point i-1
			transformAdjoint.updateAdjoint( dim , prevpos[dim] );
		}
    delete &transformAdjoint;
}


template  <typename dataT, typename interpolateT , typename subRitT> void bspline_interpolate(dataT * out, double ** transformed_skip, interpolateT ** interpolators_skip, int ndims_proc , double * map_position_skipped, int_t vec_len ,subRitT subrit, ptrdiff_t step_o_dimskip ) {
  typedef typename subRitT::iterator_lineT line_itT;
  typedef separableTransformND<dataT, int_t> transformType;
  transformType * transformptr = NULL;
    transformptr = new transformGeneralNDinterp< MaxNrOfDims, interpolateT *, double , double, int_t>( transformed_skip, interpolators_skip, ndims_proc ,map_position_skipped , vec_len);

      transformType & transform = transformptr[0];

      double * out_lp = out;
    for( line_itT lineit = subrit.begin_lines(); lineit != subrit.end() ; ++lineit, out_lp += step_o_dimskip) {

      const vector<int> & curpos = lineit.curPos();
      range<int > range_o = lineit.linerange();

      for (int dim = subrit.highestDimUpdatedLastStep(); dim>0; dim-- ) {
        transform.update(dim, curpos[dim]);
      }
      transform.read(  range_o , out_lp + range_o.first ) ;
    }
    delete &transform;
}

#undef BUILDING_CORE
#ifndef NOMEXFUN



#ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
// Test code to test the majority of the code in this function. Mainly used to debug the code during development.
#include <bitset>
void self_test_mex( mxArray * out ) {

}
#endif


void mexFunction( int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[] )
{
#ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
  // Only compile code below for performing a code test.
  if (nlhs!=1) {
    mexErrMsgTxt("One output required.");
  }
  plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
  self_test_mex( plhs[0] );
  return;
#else
  /*  out = transformCorePlane_c( in, map_position, out_size, bsplineorder, clip_image)
  % b-spline interpolates 'coeffs' to 'image' or performs the adjoint ('transpose') operation.
  % 'interpolation' has out_size>0; then coeffs ==  in, image == out
  % 'adjoint'       has out_size<0; then coeffs == out, image ==  in
  % (MATLAB) pseudocode of function:
  % for d = 1 : ndims(in)
  %   if isnan( map_position(1,d) )
  %     y{d} = 1:size(coeffs,d) ;
  %   else 
  %     y{d} = map_position(1,d)+(bsplineorder+1)/2+ (0:image_size(d)-1)*map_position(2,d);
  %   end;
  %   y{d} = permute( y{d}(:), , [2:d 1 d+1]);
  % end;
  % image = interpn(coeffs, y{:}, 'bsplineorder_determined_method' );

  This function applies a seperable b-spline transform. Since the seperability of the bspline interpolation is used it is efficient 
  as long as the output spans a siginificant fraction of the input. As this transform is linear in coeffs, the derivative w.r.t. coeffs is the same function.
  The adjoint multiplication is specified by providing a matrix with size of 'out' as input and -size(coeffs) as out_size argument. 


  % INPUTS:
  %  coeffs       : N - Dimensional bspline coefficient field. 
  %  image        : N - Dimensional image interpolated from coeffs.
  %  map_position : 2 x N mapping matrix that specifies the sampling grid in each dimension; use nan to not perform interpolation (currently only supported for the first dimensions)
  %  out_size     : N element vector with size of the output. Negative values (-size(coeffs)) specify that the adjoint transform is requested and that a array of size 'out' is given as first input. 
  %  bsplineorder : scalar (0,1,2,3) b-spline interpolation order for interpolation of the coefficient field coeffs. 
  %  clip_image   : Clipping planes specification of which part of the output should actually be computed. Currently not correctly supported (I think only both out_lp variables go wrong then)
  % OUTPUTS:
  %  out          : image or coeffs, depending on performing 'interpolation' or 'adjoint'. 
  */
#ifdef PERFORMANCECOUNTING
  timeCntT tstart_initialization = __rdtsc(); 
#endif

  /* Check for proper number of arguments. */
  if ((nrhs!=5)) {
    mexErrMsgTxt("Five inputs required: [out ] = TransformNDbspline( in, map_position, out_size, bsplineorder, clip_o ).");
  } else if((nlhs!=1)) {
    mexErrMsgTxt("One output required.");
  }
  // Map arguments to names:
  const mxArray * in		       = prhs[0]; // N-D array, double, or 1xN vector
  const mxArray * map_position = prhs[1]; // 2 x N matrix, double, [offset;step] in coeffs for point in image; point_t = point_image .* map_position(2,:) + map_position(1,:)
  const mxArray * out_size	   = prhs[2]; // N element vector
  const mxArray * bsplineorder = prhs[3]; // scalar.
  const mxArray * clip_image   = prhs[4]; // cell array with clipping planes.

  int_t tempspacebytesneeded = 0;

  /* parse inputs */
  //in:
  mwSize ndims_in = mxGetNumberOfDimensions( in );
  if ( ndims_in > MaxNrOfDims ) { 
    mexErrMsgTxt("Input has too many dimensions, increase 'MaxNrOfDims' in this mex file");
  }
  const mwSize * size_in = mxGetDimensions( in );
  if ( (ndims_in==2) && (size_in[1]==1) ) {
    ndims_in=1;
  };
  mxClassID class_in = mxGetClassID( in );
  if ((class_in != mxDOUBLE_CLASS) ){// && (class_in!= mxSINGLE_CLASS)) {
    mexErrMsgTxt("Input in should be double. (Other types can be added if meaningfull interpolation can be performed.)");
  }
  double * ptr_in_r = mxGetPr( in );
  double * ptr_in_i = NULL;
  bool iscomplex= mxIsComplex( in );
  mxComplexity iscomplexArg = mxREAL; // later overwritten if iscomplex
  if (iscomplex) {
    ptr_in_i = mxGetPi(in);
    iscomplexArg = mxCOMPLEX;
  };
  int_t imgtypesize = mxGetElementSize( in ) ;

  ptrdiff_t step_in[MaxNrOfDims+1];
  step_in[0] = 1;
  for(int k = 1; k <= ndims_in ; k++) {
    step_in[ k ] = step_in[ k-1 ] * size_in[ k-1 ];
  }


  // map_position
  if ((mxGetClassID( map_position ) != mxDOUBLE_CLASS) || mxIsComplex( map_position ) ||  (mxGetM(map_position)!=2) || (mxGetN(map_position) != ndims_in) ) {
    mexErrMsgTxt("MapT should be 2 x ndims(in) non complex double matrix");
  }
  double * ptr_map_position = mxGetPr( map_position );

  // out_size 
  if ( mxIsComplex( out_size ) ||  (mxGetNumberOfElements(out_size) != ndims_in) || (mxGetClassID( out_size  ) != mxDOUBLE_CLASS)) {
    mexErrMsgTxt("out_size should be a ndims(in) element double vector.");
  }
  double * ptr_out_size  = mxGetPr( out_size );
  mwSize size_out[ MaxNrOfDims ];
  size_out[0]=1;size_out[1]=1; // always need these initialized, even when ndims_in==1
  bool doAdjoint = (ptr_out_size [0]<0);
  for (int k = 0 ; k<ndims_in; k++ ) {
    size_out[k] = (int) ptr_out_size [k];
    if (doAdjoint) {
      size_out[k] = -size_out[k];
    }
    if (size_out[k]<=0)
      mexErrMsgTxt("out_size should be a ndims_in element double vector with positive (or all negative=>adjoint) elements.");
  }
  // construct steps of Out.
  int_t step_out[MaxNrOfDims+1];
  step_out[0] = 1;
  for(int k = 1 ; k <= ndims_in; k++) {
    step_out[ k] = step_out[ k-1] * size_out[ k-1];
  }


  // bsplineorder
  if ((mxGetNumberOfElements( bsplineorder )<1) | (mxGetNumberOfElements( bsplineorder )>2) | mxIsComplex( bsplineorder ) ) {
    mexErrMsgTxt("bsplineorder should be a non complex scalar integer, or 2 element integer vector, in the range [0 .. 3].");
  }
  int bsplineordT = (int) mxGetScalar( bsplineorder ) ;
  if ( (bsplineordT<-3) | (bsplineordT>3)) {
    mexErrMsgTxt("bsplineorder should be 0, 1, 2, or 3, or -2,-3 for OMOMS interpolation.");
  }


  ptrdiff_t size_image[MaxNrOfDims]; // out when !doAdjoint, in   when doAdjoint
  ptrdiff_t size_C[MaxNrOfDims]; // in   when !doAdjoint, out when doAdjoint
  ptrdiff_t step_image[MaxNrOfDims+1]; // out when !doAdjoint, in   when doAdjoint
  ptrdiff_t step_C[MaxNrOfDims+1]; // in   when !doAdjoint, out when doAdjoint
  if (doAdjoint) {
    for (int k = 0; k < ndims_in ; k++ ) {
      size_C[k]     = size_out[ k ];
      size_image[k] = size_in[ k ];
      step_C[k]     = step_out[ k ];
      step_image[k] = step_in[ k ];
    }
    int k = ndims_in;
    step_C[k]     = step_out[ k ];
    step_image[k] = step_in[ k ];
  } else {
    for (int k = 0; k < ndims_in ; k++ ) {
      size_C[k]     = size_in[ k ];
      size_image[k] = size_out[ k ];
      step_C[k]     = step_in[ k ];
      step_image[k] = step_out[ k ];
    }
    int k = ndims_in;
    step_C[k]     = step_in[ k ];
    step_image[k] = step_out[ k ];
  }


  // check if all points of image map to valid sample locations in coeffs.
  int_t cumszT = 1;
  tempspacebytesneeded += (ndims_in+1) * sizeof(double*); // assume sizeof( double * ) == sizeof( float *) == sizeof( any_data_type * )
  int_t numdimskip = 0;
  for (int k = 0; k < ndims_in ; k++ ) {
    // Compute required temporary storage for interpolation in dimension k+1 
    tempspacebytesneeded += cumszT * imgtypesize;
    cumszT *= size_C[ k ];

    // check positions:
    double p1 = ptr_map_position[0 + 2*k];
    double p2 = ptr_map_position[0 + 2*k] + ptr_map_position[1 + 2*k] * (size_image[ k ] -1);
    if (isnan( p2 )) {
      if (k == numdimskip) {
        numdimskip++;
        if (size_image[ k ] != size_C[ k ]) {
          mexErrMsgTxt("All no transform dimensions (map_position==NaN) should preserve size.");
        }
      } else {
        mexErrMsgTxt("NaN in map_position only allowed in the first dimension(s) to indicate no transform.");
      }
    } else if ((p1<0) || (p2<0) || (floor(p1) >= size_C[ k ] - bsplineordT) || ( floor(p2) >= size_C[ k ] -bsplineordT)) {
      mexErrMsgTxt("Invalid map_position, all points of image should map to inside of coeffs.");
    }
  }

  // read clipping planes:	
  typedef clippedRangeND<double> clipT;
  vector< clipT > clips_o;

  clips_o = build_clipranges<clipT>( clip_image , size_image, ndims_in, numdimskip );

  // Allocated tempspace
  char * tempspaceptr = NULL;
  if (tempspacebytesneeded>0) {
    tempspacebytesneeded += 16; // needed for alignment.
#ifdef ADDGUARDS
    tempspacebytesneeded += 10 * 8;
#endif
    int maxthreadsuse = 1;
    tempspaceptr = (char *) mxMalloc( tempspacebytesneeded * maxthreadsuse); // NOTE: not initialized!
  }
  tempspace tmp = tempspace(tempspaceptr, tempspacebytesneeded);

  /* Create output. */
  if (ndims_in==1) {
    size_out[1] = 1;
    plhs[0] = mxCreateNumericArray((int) 2       , size_out , class_in , iscomplexArg);
  } else {
    plhs[0] = mxCreateNumericArray((int) ndims_in, size_out , class_in , iscomplexArg);
  }
  double * ptr_out_r = mxGetPr(plhs[0]);
  double * ptr_out_i = NULL;
  if (iscomplex) {
    ptr_out_i = mxGetPi(plhs[0]);
  };




  // prepare interpolation of coeffs:
  // Build a stack of interpolators, one for each interpolated dimension. 
  // Used as: 
  // for( dim = ndims_in-1 , dim>=0, --dim )
  //   Interpolate an dim-1 dimensional slice by fixing the position in dimension dim
  //   
  // and then iterate over all positions before updating a higher dimension. 


  //typedef coefficients_base<double *, double> interpolateT;
  typedef vector_adjoint_interpolatebase<coefficients_base<double *, double> > interpolateT;
  vector< interpolateT *> interpolators( ndims_in - numdimskip );

  int_t vec_len = 1;
  for (int dim = 0 ; dim < numdimskip; ++dim ) { 
    vec_len *= size_C[ dim ];
  }

  //int_t stepT[MaxNrOfDims];
//  double * Tscratch = NULL;
  // Allocate space for the intermediate interpolation results:
  double ** transformed = tmp.get<double *>( ndims_in - numdimskip + 1 );

  if (doAdjoint) {
    // only adjoint needs to initialize output:
    for (int k = 0 ; k < step_C[ ndims_in ]; ++k) {
      ptr_out_r[k] = 0;
    } 
    if (iscomplex) {
      for (int k = 0 ; k < step_C[ ndims_in ]; ++k) {
        ptr_out_i[k] = 0;
      } 
    }
    transformed[ ndims_in - numdimskip ] = ptr_out_r;
  } else {
    transformed[ ndims_in- numdimskip ] = ptr_in_r;
  }

  for (int dim = ndims_in-1 ; dim >= numdimskip; dim-- ) { 
    transformed[ dim - numdimskip] = tmp.get<double>( step_C[ dim ] );
    
    if (doAdjoint) {
      // only the adjoint needs initialization:
      for (int k = 0 ; k < step_C[ dim ]; ++k) {
        transformed[ dim - numdimskip][k]=0;
      }
    }
    //interpolators[ dim ] = newVector_interpolate( transformed[dim+1], prodszT, size_C[dim], bsplineordT); 
    interpolators[ dim -numdimskip ] = newVector_interpolateAdj( transformed[dim - numdimskip + 1 ], step_C[ dim ], size_C[ dim ], bsplineordT); 
  }


  // create iterator
  typedef NDsubspace<double, clipT > subRitT;
  vector<int_t> steps(ndims_in - numdimskip);
  for (int k = 0 ; k < ndims_in - numdimskip; k++ ) {
    steps[k] = step_image[k + numdimskip];
  }
  subRitT subrit( ptr_out_r, steps, clips_o );
  typedef subRitT::iterator_lineT line_itT;

#ifdef PERFORMANCECOUNTING
  timeCntT tend_initialization = __rdtsc(); 
  time_used[0] = tend_initialization - tstart_initialization;
#endif



  /* Perform the actual computations:*/
  if (doAdjoint) {
    bspline_interpolate_Adjoint<double, interpolateT, subRitT>( ptr_in_r, &transformed[0], &interpolators[0], ndims_in - numdimskip, ptr_map_position+ 2*numdimskip, vec_len , subrit, step_image[ numdimskip + 1 ] );
    if (iscomplex) {
        transformed[ ndims_in- numdimskip ] = ptr_out_i;

        interpolators[ ndims_in-1 -numdimskip ] = newVector_interpolateAdj( transformed[ndims_in-1- numdimskip + 1 ], step_C[ ndims_in-1], size_C[ ndims_in-1], bsplineordT); 
        subRitT subrit_i( ptr_out_i, steps, clips_o );

        for (int dim = ndims_in-1 ; dim >= numdimskip; dim-- ) { 
          for (int k = 0 ; k < step_C[ dim ]; ++k) {
            transformed[ dim - numdimskip][k]=0;
          }
        }

        bspline_interpolate_Adjoint<double, interpolateT, subRitT>( ptr_in_i, &transformed[0], &interpolators[0], ndims_in - numdimskip, ptr_map_position+ 2*numdimskip, vec_len , subrit_i, step_image[ numdimskip + 1 ] );
    }
  } else {
    bspline_interpolate(ptr_out_r, &transformed[0], &interpolators[0], ndims_in - numdimskip, ptr_map_position+ 2*numdimskip, vec_len , subrit, step_image[ numdimskip + 1 ] ) ;
    if (iscomplex) {
        transformed[ ndims_in- numdimskip ] = ptr_in_i;

        interpolators[ ndims_in-1 -numdimskip ] = newVector_interpolateAdj( transformed[ndims_in-1- numdimskip + 1 ], step_C[ ndims_in-1], size_C[ ndims_in-1], bsplineordT); 

        subRitT subrit_i( ptr_out_i, steps, clips_o );
        
        bspline_interpolate(ptr_out_i, &transformed[0], &interpolators[0], ndims_in - numdimskip, ptr_map_position+ 2*numdimskip, vec_len , subrit_i, step_image[ numdimskip + 1 ] ) ;
        
    }
  }
#ifdef ADDGUARDS 

  tmp.checkGuard( transformed , ndims_in- numdimskip+1 );
  for (int dim = ndims_in-1 ; dim>=numdimskip; dim-- ) { 
    tmp.checkGuard( transformed[ dim - numdimskip ], step_C[ dim ] );
  }
#endif
#ifdef PERFORMANCECOUNTING
  timeCntT tend_processing = __rdtsc(); 
  time_used[1] = tend_processing - tend_initialization;
#endif
  if (tempspacebytesneeded>0) {
    mxFree(tempspaceptr);
  }
#ifdef PERFORMANCECOUNTING
  timeCntT tend_mexfunction = __rdtsc(); 
  time_used[2] = tend_mexfunction - tend_processing;
  for (int k = 0 ; k<2; ++k) {
    ptr_out_r[k] = (double) time_used[k];
  }
#endif
#endif	// end of else of ifdef BUILDTESTFUN_TRANSFORMCOREGENERAL
}
#endif // end of ifndef NOMEXFUN
