function [filt,filtdelay] = filterFromBank(F, delay)
% [filt] = filterFromBank(F)
% OR
% [filt,filtdelay] = filterFromBank(F, delay)
% Simple function to extract the filter from a filterbank 
% F should be created with (create)FilterBank
% filtdelay is the (real valued) delay of filt (in samples). 
% delay is the delay of the individual filters (as computed by (create)FilterBank). 
%
% Created by Dirk Poot, University of Antwerp
% 14-1-2010

filt = [reshape(F(:,end:-1:2)',[],1);F(end,1)];
if nargin>1 
    if numel(delay)~=size(F,2) || delay(end)-delay(1)~=1
        warning('invalid delay of filterbank, computed delay might be wrong');
    end;
    filtdelay = delay(end) * (size(F,2)-1);
else
    filtdelay = (numel(filt)-1)/2;
end;
if nargout<1
    delays = ((0:numel(filt)-1)-filtdelay)/(size(F,2)-1);
    subplot(2,1,1);
    plot(delays,filt);
    subplot(2,1,2);
    ff = abs(fft(filt,goodFFTsize(numel(filt)*3)));
    freq = (0:floor(numel(ff)/2))/numel(ff)*(size(F,2)-1);
    plot(freq, log(abs(ff(1:numel(freq)))/(size(F,2)-1)));
    filt = [];
end;