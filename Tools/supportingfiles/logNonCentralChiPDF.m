function [logP, dLogP, hLogP] = logNonCentralChiPDF( M, A, sigma, L, derivative_selector) 
% [logP, dLogP, d2LogP] = logNonCentralChiPDF( data, A, sigma , L, derivative_selector) 
%
% Computes the non-central Chi PDF. 
% Based on eq4 in Statistical noise analysis in GRAPPA using a parametrized noncentral Chi approximation model.pdf
% MRM65 (2011): 1195, by Aja-Fernandez. 
%
% 21-4-2017: Created by Dirk Poot, Erasmus MC

% sigma = pdfPar(1,:);
% L = pdfPar(2,:);
sigma2 = sigma.^2;

logA = log(A);
logM = log(M);
logSigma2 = log( sigma2 );
Z = A.*M./sigma2;
bessim1 = besseli( L-1, Z , 1);
bessim2 = besseli( L-2, Z , 1);
logI_Lm1 = log( bessim1 ) + abs( real( Z ) );
logI_Lm2 = log( bessim2 ) + abs( real( Z ) );
% logP = (1-L)*logA - log(sigma^2)+ L * logM - (M^2+A^2)/(2*sigma^2) + log( I_[L-1](A*M/(sigma^2)))  for M>=0
logP = (1-L).*logA - logSigma2 + L.*logM - ( M.^2 + A.^2 ) ./ ( 2 * sigma2 ) + logI_Lm1;

if nargout>1
    % compute derivative
    % see nonCentralChi_derivatives.nb
    if nargin<4
        derivative_selector = true(1,4);
    elseif ~islogical(derivative_selector) || numel(derivative_selector)~=4
        error('incorrect selGrHess, should be 3 element logical vector.');
    end
    % derivative wrt M:
    if derivative_selector(1)
        dlogPdM = reshape( 1./M - M./sigma2 + A./sigma2 .* bessim2./bessim1 ,[],1);
    else
        dlogPdM = [];
    end;
    % derivative wrt A:
    if derivative_selector(2)
        dlogPdA = reshape( (2-2*L)./A - A./sigma2 + M./sigma2 .* bessim2./bessim1 , [], 1);
    else
        dlogPdA = [];
    end;
    % derivative wrt sigma:
    if derivative_selector(3)
        dlogPdsigma = reshape( ( (A./sigma).^2 + (M./sigma).^2 + 2*(L-2)- 2*Z.* bessim2./bessim1 )./sigma ,[], 1) ;
    else
        dlogPdsigma = [];
    end;
    dLogP = [ dlogPdM dlogPdA dlogPdsigma ];
    
    if nargout>2
        bessi = besseli( L, Z , 1);
        if derivative_selector(1)
            dlogPdMM = (1-2*L)./M.^2+(A-sigma).*(A+sigma)./sigma2.^2 + A./M.* bessi .*( (1-2*L) .* bessim1 - Z .* bessi)./ (sigma.^2 .*bessim1.^2);
        end;
        if derivative_selector(2)
            dlogPdAA = (M-sigma).*(M+sigma)./sigma2.^2 + M./A.* bessi .*( (1-2*L) .* bessim1 - Z .* bessi)./ (sigma.^2.* bessim1.^2);
        end;
        
        if isequal(derivative_selector,[true false false false])
            hLogP = dlogPdMM(:);
        elseif isequal(derivative_selector,[false true false false])
            hLogP = dlogPdAA(:);
        else
            error('hessian with this selGrHess pattern not yet supported');
        end;
    end;
end;

function test()
%% Test if equal to rice (should be when L==1)
Xv = linspace(0,10,9);
Av = linspace(0,10,12);
sv = [.7 1 1.8754];
[X,A,s] = ndgrid(Xv,Av,sv);
[pR,dpR,hpR] = logricepdf( X,A,s, [true false false]);
L=1; % for L==1, nonCentralChi is equal to rice
[p,dpX,hpX] = logNonCentralChiPDF( X,A,s, L, [true false false false]);
imagebrowse(cat(5,pR, p),[-100 0])
imagebrowse(reshape( cat(5,dpR,dpX), [size(p), 2]),[-10 10])
plot(dpR-dpX)
imagebrowse(reshape( cat(5,hpR,hpX), [size(p), 2]),[-10 10])
plot(hpR-hpX)
%% different derivative
[pR,dpR,hpR] = logricepdf( X,A,s, [false true false]);
L=1; % for L==1, nonCentralChi is equal to rice
[p,dpX,hpX] = logNonCentralChiPDF( X,A,s, L, [false true false false]);
imagebrowse(cat(5,pR, p),[-100 0])
imagebrowse(reshape( cat(5,dpR,dpX), [size(p), 2]),[-10 10])
plot(dpR-dpX)
imagebrowse(reshape( cat(5,hpR,hpX), [size(p), 2]),[-10 10])
plot(hpR-hpX)

%% test higher L
step = .1;
X = step/2 : step : 50;
A = 0.01;
sigma = 1;
L = 10; 
[p,dpX,hpX] = logNonCentralChiPDF( X,A,sigma, L, [true false false false]);
[pA,dpA,hpA] = logNonCentralChiPDF( X,A,sigma, L, [false true false false]);
% integrates to 1?
sum(exp(p))*step
% has clear offset?
plot(X,[exp(p)' exp(pA)'])
%% check derivative (wrt X):
delta = .0001;
[ph,dph] = logNonCentralChiPDF( X+delta,A,sigma, L, [true false false false]);
[pl,dpl] = logNonCentralChiPDF( X-delta,A,sigma, L, [true false false false]);
dp_num = (ph-pl)/(2*delta);
plot(X', [dpX, dp_num']*[1 0 1;0 1 -1])
title('Derivative wrt X')
legend('analytical derivative','numerical derivative', 'difference')
%% check second derivative (wrt X):
hp_num = (dph-dpl)/(2*delta);
plot(X', [hpX, hp_num]*[1 0 1;0 1 -1])
title('hessian wrt X')
legend('analytical hessian','numerical hessian', 'difference')
%% check derivative (wrt A):
delta = .0001;
[ph,dph] = logNonCentralChiPDF( X,A+delta,sigma, L, [false true false false]);
[pl,dpl] = logNonCentralChiPDF( X,A-delta,sigma, L, [false true false false]);
dp_num = (ph-pl)/(2*delta);
plot(X', [dpA, dp_num']*[1 0 1;0 1 -1])
title('Derivative wrt A')
legend('analytical derivative','numerical derivative', 'difference')
%% check second derivative (wrt X):
hp_num = (dph-dpl)/(2*delta);
plot(X', [hpA, hp_num]*[1 0 1;0 1 -1])
title('hessian wrt A')
legend('analytical hessian','numerical hessian', 'difference')
