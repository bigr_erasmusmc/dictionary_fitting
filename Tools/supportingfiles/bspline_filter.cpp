#ifndef BSPLINE_PROC_CPP
#define BSPLINE_PROC_CPP
/* This file provides a set of classes dealing with b-splines and interpolation.
 * There are 3 different 'things' in this file:
 *   'coefficient' classes/methods ('push') : for a given location (0<= location < 1) store (output iterator) a list of coefficient of the filter.
 *   'filter' classes/methods      ('pull') : prepare for a given location (0<= location < 1)
 *											: then get iterator
 *   sample_prepared  : class that does the sampling 
 *								
 *
 * 
 * Test code by building: mex bspline_filter.cpp -DBUILDTESTFUN_BSPLINE_PROC -v -I"./Tools/supportingfiles/" CXXFLAGS="\$CXXFLAGS -march=barcelona"
 *           and call   : a = bspline_filter();
 *			 Inspect a for correct results. See self_test_mex for explaination of what to expect.
 *           Don't define 'BUILDTESTFUN_BSPLINE_PROC' when including this from external code.
 * 
 * class bSplineCoeffs is a static function class templated over bspline order and type of the returned coefficients.
 *   It computes bSpline coefficients.
 *   Typically you want to use a wrapped version, such as described below.
 * 
 * class coefficients_base, templated over filterType, is a base class for obtaining 
 *   filter coefficients. Use bspline_sampling_coefficients for a specific set of coefficients.
 *
 * class bspline_sampling_coefficients, a class wrapper around bSplineCoeffs, which is derived from 
 *   coefficients_base. Templated over bspline order and filterType.
 * 
 * function newbSplineSamplingCoefficients( order ): returns a bspline_sampling_coefficients object with the specified b-spline order.
 * 
 * class filter_base, templated over fitlertype, is a pure virtual base class that allows preparing coefficients
 *   and subsequently indexed access to these. Derived classes will implement a cache for the coefficients to allow quick access.
 * 
 * class bSplineFilter is a wrapper class around bSplineCoeffs, derived from filter_base.
 *   Thus it allows preparing coefficients and sampling source data with these coefficients. Still a quite
 *   raw class templated over b-spline length and coefficientType. 
 *
 * function newbSplineFilter( order ): returns a bSplineFilter with the specified b-spline order.
 *
 * class sample_prepared: a static class that allows sampling with a prepared 'filter_base'
 *   Samples one sample or a vector, allows specification of stride and step.
 * 
 * class sampleVect allows to interpolate a collection of vectors with any 'filter_base'
 *   bSplineSampleVect<[bsplineorder=-1,] datatype>( source , step , length, stride, nvectors [, bsplineorder] )
 *   The method sample( * out, position )
 *   samples the input vectors around the column specified by position.
 *   inputs:
 *		 step   : the step (in number of elements) from 1 element of a vector to the next.
 *		 length : Number of elements in each vector.
 *		 stride : the step (in number of elements) from 1 vector to the next.
 *		 nvectors: number of vectors given.
 *       bsplineorder: order of the b-spline that is used.
 *   If you don't want to store the entire interpolated vector, you can use the version templated over bsplineorder.
 *   get subtype() and use specialization? where '?' is the value of subtype.
 *	 and call prepare( position ) 
 *		before calling sample( i ) to get element i of the interpolated vector.
 * 
 *  class filter: a filterbank that can be interpolated with bsplines.
 *      determine filter.subtype() to determine which specialization should be called:
 *      e.g. for subtype()==2:
 *      filter::specialization2 spec = filter::specialization2(filter);
 *      spec.prepare( loc ) , spec.prepareDerivative( loc ), spec.prepareValueAndDerivative( loc )
 *      note important!!:  0 <= loc < 1
 *      and then: spec.sample[Derivative]( i ) to get interpolated [partial derivative w.r.t. loc of] element i.
 *      with 0 <= i < filter.length()
 *
 *  class NDimage: image type, templated over data type and dimensionality. Can be sampled by a sampler e.g. a bspline sampler.
 *  
 *  See  function 'self_test_mex' for example usage.
 * 
 * Created by Dirk Poot, Erasmus MC
 */

/* B-spline interpolation in columns of F. 
*/
#include "math.h"
#include <algorithm>
#include <iterator>
#ifdef BUILDTESTFUN_BSPLINE_PROC
    #include "mex.h"
#endif
#include "emm_vecptr.hxx"
#include "emm_vec.hxx"
#include "lineReadWriters.cpp"
#pragma message("compiling (/preprocessing) bspline_filter.cpp")
        
/* bSplineCoeffs is the core class that computes the b-spline coefficients.
 * bSplineCoeffs provides the following methods, the first 3 templated over output_iterator type:
 *    void get_coefficients( output_iterator, sample_location )  
 *    void get_coefficientsDerivative( output_iterator, sample_location , scale)
 *    void get_coefficientsAndDerivative( output_iterator_value, output_iterator_Derivative, sample_location , scale)
 *    length()     : length of the filter (number of coefficients)
 *    delay()      : delay of the filter (in # samples).
 * And the following static information:
 *    fixedlength  : length of the filter (number of coefficients)
 *    value_type   : type of the elements
 * As for all filters:   0 <= sample_location <1
 * scale = scale factor for the derivative (for continued derivatives) 
 */
template <int bsplineord, typename filterType> class bSplineCoeffs ;


/* fixedLengthCoefficients_base
 *  Base class for all 'coefficient' (i.e. push) classes
 *
 *  This is a fully static template class that does the general stuff for 
 *   all b-spline coefficients of the different order.
 *  Provides a default implementation of get_coefficientsAndDerivative
 *
 * fixedLengthCoefficients_base is templated over the child type so the exact full type 
 * is known at compile time to allow maximum optimization. 
 */

template <typename child, typename valueType> class fixedLengthCoefficients_base { 
public:
	typedef valueType value_type;
    // Default implementation of get_coefficientsAndDerivative
    // Calling separate functions of child. 
	template <typename output_iterator> 
            static inline void get_coefficientsAndDerivative( output_iterator value , output_iterator derivative, value_type location, value_type scale ) {
		child::get_coefficients( value, location );
		child::get_coefficientsDerivative( derivative, location , scale);
	}
    
    // (Default implementation of) multi-location versions; 
    // Calling single location functions of child. 
    template <typename output_iterator, typename input_iterator> 
            static inline void get_coefficients( output_iterator value, input_iterator locationIterator, input_iterator locationIteratorEnd ) {
        for ( ; locationIterator != locationIteratorEnd; ++locationIterator, value += child::fixedlength ) {
            child::get_coefficients( value, *locationIterator );
        }
    }
    template <typename output_iterator, typename input_iterator> 
            static inline void  get_coefficientsDerivative( output_iterator derivative, input_iterator locationIterator, input_iterator locationIteratorEnd, valueType scale) {
        for ( ; locationIterator != locationIteratorEnd; ++locationIterator, derivative += child::fixedlength ) {
            child::get_coefficientsDerivative( derivative, *locationIterator, scale );
        }
    }
    template <typename output_iterator, typename input_iterator> 
            static inline void  get_coefficientsAndDerivative( output_iterator value, output_iterator derivative, input_iterator locationIterator, input_iterator locationIteratorEnd, valueType scale)  {
        for ( ; locationIterator != locationIteratorEnd; ++locationIterator, value += child::fixedlength, derivative += child::fixedlength ) {
            child::get_coefficientsAndDerivative( value, derivative, *locationIterator, scale );
        }
    }    

    template <typename index_output_iterator, typename output_iterator, typename input_iterator> 
            static inline void get_indexAndCoefficients( index_output_iterator start_index, output_iterator value, input_iterator locationIterator, input_iterator locationIteratorEnd ) {
        typedef typename iterator_traits< input_iterator >::value_type position_value_type;
        typedef typename iterator_traits< index_output_iterator >::value_type index_value_type;
        for ( ; locationIterator != locationIteratorEnd; ++locationIterator, ++start_index, value += child::fixedlength ) {
            position_value_type  position_value  = *locationIterator - delay();
            index_value_type     position_index  =  floor( position_value );
                                 *start_index    =  position_index ;
			                     position_value -=  position_index;
            child::get_coefficients( value, position_value );
        }
    }

    // General interface methods, length and delay. 
	static inline int length() {
		return child::fixedlength;
	}
	static inline value_type delay() {
		return ((value_type) length() ) / 2. -1;
	}
};


template <typename filterType> class bSplineCoeffs<0,filterType> : public fixedLengthCoefficients_base< bSplineCoeffs<0,filterType>, filterType > {
    /* bspline order 0 implementation:
     */
public:
	typedef filterType value_type;
	enum {fixedlength = 1};
	template <typename output_iterator> static inline void get_coefficients( output_iterator bsplcoeffs, filterType location ) {
		*bsplcoeffs=1;
	}
	template <typename output_iterator> static inline void get_coefficientsDerivative( output_iterator bsplcoeffsD, filterType location , filterType scale) {
		*bsplcoeffsD=1;
		mexErrMsgTxt("Can't compute derivative of 0 order b-spline.");
	}
};

template <typename filterType> class bSplineCoeffs<1,filterType> : public fixedLengthCoefficients_base< bSplineCoeffs<1,filterType>, filterType> {
    /* bspline order 1 implementation:
     */
public:
	typedef filterType value_type;
	enum {fixedlength = 2};
	template <typename output_iterator> static inline void get_coefficients( output_iterator bsplcoeffs, filterType location ) {
		*bsplcoeffs = 1-location; ++bsplcoeffs;
		*bsplcoeffs =   location;
	}
	template <typename output_iterator> static inline void get_coefficientsDerivative( output_iterator bsplcoeffsD, filterType location , filterType scale) {
		*bsplcoeffsD = - scale; ++bsplcoeffsD;
		*bsplcoeffsD =   scale;
	}
};
template <typename filterType> class bSplineCoeffs<2,filterType> : public fixedLengthCoefficients_base< bSplineCoeffs<2,filterType>, filterType >  {
    /* bspline order 2 implementation:
     */
public:
	typedef filterType value_type;
	enum {fixedlength = 3};
	template <typename output_iterator> static inline void get_coefficients( output_iterator bsplcoeffs, filterType location ) {
		filterType locationr = 1-location;
		*bsplcoeffs = .5*locationr*locationr;  ++bsplcoeffs;
		*bsplcoeffs = .5+ location *locationr; ++bsplcoeffs;
		*bsplcoeffs = .5* location*location;
	}
	template <typename output_iterator> static inline void get_coefficientsDerivative( output_iterator bsplcoeffsD, filterType location , filterType scale) {
		filterType locationr = 1-location;
		*bsplcoeffsD = - locationr * scale;            ++bsplcoeffsD;
		*bsplcoeffsD =  (locationr -location)* scale;  ++bsplcoeffsD;
		*bsplcoeffsD =  location * scale;
	}
};

template <typename filterType> class bSplineCoeffs<3,filterType> : public fixedLengthCoefficients_base< bSplineCoeffs<3,filterType> , filterType> {
    /* bspline order 3 implementation:
     */
public:
	typedef filterType value_type;
	enum {fixedlength = 4};
	template <typename output_iterator> static inline void get_coefficients( output_iterator bsplcoeffs, filterType location ) {
		filterType locationr = 1-location;
		filterType sqr_location  = location *location;
		filterType sqr_locationr = locationr*locationr;

		*bsplcoeffs = (1. /6.) * sqr_locationr * locationr;				++bsplcoeffs;
		*bsplcoeffs = (1. /6.) * (4-3*sqr_location *(1 + locationr) );  ++bsplcoeffs;
		*bsplcoeffs = (1. /6.) * (4-3*sqr_locationr*(1 + location ) );  ++bsplcoeffs;
		*bsplcoeffs = (1. /6.) * sqr_location  * location ;
	}
	template <typename output_iterator> static inline void get_coefficientsDerivative( output_iterator bsplcoeffsD, filterType location , filterType scale) {
		filterType locationr = 1-location;
		filterType sqr_location  = location *location;
		filterType sqr_locationr = locationr*locationr;
		scale *= 0.5;

		*bsplcoeffsD = -scale * sqr_locationr ;						 ++bsplcoeffsD;
		*bsplcoeffsD = -scale * (4 *location  - 3 * sqr_location );  ++bsplcoeffsD;
		*bsplcoeffsD =  scale * (4 *locationr - 3 * sqr_locationr);  ++bsplcoeffsD;
		*bsplcoeffsD =  scale * sqr_location  ;
	}
};

template <int bsplineord, typename filterType> class omomsCoeffs ;

template <typename filterType> class omomsCoeffs<2,filterType> : public fixedLengthCoefficients_base< omomsCoeffs<2,filterType>, filterType >  {
    /* omomse order 2 implementation:
     */
public:
	typedef filterType value_type;
	enum {fixedlength = 3};
	template <typename output_iterator> static inline void get_coefficients( output_iterator bsplcoeffs, filterType location ) {
        // this is bspline order 2 + 1/60 second derivative bspline order 2
		filterType locationr = 1-location;
		*bsplcoeffs = .5*locationr*locationr  + (1./60.); ++bsplcoeffs;
		*bsplcoeffs = .5+ location *locationr - (2./60.); ++bsplcoeffs;
		*bsplcoeffs = .5* location*location   + (1./60.);
	}
	template <typename output_iterator> static inline void get_coefficientsDerivative( output_iterator bsplcoeffsD, filterType location , filterType scale) {
		mexErrMsgTxt("Cant compute derivative of 2 order omoms (function discontinous at some points).");
	}
};

template <typename filterType> class omomsCoeffs<3,filterType> : public fixedLengthCoefficients_base< bSplineCoeffs<3,filterType> , filterType> {
    /* omoms order 3 implementation:
     */
public:
	typedef filterType value_type;
	enum {fixedlength = 4};
	template <typename output_iterator> static inline void get_coefficients( output_iterator bsplcoeffs, filterType location ) {
        // this is bspline order 3 + 1/42 second derivative bspline order 3
		filterType locationr = 1-location;
		filterType sqr_location  = location *location;
		filterType sqr_locationr = locationr*locationr;

		*bsplcoeffs = (1. /6.) * sqr_locationr * locationr            + (1./42.)*locationr;         ++bsplcoeffs;
		*bsplcoeffs = (1. /6.) * (4-3*sqr_location *(1 + locationr) ) + (1./42.)*(3*location-2);    ++bsplcoeffs;
		*bsplcoeffs = (1. /6.) * (4-3*sqr_locationr*(1 + location ) ) + (1./42.)*(1-3*location);    ++bsplcoeffs;
		*bsplcoeffs = (1. /6.) * sqr_location  * location             + (1./42.)*location;
	}
	template <typename output_iterator> static inline void get_coefficientsDerivative( output_iterator bsplcoeffsD, filterType location , filterType scale) {
        // note derivative is not continuous (but finite)
		filterType locationr = 1-location;
		filterType sqr_location  = location *location;
		filterType sqr_locationr = locationr*locationr;
		scale *= 0.5;

		*bsplcoeffsD = -scale * sqr_locationr                      - (1./42.);	++bsplcoeffsD;
		*bsplcoeffsD = -scale * (4 *location  - 3 * sqr_location ) + (3./42.);  ++bsplcoeffsD;
		*bsplcoeffsD =  scale * (4 *locationr - 3 * sqr_locationr) - (3./42.);  ++bsplcoeffsD;
		*bsplcoeffsD =  scale * sqr_location                       + (1./42.);
	}
};

// Define templated function class to sample with a given filter and source data. 
// optimized to use vectorizing when possible.
// Also supports sampling a vector (with the same coefficients). 

template < typename value_type> struct valAndDeriv {
    // super tiny helper class to store a value and derivative.
public:
	value_type val;
	value_type deriv;
	valAndDeriv(value_type val_, value_type deriv_): val(val_), deriv(deriv_) {};
};
/* // some template definitions for the template meta programming required:
// IF, use as : IF< boolean_value , Then, Else>::RET
template <bool condition, class Then, class Else> struct IF {
    typedef Then RET;
};
template <class Then, class Else> struct IF<false, Then, Else> {
    typedef Else RET;
};
// AND, use as: AND< boolean_valueA, boolean_valueB>::RET
template <bool conditionA, bool conditionB> struct AND {
	enum {RET = false};
};
template <> struct AND< true, true > {
	enum {RET = true};
};
// A dummy/signaling type that should not be actually used. Allow casting of any type to BAD_TYPE.
struct BAD_TYPE { 
	template < typename anyType> BAD_TYPE( anyType dummy) {
		mexErrMsgTxt("Don't create any objects of 'BAD_TYPE'.");
	};
};
class empty_type { ; }; // helper class for ISEMPTY function
// ISEMPTY, use as ISEMPTY< some_type >::RET
// returns true if some_type is empty_type.
template< typename T > struct ISEMPTY {
	enum {RET=false};};
template<> class ISEMPTY<empty_type> {
	enum {RET=true};};*/ 
// Some garbage functions to get every end case to compile:
template <int vlen> struct vec<BAD_TYPE, vlen> {
	enum { naturalLen =0 };
};
template <typename T1, typename T2> struct vecptrhelper_c {
	static inline T1 get( T1 in, T2 in_vec ) {
		return in_vec.getRawPointer();
	}
};
template <typename T1 > struct vecptrhelper_c<T1 , BAD_TYPE >{
static inline T1 get( T1 in, BAD_TYPE in_vec ) {
		return in;
	}
};
inline double sum( BAD_TYPE v) {
	return 0.;
};
template <typename T1, typename T2> inline T1 vecptrhelper(T1 in, T2 in_vec) {
	return vecptrhelper_c<T1, T2>::get(in, in_vec);
}




using std::iterator_traits;
template < typename int_t, typename itType1 , typename itType2, bool mayVectorise = true> class sample_prepared {
    /* a static class that performs sampling 
     * Samples one sample or a vector, allows specification of stride and step.
     * Tries to vectorize the computations (when mayVectorise==true).
     * Basically is a (non reversed) convolution.
     *
     * Pseudo code of algorithm
     * out = sample_prepared< integer_type, iterator_in_type, iterator_F_type, request_vectorizing >::value( in, F, n)
     *     out = 0;
     *     for( ;n>0; --n, ++in, ++F)
     *          out += (*in) * (*F)
     * value method is also available when in is a set of vectors that need to be interpolated:
     * value( in, F, out, length_vec, length_filt, stepin, stride) 
     *      for(; length_vec>0; ++out, in+=stepin)
     *          tmp = 0     
     *          for(l=length_vec, inl=in, Fl=F; l>0; --l, inl+= stride, ++F)
     *               tmp += (*inl) * (*Fl)
     *          *out = tmp
     * out = sample_prepared< .. >::derivative( in, F, n)
     *     out = 0;
     *     for( ;n>0; --n, ++in, ++F)
     *         out += (*in) * (*F.derivative);
     *
     * the adjoint methods evaluate the adjoint operation of 'value':
     * adjoint( out, F, val, n) 
     *     for(; n>0; ++out, ++F)
     *         *out+= val * (*F);
     * 
     */
	// if itType1 and itType2 are both supported by vec (and mayVectorise==true) then 
	// in wrap in and F (or out) with vec and call again.
public:
	typedef sample_prepared<int_t, itType1, itType2, mayVectorise> Self;

	typedef typename iterator_traits<itType1>::value_type procType; 
	enum {vlen = vec<procType,16>::naturalLen};
	enum {doVectorize = false && mayVectorise && AND< vecptr<itType1,vlen>::supported, vecptr<itType2,vlen>::supported>::RET};
	typedef typename IF< doVectorize  , vecptr<itType1,vlen>, BAD_TYPE>::RET vecItType1;
	typedef typename IF< doVectorize  , vecptr<itType2,vlen>, BAD_TYPE>::RET vecItType2;

	typedef sample_prepared<int_t, vecItType1, vecItType2, mayVectorise> vecSelf;

	typedef procType value_type; 
	typedef procType derivative_type; 
	typedef valAndDeriv<procType> valueAndDerivative_type;
	typedef typename vecSelf::value_type vec_value_type;
	typedef typename vecSelf::derivative_type vec_derivative_type; 
	typedef typename vecSelf::valueAndDerivative_type vec_valueAndDerivative_type; 

	static inline value_type value( itType1 in, itType2 F, int_t nToDo) {
		// in and F should be input iterator type.
		procType tempOut; 
		if (doVectorize) {
			int_t nToDov = nToDo/vlen;
			vecItType1 in_vec(in);
			vecItType2 F_vec(F);
			vec_value_type tmp = vecSelf::value( in_vec, F_vec, nToDov );
			in = vecptrhelper(in, in_vec);
			F = vecptrhelper(F, F_vec);
			nToDo -= nToDov*vlen;
			tempOut = sum( tmp );
		} else {
			tempOut = (procType) 0.0; 
		}
		for (; nToDo>0; --nToDo, ++in, ++F) {   
			tempOut  += (*in) * (*F);
		}
		return tempOut;
	}
	static inline value_type derivative ( itType1 in, itType2 F, int_t nToDo) {
		// in and F should be input iterator type.
		procType tempOut; 
		if (doVectorize) {
			int_t nToDov = nToDo/vlen;
			vecItType1 in_vec(in);
			vecItType2 F_vec(F);
			vec_derivative_type tmp = vecSelf::derivative( &in_vec, &F_vec, nToDov );
			in = in_vec.ptr;
			F = F_vec.ptr;
			nToDo -= nToDov*vlen;
			tempOut = sum( tmp );
		} else {
			tempOut = (procType) 0.0; 
		}
		for (; nToDo>0; --nToDo, ++in, ++F) {   
			tempOut  += (*in) * (*F.derivative);
		}
		return tempOut;
	}
	static inline valueAndDerivative_type valueAndDerivative( itType1 in, itType2 F, int_t nToDo) {
		// in and F should be input iterator type.
		procType tempOut; 
		procType dtempOut;
		if (doVectorize) {
			int_t nToDov = nToDo/vlen;
			vecItType1 in_vec(in);
			vecItType2 F_vec(F);
			vec_valueAndDerivative_type tmp = vecSelf::valueAndDerivative( &in_vec, &F_vec, nToDov );
			in = in_vec.ptr;
			F = F_vec.ptr;
			nToDo -= nToDov*vlen;
			tempOut = sum( tmp.val );
			dtempOut = sum( tmp.dval );
		} else {
			tempOut = (procType) 0.; 
			dtempOut = (procType) 0.;
		}
		for (; nToDo>0; --nToDo, ++in, ++F) {   
			tempOut  += (*in) * (*F);
			dtempOut += (*in) * (*F.derivative);
		}
		return valueAndDerivative_type( tempOut, dtempOut);
	}
	static inline void adjoint( itType1 out, itType2 F, value_type val, int_t nToDo) {
		// out should be output iterator type and F should be input iterator type.
		if (doVectorize) {
			int_t nToDov = nToDo/vlen;
			vecItType1 out_vec(out);
			vecItType2 F_vec(F);
			vecSelf::adjoint( &out_vec, &F_vec, vec_value_type(val), nToDov );
			out = vecptrhelper(out, out_vec);
			F = vecptrhelper(F, F_vec);
			nToDo -= nToDov*vlen;
		}
		for (; nToDo>0; --nToDo, ++out, ++F) {   
			*out  += (val) * (*F);
		}
	}

	// sample an vector out of an array of vectors:
	//template <typename filtType> inline void operator() ( itType1 in, filtType F, itType2 out, int_t length_vec, int_t length_filt, int_t stepin, int_t stride) {
	template <typename filtType> static inline void value( itType1 in, filtType F, itType2 out, int_t length_vec, int_t length_filt, int_t stepin, int_t stride) {
		// Sample a vector from an array of vectors
		//
		// Elements of the vectors (of in) are separated by stepin.
		// The vectors of in are separated by stride.
		// length_vec specifies number of elements of vector out to compute. These should (obviously) be present in in.
		// length_filt is the length of the prepared filter over which F is an iterator.
		// NOTE: itType1 = input iterator
		//       itType2 = output iterator
		if (doVectorize && stepin==1) {
			int_t length_vec_v = length_vec / vlen;
			int_t stepin_v = 1;//stepin ;
			vecItType1 in_vec(in);
			vecItType2 out_vec(out);
			vecSelf::value( in_vec, F, out_vec, length_vec_v, length_filt, stepin_v, stride);
			in = vecptrhelper(in, in_vec);
			out = vecptrhelper(out, out_vec);
			length_vec -= length_vec_v * vlen;
		}
		for (; length_vec>0; --length_vec, ++out,in+=stepin) {
			typedef lineType< itType1 > stride_in_type;
			typedef sample_prepared<int_t, stride_in_type, itType2, mayVectorise> stridedSelf;
			*out = stridedSelf::value( stride_in_type(in, stride) , F, length_filt );
		}
	}
	template <typename filtType> static inline void adjoint( itType1 out, filtType F, itType2 value, int_t length_vec, int_t length_filt, int_t stepin, int_t stride) {
		// Adjoint sample a vector from an array of vectors
		//
		// Elements of the vectors (of out) are separated by stepin.
		// The vectors of out are separated by stride.
		// length_vec specifies number of elements of vector value to compute. These should (obviously) be present in value.
		// length_filt is the length of the prepared filter over which F is an iterator.
		// NOTE: itType1 = output iterator
		//       itType2 = input iterator
		if (doVectorize && stepin==1) {
			int_t length_vec_v = length_vec / vlen;
			int_t stepin_v = 1;//stepin ;
			vecItType1 in_vec(out);
			vecItType2 out_vec(value);
			vecSelf::adjoint( &in_vec, F, &out_vec, length_vec_v, length_filt, stepin_v, stride);
			out = vecptrhelper(out, in_vec);
			value = vecptrhelper(value, out_vec);
			length_vec -= length_vec_v * vlen;
		}
		for (; length_vec>0; --length_vec, ++value, out+=stepin) {
      typedef lineType<itType1> stride_in_type;
			typedef sample_prepared<int_t, stride_in_type, itType2, mayVectorise> stridedSelf;
			stridedSelf::adjoint( stride_in_type(out, stride) , F, *value, length_filt );
      *value = 0;
		}
	}

};
// end case (used for vecSelf when doVectorize==false)
template < typename int_t, bool mayVectorise> class sample_prepared<int_t, BAD_TYPE, BAD_TYPE, mayVectorise> {
public:
	typedef BAD_TYPE value_type;
	typedef BAD_TYPE derivative_type; 
	typedef BAD_TYPE valueAndDerivative_type;
	static inline value_type value( BAD_TYPE in, BAD_TYPE F, int_t nToDo) { 
		return (value_type) 0.;
	}
  static inline void adjoint( BAD_TYPE out, BAD_TYPE F, value_type val, int_t nToDo) {};
  template <typename filtType> static void value(BAD_TYPE in, filtType F, BAD_TYPE out, int_t length_vec, int_t length_filt, int_t stepin, int_t stride) {};
  template <typename filtType> static inline void adjoint( BAD_TYPE out, filtType F, BAD_TYPE value, int_t length_vec, int_t length_filt, int_t stepin, int_t stride) {};
};






// base class for wrapper that allows run-time class determination of the above static classes:
// still templated over data types.
template <typename filterType_, typename locationType , typename int_t = ptrdiff_t> class coefficients_base {
public:
  typedef filterType_ filterType;
	typedef typename iterator_traits<filterType>::value_type value_type; 
	typedef locationType location_type; 
	enum {fixedlength = -1};
	virtual void get_coefficients( filterType coeffs, location_type location ) = 0;	
    virtual void get_coefficientsDerivative( filterType coeffs, location_type location , value_type scale ) = 0;
	virtual void get_coefficientsAndDerivative( filterType value , filterType derivative, location_type location, value_type scale ) {
		get_coefficients( value, location );
		get_coefficientsDerivative( derivative, location , scale);
	}
	virtual int_t length() =0;
	virtual value_type delay() =0;
};


//template <int bsplineorder, typename filterType> class bspline_coefficients : public coefficients_base<filterType *, filterType> {
template <typename staticbase, typename filterType_ ,typename locationType, typename int_t = ptrdiff_t> class coeffs_static2virtual_wrapper : public coefficients_base<filterType_, locationType, int_t> {
    /* Bspline 'coefficients' in a class derived from a puriform base class.*/
private:
//	typedef bSplineCoeffs< bsplineorder, filterType> staticbase;
public:	
    typedef filterType_ filterType;
	typedef typename iterator_traits<filterType>::value_type value_type; 
	enum {fixedlength = staticbase::fixedlength};
	void get_coefficients( filterType  coeffs, locationType location ) {
		staticbase::get_coefficients( coeffs, location );
	}
	void get_coefficientsDerivative( filterType coeffs, locationType location ,  value_type scale ) {
		staticbase::get_coefficientsDerivative( coeffs, location , scale);
	}
	void get_coefficientsAndDerivative( filterType  value, filterType  derivative, locationType location ,  value_type scale ) {
		staticbase::get_coefficientsAndDerivative( value, derivative, location , scale );
	}
	int_t length() {
		return staticbase::length();
	}
	inline locationType delay() {
		return staticbase::delay();
	}
};

template <int bsplineorder, typename filterType,typename locationType, typename int_t = ptrdiff_t> class bspline_coefficients : public coeffs_static2virtual_wrapper< bSplineCoeffs< bsplineorder, locationType> , filterType , locationType, int_t> 
{ // implementation entirely in base class; this is just a convenience rename to provide virtual functions for the bSplineCoeffs static class
};
template <int bsplineorder, typename filterType,typename locationType, typename int_t = ptrdiff_t> class omoms_coefficients : public coeffs_static2virtual_wrapper< omomsCoeffs< bsplineorder, locationType > , filterType, locationType , int_t> 
{ // implementation entirely in base class; this is just a convenience rename to provide virtual functions for the omomsCoeffs static class
};

// function to create class of arbitrary order b-spline:
template <typename filterType, typename int_t> coefficients_base<filterType * , filterType, int_t> * newbSplineSamplingCoefficients( int order ) {
	if (order==0) {
		return new bspline_coefficients<0, filterType * ,filterType, int_t>();
	} else if (order==1) {
		return new bspline_coefficients<1, filterType * ,filterType, int_t>();
	} else if (order==2) {
		return new bspline_coefficients<2, filterType * ,filterType, int_t>();
	} else if (order==3) {
		return new bspline_coefficients<3, filterType * ,filterType, int_t>();
	} else {
        mexErrMsgTxt("Unsupported b-spline order. Currently only b-spline order 0,1,2 and 3 are supported.");
        return NULL;
    }
};

template <typename filterType, typename int_t> coefficients_base<filterType * , filterType, int_t> * newOMOMSSamplingCoefficients( int order ) {
	if (order==0) {
		return new bspline_coefficients<0, filterType * ,filterType, int_t>();
	} else if (order==1) {
		return new bspline_coefficients<1, filterType * ,filterType, int_t>();
	} else if (order==2) {
		return new omoms_coefficients<2, filterType * ,filterType, int_t>();
	} else if (order==3) {
		return new omoms_coefficients<3, filterType * ,filterType, int_t>();
	} else {
        mexErrMsgTxt("Unsupported OMOMS order. Currently only OMOMS order 0,1,2 and 3 are supported (0 and 1 are equal to b-spline with order 0 and 1).");
        return NULL;
    }
};

// iterator class we need for filters:
template <typename objType> class filter_iterator_base : public std::iterator< std::random_access_iterator_tag , typename objType::value_type > {
private:
	typedef filter_iterator_base<objType> self;
	typedef typename objType::value_type value_type;
	objType & obj;
	int curindex;
public:
	filter_iterator_base(objType & obj_, int index=0) : obj(obj_), curindex(index) { ; } ;
	inline bool operator==(const self & b) const {
		return (obj==b.obj) && (curindex == b.curindex);
	};
	inline bool operator!=(const self & b) const {
		return (obj != b.obj) || (curindex != b.curindex);
	};
	inline value_type operator*() {
		return obj[curindex];
	};
	inline int operator++() {
		return ++curindex;
	};
	inline int operator++(int) {
		return curindex++;
	};
	inline int operator+=(int cnt) {
		return curindex+=cnt;
	};
	inline int operator-=(int cnt) {
		return curindex-=cnt;
	};
	inline value_type operator[]( int i ) {
		return obj[curindex+i];
	};
};
// second branch of wrapper; this time the coefficients can be indexed and iterated over.
// These classes allow preparation of the coefficients and (typically) have internal 
// storage to store the prepared coefficients for quick retreival.
// Two base classes provided. The first one has virtual functions, so can be used as basic type
// the second one is just a common-parts base class over which derived classes can be templated (avoids virtual functions).
template <typename filterValType, typename locationType = filterValType> class filter_base {
public:
	typedef filterValType value_type; 
	typedef locationType location_type; 
	typedef filter_base<filterValType, location_type> self;
	typedef filter_iterator_base<self> iterator_type;
	enum {fixedlength = -1};
	virtual void prepare( location_type location ) = 0;	
    virtual void prepareDerivative( location_type location , value_type scale ) = 0;
	virtual void prepareValueAndDerivative( location_type location , value_type scale ) {
		prepare( location );
		prepareDerivative( location , scale );
	}
	virtual value_type operator[]( int i ) =0;
	virtual iterator_type begin() {
		return iterator_type( *this, 0);
	}
    /*virtual iterator_type beginDerivative() {
        return &bsplcoeffsD[0];
    }*/
	virtual int length() =0;
	virtual location_type delay() =0;
};

template <typename derivedType, typename valueT> class filter_base_no_virtual {
public:
	typedef valueT value_type;
	typedef filter_base_no_virtual<derivedType, valueT> self;
	typedef filter_iterator_base<derivedType> iterator_type;
	iterator_type begin() {
		return iterator_type( static_cast<derivedType*>(this), 0);
	}
};

// forward declaration of bSplineFilter.
template <int bsplineord, typename filterValType, bool virtualbase = false > class bSplineFilter;
// bSplineFilter_Thelper: empty helper class to allow the template option virtualbase, 
// which selects the base class (with virtual functions or not) from which bSplineFilter  
// is derived.
template <int bsplineord, typename filterValType, bool virtualbase > struct bSplineFilter_Thelper {
	typedef filter_base< filterValType, filterValType > parent_virtual;
	typedef filter_base_no_virtual< bSplineFilter<bsplineord, filterValType, virtualbase >, filterValType > parent_novirtual;
	typedef typename IF< virtualbase , parent_virtual , parent_novirtual >::RET parent;
    
};
template<bool Cond, class T = void> struct enable_if {};
template<class T> struct enable_if<true, T> { typedef T type; };

template < typename T, bool virtualbase> struct makeiterator {
    static inline typename T::iterator_type make(const T & t) {
        return typename T::iterator_type( (typename T::baseType &) t , 0 );
    }
};
template < typename T> struct makeiterator<T, false> {
    static inline typename T::iterator_type make(const T & t) {
        return t.curcoeffs;
    }
};

template <int bsplineord, typename filterValType, bool virtualbase > class bSplineFilter : public bSplineFilter_Thelper<bsplineord, filterValType, virtualbase>::parent {
public:
	typedef typename bSplineFilter_Thelper<bsplineord, filterValType, virtualbase>::parent baseType;
	typedef filterValType value_type;
	typedef value_type location_type;
	typedef bSplineFilter<bsplineord, filterValType, virtualbase> self;
	typedef bSplineCoeffs<bsplineord, value_type> staticbase;
	typedef typename IF< virtualbase, typename baseType::iterator_type, value_type *>::RET iterator_type;
	enum {fixedlength = staticbase::fixedlength};
protected:
	value_type bsplcoeffs[ fixedlength ];
	value_type bsplcoeffsD[ fixedlength ];
    value_type * curcoeffs;
    friend struct makeiterator<self, virtualbase>;
private:
    
public:
//	bSplineFilter() {}; // default constructor, call prepare[Derivative] to initialize coefficients.
	inline void prepare( location_type location ) {
		staticbase::get_coefficients( bsplcoeffs, location );
        curcoeffs = &bsplcoeffs[0];
	}
	inline void prepareDerivative( location_type location , filterValType scale ) {
		staticbase::get_coefficientsDerivative( bsplcoeffsD, location , scale);
        curcoeffs = &bsplcoeffsD[0];
	}
	inline void prepareValueAndDerivative( location_type location , filterValType scale ) {
		staticbase::get_coefficientsAndDerivative( bsplcoeffs, bsplcoeffsD, location , scale);
        curcoeffs = &bsplcoeffs[0];
	}
	inline value_type operator[]( int i ) {
		return curcoeffs[i];
	}
    inline value_type getDerivativeElement( int i ) {
        return bsplcoeffsD[i];
    }
	inline int length() {
		return fixedlength;
	}
	inline location_type delay() {
		return staticbase::delay();
	}
	inline iterator_type begin() {
        curcoeffs =&bsplcoeffs[0] ;
		return makeiterator<self, virtualbase>::make( *this );
	}
    inline iterator_type beginDerivative() {
        curcoeffs = &bsplcoeffsD[0];
        return makeiterator<self, virtualbase>::make( *this );
    }
};

// function to create class of arbitrary order b-spline:
template <typename filterType> filter_base<filterType, filterType> * newbSplineFilter( int order ) {
	if (order==0) {
		return new bSplineFilter<0, filterType, true>();
	} else if (order==1) {
		return new bSplineFilter<1, filterType, true>();
	} else if (order==2) {
		return new bSplineFilter<2, filterType, true>();
	} else if (order==3) {
		return new bSplineFilter<3, filterType, true>();
	} else {
        mexErrMsgTxt("Unsupported b-spline order. Currently only b-spline order 0,1,2 and 3 are supported.");
        return NULL;
    }
}

// filterbank_coefficients : can be based on coefficients_base, filter_base, or empty_type : empty implies filter_base_no_virtual to enable iterator
template <typename dataType, typename columnFilterType_, typename baseType = coefficients_base< dataType , typename columnFilterType_::location_type, int_t> , bool check_bounds=false, typename int_t = ptrdiff_t> 
  class filterbank_coefficients : public IF< ISEMPTY< baseType >::RET , 
                                            filter_base_no_virtual< filterbank_coefficients< dataType, columnFilterType_, baseType , check_bounds , int_t>, columnFilterType_  > ,
                                            baseType >::RET {

public:
	typedef typename iterator_traits<dataType>::value_type value_type; 
  typedef          columnFilterType_ columnFilterType;
	typedef typename columnFilterType::location_type location_type;
  
	enum {fixedlength = -1};
private:
	dataType F;
	int_t lengthF;
	int_t nsteps;
	int_t stride;
	dataType curColF;
	columnFilterType columnsampler;

	inline  location_type setcolumn( location_type location ) {
		location *= nsteps;
		if (check_bounds) {
			if ( (location<0) || (location >= nsteps) ) {
				mexErrMsgTxt("Sampling out of bounds.");
			}
		}
		int_t curcol = (int_t) location;
		curColF = F + curcol * stride;
		return location - curcol;
	}
	
public:
	filterbank_coefficients( dataType F_ , int_t length_, int_t stride_, int_t nsteps_, columnFilterType columnsampler_ ) : 
		F(F_), curColF(F_), lengthF(length_) , stride(stride_), nsteps( nsteps_) , columnsampler(columnsampler_) {
	};

	inline void prepare(location_type location) {
		//assert( (location>=0) && (location<=1) );
		columnsampler.prepare( setcolumn( location ) );
	}
	inline void prepareDerivative(location_type location) {
		columnsampler.prepareDerivative( setcolumn( location ) , nsteps );
	}
	inline void prepareValueAndDerivative(location_type location) {
		columnsampler.prepareValueAndDerivative( setcolumn( location ), nsteps );
	}
	inline value_type operator[]( int_t i ) {
		return sample_prepared<int_t, dataType , dataType, true >::sample( lineType<dataType>( curColF + i, stride) , columnsampler.begin(), columnsampler.length() );
		//sample_prepared< dataType, columnFilterType::iterator >
	}

	inline void get_coefficients( dataType coeffs, location_type location ) {
		columnsampler.prepare( setcolumn( location ) );
        typedef typename columnFilterType::iterator_type columnFilterIteratorType;
		sample_prepared<int_t, dataType , dataType, true >::value( curColF, columnsampler.begin(), coeffs, lengthF, columnsampler.length(), 1, stride);
	}
	inline void get_coefficientsDerivative(dataType coeffsD, location_type location , value_type scale ) {
		mexErrMsgTxt("not yet implemented.");
		//columnsampler.prepareDerivative( setcolumn( location ) , nsteps * scale);
		//sample_prepared.derivative( curColF, columnsampler.begin(), coeffsD, lengthF, columnsampler.length(), 1, stride);
	}

	inline int_t length() {
		return lengthF;
	}
	inline value_type delay() {
		return ((value_type) lengthF) / 2.-1;
	}
};

// function to create an instance of a filterbank with arbitrary order b-spline interpolation of the columns:
template <typename filterType> filter_base<filterType, filterType> * newFilterbankFilter( filterType * F_ , int length_, int nsteps_, int bsplineinterporder ) {

	typedef filter_base<filterType, filterType> baseType;
	if ( (bsplineinterporder<0) || (bsplineinterporder>3) ) {
		mexErrMsgTxt("Only b-spline order 0,1,2 and 3 are supported.");
	}

	if (bsplineinterporder==0) {
		typedef bSplineFilter<0, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType *, bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, length_, nsteps_, bSplineT() ) ;
	} else if (bsplineinterporder==1) {
		typedef bSplineFilter<1, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType , bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, length_, nsteps_, bSplineT() ) ;
	} else if (bsplineinterporder==2) {
		typedef bSplineFilter<2, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType , bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, length_, nsteps_, bSplineT() ) ;
	} else if (bsplineinterporder==3) {
		typedef bSplineFilter<3, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType , bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, length_, nsteps_, bSplineT() ) ;
	} 
}
// function to create an instance of a filterbank with arbitrary order b-spline interpolation of the columns:
template <typename filterType, typename int_t> coefficients_base<filterType *, filterType, int_t> * newFilterbankCoefficients( filterType * F_ , int_t length_, int_t nsteps_, int bsplineinterporder ) {
	typedef coefficients_base<filterType *, filterType, int_t> baseType;
	
	int_t stride = length_;
	if (bsplineinterporder==0) {
		typedef bSplineFilter<0, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType *, bSplineT, baseType, false, int_t > filterbankT;
		return new filterbankT( F_ , length_, stride, nsteps_, bSplineT() ) ;
	} else if (bsplineinterporder==1) {
		typedef bSplineFilter<1, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType *, bSplineT, baseType, false, int_t > filterbankT;
		return new filterbankT( F_ , length_, stride, nsteps_, bSplineT() ) ;
	} else if (bsplineinterporder==2) {
		typedef bSplineFilter<2, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType *, bSplineT, baseType, false, int_t > filterbankT;
		return new filterbankT( F_ , length_, stride, nsteps_, bSplineT() ) ;
	} else if (bsplineinterporder==3) {
		typedef bSplineFilter<3, filterType>  bSplineT;
		typedef filterbank_coefficients<filterType *, bSplineT, baseType, false, int_t > filterbankT;
		return new filterbankT( F_ , length_, stride, nsteps_, bSplineT() ) ;
	} else {
		mexErrMsgTxt("Unsupported b-spline order. Currently only b-spline order 0,1,2 and 3 are supported.");
        return NULL;
	}
}


// vector_interpolate : can be based on coefficients_base, filter_base, or empty_type : empty implies filter_base_no_virtual to enable iterator
template <typename dataType_, typename columnFilterType_, typename baseType = coefficients_base< dataType_ , typename columnFilterType_::location_type>, bool check_bounds = false, typename int_t = ptrdiff_t > 
  class vector_interpolate: public IF< ISEMPTY< baseType >::RET , filter_base_no_virtual< vector_interpolate< dataType_, columnFilterType_, baseType ,check_bounds>, columnFilterType_ > , baseType >::RET {
public:
  typedef dataType_ dataType;
	typedef typename iterator_traits<dataType>::value_type value_type; 
	typedef          columnFilterType_ columnFilterType;
  typedef typename columnFilterType::location_type location_type;
	enum {fixedlength = -1};
protected:
	dataType F;
	int_t lengthF;
	int_t ncolumns;
	int_t stride;
	dataType curColF;
	columnFilterType columnsampler;
#if defined _MSC_VER && _MSC_VER < 1900
    // Hack because Visual Studio does not support default template arguments on (member) functions and gcc does not support explicit template arguments on protected members of base class so needs default. 
    #define DEFAULT_BOUNDS 
    #define DEFAULT_BOUNDS_ARG < true > 
#else
    #define DEFAULT_BOUNDS = true
    #define DEFAULT_BOUNDS_ARG 
#endif
	template< bool check_bounds_  DEFAULT_BOUNDS > inline location_type setcolumn( location_type location ) {
		if (check_bounds_ && check_bounds) {
			if ( (location<0) || (location + columnsampler.length() > ncolumns) ) {
				mexErrMsgTxt("Sampling out of bounds.");
			}
		}
		int curcol = (int) location;
		curColF = F + curcol * stride;
		return location - curcol;
	}
	
public:
	vector_interpolate( dataType F_ , int_t length_, int_t stride_, int_t ncolumns_, columnFilterType columnsampler_ ) : 
		F(F_), curColF(F_), lengthF(length_) , stride(stride_), ncolumns( ncolumns_) , columnsampler(columnsampler_) {
	};

	inline void prepare(location_type location) {
		//assert( (location>=0) && (location<=1) );
		columnsampler.prepare( setcolumn<check_bounds>( location ) );
	}
	inline void prepareDerivative(location_type location) {
		columnsampler.prepareDerivative( setcolumn<check_bounds>( location ) , 1 );
	}
	inline void prepareValueAndDerivative(location_type location) {
		columnsampler.prepareValueAndDerivative( setcolumn<check_bounds>( location ), 1 );
	}
	inline value_type operator[]( int_t i ) {
		return sample_prepared<int_t, dataType , dataType, true >::value( lineType<dataType>( curColF + i, stride) , columnsampler.begin(), columnsampler.length() );
		//sample_prepared< dataType, columnFilterType::iterator >
	}

	inline void get_coefficients( dataType coeffs, location_type location ) {
		columnsampler.prepare( setcolumn<check_bounds>( location ) );
		sample_prepared<int_t, dataType , dataType, false>::value( curColF, columnsampler.begin(), coeffs, lengthF, columnsampler.length(), 1, stride);
	}
	inline void get_coefficientsDerivative(dataType coeffsD, location_type location , value_type scale ) {
		mexErrMsgTxt("not yet implemented.");
//		columnsampler.prepareDerivative( setcolumn<check_bounds>( location ) , ncolumns * scale);
//		sample_prepared<int_t, dataType , dataType, true >::derivative( curColF, columnsampler.begin(), coeffsD, lengthF, columnsampler.length(), 1, stride);
	}
	void get_coefficients( dataType out,  location_type location_origin, location_type location_step , int numsamples ) {
		if ( !(location_origin>=0) || !(location_origin+columnsampler.length()< ncolumns) || !(location_origin+location_step*(numsamples-1)>=0) || !(location_origin+location_step*(numsamples-1) + columnsampler.length() < ncolumns) ) {
			mexErrMsgTxt("Sampling out of bounds.");
		}
		if (lengthF==1) {
			for (int k = 0; k<numsamples; ++k, ++out) {
				location_type location = location_origin + k * location_step;
				columnsampler.prepare( setcolumn<false>( location ) );
				*out = sample_prepared<int_t, dataType , dataType, true>::value< columnFilterType::iterator_type >( lineType<dataType>(curColF, stride).begin(), columnsampler.begin(), columnsampler.length() );
			}
		} else {
			for (int k = 0; k<numsamples; k++, out+= lengthF) {
				location_type location = location_origin + k * location_step;
				columnsampler.prepare( setcolumn<false>( location ) );
				sample_prepared<int_t, dataType , dataType, true >::value< columnFilterType::iterator_type >( curColF, columnsampler.begin(), out, lengthF, columnsampler.length(), 1, stride);
			}
		}
	}
	inline int_t length() {
		return lengthF;
	}
	inline value_type delay() {
		return ((value_type) lengthF) / 2.-1;
	}
};

template <typename baseType > class vector_adjoint_interpolatebase: public baseType { // derive from a coefficients_base
public:
  typedef typename baseType::location_type location_type; 
  typedef typename baseType::value_type value_type;
  typedef typename baseType::filterType filterType;
  virtual void adjoint( location_type location, filterType value) = 0;
};
template <typename baseType > class vector_adjoint_interpolate: public baseType { // derive from a vector_interpolate
public:
  typedef typename baseType::location_type location_type; 
  typedef typename baseType::value_type value_type;
  typedef typename baseType::filterType filterType;
  typedef typename baseType::columnFilterType columnFilterType;
  typedef typename baseType::dataType dataType;

  vector_adjoint_interpolate( filterType F_ , int length_, int stride_, int ncolumns_, columnFilterType columnsampler_ ) : 
		baseType(F_, length_ , stride_,  ncolumns_ , columnsampler_) {
	};
  inline void adjoint( location_type location, filterType value){
		this->columnsampler.prepare( this->setcolumn DEFAULT_BOUNDS_ARG ( location ) );
		sample_prepared<int, dataType , dataType, false>::adjoint( this->curColF, this->columnsampler.begin(), value, this->lengthF, this->columnsampler.length(), 1, this->stride);
  }
};

// function to create an instance of a filterbank with arbitrary order b-spline interpolation of the columns:
template <typename filterType, typename int_t, typename int_columns> coefficients_base<filterType *, filterType, int_t> * newVector_interpolate( filterType * F_ , int_t length_, int_columns ncolumns_, int bsplineinterporder ) {
	typedef coefficients_base< filterType *, filterType, int_t> baseType;
	int_t stride_ =length_;
	if (bsplineinterporder==0) {
		typedef bSplineFilter<0, filterType>  bSplineT;
		typedef vector_interpolate<filterType *, bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else if (bsplineinterporder==1) {
		typedef bSplineFilter<1, filterType>  bSplineT;
		typedef vector_interpolate<filterType *, bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else if (bsplineinterporder==2) {
		typedef bSplineFilter<2, filterType>  bSplineT;
		typedef vector_interpolate<filterType *, bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else if (bsplineinterporder==3) {
		typedef bSplineFilter<3, filterType>  bSplineT;
		typedef vector_interpolate<filterType *, bSplineT, baseType > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else {
		mexErrMsgTxt("Currently unsupported b-spline order. Only b-spline order 0,1,2 and 3 are supported.");
        return NULL;
	}
}

// function to create an instance of a filterbank with arbitrary order b-spline interpolation of the columns:
template <typename filterType> vector_adjoint_interpolatebase<coefficients_base<filterType *, filterType> > * newVector_interpolateAdj( filterType * F_ , int length_, int ncolumns_, int bsplineinterporder ) {
	typedef vector_adjoint_interpolatebase< coefficients_base< filterType *, filterType> > baseType;
	int stride_ =length_;
	if (bsplineinterporder==0) {
		typedef bSplineFilter<0, filterType>  bSplineT;
		typedef vector_adjoint_interpolate< vector_interpolate<filterType *, bSplineT, baseType > > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else if (bsplineinterporder==1) {
		typedef bSplineFilter<1, filterType>  bSplineT;
		typedef vector_adjoint_interpolate< vector_interpolate<filterType *, bSplineT, baseType > > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else if (bsplineinterporder==2) {
		typedef bSplineFilter<2, filterType>  bSplineT;
		typedef vector_adjoint_interpolate< vector_interpolate<filterType *, bSplineT, baseType > > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else if (bsplineinterporder==3) {
		typedef bSplineFilter<3, filterType>  bSplineT;
		typedef vector_adjoint_interpolate< vector_interpolate<filterType *, bSplineT, baseType > > filterbankT;
		return new filterbankT( F_ , length_, stride_, ncolumns_, bSplineT() ) ;
	} else {
		mexErrMsgTxt("Currently unsupported b-spline order. Only b-spline order 0,1,2 and 3 are supported.");
        return NULL;
	}
}

#ifdef BUILDTESTFUN_BSPLINE_PROC
#include "NDimage_new.cpp"

void self_test_mex( mxArray * out ) {
	// Perform some tests to check the code above as thouroughly as possible.
	// Return computed values as we can't independently check them.
	// Out should be an initialized 2 element struct array.
    typedef double filterType;
	typedef filter_base<filterType, filterType> bSplT;
    const int numorders = 4;
	char * fieldnames[numorders] = {"test_bspline0","test_bspline1","test_bspline2","test_bspline3"};
	for (int bsplineord=0;bsplineord<numorders;bsplineord++){
		mxAddField(out , fieldnames[bsplineord]);
	}
	// Test explicit bspline order 0..3
    // fill test_bspline0-4 with the coefficients for locations from 0..1 in nsteps.
	double tstsignal[numorders] = {0,0,0,0};
	for( int bsplineord = 0; bsplineord < numorders; bsplineord++) {
		const int nsteps = 100;
		//bSplT bspl = bSplT( bsplineord );
        bSplT * bspl = newbSplineFilter<filterType>( bsplineord );
		int doderiv=2;
		if (bsplineord==0) doderiv=1;

		for (int doderivative =0; doderivative<doderiv; doderivative++ ) {
			mxArray * testbspl = mxCreateDoubleMatrix( bsplineord+1 , nsteps, mxREAL);
			mxSetField(out, doderivative, fieldnames[bsplineord], testbspl);
			double * testbsplp = mxGetPr( testbspl );
			for (int i =0 ; i<nsteps; i++ ) {
				if (doderivative==0) {
					bspl->prepare( ((double) i) / ((double) nsteps-1) );
				} else {
					bspl->prepareDerivative( ((double) i) / ((double) nsteps-1) , 1);
				}
				for (int j =0; j<=bsplineord ;j++ ){
					*testbsplp = (*bspl)[j];
					testbsplp++;
				}
			}
		}
	}
    
	// create test image
	mxArray * testimg = mxCreateDoubleMatrix( 10 , 10, mxREAL);
	double * testimgp = mxGetPr( testimg );
	ptrdiff_t imgsz[2] = {10,10};
	ptrdiff_t stride[2] = {1,10};
	testimgp[0 + 0* 10] = 1;
	testimgp[0 + 0* 10] = 1;
   
	NDimage<2, double> img = NDimage<2,double>(testimgp, imgsz , stride, 4);
    
	mxAddField(out , "testimg");
	mxSetField(out, 0, "testimg", testimg);
    

	// test interpolation of image 
    
	const int interpszv1 = 20;
	const int interpszv2 = 20;
	mxArray * testimgoutv = mxCreateDoubleMatrix( interpszv1 , interpszv2, mxREAL);
	double * testimgoutvp = mxGetPr( testimgoutv );
    typedef bSplineCoeffs<2, filterType > interpolType;
    interpolType interpolator;
    double point[2];
    for (int k2 = 0; k2<interpszv2; ++k2) {
        for (int k1 = 0; k1<interpszv1; ++k1) {
            point[0] = 0 + k1 * .12;
            point[1] = -1 + k2 * .31;
            *testimgoutvp  = img.sample<double, double , interpolType>( &point[0] , interpolator); 
            ++testimgoutvp;
        }
	}
    /*	
    if (v.subtype()==2) {

		sampleVectT::specialization2 vs = sampleVectT::specialization2( v );
		for (int k=0;k<interpszv; k++) {
			vs.prepare( k*0.1 );
			for (int j = 0 ; j<4; j++ ) {
				testimgoutvp[ k * imgsz[0] + imgsz[0]-4 + j] = 
						vs.sample( j );
			}
		}
	} else {
		mexErrMsgTxt("Subtype 2 expected.");
	}*/
	mxAddField(out , "testinterpolvec");
	mxSetField(out, 0, "testinterpolvec", testimgoutv);		 

/*// test sampling of filterbank:
	typedef filter<double> filterT;
	const int nfsteps = imgsz[1]-2;
	filterT f = filterT(testimgp, imgsz[0], nfsteps, 2);
	// enforce filterbank periodicity constraint:
	for (int k=0;k<2; k++) {
		testimgp[ (k + nfsteps) * imgsz[0] + 0  ] = 0;
		for (int j=0;j<f.length()-1;j++) {
			testimgp[ (k + nfsteps) * imgsz[0] + j +1 ] = testimgp[ (k ) * imgsz[0] + j  ];
		}
	}
	const int interpszf = 100;
	mxArray * testfsamp = mxCreateDoubleMatrix( imgsz[0] , interpszf, mxREAL);
	double * testfsampp = mxGetPr( testfsamp );
	mxArray * testdfsamp = mxCreateDoubleMatrix( imgsz[0] , interpszf, mxREAL);
	double * testdfsampp = mxGetPr( testdfsamp );
	if (f.subtype()==2) {
		filterT::specialization2 fs = filterT::specialization2( f );
		for (int k=0;k<interpszf; k++) {
			fs.prepareValueAndDerivative( (double) k / (double) interpszf );
			for (int j = 0 ; j<fs.length(); j++ ) {
				testfsampp[ k * imgsz[0] + j]  = fs.sample( j );
				testdfsampp[ k * imgsz[0] + j] = fs.sampleDerivative( j );
			}
		}
	} else {
		mexErrMsgTxt("filter subtype 2 expected.");
	}
	mxAddField(out , "testfiltsamp");
	mxSetField(out, 0, "testfiltsamp", testfsamp);		 
	mxSetField(out, 1, "testfiltsamp", testdfsamp);		 

	
	const int interpsz = 100;
	const double interpsc = 1.0/30;
	const double offset = -1;
	typedef bSplineFilter<2,double> bSplTi;
	bSplTi bspli[2];
	mxArray * testimgout = mxCreateDoubleMatrix( interpsz , interpsz, mxREAL);
	double * testimgoutp = mxGetPr( testimgout );
	double pos[2];
	for (int i = 0; i<interpsz; i++ ) {
		pos[1] = i*interpsc+offset;
		for (int j =0 ; j<interpsz; j++ ) {
			pos[0] = j*interpsc+offset;

			*testimgoutp = img.sample<double>(pos, bspli);
			testimgoutp++;
		}
	}
	mxAddField(out , "testimginterpolated");
	mxSetField(out, 0, "testimginterpolated", testimgout);
*/

}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[] )
{
	if (nlhs!=1) {
		mexErrMsgTxt("One output required.");
	}
	plhs[0] = mxCreateStructMatrix(2,1,0, NULL);
	self_test_mex( plhs[0] );
}
#endif // BUILDTESTFUN_BSPLINE_PROC

#endif
