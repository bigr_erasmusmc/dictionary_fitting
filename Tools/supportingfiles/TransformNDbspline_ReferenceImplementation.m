function [out, DoutDT ]  = TransformNDbspline_ReferenceImplementation( in, T, mapT, size_out_arg, filterbank, bsplineorder, AR, clip_o, boundary_condition)
% see TransformNDbspline
% This is a slow reference implementation. Use the mex file for speed. 
% This implementation is supposed to be more readable and less error prone, but does not exactly match the mex file. 
% (for speed and because some things are easier done in MATLAB). 
%
% Created by Dirk Poot

if nargin~=9
    error('9 inputs required');
end;
if numel(size_out_arg) ~= ndims( in )
    error('newSz should be a vector of length ndims(in)');
end;
doDoutDT = nargout>1;

tagdims = find( isnan( size_out_arg ) );
procdims = find( ~isnan( size_out_arg ) );
doAdjoint = all( size_out_arg(procdims) < 0 );
if doAdjoint
    size_out_arg( procdims ) = -size_out_arg( procdims );
    if doDoutDT
        error('cannot compute DoutDT of adjoint transform.');
    end;
end;
if any( size_out_arg < 0 )
    error('newSz should be positive (or all negative to specify adjoint)');
end;

size_in = size(in);
size_out = size_out_arg;
size_out(tagdims) = size_in( tagdims );
if doAdjoint
    size_i = size_out;
    size_o = size_in;
else
    size_i = size_in;
    size_o = size_out;
end;
size_Tfull = [numel(procdims) size_o]; 
size_Tfull(tagdims+1) = 1; 
size_T = size(T);
size_T(end+1:numel(size_Tfull)) = 1; % extend with scalar dimensions. 

isAffineTransform = (ndims(T)==2) && (size(T,1)==numel(procdims)) && (size(T,2)==size(T,1)+1) && isempty(mapT);

if isempty(bsplineorder) || numel(bsplineorder) > 1+(~isAffineTransform) 
    error('bsplineorder should be scalar or up to 2 element vector when T is not specifying an affine transform');
end;
bsplineorderT = bsplineorder(end);
bsplineorder = bsplineorder(1);

if isempty(filterbank)
    filtFun = @(rem) getBsplineFilter( rem , bsplineorder);
    filtFunDelay = (bsplineorder-1)/2;
else
    filtFun = @(rem)  getFilterFromFilterbank( rem , filterbank, bsplineorder);
    filtFunDelay = size(filterbank,1)/2-1;
end;
prefilterwithAR = ~isempty(AR);

if isempty( boundary_condition )
    boundary_condition = 0;
end

% test clip_o
if ~isempty(clip_o)
    if ~iscell(clip_o) || numel(clip_o) ~= numel(procdims)
        error('clip_o should be numel(procdims) element cell array');
    end
    for dimidx = 1 : numel(procdims) 
        if ~isa(clip_o{dimidx},'double') || size( clip_o{dimidx},2) ~= (numel(procdims)-dimidx+2) 
            error('clip_o{ dimidx } should be of class double and size(clip_o{dimidx},2) should be (numel(procdims)-dimidx+2) ');
        end;
    end;
end;



% Construct full sampling position matrix:
if isAffineTransform
    % NOTE: numel(procdims) == size(T,1) == size(T,2)-1
    x = cell(1,size(T,2));
    for dimidx = 1:numel(procdims)
        x{ dimidx } = 0 : size_o( procdims( dimidx ) )-1;
    end;
    x{ end } = 1;
    [x{:}] = ndgrid(x{:});
    for dimidx = 1 : numel(x)
        x{dimidx} = x{dimidx}(:)';
    end;
    allX = vertcat(x{:});
    Tfull = reshape( T * allX, size_Tfull);
elseif ~isempty(mapT)
    if size_T(1) ~= numel(procdims)
        error('Size(T,1) should be numel(procdims), which is the number on non nan newSz elements');
    end;
    if any( size_T( tagdims ) ~=1 )
        error('Input T should have size 1 in ''tag along'' dimensions');
    end;
    
    filtTFun = @(rem) getBsplineFilter( rem , bsplineorderT);
    filtTFunDelay = (bsplineorderT-1)/2;
    Ttmp = T;
    for dim = procdims+1
        size_Ttmp = size(Ttmp);
        size_Ttmp( dim ) = size_Tfull( dim );
        size_write = size_Ttmp; size_write( dim )= 1;
        
        Ttmp2 = zeros(size_Ttmp);
        sel_i = cell(1,ndims(T)); sel_i(:)={':'};
        sel_o = sel_i;
        perm_ord =  [ dim 1:dim-1 dim+1:ndims(Ttmp)] ;
        for idx_dim = 0 : size_Tfull( dim )-1
            pnt_T = idx_dim * mapT( 2, dim-1) + mapT(1,dim-1) + 1;
            [read_idx, filt] = getFiltFromPos( pnt_T -filtTFunDelay, filtTFun, 1, size(Ttmp, dim), boundary_condition );
            sel_i{dim} = read_idx;
            sel_o{dim} = idx_dim+1;
            
            Ttmp2(sel_o{:}) = reshape( filt' * reshape( permute( Ttmp( sel_i{:}), perm_ord), numel(read_idx) ,[]), size_write );
        end;
        Ttmp = Ttmp2;
    end;
    Tfull = Ttmp;
elseif ~isempty(T)
    if ~isequal(size_T, size_Tfull) 
        error('size of T does not match the expected size of a full transform');
    end;
    Tfull = T;
else
    % fall through case:
    out = in;
    return;
end;
% Tfull is now a full sampling position matrix, specifying the sample position for every output point.


% Apply prefiltering:
if prefilterwithAR 
    filters = struct;
    for dim = procdims
        filters(dim).ar = [1 AR(2:end)];
        filters(dim).runout = AR(1);
    end;
    if ~doAdjoint
        in = filterND_c( in, filters );
    end;
end;

% do actual sampling:
out = zeros( size_out );
sel_i = cell(1, numel(size_out) );sel_i(:)={':'};
sel_o = sel_i;
curfilt = cell(1, size(Tfull,1));
if doDoutDT
    dcurfilt = curfilt;
    DoutDTfull = zeros( [size_out size(Tfull,1) ] );
end;
procvox = prod( size_o( procdims ));
curoutidx = cell( 1,numel(procdims) );
size_sel_o = size_o( tagdims ); size_sel_o(end+1:2) = 1; 
for k = 1 : procvox
    %[curoutidx{:}]= ind2sub( size_o, k ); 
    [curoutidx{:}]= ind2sub( size_o( procdims ), k ); %IS THIS CORRECT?

    accept = true;
    
    if ~isempty( clip_o )
        for dimidx = numel(procdims ) : -1: 1
            curx = [curoutidx{dimidx:end}, -1];
            accept = all( clip_o{  dimidx  } *  curx' <=0 ) ;
            if ~accept
                break;
            end;
        end;
        if ~accept 
            % dont compute this voxel. 
            continue;
        end;
    end;
    
    sel_o( procdims ) = curoutidx;
    % get support region and filter in each dimension:
    for dimidx = 1:numel(procdims)
        dim = procdims(dimidx);
        if doDoutDT
            [ sel_i{dim}, curfilt{dim} , dcurfilt{dim} ] = getFiltFromPos( Tfull(dimidx, k) - filtFunDelay + 1 , filtFun, 1, size_i(dim), boundary_condition );
        else
            [ sel_i{dim}, curfilt{dim} ] = getFiltFromPos( Tfull(dimidx, k)-filtFunDelay +1 , filtFun, 1, size_i(dim), boundary_condition );
        end;
        if isempty( sel_i{dim} )
            accept = false;
            break;
        end
    end;
    if ~accept 
        % dont compute this voxel. 
        continue;
    end;
    if doAdjoint
        % Adjoint of store reduced result:
        selp = in( sel_o{:} );
        % Adoint of reduce:
        size_proddims = zeros(1,numel(procdims));
        for dimidx = numel( procdims ) : -1 : 1
            dim =  procdims( dimidx );
            selp = curfilt{dim} * reshape( selp, 1, []);
            size_proddims( dimidx ) = numel(curfilt{dim});
        end;
        selp = reshape( selp , [size_proddims size_i( tagdims )] );
        % Adjoint of extract support region:
        sel = ipermute( selp, [procdims tagdims]);
        out( sel_i{:} ) = out( sel_i{:} ) + sel ;
    else
        % extract support region:
        sel = in( sel_i{:} );
        selp = permute( sel, [procdims tagdims]);
        % reduce:
        for dimidx = 1 : numel( procdims ) 
            dim =  procdims( dimidx );
            selp = curfilt{dim}' * reshape( selp, numel(sel_i{dim}),[]);
        end;
        % store reduced result:
        out( sel_o{:} ) = reshape( selp, size_sel_o );
        
        % Compute derivative of out wrt. T
        if doDoutDT
            for paridx = 1 : numel( procdims ) 
                selp = permute( sel, [procdims tagdims]);
                for dimidx = 1 : numel( procdims ) 
                    dim =  procdims( dimidx );
                    if dimidx == paridx
                        selp = dcurfilt{dim}' * reshape( selp, numel(sel_i{dim}),[]);
                    else
                        selp = curfilt{dim}' * reshape( selp, numel(sel_i{dim}),[]);
                    end;
                end;
                % store reduced result:
                DoutDTfull( sel_o{:} , paridx ) = reshape( selp, size_sel_o );
            end;
        end;
    end;
end;
if doDoutDT
    % map DoutDTfull to DoutDT. 'If' structure matches 'if' structure of Tfull construction 
    if isAffineTransform
        % Tfull(i, k ) = sum_dim T(i, dim) * x{dim}( k )
        % out(k) = fun( Tfull( :, k ) )
        % Dout(k)DT(:,l) = Dout(k)DTfull(k) * DTfull(k)DT(l)
        %              = Dout(k)DTfull(k) * ( x{l}(k) )
        DoutDT = bsxfun(@times, DoutDTfull, reshape(allX',[size_out 1 size(T,2)]));
    elseif ~isempty(mapT)
        error('cannot compute for mapped T');
    elseif ~isempty(T)
        DoutDT = reshape( permute( DoutDTfull, [tagdims procdims numel(size_out)+1] ) , [prod(size_out(tagdims)) prod(size_out(procdims)) size(Tfull,1) ]); 
    else
        error('cannot reach');
    end;
end;
% Adjoint of Apply prefiltering:
if prefilterwithAR && doAdjoint
    out = filterND_c( out, filters );
end;

function [indx, filt, dfilt] =  getFiltFromPos( pnt_T , filtfun , minidx, maxidx, boundary_condition)
start = floor(pnt_T);
rem = pnt_T-start;
if nargout >= 3
    [filt, dfilt] = filtfun(rem);
else
    filt = filtfun( rem );
end;
indx = [start+(0:numel(filt)-1)]';
inner = indx>=minidx & indx <= maxidx;

switch boundary_condition
    case 0 % zero boundary conditions
        filt = filt(inner);
        indx = indx(inner);
        if nargout >= 3
            dfilt = dfilt(inner);
        end;
    case 1 % mirror boundary conditions
        outer = ~inner;
        idx_outer = find(outer);
        
        for k = 1:numel(idx_outer)
                while indx(idx_outer(k)) < minidx || indx(idx_outer(k)) > maxidx
                    if indx(idx_outer(k)) < minidx
                        indx(idx_outer(k)) = 1 - indx(idx_outer(k)); % minidx + (minindx-(indx(idx_outer(k))+1))
                    else
                        indx(idx_outer(k)) = 2*maxidx + 1 - indx(idx_outer(k)); % manidx + (maxindx-(indx(k)-1))
                    end
                end
            
        end
        
%         filt = filt.*inner + flip( circshift( filt.*outer, [1 0]) );        
%         filt = filt(inner);
%         indx = indx(inner);
%         
%         if nargout >= 3
%             dfilt = dfilt.*inner + circshift( flip( dfilt.*outer ), [-1 0]) ;
%             dfilt = dfilt(inner);
%         end;        
    otherwise
        error('unknown boundary conditions \n');
end

function [effectivefilters , Deffectivefilters]= getBsplineFilter( rem , bsplineorder)
rem = rem(:).';
if any(rem<0) || any(rem>=1) 
    error('invalid rem');
end;
% implementation 'copied' from bspline_filter.cpp
if bsplineorder == 0
    effectivefilters = ones(1,numel(rem));
    if nargout>1
        error('Derivative of bspline with order 0 is discontinuous');
    end;
elseif bsplineorder == 1 || bsplineorder == -1
    effectivefilters = [1-rem;
                        rem];
    if nargout>1
        Deffectivefilters = [-1;1]*ones(size(rem));
    end;
elseif bsplineorder == 2
    effectivefilters = [.5*(1-rem).^2; 
                        .5+rem.*(1-rem); 
                        .5*rem.^2 ];
    if nargout>1
        Deffectivefilters = [-(1-rem);
                             (1-rem)-rem;
                             rem];
    end;
elseif bsplineorder == 3
    effectivefilters = [(1-rem).^3 ; 
                        4-3*rem.^2.*(2-rem) ; 
                        4-3*(1-rem).^2.*(1+rem) ; 
                        rem.^3 ]/6;
    if nargout>1
        Deffectivefilters = [-(1-rem).^2;
                             -2*rem.*(2-rem)+rem.^2;
                             2*(1-rem).*(1+rem)-(1-rem).^2;
                             rem.^2]*.5;
    end;                    
elseif bsplineorder == -2 % OMOMS order 2 : this is bspline order 2 + 1/60 second derivative bspline order 2
    effectivefilters = [.5*(1-rem).^2 + 1/60;
                        .5+rem.*(1-rem)-2/60; 
                        .5*rem.^2 +1/60];
    if nargout>1
        error('Derivative of OMOMS with order 2 is discontinuous');
    end;                    
elseif bsplineorder == -3 % OMOMS order 3 : this is bspline order 3 + 1/42 second derivative bspline order 3
    effectivefilters = [(1-rem).^3              + 6/42*(1-rem); 
                        4-3*rem.^2.*(2-rem)     + 6/42*(3*rem-2);
                        4-3*(1-rem).^2.*(1+rem) + 6/42*(1-3*rem); 
                        rem.^3                  + 6/42*rem        ]/6;
    if nargout>1
        error('Derivative of OMOMS with order 3 is discontinuous');
    end;                    
else
    error('invalid or unsuported bsplineorder; valid: {-3, -2, -1, 0, 1, 2, 3}, with negative for OMOMS');
end;

function effectivefilters = getFilterFromFilterbank( rem , filterbank, bsplineorder)
% copied from interpolate_with_filterbank.m
if any(rem(:)<0) || any(rem(:)>=1) 
    error('invalid rem');
end;
nsteps = size(filterbank,2)-bsplineorder;

col = rem * nsteps;
coli = floor(col)+1;
colfrac = (col(:)-(coli(:)-1))';


if bsplineorder == 0
    effectivefilters = filterbank(:,coli) ;
elseif bsplineorder == 1
    diffbank = diff(filterbank,[],2);
    effectivefilters = filterbank(:,coli) + bsxfun(@times, colfrac, diffbank(:,coli));
elseif bsplineorder == 2
    effectivefilters = bsxfun(@times, .5*(1-colfrac).^2     , filterbank(:, coli     )) + ...
                       bsxfun(@times, .5+colfrac-colfrac.^2 , filterbank(:, coli + 1 )) + ...
                       bsxfun(@times, .5*(colfrac).^2      , filterbank(:, coli + 2 ));
elseif bsplineorder == 3
    one_mfrac = 1-colfrac;
    sqr_frac = colfrac.*colfrac;
    sqr_onemfr = one_mfrac.*one_mfrac;
    effectivefilters =(bsxfun(@times,     sqr_onemfr.*    one_mfrac   , filterbank(:, coli     )) + ...
                       bsxfun(@times, 4-3*sqr_frac  .*( 2-colfrac   ) , filterbank(:, coli + 1 )) + ...
                       bsxfun(@times, 4-3*sqr_onemfr.*( 2-one_mfrac ) , filterbank(:, coli + 2 )) + ...
                       bsxfun(@times,     sqr_frac  .*    colfrac     , filterbank(:, coli + 3 )) ) *(1/6);
else
    error('invalid or unsuported bsplineorder; valid: {0, 1, 2, 3}');
end;


