function [X] = wrapJacobianMul(J, X, flag, JXfun, JTXfun ,multiplex)
% [JX] = wrapJacobian(J, X, id, JX, JTX, multiplex)
%
% Wrapper function to join separate Jacobian and jacobian transpose
% multiplication functions. 
% Usefull for validateJacobian and lsqnonlin.
%
% INPUTS:
%  J : Jacobian information
%  X : vector (/matrix) to be multiplied by J
%  flag : flag that selects multiplication with J (flag>0), J' (flag<0)  or J'*J (flag==0)
%  JXfun : function that multiplies J with X,  "J*X" = JXfun( J, X )
%  JTXfun : function that multiplies J' with X,  "J'*X" = JTXfun( J, X )
%  multiplex: boolean, default = true; if true JXfun and JTXfun support multiple columns in X, if false
%             JXfun and JTXfun are called for each column of X seperately. 
%
% Example usage:
%  JacMulFul = @(J, X, flag) wrapJacobianMul(J, X, flag, JXfun, JTXfun , multiplex)
%  validateJacobian( fun, x, JacMulFul )
%
% Created by Dirk Poot, Erasmus MC, begin 2016


if nargin<6 || isempty(multiplex)
    multiplex = true;
end;

if flag>=0 
    if multiplex || size(X,2)==1
        X = JXfun(J, X);
    else
        X2 = cell(1,size(X,2));
        for k=1:numel(X2)
            X2{k} = JXfun(J, X(:,k));
        end;
        X = [X2{:}]; % horizontal concatentation
    end;
end;
if flag<=0
    if multiplex || size(X,2)==1
        X = JTXfun(J, X);
    else
        X2 = cell(1,size(X,2));
        for k=1:numel(X2)
            X2{k} = JTXfun(J, X(:,k));
        end;
        X = [X2{:}]; % horizontal concatentation
    end;
end;