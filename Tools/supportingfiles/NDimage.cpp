#ifndef NDIMAGE
#define NDIMAGE
/* This file provides a set of classes dealing with b-splines
 * 
 * Test code by building: mex bspline_proc.cpp -DBUILDTESTFUN_BSPLINE_PROC
 *           and call   : a = bspline_proc();
 *			 Inspect a for correct results.
 *           don't define 'BUILDTESTFUN_BSPLINE_PROC' when including this from external code.
 * 
 * class bSplineCoeffs is a static function class, that computes bSpline coefficients.
 * Typically you want to use a wrapped version, such as described below.
 * 
 * class bSplineCoefficients is a wrapper class around bSplineCoeffs that allows 
 *   preparing coefficients and sampling source data with these coefficients. Still a quite
 *   raw class templated over b-spline length and coefficientType. If b-spline length ==-1, 
 *   the length can be set at run-time.
 *
 * class bSplineSampleVect allows to b-spline interpolate vectors.
 *   bSplineSampleVect<[bsplineorder=-1,] datatype>( source , step , length, stride, nvectors [, bsplineorder] )
 *   The method sample( * out, position )
 *   samples the input vectors around the column specified by position.
 *   inputs:
 *		 step   : the step (in number of elements) from 1 element of a vector to the next.
 *		 length : Number of elements in each vector.
 *		 stride : the step (in number of elements) from 1 vector to the next.
 *		 nvectors: number of vectors given.
 *       bsplineorder: order of the b-spline that is used.
 *   If you don't want to store the entire interpolated vector, you can use the version templated over bsplineorder.
 *   get subtype() and use specialization? where '?' is the value of subtype.
 *	 and call prepare( position ) 
 *		before calling sample( i ) to get element i of the interpolated vector.
 * 
 *  class filter: a filterbank that can be interpolated with bsplines.
 *      determine filter.subtype() to determine which specialization should be called:
 *      e.g. for subtype()==2:
 *      filter::specialization2 spec = filter::specialization2(filter);
 *      spec.prepare( loc ) , spec.prepareDerivative( loc ), spec.prepareValueAndDerivative( loc )
 *      note important!!:  0 <= loc < 1
 *      and then: spec.sample[Derivative]( i ) to get interpolated [partial derivative w.r.t. loc of] element i.
 *      with 0 <= i < filter.length()
 *
 *  class NDimage: image type, templated over data type and dimensionality. Can be sampled by a sampler e.g. a bspline sampler.
 *  
 *  See  function 'self_test_mex' for example usage.
 * 
 * Created by Dirk Poot, Erasmus MC
 */

/* B-spline interpolation in columns of F. 
*/
#include "math.h"
#include <algorithm>
#include "bspline_interp_base.cpp"


// ND-IMAGE sampling function, compile time known length coefficients. 
template <int N, int ncoeffsperdim, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		proctype ret = 0;//Numeric<filtertype>.Zero ;
		// compiler should!!! unroll loop (but MSVC probably does not)
		for (int k=0; k < ncoeffsperdim ;k++, source += steps[N-1]) {
			ret += sampleNDimagefun<N-1, ncoeffsperdim, imgtype, proctype, filttype>::sample(source , steps, coeffs) * coeffs[ N-1 ][ k ];
		}
		return ret;
	}
};
template <int N, int ncoeffsperdim, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<N, ncoeffsperdim, imgtype, proctype, filttype * > {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype ** coeffs) {
		proctype ret = 0;//Numeric<filtertype>.Zero ;
		// compiler should!!! unroll loop (but MSVC probably does not)
		for (int k=0; k < ncoeffsperdim ;k++, source += steps[N-1]) {
			typedef filttype * filttypearg;
			ret += sampleNDimagefun<N-1, ncoeffsperdim, imgtype, proctype, filttypearg>::sample(source , steps, coeffs) * (*coeffs[ N-1 ])[ k ];
		}
		return ret;
	}
};
// end case: sample source image and convert to processing type.
template <int ncoeffsperdim, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<0, ncoeffsperdim, imgtype, proctype, filttype> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		return (proctype) (*source); 
	}
};
template <int ncoeffsperdim, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<0, ncoeffsperdim, imgtype, proctype, filttype *> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype ** coeffs) {
		return (proctype) (*source); 
	}
};

// ND-IMAGE sampling function, runtime known length of coefficients. 
template <int N, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<N,-1,imgtype, proctype, filttype> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		proctype ret = 0;//Numeric<filtertype>.Zero ;
		for (int k=0; k < coeffs[N-1].length() ; k++, source += steps[N-1]) {
			ret += sampleNDimagefun<N-1, -1, imgtype, proctype, filttype>::sample(source , steps, coeffs) * coeffs[ N-1 ][ k ];
		}
		return ret;
	}
};
template <int N, typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<N,-1,imgtype, proctype, filttype * > {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype ** coeffs) {
		proctype ret = 0;//Numeric<filtertype>.Zero ;
		for (int k=0; k < coeffs[N-1]->length() ; k++, source += steps[N-1]) {
			typedef filttype * filttypearg;
			ret += sampleNDimagefun<N-1, -1, imgtype, proctype, filttypearg>::sample(source , steps, coeffs) * (*coeffs[ N-1 ])[ k ];
		}
		return ret;
	}
};

// end case: sample source image and convert to processing type.
template <typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<0, -1, imgtype, proctype, filttype> {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype * coeffs) {
		return (proctype) (*source); 
	}
};
template <typename imgtype, typename proctype, typename filttype> class sampleNDimagefun<0, -1, imgtype, proctype, filttype * > {
public:
	static inline proctype sample( imgtype * source, ptrdiff_t * steps , filttype ** coeffs) {
		return (proctype) (*source); 
	}
};

template <int N, typename imgtype> class NDimage;

template <typename imgtype> class NDimage_base {
private:
	int ndims;
protected:
	imgtype * origin;
	NDimage_base( imgtype * origin_, int ndims_) {
		if ( (ndims_<2) || (ndims_>4) ) {
			mexErrMsgTxt("Image dimension should be 2, 3 or 4.");
		}
		ndims = ndims_;
		origin = origin_;
	}
public:
	int Ndims() { return ndims; };
	template< typename procType, typename locationType, typename samplerType> procType sample( locationType point[], samplerType samplers[]) {
		if (ndims==2) {
			return ((NDimage<2,imgtype>*)this)->sample<procType>(point, samplers );
		} else if (ndims==3) {
			return ((NDimage<3,imgtype>*)this)->sample<procType>(point, samplers );
		} else if (ndims==4) {
			return ((NDimage<4,imgtype>*)this)->sample<procType>(point, samplers );
		} 
	}
};

template< typename T > struct dereference {
	typedef T value_type;
};

template< typename T> struct dereference<T *> {
	typedef T value_type;
};


template <int N, typename imgtype> class NDimage : public NDimage_base<imgtype> {
private:
	template< typename samplerType> class sample_range_helper {
	public: 
		samplerType * original;
		int offset;
		int newlength;
		sample_range_helper(){}; // default constructor.
		sample_range_helper(samplerType * orig, int off, int len) : original(orig),offset(off), newlength(len) {
		};
		inline int length() {
			return newlength;
		}
		inline typename imgtype operator[](int i) {
			return (*original)[i+offset];
		}
		/*sample_range_helper & operator= (const sample_range_helper & other) 
		{
			original = other.original;
			offset = other.offset;
			newlength = other.newlength;
		    return *this;
	    }*/
	};

protected:
	ptrdiff_t size[ N ];
	ptrdiff_t stride[ N ];
public:
	NDimage( imgtype * origin_, const ptrdiff_t * size_, const ptrdiff_t * stride_) : NDimage_base(origin_, N) {
		for (int i=0;i<N;i++) {
			size[i]=size_[i];
		}
		for (int i=0;i<N;i++) {
			stride[i]=stride_[i];
		}
	};

	template< typename procType, typename locationType, typename samplerType> procType sample( locationType point[], samplerType * samplers) {
		imgtype * sampleorigin = origin;
		bool allinrange = true;
		ptrdiff_t loc[N];
		for (int i=0;i<N;i++) {
			locationType pti = point[i] - samplers[i]->delay();
			loc[i] = (ptrdiff_t) floor(pti);
			pti -= loc[i];
			samplers[i]->prepare( pti );
			allinrange &= (loc[i]>=0) && (loc[i]+samplers[i]->length() <= size[i]);
			sampleorigin += loc[i] * stride[i];
		}
		if (!allinrange) {
			//mexErrMsgTxt("Trying to sample (partly) outside of image, not supported (yet).");
			typedef sample_range_helper<dereference<samplerType>::value_type> subsampT;
			subsampT subsamp[N];
			for (int i=0;i<N;i++) {
			    using std::min;
			    using std::max;

				int offset    = (int) max( (ptrdiff_t) 0, -loc[i]);
				int newlength = (int) min( (ptrdiff_t) samplers[i]->length(), size[i]-loc[i] ) - offset;
				if (newlength<=0) {
					return 0;
				}
				sampleorigin += offset * stride[i];
				subsamp[i] = subsampT(samplers[i], offset, newlength);
			}
			return sampleNDimagefun<N, -1 , imgtype, procType, subsampT>::sample(  sampleorigin, stride , subsamp );
		} else {
			return sampleNDimagefun<N, dereference<samplerType>::value_type::fixedlength , imgtype, procType, samplerType>::sample(  sampleorigin, stride , samplers );
		}
	}
};


template <typename imgtype> NDimage_base<imgtype> * newNDimage( imgtype * origin_, const ptrdiff_t * size_, const ptrdiff_t * stride_, int N) {
	if (N==2) {
		return new NDimage<2,imgtype>(origin_, size_, stride_);
	} else if (N==3) {
		return new NDimage<3,imgtype>(origin_, size_, stride_);
	} else if (N==4) {
		return new NDimage<4,imgtype>(origin_, size_, stride_);
	} else {
		mexErrMsgTxt("Image dimension should be 2, 3 or 4.");
	}
}

#endif