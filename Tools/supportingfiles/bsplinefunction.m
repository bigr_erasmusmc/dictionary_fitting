function [ out , doutdx , d2outdx2] = bsplinefunction( n, x )
% [ out , doutdx ] = bsplinefunction( n, x ) 
% returns center-symmetric B-spline of degree n for positions x
% See, Unser (1999), Box 1. B-Splines
%
% NOTE: this is an basic, but quite slow implementation to evalute 
% the b-spline functions. High performance routines should use dedicated 
% functions (of specific order). 
%
% 2016, W. Van Valenberg, TUDelft: Created 

k = (0:n+1)';

positions = bsxfun(@minus, repmat(x, n+2, 1), k ) + (n+1)/2 ;
positivepos = positions>=0;
positionsPn = positions ;
positionsPn( positivepos ) = positions( positivepos ).^n;
positionsPn(~positivepos ) = 0;

coef = factorial(n+1)*(-1).^k ./ (factorial(k).*factorial(n+1-k));
factn = factorial(n);
out = sum( bsxfun(@times, coef, positionsPn), 1) / factn;
if nargout>=2
    dpositionsPndx = positionsPn;
    dpositionsPndx( positivepos ) = n*positions( positivepos ).^(n-1);
    doutdx = sum( bsxfun(@times, coef, dpositionsPndx), 1) / factn;
    if nargout>=3
        d2positionsPndx2 = positionsPn;
        d2positionsPndx2( positivepos ) = n*(n-1)*positions( positivepos ).^(n-2);
        d2outdx2 = sum( bsxfun(@times, coef, d2positionsPndx2), 1) / factn;
    end;
end;
end

function test()
%% Show bspline functions for first orders:

x = linspace(-6,6,1000);
n = 0:5;
out = cell(size(n)); dout = out; d2out =out;
legstr = out;
for k = 1 : numel( n );
    [out{k}, dout{k}, d2out{k} ] = bsplinefunction( n(k) , x );
    legstr{k} = sprintf('bsplineord = %d',n(k));
end
subplot(3,1,1);
plot(x, vertcat(out{:})');
legend(legstr{:})
subplot(3,1,2);
plot(x, vertcat(dout{:})');
legend(legstr{:})
subplot(3,1,3);
plot(x, vertcat(d2out{:})');
legend(legstr{:})

end