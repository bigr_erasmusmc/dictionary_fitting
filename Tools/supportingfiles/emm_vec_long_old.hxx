/* This file provides the implementation of the  vec< long , *>  type
 * Only for including in emm_vec.hxx.
 * Don't include anywhere else.
 *
 * Created by Dirk Poot, Erasmus MC,
 * Last modified 10-3-2015
 */

// Specify the type as preprocessor directive instead of template argument to have precise control over the types for which this 
// vec template works. 
#ifndef VECLONGTYPE 
  #define VECLONGTYPE long
#endif

template < int vlen> struct vec< VECLONGTYPE, vlen > : private vec< getIntBaseType< VECLONGTYPE >::Type , vlen> {
private:
  typedef typename getIntBaseType< VECLONGTYPE >::Type baseIntType; // we (should) make sure that baseIntType and long have the same memory format. Hence we can use reinterpret_cast. 
  typedef vec< baseIntType , vlen> Superclass;
  explicit vec( const Superclass v ): Superclass( v ) {};
  Superclass getAsSuperclass( ) const {
    return Superclass( xmm, Superclass::numregisters );
  }
public:
  typedef VECLONGTYPE T;
  typedef T value_type;
  enum {length = vlen};
  enum {naturalLen = (REGISTER_NUM_BYTES/sizeof(T))} ;
  enum {supported = vec_support<T, vlen>::supported };

  explicit vec (const T *v) : Superclass( reinterpret_cast<const baseIntType *>( v ) ) {};
/*  template <typename int_t> explicit vec(const T *v, int_t stride) : Superclass( reinterpret_cast<const baseIntType *>( v ) , stride) {};
  static vec loada( const T *v ) {
    return vec( Superclass::loada( reinterpret_cast<const baseIntType *>( v ) ) );
  };
  explicit vec (const T v) : Superclass( reinterpret_cast<const baseIntType >( v ) ) {};
  static vec zero();
  */
  // Type conversion routine:
  template <typename From> explicit vec(const vec<From, vlen> &v);
  
  /*inline*/ void store( T * v) {
    Superclass::store( reinterpret_cast<baseIntType * >( v ) );
  };
  /*
  template <typename int_t> inline void store( T *v, int_t stride) {
    Superclass::store( reinterpret_cast<baseIntType * >( v ), stride);
  };

  inline vec operator* (const vec<T, vlen> &v) const {
    return vec( Superclass::operator*( v ) ); };
  inline vec operator+ (const vec<T, vlen> &v) const {
    return vec( Superclass::operator+( v ) ); };
  inline vec operator- (const vec<T, vlen> &v) const {
    return vec( Superclass::operator-( v ) ); };
  inline vec operator/ (const vec<T, vlen> &v) const {
    return vec( Superclass::operator/( v ) ); };
  inline void operator*= (const vec<T, vlen> &v) {
    Superclass::operator*=( v ) ; };
  inline void operator+= (const vec<T, vlen> &v) {
    Superclass::operator+=( v.getAsSuperclass( ) ) ; };
  /*inline void operator-= (const vec<T, vlen> &v) {
    Superclass::operator-=( v ) ; };
  inline void operator/= (const vec<T, vlen> &v) {
    Superclass::operator/=( v ) ; };

  // scalar versions:
  inline vec operator* (const T v) const {
    return vec( Superclass::operator*( v ) ); };
  inline vec operator+ (const T &v) const {
    return vec( Superclass::operator+( v ) ); };
  inline vec operator- (const T &v) const {
    return vec( Superclass::operator-( v ) ); };
  inline vec operator/ (const T &v) const {
    return vec( Superclass::operator/( v ) ); };
  inline void operator*= (const T &v) {
    Superclass::operator*=( v ) ; };*/
  inline void operator+= (const T &v) {
    Superclass::operator+=( static_cast<const baseIntType>( v ) ) ; }
  /*inline void operator-= (const T &v) {
    Superclass::operator-=( v ) ; };
  inline void operator/= (const T &v) {
    Superclass::operator/=( v ) ; };
  inline vec operator= (const T &v) {
    return vec( Superclass::operator=( v ) ); };

  /* // comparison to bitmask:
  inline vec operator> (const vec &v) const;
  inline vec operator>= (const vec &v) const;
  inline vec operator== (const vec &v) const;
  inline vec operator<= (const vec &v) const;
  inline vec operator< (const vec &v) const;
  inline vec operator!= (const vec &v) const;

  // bit wise operators:
  inline vec operator>> (const int shiftcount) const;
  inline vec operator<< (const int shiftcount) const;
  inline vec operator| (const vec<T, vlen> &v) const;
  inline vec operator& (const vec<T, vlen> &v) const;*/
  template <typename, int> friend struct vec;
};

#undef VECLONGTYPE 