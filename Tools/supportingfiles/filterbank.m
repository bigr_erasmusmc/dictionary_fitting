function [F,delay] = filterbank(filt, L, nsteps, filtdelay, bsplineord)
% [F] = filterbank(filt, L, nsteps, [],  bsplineord)
% OR
% [F, delay] = filterbank(filt, L, nsteps, filtdelay, bsplineord)
% Simple function to create a filterbank with nsteps filters, each with length L from
% filt.
% filt should be designed with designfilter
% F can be used with affineTransformCore and other interpolation routines.
% filtdelay is the (real valued) delay of filt (in samples). (filtdelay is zero when
% filt = [1;zeros])
% delay is the delay of the individual filters.
%
% Created by Dirk Poot, University of Antwerp
% 14-1-2010

if nargin<5
    bsplineord = 1; % first order b-spline interpolation (=linear) by default.
end;
if isempty(L)
    % solve minimal integer L for which: addz>=0 => 0 <= nsteps*L+1- numel(filt)
    L = ceil((numel(filt)-1)/nsteps);
end;
addz = nsteps*L+bsplineord- numel(filt);
if addz>0
    disp(['adding ' num2str(addz) ' zeros']);
    adddelay = floor(addz/2);
    filt = [zeros(floor(addz/2),1); filt(:); zeros(ceil(addz/2),1)];
elseif addz<0
    warning(['reducing filter, removing in total ' num2str(-addz) ' elements from the start and end']);
    filt = filt(1+floor(-addz/2):end-ceil(-addz/2));
    adddelay = - floor(-addz/2);
else
    adddelay = 0;
end;

F = fliplr(reshape(filt(1+bsplineord:end),nsteps,L)');
filtrunin = filt(bsplineord:-1:1);
F = [F [filtrunin(:)';F(1:end-1,1:bsplineord)]];
if nargout>1
    delay = (filtdelay+adddelay)/nsteps -1 + (0:nsteps+bsplineord-1)/nsteps;
end;