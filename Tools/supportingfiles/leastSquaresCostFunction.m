function [f, g, h_info] = leastSquaresCostFunction( X, A, At, Y, multipleX, min_Y, dot_Y, regulfun)
% [f, g, h_info] = leastSquaresCostFunction( X, A, At, Y [,multipleX, min_Y, dot_Y, regulfun])
%
% Computes 
%  f( X ) =  .5* | Y - A * X |^2              [ + 0.5* X'*regulfun(X) ]
%         == .5* (Y - A * X)'*(Y - A * X)     [ + 0.5* dot(X,regulfun(X)) ]
%
% To be used as cost function for non-linear optimization routines.
% Especially, as first argument of conjgrad_nonlin.
%
% Obviously, for purely quadratic problems you can use a quadratic solver such as
% cgiterLS, or, when A can be given explicitly, with the simple matlab
% command: X = A\Y.
% The benefit of this function is when there are additional cost terms that
% should be combined. In that case this function allows easy conversion
% from such least squares formulation to a general cost function. 
% Also see leastSquaresCostFunction_nonlinear.m for nonlinear functions. 
%
% Obtain an hessian muliplication routine by calling with 'hessmulfun' as
% first input argument. The other arguments should be provided:
% hessmulfun = leastSquaresCostFunction( 'hessmulfun', A, At, Y)
% useage: d =  hessmulfun(h_info, x)   % multiply x with the hessian.
%
% INPUTS:
%   x  : parameter vector
%  A, At : functions taking 1 argument and computing A(b) == A*x, 
%           or At(c) == A'*y, respectively. (Make sure complex conjugate is
%           used for complex A)
%  Y   : measurements vector. The optimum of the cost function is at the
%        least squares solution of X with the data Y. 
%  multiplerhs : default = true : multiple x can be used simultanuously.
%                if false only 1 x is supported by A and At, 
%  min_Y : Two argument function that subtracts the second argument from the first
%          Default : @(Y,AX) Y-AX; 
%  dot_Y : single argument function that computes the dot product of the argument with itself. 
%          Default: @(residual) dot(residual, residual)
%          NOTE: min_Y and dot_Y are introduced to allow custom 'Y' formats. 
%  regulfun : Additional quadratic term specifying a hermitian (symmetric) matrix multiplication
%             E.g. to specify Tikhonov regularization: regulfun = @(x) lambda.*x;
%             with some appropriate regularization strength lambda. 
%
% OUTPUTS:
%  f   : cost function value
%  g   : gradient of cost function value with respect to X
%  h_info : hessian info. 
%
% Created by Dirk Poot, 24-7-2015, Erasmus MC, 

if nargin<5 || isempty(multipleX)
    multipleX = true;
end;
if nargin<6 || isempty(min_Y)
    min_Y = @(Y,AX) Y-AX;
end;
if nargin<7 || isempty(dot_Y)
    dot_Y = @(A) dot(A,A);
end;
if nargin<8
    regulfun = [];
end;
if isequal(X,'hessmulfun')
%     f = @(hinfo, x) reshape( At(A( reshape( x, [hinfo.sz size(x,2)])) ), [],size(x,2));
    f = @(hinfo, x) leastSquaresCostFunction_hessmul(hinfo, x, A, At, multipleX, regulfun);
    return;
end;

% f = .5 * (Y - A * X)'*(Y - A * X)    + .5* X'*R*X
% g = df/dX = A'*(Y - A * X)           +  R*X
% h = A'*A                             +  R 

res = min_Y(Y,A( X ));

f = .5 * dot_Y(res);
if ~isempty(regulfun)
    regulX = regulfun(X);
    f = f + .5 * dot(X(:),regulX(:));
end;
if nargout>1
    g = - At( res );
    if ~isempty(regulfun)
        g = g + reshape(regulX,size(g));
    end;
    h_info.sz = size(X);
    h_info.multipleX = multipleX;
end;
end
function Hx = leastSquaresCostFunction_hessmul(hinfo, x, A, At, multipleX, regulfun)
    if ~multipleX && size(x,2)>1 && size(x,1)==prod(hinfo.sz)
        % split over x
        Hx = cell(1,size(x,2));
        for k=1:numel(Hx)
            Hx{k} = reshape( At( A( reshape( x(:,k) , hinfo.sz) ) ) , [],1);
        end;
        if ~isempty(regulfun)
            for k=1:numel(Hx)
                Hx{k} = Hx{k} + regulfun( reshape( x(:,k) , hinfo.sz)) ;
            end;
        end;
        Hx = [Hx{:}];
    else
        Hx = reshape( At( A( reshape(x , [hinfo.sz size(x,2)]) ) ), [], size(x,2) );
        if ~isempty(regulfun)
            Hx = Hx + reshape( regulfun( reshape( x , [hinfo.sz size(x,2)] ) ) , size(Hx));
        end;
    end;
end