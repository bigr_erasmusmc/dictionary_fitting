function [y, dy, d2ydmudmu] = lognormComplexpdf(x,mu,sigma, derivativeselector)
%LOGNORMPDF Normal probability density function (pdf).
%   [Y , dydmu, d2ydmudmu] = LOGNORMPDF(X,MU,SIGMA [,derivativeselector]) 
%   Returns the logarithm of the pdf of the
%   normal distribution with complex valued mean MU and standard deviation SIGMA,
%   evaluated at the values in complex values X. 
%   The size of Y is the common size of the input arguments.  A scalar
%   input functions as a constant matrix of the same size as the other
%   inputs.
%   Optionally the second and third output argument return the first and second derivative of 
%   Y with respect to the 'mu' argument, respectively.
%   derivativeselector : default = [false true false]; should be 3 element logical vector.
%       The first (and second) derivatives are calculated for the selected parameters ['x' 'mu' 'sigma']
%       if derivative wrt to multiple variables is requested, the individual derivatives are concatenated 
%       in dimensions ndims(y)+1.
%
%   Default values for MU and SIGMA are 0 and 1 respectively.
%
%   See also NORMCDF, NORMFIT, NORMINV, NORMLIKE, NORMRND, NORMSTAT.
% Created by Dirk Poot, Erasmus MC

outargs = cell(nargout, 2);

[outargs{:,1}] = lognormpdf(real(x),real(mu),sigma, derivativeselector);
[outargs{1:min(2,nargout),2}] = lognormpdf(imag(x),imag(mu),sigma, derivativeselector);

y =[outargs{1,1};outargs{1,2}];
if nargout>1
    dy = complex(outargs{2,1}, outargs{2,2});
    if nargout>2
        d2ydmudmu = outargs{3,1};
        if derivativeselector(3)
            error('Cannot yet compute hessian of logarithm of complex-normal distribution with respect to sigma.');
        end;
    end;
end;