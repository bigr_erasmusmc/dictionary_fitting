function [f, dfdnu] = besseli_dnu( nu, z, divExpZ)
% [f, dfdnu]= besseli_dnu( nu, z)
%
% Computes:
% f = besseli( nu, z) 
% dfdnu = derivative( besseli(nu, z) , nu ) 
%
% For large z (=when using the gaussian approximation), the accuracy is quite low. 
% Solution could be to always use the sum, but that will be slow (requiring approx 12000 terms for z=1e7)
%
% 21-4-2017: Created by Dirk Poot, Erasmus MC
%
%  based on http://functions.wolfram.com/Bessel-TypeFunctions/BesselI/20/ShowAll.html
%  I_nu^(1,0)(z) = I_nu(z) log(z/2) - sum_{k=0..inf} Psi( k+nu+1) / (k! Gamma( k+nu+1) ) * (z/2)^(2*k+nu)
%
%  However, the subtraction here suffers from large cancelation for large z. 
%
% Note that
% I_nu(z) = sum_{k=0..inf} 1 / (k! Gamma( k+nu+1) ) * (z/2)^(2*k+nu)
%
% So
% I_nu^(1,0)(z) = sum_{k=0..inf} (log(z/2)- Psi( k+nu+1)) / (k! Gamma( k+nu+1) ) * (z/2)^(2*k+nu)
%
% First try to approximate the sum in I_nu^(1,0)(z) : 
% As Psi is almost constant (for the following arguments)
% and we consider positive z:
% the largest term in the sum is when (z/2)^2  is k*(k+nu)   (or maybe for slightly larger k )
%   As for larger k, denominator grows faster, for smaller k denominator grows slower than (z/2)^2 ; observe by taking log of argument of the sum (excluding psi)
% => k*(k+nu) = (z/2)^2
% => k^2 + k nu - (z/2)^2 ==0
% => kmax = .5* sqrt( nu^2+z^2) - .5*nu  (selecting positive k solution from Solve[k^2 + k nu - (z/2)^2 == 0, k])
%
% The logarithm of the terms is roughly quadratic away from kmax, approx by taylor expansion as:
%  log( term(k) ) =            (2*kmax+nu) * log( z/2 ) +log( log(z/2)-psi(0,1+kmax+nu) ) - gammaln(1+kmax) - gammaln(1+kmax+nu)
%          +     (k-kmax)   * (- log(4) + 2 log(z) - psi(0, 1+kmax) - psi(0, 1+kmax+nu) + psi(1, 1+kmax+nu)/psi(0,1+kmax+nu)
%          +.5 * (k-kmax)^2 * ( - psi(1, 1+kmax) - psi(1, 1+kmax+nu) - ( psi(1, 1+kmax+nu)/psi(0,1+kmax+nu) )^2 + ( psi(2, 1+kmax+nu)/psi(0,1+kmax+nu) )^2 )
% (see nonCentralChi_derivatives.nb; NOTE: Above is without expanding I_nu(z) into the sum)
% (Also note Mathematica PolyGamma[ a, b] == MATLAB psi( a, b )
% Gaussian approximation:
% a + b* (k-kmax) + c *.5 *(k-kmax)^2 == logA - .5* log( 2*sigma^2*pi) - (k-mu)^2/(2*sigma^2)
%
% b + c * (k-kmax) ==0  =>  b-c*kmax == -c * k  =>  k == ( -b/c+ kmax ) =mu
% => mu = kmax - b/c  ; kmax = mu + b/c
%
% => k -> x + mu
% => a + b*(x+mu - mu-b/c) + .5 * c *( x+mu - mu -b/c)^2 == logA - .5* log(2*sigma^2*pi) - x^2 / (2*sigma^2)
% => a + b*x -b^2/c + .5 * c *( x-b/c)*( x-b/c) == logA - .5* log(2*sigma^2*pi) - x^2 / (2*sigma^2)
% => a -b^2/c   +   x* b  + .5 * c *( x^2 - 2*x *b/c + b^2/c^2) == logA - .5* log(2*sigma^2*pi) - x^2 / (2*sigma^2)
% => a-b^2/c+.5*b^2/c   + .5 * c *( x^2 ) == logA - .5* log(2*sigma^2*pi) - x^2 / (2*sigma^2)
% => c = -1/sigma^2 => sigma = sqrt(-1/c)
% => logA =  a-.5*b.^2/c  +  .5* log(2*sigma^2*pi)
%
% However, this has large cancelation. 
%  With including I_nu(z) into the sum the expansion becomes:
%  log( term(k) ) =  ... does not work due to the zero crossing of log(z/2)-psi(0,1+k+nu) in the relevant range of k ....
%
% Seeing the approximation as taking the expectation of a function. 
% use taylor expansion of function around kmax:
%  fun(k) =          log(z/2)-psi(0,1+kmax+nu)      % aP
%          -    (k-kmax)   * (psi(1,1+kmax+nu) )    % bP
%          -1/2*(k-kmax)^2 * (psi(2,1+kmax+nu) )    % cP
%          -1/6*(k-kmax)^3 * (psi(3,1+kmax+nu) )    % dP
%  'Gauss' approximation; second order polynomial approximation of log of sum term
%  log( term(k) ) =            (2*kmax+nu) * log( z/2 ) - gammaln(1+kmax) - gammaln(1+kmax+nu)  % aG
%          +     (k-kmax)   * ( 2 log(z/2) - psi(0, 1+kmax) - psi(0, 1+kmax+nu) )               % bG
%          +.5 * (k-kmax)^2 * ( - psi(1, 1+kmax) - psi(1, 1+kmax+nu) )                          % cG
%
% then with Mathematica:
% Integrate[(aP + bP x + 1/2 cP x^2 + 1/6 dP x^3)  Exp[ aG + bG x + 1/2  cG  x^2], {x, -\[Infinity], \[Infinity]},  Assumptions -> {cG < 0}] 
% = ((3 cG (2 bG bP cG - bG^2 cP + cG (-2 aP cG + cP)) + bG (bG^2 - 3 cG) dP) exp(aG - bG^2/(2 cG)) Sqrt(pi/2) )/(3 (-cG)^( 7/2))

absZ = abs(real(z));
expZ = exp(absZ);
f = besseli( nu, z, 1);

% approximate k-index of maximum magnitude term in sum:
kmax = .5* sqrt( nu.^2+z.^2) - .5*nu - .2050 * log(1+log(1+z));

if 0
    % old case:
    % Taylor expansion of logarithm of term around kmax:
    a = (2*kmax+nu) .* log( z/2 ) +log(psi(0,1+kmax+nu)) - gammaln(1+kmax) - gammaln(1+kmax+nu);
    b = - log(4) + 2*log(z) - psi(0, 1+kmax) - psi(0, 1+kmax+nu) + psi(1, 1+kmax+nu)./psi(0,1+kmax+nu);
    c =  - psi(1, 1+kmax) - psi(1, 1+kmax+nu) - ( psi(1, 1+kmax+nu)./psi(0,1+kmax+nu) ).^2 + ( psi(2, 1+kmax+nu)./psi(0,1+kmax+nu) ).^2 ;

    % Gausian approximation of sum term:
    sigma = sqrt(-1./c);
    logSumG = a - .5* b.^2./c + .5* log(2*sigma.^2*pi);
    kstart = max(0,  ceil( kmax - 4*sigma ) ) ;
    kend =  floor( kmax + 4*sigma) ;

    logSum = logSumG - absZ;
    % Explicit sum for terms for which gaussian approximation is (expected to be) not good enough:
    for idx = find( kstart(:)' < 50 )
        k = kstart(idx) : kend(idx );
        logsumterms = log(psi( k + nu + 1 )) - gammaln(k+1)- gammaln( k + nu + 1) + (2.*k+nu) .* log(z(idx)/2) - absZ(idx);
        logSum(idx) = log( sum( exp( logsumterms )) );
    end;
    if 0
        %%
        logsumterms_GaussApprox = a + (k-kmax)*b + .5*(k-kmax).^2*c;
        plot(k, [logsumterms(:) logsumterms_GaussApprox(:)]*[1 0 1;0 1 -1])
    end;
    dfdnu = f .* log(z/2) - exp( logSum ); % Huge cancelation for large z
elseif 0
    % New case:
    % Taylor expansion of logarithm of sum term around kmax:
    aG = (2*kmax+nu) .* log( z/2 ) - gammaln(1+kmax) - gammaln(1+kmax+nu);
    bG = 2*log(z/2) - psi(0, 1+kmax) - psi(0, 1+kmax+nu) ;
    cG =  - psi(1, 1+kmax) - psi(1, 1+kmax+nu) ;
    
    aP =  log(z/2) - psi(0,1+kmax+nu);
    bP =  - psi(1,1+kmax+nu);
    cP =  - psi(2,1+kmax+nu);
    dP =  - psi(3, 1+kmax+nu);

    % Gausian approximation of sum term:
    dfdnuG = ((3*cG.* (2* bG.* bP.* cG - bG.^2 .*cP + cG .*(-2.* aP.* cG + cP)) + bG.* (bG.^2 - 3* cG).* dP).* exp(aG-absZ - bG.^2./(2* cG)).* sqrt(pi/2) )./(3 *(-cG).^( 7/2));
    sigma = sqrt(-1./cG);
    kstart = max(0,  ceil( kmax - 4*sigma ) ) ;
    kend =  floor( kmax + 4*sigma) ;

    dfdnu = dfdnuG;
    % Explicit sum for terms for which gaussian approximation is (expected to be) not good enough:
    for idx = find( kstart(:)' < 50 )
        %%
        kidx = idx;
        if numel(nu)==1
            nuidx = nu;
        else
            nuidx = nu(idx);
        end;
        k = kstart(idx) : kend( idx ) ;
        P = log(z(idx) /2)-psi(0,1+k'+nuidx) ;
        logsumterms =  - gammaln(k+1)- gammaln( k + nuidx + 1) + (2.*k+nuidx) .* log(z(idx)/2) - absZ(idx);
        dfdnu(idx) =  exp( logsumterms ) *P  ;
        if 1
            %%
            logsumterms_GaussApprox = aG(kidx) - absZ(kidx) + (k-kmax(kidx))*bG(kidx) + .5*(k-kmax(kidx)).^2*cG(kidx);
            plot(k, [logsumterms(:) logsumterms_GaussApprox(:)]*[1 0 1;0 1 -1])
            legend('terms','gaussapprox','difference')
            %% test FFT:
            kstep = median(diff(k));
            % using http://mathworld.wolfram.com/FourierTransformGaussian.html
            v = exp(logsumterms').*P;
            lF = log(abs(fft(v)));
            freq = (0:numel(lF)-1)/numel(lF)/kstep;
            lFgauss = .5*log(v'*v/kstep *pi / (-cG(kidx)*.5*sqrt(numel(v)*kstep) ) ) -pi^2 * (freq'.^2)./(-cG(kidx)*.5); 
            plot(freq,[lF, lFgauss ]);
            ylim([-20 0]+max(max(lF),max(lFgauss)))
            xlim([0 1/kstep]);
            % so at freq = 1/kstep we should have  -pi^2 * (freq'.^2)./(-cG(kidx)*.5) < log( aliasing );
            % where log(aliasing) = log(eps)-log(z+1), where the log(z+1) is substracted to very roughly compensate for the cancelation that is present. 
            % Solving for equality:
            % => -pi^2 * ((1/kstep).^2)./(-cG(kidx)*.5) = log( aliasing ) 
            % =>   -pi^2 ./(-cG(kidx)*.5)/log( aliasing ) =  kstep.^2
            % => kstep = pi / sqrt( cG(kidx)*.5 * log(aliasing) )  
        end;
    end;
else
    % newer case, Estimating aliasing error (FFT)
    % and truncation error
    % Taylor expansion of logarithm of sum term around kmax:
    aG = (2*kmax+nu) .* log( z/2 ) - gammaln(1+kmax) - gammaln(1+kmax+nu);
    bG = 2*log(z/2) - psi(0, 1+kmax) - psi(0, 1+kmax+nu) ;
    cG =  - psi(1, 1+kmax) - psi(1, 1+kmax+nu) ;
    
    % Gausian approximation of sum term:
%     dfdnuG = ((3*cG.* (2* bG.* bP.* cG - bG.^2 .*cP + cG .*(-2.* aP.* cG + cP)) + bG.* (bG.^2 - 3* cG).* dP).* exp(aG-absZ - bG.^2./(2* cG)).* sqrt(pi/2) )./(3 *(-cG).^( 7/2));
    logAcceptRelError = log(eps)-log(absZ);
    kstep = (pi*sqrt(2)) ./ sqrt( cG  .* logAcceptRelError );
%     sigma = sqrt(-1./cG);
    % kstart and kend solutions from  .5 * (k-kmax).^2 * cG == logAcceptRelError
    % => k = kmax +/- sqrt(  logAcceptRelError/(.5* cG) ) 
    krange = sqrt(  logAcceptRelError./(.5* cG) ) ; 
    kstart = max(0,   kmax - krange  ) ; 
    kend =  kmax + krange + kstep; % due to shape decay at the right hand side is slower, use one step extra. 
    % if continuous approximation may not be really valid, do the exact sum, extending boundaries slightly just in case. 
    discretize = kstep<2;
    kstep(discretize) = 1;
    kstart(discretize) = floor( kstart( discretize ));
    kend(discretize) = ceil( kend(discretize));
    
    dfdnu = zeros(size(cG));
    firstterm= dfdnu; lastterm=dfdnu;
    % Explicit sum for terms for which gaussian approximation is (expected to be) not good enough:
    for idx = 1:numel(kstart)
        %%
        if numel(nu)==1
            nuidx = nu;
        else
            nuidx = nu(idx);
        end;
        k = kstart(idx) : kstep(idx) : kend( idx );
        P = log(z(idx) /2)-psi(0,1+k'+nuidx) ; % substantial precision loss due to cancelation (but not dominant)
        if 0
            % NOTE: for large z, the following line is numerically suboptimal (causes the majority of the precision loss of the result due to cancelation)
            % Possible solution: compute gammaln and log(z/2) in high precision (say error < eps(.5) ) and evaluate carefully 
            logsumterms =  - gammaln(k+1)- gammaln( k + nuidx + 1) + (2.*k+nuidx) .* log(z(idx)/2) - absZ(idx);
        else
            % using accurate_gammaln
            [loggammak_hir, loggammak_lor] = accurate_gammaln( k + 1 );
            [loggammakn_hir, loggammakn_lor] = accurate_gammaln( k + nuidx + 1 );
            [logz_hir, logz_lor] = accurate_log( z(idx)/2 );
            lsb = floor( log2(max(loggammakn_hir)) ) -50; % least significant bit (==eps(rounder)) is 2^lsb
            rounder = 3 * 2^( lsb + 51 );
            loggammak_hi = (loggammak_hir+ loggammak_lor + rounder)-rounder;
            loggammak_lo = (loggammak_hir-loggammak_hi) + loggammak_lor;
            loggammakn_hi = (loggammakn_hir+ loggammakn_lor + rounder)-rounder;
            loggammakn_lo = (loggammakn_hir-loggammakn_hi) + loggammakn_lor;
            absZ_hi = (absZ(idx) + rounder) - rounder;
            absZ_lo = absZ(idx) - absZ_hi;
            % now eps(rounder) should be eps(rounder_k)*eps(rounder_logz)
            lsb_k = floor( log2( 2*k(end) + nuidx ) ) - 25;
            lsb_lgz = lsb - lsb_k;
            rounder_k = 3 * 2^( lsb_k + 51 );
            rounder_lgz = 3 * 2^( lsb_lgz + 51 );
            knu_hi = ((2.*k+nuidx) + rounder_k)- rounder_k;
            knu_lo = (2.*k-knu_hi)+nuidx;
            logz = logz_hir+ logz_lor;
            logz_hi = (logz + rounder_lgz)-rounder_lgz;
            logz_lo = (logz_hir-logz_hi) + logz_lor;
            logsumterms_hi =  -loggammak_hi -loggammakn_hi + knu_hi .* logz_hi - absZ_hi;
            logsumterms_lo =  -loggammak_lo -loggammakn_lo + knu_lo .* logz + knu_hi .* logz_lo  - absZ_lo;
            logsumterms = logsumterms_hi + logsumterms_lo; % main roundoff error. 
        end;
        dfdnu(idx) =  kstep(idx)*(exp( logsumterms ) *P ) ;
        firstterm(idx) = kstep(idx)*(exp(logsumterms(1) ) *P(1) ) ;
        lastterm(idx) = kstep(idx)*(exp(logsumterms(end) ) *P(end) ) ;
    end;
        if 0
            %%
            logsumterms_GaussApprox = aG(kidx) - absZ(kidx) + (k-kmax(kidx))*bG(kidx) + .5*(k-kmax(kidx)).^2*cG(kidx);
            plot(k, [logsumterms(:) logsumterms_GaussApprox(:)]*[1 0 1;0 1 -1])
            legend('terms','gaussapprox','difference')
            %% test FFT:
            kstep = median(diff(k));
            % using http://mathworld.wolfram.com/FourierTransformGaussian.html
            v = exp(logsumterms').*P;
            lF = log(abs(fft(v)));
            freq = (0:numel(lF)-1)/numel(lF)/kstep;
            lFgauss = .5*log(v'*v/kstep *pi / (-cG(kidx)*.5*sqrt(numel(v)*kstep) ) ) -pi^2 * (freq'.^2)./(-cG(kidx)*.5); 
            plot(freq,[lF, lFgauss ]);
            ylim([-20 0]+max(max(lF),max(lFgauss)))
            xlim([0 1/kstep]);
            % so at freq = 1/kstep we should have  -pi^2 * (freq'.^2)./(-cG(kidx)*.5) < log( aliasing );
            % where log(aliasing) = log(eps)-log(z+1), where the log(z+1) is substracted to very roughly compensate for the cancelation that is present. 
            % Solving for equality:
            % => -pi^2 * ((1/kstep).^2)./(-cG(kidx)*.5) = log( aliasing ) 
            % =>   -pi^2 ./(-cG(kidx)*.5)/log( aliasing ) =  kstep.^2
            % => kstep = pi / sqrt( cG(kidx)*.5 * log(aliasing) )  
            %% plot first,last term divided by value.
            % this should be < eps (or actually the next term should be <eps) to avoid truncation error.  
            figure(1);
            plot(firstterm./dfdnu .* (kstart>0) )
            figure(2);
            plot(lastterm./dfdnu  )
        end;
end;

if nargin<3 || ~isequal( divExpZ,1)
    dfdnu = dfdnu .* expZ;
end;
function test()
%%
nu = [1.5 15];
z = exp(linspace(0,log(1e8),1000)');%sort(exp(log(10000000)*rand(2000,1)));
nuM = ones(size(z))*nu; 
zM = z*ones(size(nu));
scale = 1; % use divide by exp(real(z)) argument to avoid overflow.  
[f, dfdnu] = besseli_dnu( nuM, zM ,scale);
% numerical derivative:
delta = .001;
fhi = besseli( nuM+delta, zM,scale);
flo = besseli( nuM-delta, zM,scale);
dfdnu_num = (fhi-flo)/(2*delta);
figure(1);
subplot(2,1,1);
plot(z,log(f)); title('log f'); xlabel('z');
set(gca,'Xscale','log')
subplot(2,1,2);
plot(z,reshape( log(-[dfdnu(:) dfdnu_num(:)])*[1 0 1;0 1 -1],numel(z),[])); title('log -dfdnu')
set(gca,'Xscale','log')
legend('analytic','numerical','difference')
figure(3)
plot(diff(log(-[dfdnu(:) dfdnu_num(:)]))./diff(log([zM(:) zM(:)])))
% figure(2);
% plot(z,log(-dfdnu_num)-(z-10));title('log(-dfdnu_num)-(z-10)')
% set(gca,'Xscale','log')