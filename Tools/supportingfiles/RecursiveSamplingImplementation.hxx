#ifndef RECURSIVESAMPLINGIMPLEMENTATION
#define RECURSIVESAMPLINGIMPLEMENTATION
/** \class RecursiveSamplingImplementation
 *
 * \brief This set of helper classes contains the actual implementation of the
 * recursive ND image sampling, including jacobians and derivatives. 
 *
 * GetSample computes
 *   
 *
 * This set of RecursiveBSplineImplementation is templated over all of the 
 * relevant types.
 * This allows using efficient (SSE/AVX optimized) vector implementation for
 * GetSample. Also this same version can now be used to sample scalar as
 * well as vector images of compile time known vector length. (Technically 
 * dynamic vector lengths are possible as well with the right types, but that will nececarily
 * be slower than using the vector image variant described below.)
 *
 *
 * STRUCTURE:
 *   each method has it's own class with only 1 method:
 *      RecursiveBSplineImplementation_(methodName)::(methodName)( ... )
 *   where '(methodName)' is one of the following:
 *      GetSample                    // old name: TransformPoint
 *      GetSpatialJacobian
 *      GetSpatialHessian
 *      GetJacobian                  // Note that it is almost always more efficient to use MultiplyJacobianWithValue.
 *      GetJacobianOfSpatialJacobian // TODO: A MultiplyWithJacobianOfSpatialJacobian should be implemented for more efficiency
 *      GetJacobianOfSpaialHessian   // TODO: A MultiplyWithJacobianOfSpatialHessian should be implemented for more efficiency
 *      GetNonZeroIndices
 *      GetWeightsAndNonZeroIndices
 *      MultiplyJacobianWithValue    // similar to old name, which I consider too specific: EvaluateJacobianWithImageGradientProduct
 *                                   // This is the adjoint of 'GetSample' :
 *                                   //    if you regard the sampling as matrix multiplication: GetSample(mu, ..) == Jacobian * mu
 *                                   //    then MultiplyJacobianWithValue( jac, value, ...) == transpose(Jacobian) * value
 *   TEMPLATE ARGUMENTS (selected from this list as appropriate for each method):
 *      SpaceDimension               : Dimensionality of the image. This is what is recursed over. 
 *      NumWeightsPerDimension       : Number of weights that is used in each dimension.
 *                                     -1 indicates the use of 'startEnds' instead of a fixed number.  
 *                                     When possible use a compile time known length as this allows significant 
 *                                     optimizations by the compiler. 
 *      InputImagePointerType, OutputImagePointerType :
 *                                     The pointer type to the image. 'Input' may (/should) be const.
 *      InputValueType,OutputValueType : interpolated values from ImagePointer value;   
 *      weightsPointerType           : Pointer type of the (const) weights array.  
 *      OutputIndexPointerType       : The pointer type to which the nonZero indices are stored. 
 *      gridOffsetTable0             : Step in 'image' in the first dimension (the dimension over which we iterate fastest)
 *                                     1 (=Default) : neighboring memory addresses. 
 *                                     0            : no fixed step size; use stepsTable also for the first dimension.
 *                                     USE_STEPS    : stepsTable specifies each step (for mirror boundary condition)
 *   
 *   INPUT ARGUMENTS selected from this list as appropriate for each method):   
 *     image  : Pointer to the SpaceDimension dimensional image.
 *     stepsTable : if gridOffsetTable0 == USE_STEPS
 *                      max(startEnds) element array specifying each step in each dimension
 *                      (so, when NumWeightsPerDimension != -1 that is: NumWeightsPerDimension x SpaceDimension )
 *                  else
 *                      SpaceDimension  long vector specifying the constant step size in each dimension
 *     weights    : max(startEnds) element array specifying the weights. 
 *                  (so, when NumWeightsPerDimension != -1 that is: NumWeightsPerDimension x SpaceDimension )
 *     startEnds : 2 x SpaceDimension array, specifying the start and end of the weights in each dimension. 
 *                When NumWeightsPerDimension != -1 :
 *                 startEnds[ 0 + 2 * dimension ] =  dimension    * NumWeightsPerDimension 
 *                 startEnds[ 1 + 2 * dimension ] = (dimension+1) * NumWeightsPerDimension 
 *                for dimension = 0 ... SpaceDimension-1. This is implicit (startEnds can be NULL)
 *     nzji   : Output Pointer in which the nonzero indices are stored. 
 *     
 *
 *   REASONS FOR THIS STRUCTURE:
 *   - To allow easy addition of specialized end cases.
 *   - To keep the code of each method, including the end cases, together in the file.
 *   - To allow variations in template arguments.
 *   - The reason to put a static method in a class rather than using functions is that
 *     c++ does not allow partial specialization of function templates, which in this
 *     case makes it impossible to construct the end cases.
 *
 * Other remarks:
 *   - I (DPoot) have experimented with pre-fetching. However, at this moment my conclusion is that that does not
 *     really help when the images are large, and seriously hurts performance for small images. I think the main
 *     reason is that the pre-fetching adds so many instructions that latency cannot be hidden efficiently. Also
 *     the (effective) instruction throughput seriously reduces as without pre-fetching (and small images) already
 *     the maximum throughput is almost reached.
 *   - If you need to sample vector images, use an (SSE/AVX optimized) fixed-length vector class for OutputType and a compatible InputPointerType
 *     for example vec< type, vlen> with the accompanying vecptr< type, vlen> as InputPointerType.
 *
 * 
 * Created by Dirk Poot, Erasmus MC
 */

#define USE_STEPS 2873462 // USE_STEPS is used as gridOffsetTable0 value. If set a 'steps' argument is assumed, instead of the 'gridOffsetTable' argument.
                          // Function overloading to accomplish this is not possible, since both have the same type. Hence we need a template argument
                          // for differentiating this.
                          // The main reason to reuse the 'gridOffsetTable0' argument is that this already changes the interpretation of the gridOffsetTable[ k + HelperConstVariable ]argument.
                          // Use a large integer for USE_STEPS to make sure that if it (accidentally) is interpreted as gridOffsetTable0 it will most likely cause
                          // segfaults (useful to diagnose the issue). Also it is extremely unlikely that a actual gridOffsetTable0 is such a large integer.

#include "math.h"
#include <vector>
#include <algorithm>
#include "standard_templates.hxx"
#include "bspline_filter.cpp"


// Helper function class: RecursiveSamplingImplementation_GetSample
// This class provides 1 static function that recursively calls itself to do the actual sampling using precomputed coefficients.
// Templated over all argument types to maximize flexibility. Provided as class to allow partial template specialization.
//
// Call as:
//   output_value = RecursiveSamplingImplementation_GetSample< SpaceDimension, ncoeffsperdim, imagePointerType, outputValueType, weightsPointerType>::sample( source , steps, coeffs,  startEnds) ;
//
// INPUTS:
//   SpaceDimension  : dimensionality of source
//   ncoeffsperdim : number of coefficients per dimension. Use -1 to specify with 'startEnds'
//   imagePointerType : pointer type to elements of source
//   outputValueType  : type of output value
//   weightsPointerType : pointer type to the (precomputed) filter coefficients. 
//  source  : pointer to origin of the support region for the current sample. 
//  steps   : SpaceDimension element int vector where step[i] specifies the step in source for a step in dimension i. 
//  coeffs  : ncoeffsperdim * SpaceDimension vector with filter coefficients 
//  startEnds : only used when ncoeffsperdim==-1 (i.e. unspecified)
//              2 x SpaceDimension matrix where that specifies the coeffs that are used to sample in dimension dim:
//               coeffs[ startEnds[ 0 + dim*2 ] .. startEnds[ 1 + dim*2 ] ] 
//              
// ND-IMAGE sampling function, compile time known length coefficients. 
typedef  int_t OffsetValueType;

template <int SpaceDimension, int NumWeightsPerDimension, typename InputImagePointerType, typename OutputValueType, typename weightsPointerType, int gridOffsetTable0 = 1 > 
class RecursiveSamplingImplementation_GetSample {
    public:	
        static inline OutputValueType GetSample( InputImagePointerType image, const OffsetValueType * stepsTable, weightsPointerType weights, int * startEnds) {
            OutputValueType value = 0;//Numeric<filtertype>.Zero ;
            const OffsetValueType bot              = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];

            int startIndex, endIndex;
            if (NumWeightsPerDimension==-1) {
                // use startEnds
                startIndex = startEnds[(SpaceDimension-1)*2]; 
                endIndex = startEnds[(SpaceDimension-1)*2+1];
            } else {
                // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
                startIndex = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
                endIndex = startIndex +NumWeightsPerDimension;
            }
            InputImagePointerType image_orig = image ;
            for (int k = startIndex; k < endIndex; ++k ) {
                if( gridOffsetTable0 == USE_STEPS ) {
                    image = image_orig + stepsTable[ k ]; // stepsTable specifies for each step individually
                };
                value += RecursiveSamplingImplementation_GetSample<SpaceDimension-1, NumWeightsPerDimension, InputImagePointerType, OutputValueType, weightsPointerType, gridOffsetTable0 >
                       ::GetSample(image, stepsTable, weights, startEnds) * weights[ k ];
               if( gridOffsetTable0 != USE_STEPS ){
                image += bot;
              }
            }
            return value;
        };
};
// end case: sample image image and convert to processing type.
template <int NumWeightsPerDimension, typename InputImagePointerType, typename OutputValueType, typename weightsPointerType, int gridOffsetTable0> 
class RecursiveSamplingImplementation_GetSample<0, NumWeightsPerDimension, InputImagePointerType, OutputValueType, weightsPointerType, gridOffsetTable0> {
    public: static inline OutputValueType GetSample( InputImagePointerType image, const OffsetValueType * stepsTable, weightsPointerType weights, int * startEnds) {
		return (OutputValueType) (*image); 
	}
};
/*
// ND-IMAGE sampling function, runtime known length of coefficients. 
template <int SpaceDimension, typename InputImagePointerType, typename OutputValueType, typename weightsPointerType> 
class RecursiveSamplingImplementation_GetSample<SpaceDimension,-1,InputImagePointerType, OutputValueType, weightsPointerType> {
    public:	static inline OutputValueType sample( InputImagePointerType image, ptrdiff_t * stepsTable , weightsPointerType weights, int * startEnds) {
		OutputValueType ret = 0;//Numeric<filtertype>.Zero ;
		for (int k=startEnds[(SpaceDimension-1)*2]; k < startEnds[(SpaceDimension-1)*2+1] ; k++, image += stepsTable[SpaceDimension-1]) {
			ret += RecursiveSamplingImplementation_GetSample<SpaceDimension-1, -1, InputImagePointerType, OutputValueType, weightsPointerType>::sample(image , stepsTable, weights, startEnds) * weights[ k ];
		}
		return ret;
	}
};

// end case: sample image image and convert to processing type.
template <typename InputImagePointerType, typename OutputValueType, typename weightsPointerType> 
class RecursiveSamplingImplementation_GetSample<0, -1, InputImagePointerType, OutputValueType, weightsPointerType> {
    public:	static inline OutputValueType sample( InputImagePointerType image, ptrdiff_t * stepsTable , weightsPointerType weights, int * startEnds) {
		return (OutputValueType) (*image); 
	}
};

*/


/** \class RecursiveSamplingImplementation_GetSampleAndDerivative
 *
 * \brief Define general case
 */

//OLD: template< class OutputPointerType, unsigned int SpaceDimension, unsigned int SplineOrder, class InputPointerType, int gridOffsetTable0 = 1 >
template <int SpaceDimension, int NumWeightsPerDimension, typename InputImagePointerType, typename OutputPointerType, typename weightsPointerType, int gridOffsetTable0 = 1 > 
class RecursiveSamplingImplementation_GetSampleAndDerivative{
public:
  typedef typename std::iterator_traits< OutputPointerType >::value_type OutputValueType; //\todo: is this the proper use of std::iterator_traits? Preferably we use 'using std::iterator_traits', to allow custom template specializations.

  /** GetSpatialJacobian recursive implementation. */
  static inline void GetSampleAndDerivative(
            OutputPointerType valueAndDerivativePointer,
            InputImagePointerType imagePointer,
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            int * startEnds ) {
    /** Create a temporary valueAndDerivative to hold the output of the recusively called function
     *  and initialize the original. */
    OutputValueType tmp_valueAndDerivative[ SpaceDimension ];
    for( unsigned int n = 0; n < SpaceDimension + 1; ++n ) {
      valueAndDerivativePointer[ n ] = 0.0;
    }

    const OffsetValueType bot = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex + NumWeightsPerDimension;
    }
    
    /** Make a copy of the pointers to mu. The pointer will move later. */
    InputImagePointerType imagePointer_original = imagePointer;

    for( int k = startIndex; k < endIndex; ++k  ) {
      if( gridOffsetTable0 == USE_STEPS ) {
        imagePointer = imagePointer_original + stepsTable[ k ];
      };
      RecursiveSamplingImplementation_GetSampleAndDerivative< SpaceDimension-1, NumWeightsPerDimension, InputImagePointerType, OutputValueType *, weightsPointerType, gridOffsetTable0 >
        ::GetSampleAndDerivative( tmp_valueAndDerivative, imagePointer, stepsTable, weights, derivativeWeights, startEnds);

      // Multiply by the weights
      for( unsigned int n = 0; n < SpaceDimension; ++n ) {
        valueAndDerivativePointer[ n ] += tmp_valueAndDerivative[ n ] * weights[ k ];
      }
      // Multiply 'value' part by the derivative weights and put in last element of valueAndDerivativePointer:
      valueAndDerivativePointer[ SpaceDimension ] += tmp_valueAndDerivative[ 0 ] * derivativeWeights[ k ];

      // move to the next mu
      if( gridOffsetTable0 != USE_STEPS ) {
        imagePointer += bot;
      }
    } // end loop over k

  } // end GetSpatialJacobian()


};


/** \class RecursiveSamplingImplementation_GetSampleAndDerivative
 *
 * \brief Define the end case for SpaceDimension = 0.
 */

template <int NumWeightsPerDimension, typename InputImagePointerType, typename OutputPointerType, typename weightsPointerType, int gridOffsetTable0 >         
class RecursiveSamplingImplementation_GetSampleAndDerivative< 0, NumWeightsPerDimension, InputImagePointerType, OutputPointerType, weightsPointerType, gridOffsetTable0 > {
public:

  /** GetSpatialJacobian recursive implementation. */
  static inline void GetSampleAndDerivative(
    OutputPointerType valueAndDerivativePointer, // over here only 'value' as there are zero spatial dimensions.
    InputImagePointerType imagePointer,
    const OffsetValueType * stepsTable,
    weightsPointerType weights, 
    weightsPointerType derivativeWeights,
    int * startEnds )
  {
    *valueAndDerivativePointer = *imagePointer;
  } // end GetSpatialJacobian()
};



/** \class RecursiveSamplingImplementation_GetVecSampleAndDerivative
 *   
 * \brief Define general case
 * NOTE: valueAndDerivativeAndTempspacePointer should have space for :
 *         SpaceDimension +1 + sum_d = 1..(SpaceDimension-1)  NumWeightsPerDimension * (d+1)
 *         = 1/2 SpaceDimension *(2 + NumWeightsPerDimension *(3 + SpaceDimension))
 *       vectors. This space does not need to be initialized (but obviously it needs to be allocated!)  
 * Also see RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
 *      which provides an alternative version that does not requires substantial temporary memory, but in more flops in most cases; it may be faster)
 */

template <int SpaceDimension, int NumWeightsPerDimension, typename InputImagePointerType, typename OutputPointerType, typename weightsPointerType, int gridOffsetTable0 = 1 > 
class RecursiveSamplingImplementation_GetVecSampleAndDerivative{
public:
  typedef typename std::iterator_traits< OutputPointerType >::value_type OutputValueType; //\todo: is this the proper use of std::iterator_traits? Preferably we use 'using std::iterator_traits', to allow custom template specializations.

  /** GetSpatialJacobian recursive implementation. */
  static inline void GetVecSampleAndDerivative(
            OutputPointerType valueAndDerivativeAndTempspacePointer, const int vecStride, 
            InputImagePointerType imagePointer, const int vecLen, 
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            int * startEnds ) {

    const OffsetValueType bot = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex + NumWeightsPerDimension;
    }

    /** Create a temporary valueAndDerivative to hold the output of the recusively called function
     *  and initialize the original. */
    {OutputPointerType tmp_valueAndDerivativeAndTempspacePointer = valueAndDerivativeAndTempspacePointer + (SpaceDimension+1) * vecStride  ;
    
    /** Make a copy of the pointers to mu. The pointer will move later. */
    InputImagePointerType imagePointer_original = imagePointer;

    /** Gather lower level results: */
    for( int k = startIndex; k < endIndex; ++k  ) {
      if( gridOffsetTable0 == USE_STEPS ) {
        imagePointer = imagePointer_original + stepsTable[ k ];
      };
      RecursiveSamplingImplementation_GetVecSampleAndDerivative< SpaceDimension-1, NumWeightsPerDimension, InputImagePointerType, OutputValueType *, weightsPointerType, gridOffsetTable0 >
        ::GetVecSampleAndDerivative( tmp_valueAndDerivativeAndTempspacePointer, vecStride, imagePointer, vecLen, stepsTable, weights, derivativeWeights, startEnds);
      tmp_valueAndDerivativeAndTempspacePointer += vecStride * SpaceDimension;
      // move to the next mu
      if( gridOffsetTable0 != USE_STEPS ) {
        imagePointer += bot;
      }
    }} // end loop over k
    /* single loop over vec to minimize memory accesses (assuming used values at least reside in cache sufficently long; 
       but preferably compiler unrols the 'n' and 'k' loops and the code below is just a bunch of register-register multiply-adds */
    for (int idx = 0 ; idx< vecLen ; ++idx, ++valueAndDerivativeAndTempspacePointer ) {
      for( unsigned int n = 0; n < SpaceDimension; ++n ) {
        OutputPointerType tmp_valueAndDerivativeAndTempspacePointer = valueAndDerivativeAndTempspacePointer + (SpaceDimension+1+n) * vecStride  ;
        OutputValueType accum = 0 ;
        for ( int k = startIndex; k < endIndex; ++k  , tmp_valueAndDerivativeAndTempspacePointer+= vecStride * SpaceDimension) {
          // Multiply by the weights
          accum  += *tmp_valueAndDerivativeAndTempspacePointer * weights[ k ];
        }
        valueAndDerivativeAndTempspacePointer[ n * vecStride ] = accum; 
      }
      {// Multiply 'value' part by the derivative weights and put in last element of valueAndDerivativeAndTempspacePointer:
        OutputPointerType tmp_valueAndDerivativeAndTempspacePointer = valueAndDerivativeAndTempspacePointer + (SpaceDimension+1) * vecStride  ;
        OutputValueType accum = 0 ;
        for ( int k = startIndex; k < endIndex; ++k  , tmp_valueAndDerivativeAndTempspacePointer+= vecStride * SpaceDimension) {
          accum += *tmp_valueAndDerivativeAndTempspacePointer * derivativeWeights[ k ];
        }
        valueAndDerivativeAndTempspacePointer[ SpaceDimension * vecStride ] = accum; 
      }
    }
  } // end GetVecSampleAndDerivative()
};


/** \class RecursiveSamplingImplementation_GetVecSampleAndDerivative
 *
 * \brief Define the end case for SpaceDimension = 1.
 */

template <int NumWeightsPerDimension, typename InputImagePointerType, typename OutputPointerType, typename weightsPointerType, int gridOffsetTable0 >         
class RecursiveSamplingImplementation_GetVecSampleAndDerivative< 1, NumWeightsPerDimension, InputImagePointerType, OutputPointerType, weightsPointerType, gridOffsetTable0 > {
public:
static const int SpaceDimension = 1; 
  typedef typename std::iterator_traits< OutputPointerType >::value_type OutputValueType; //\todo: is this the proper use of std::iterator_traits? Preferably we use 'using std::iterator_traits', to allow custom template specializations.
  /** GetSpatialJacobian recursive implementation. */
  static inline void GetVecSampleAndDerivative(
            OutputPointerType valueAndDerivativeAndTempspacePointer, const int vecStride,
            InputImagePointerType imagePointer, const int vecLen, 
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            int * startEnds ) {

    const OffsetValueType bot = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex + NumWeightsPerDimension;
    }

  
    /** Make a copy of the pointers to mu. The pointer will move later. */
    InputImagePointerType imagePointer_original = imagePointer;

    /* single loop over vec to minimize memory accesses (assuming used values at least reside in cache sufficently long; 
       but preferably compiler unrols the 'n' and 'k' loops and the code below is just a bunch of register-register multiply-adds */
    for (int idx = 0 ; idx< vecLen ; ++idx, ++valueAndDerivativeAndTempspacePointer, ++imagePointer_original ) {
      imagePointer = imagePointer_original;
      OutputValueType accum  = 0 ;
      OutputValueType accumD = 0 ;
      for ( int k = startIndex; k < endIndex; ++k ) {
        if( gridOffsetTable0 == USE_STEPS ) {
          imagePointer = imagePointer_original + stepsTable[ k ];
        };
        // Multiply by the weights
        OutputValueType imageValue = *imagePointer; 
        accum  += imageValue * weights[ k ];
        accumD += imageValue * derivativeWeights[ k ];

        if( gridOffsetTable0 != USE_STEPS ) {
          imagePointer += bot;
        }
      }
      valueAndDerivativeAndTempspacePointer[         0 ] = accum; 
      valueAndDerivativeAndTempspacePointer[ vecStride ] = accumD; 
    }
  } // end GetVecSampleAndDerivative()
};

/** \class RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
 *   
 * \brief Define general case
 * Also see RecursiveSamplingImplementation_GetVecSampleAndDerivative
 *      which provides an alternative version that does require substantial temporary memory, but less flops in most cases; it may be faster.
 */

template <int SpaceDimension, int NumWeightsPerDimension, typename InputImagePointerType, typename OutputPointerType, typename weightsPointerType, int gridOffsetTable0 = 1 , int SpaceDimensionOut = SpaceDimension> 
class RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt{
public:
   typedef typename std::iterator_traits< OutputPointerType >::value_type OutputValueType; //\todo: is this the proper use of std::iterator_traits? Preferably we use 'using std::iterator_traits', to allow custom template specializations.
   typedef typename std::iterator_traits< weightsPointerType >::value_type WeightsValueType; 
   // External routine:
   static inline void GetVecSampleAndDerivative(
            OutputPointerType valueAndDerivativePointer, const int vecStride, 
            InputImagePointerType imagePointer, const int vecLen, 
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            int * startEnds ) {
      WeightsValueType one[1] ={1};
      for (int k  = 0 ; k < vecLen * (SpaceDimensionOut +1 ) ; ++ k ) {
        valueAndDerivativePointer[ k ] = 0;  // initialize memory. 
      }
      GetVecSampleAndDerivative(valueAndDerivativePointer, vecStride, imagePointer, vecLen, stepsTable, weights, derivativeWeights, startEnds , one );
   }

  /** GetSpatialJacobian recursive implementation; internal function */
  static inline void GetVecSampleAndDerivative(
            OutputPointerType valueAndDerivativePointer, const int vecStride, 
            InputImagePointerType imagePointer, const int vecLen, 
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            const int * startEnds , const WeightsValueType * curweights) {

    const OffsetValueType bot = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex + NumWeightsPerDimension;
    }

    /** Create a temporary valueAndDerivative to hold the output of the recusively called function
     *  and initialize the original. */
    WeightsValueType nextweights[ SpaceDimensionOut-SpaceDimension + 2 ] ; // e.g.: if SpaceDimension==1 we prepare the weights for dimension 0, which requires SpaceDimensionOut+1 elements. 
    
    /** Make a copy of the pointers to mu. The pointer will move later. */
    InputImagePointerType imagePointer_original = imagePointer;

    /** Gather lower level results: */
    for( int k = startIndex; k < endIndex; ++k  ) {
      if( gridOffsetTable0 == USE_STEPS ) {
        imagePointer = imagePointer_original + stepsTable[ k ];
      };
      nextweights[1] = curweights[0] * derivativeWeights[ k ]; // derivative SpaceDimension
      nextweights[0] = curweights[0] * weights[ k ]; // value
      for ( int prevdims = 1; prevdims <= SpaceDimensionOut-SpaceDimension ; ++prevdims ) {
        nextweights[prevdims+1] = curweights[prevdims] * weights[ k ]; // derivatives SpaceDimension+1 .. SpaceDimensionOut
      }
      RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt< SpaceDimension-1, NumWeightsPerDimension, InputImagePointerType, OutputPointerType, weightsPointerType, gridOffsetTable0, SpaceDimensionOut >
        ::GetVecSampleAndDerivative( valueAndDerivativePointer, vecStride, imagePointer, vecLen, stepsTable, weights, derivativeWeights, startEnds, nextweights);
      // move to the next mu
      if( gridOffsetTable0 != USE_STEPS ) {
        imagePointer += bot;
      }
    } // end loop over k
  } // end GetVecSampleAndDerivative()
};


/** \class RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt
 *
 * \brief Define the end case for SpaceDimension = 0.
 */

template <int NumWeightsPerDimension, typename InputImagePointerType, typename OutputPointerType, typename weightsPointerType, int gridOffsetTable0 , int SpaceDimensionOut>         
class RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt< 0, NumWeightsPerDimension, InputImagePointerType, OutputPointerType, weightsPointerType, gridOffsetTable0, SpaceDimensionOut> {
public:
static const int SpaceDimension = 0; 
  typedef typename std::iterator_traits< OutputPointerType >::value_type OutputValueType; //\todo: is this the proper use of std::iterator_traits? Preferably we use 'using std::iterator_traits', to allow custom template specializations.
   typedef typename std::iterator_traits< weightsPointerType >::value_type WeightsValueType; 

   enum {vlen = vec<OutputValueType,16>::naturalLen *2 }; // TODO: may want to test if 2 x naturalLen exists; 2 x chosen to reduce loop overhead.  
	enum {doVectorize = AND< vecptr<InputImagePointerType,vlen>::supported, vecptr<OutputPointerType,vlen>::supported>::RET};
	typedef typename IF< doVectorize  , vecptr<InputImagePointerType,vlen>, BAD_TYPE>::RET InputImagePointerType_vec;
	typedef typename IF< doVectorize  , vecptr<OutputPointerType,vlen>, BAD_TYPE>::RET OutputPointerType_vec;
	typedef RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt<0, NumWeightsPerDimension, InputImagePointerType_vec, OutputPointerType_vec, weightsPointerType, gridOffsetTable0, SpaceDimensionOut> vecSelf;


  /** GetSpatialJacobian recursive implementation. */
  static inline void GetVecSampleAndDerivative(
            OutputPointerType valueAndDerivativePointer, const int vecStride,
            InputImagePointerType imagePointer, const int vecLen, 
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            const int * startEnds , const WeightsValueType * curweights) {
    int idx = 0 ;
    if (doVectorize &&  (vecStride/(vlen==0?1:vlen))*vlen ==vecStride ) { // if vlen==0, doVectorize should always be zero, but adding the condition avoids division by zero warnings in gcc. As vlen is a compile time known constant the condition should be optimized away. 
      int vecLen_vec = vecLen/(vlen==0?1:vlen);
			InputImagePointerType_vec imagePointer_vec(imagePointer);
			OutputPointerType_vec valueAndDerivativePointer_vec(valueAndDerivativePointer);
      vecSelf::GetVecSampleAndDerivative(valueAndDerivativePointer_vec, vecStride/(vlen==0?1:vlen), imagePointer_vec,  vecLen_vec , stepsTable, weights, derivativeWeights, startEnds , curweights);
      idx += vlen *  vecLen_vec ;
      valueAndDerivativePointer += vlen *  vecLen_vec ;
      imagePointer += vlen *  vecLen_vec ;
    }
    /* loop over vec once to minimize main memory accesses (image is not in cache)
       Assuming valueAndDerivativePointer content is in cache. */
    for (; idx< vecLen ; ++idx, ++valueAndDerivativePointer, ++imagePointer ) {
      OutputValueType imageValue = *imagePointer; 
      for ( int k = 0; k <= SpaceDimensionOut; ++k ) { // SpaceDimensionOut+1 elements combining [ value, derivative]
        // Multiply by the weights
        valueAndDerivativePointer[ k*vecStride ] += imageValue * curweights[ k ];
      }
    }
  } // end GetVecSampleAndDerivative()
};

template <int NumWeightsPerDimension, typename weightsPointerType, int gridOffsetTable0 , int SpaceDimensionOut>         
class RecursiveSamplingImplementation_GetVecSampleAndDerivative_Alt< 0, NumWeightsPerDimension, BAD_TYPE, BAD_TYPE, weightsPointerType, gridOffsetTable0, SpaceDimensionOut> {
public:
   typedef typename std::iterator_traits< weightsPointerType >::value_type WeightsValueType; 

  /** GetSpatialJacobian recursive BAD_TYPE end case. */
  static inline void GetVecSampleAndDerivative(
            BAD_TYPE valueAndDerivativePointer, const int vecStride,
            BAD_TYPE imagePointer, const int vecLen, 
            const OffsetValueType * stepsTable,
            weightsPointerType weights, 
            weightsPointerType derivativeWeights,
            const int * startEnds , const WeightsValueType * curweights) {  };
};

/** \class RecursiveSamplingImplementation_MultiplyJacobianWithValue
 *
 * \brief Define general case
 * This class should almost always be preferred used over GetSpatialJacobian and ComputeNonZeroJacobianIndices.
 *
 * INPUTS:
 *    gradCoefficients : identical type (and size) to mu input in the corresponding GetSample.
 *                       The result of the multiplication with the Jacobian is added to this vector.
 *    weight : scaling applied to multiplication (typically == 1.0)
 *    gradValue : same type as output of GetSample. Typically contains the image gradient.
 *                You might want to add a reference in InputValueType (so that gradValue is passed by reference, rather than by value)
 *    gridOffsetTable & weights1D : identical to the ones used in GetSample.
 *
 * NOTE for programmers of this class:
 *   Currently there is a difference in recursion between GetSample and MultiplyJacobianWithValue.
 *   This difference is not needed; in MultiplyJacobianWithValue we could multiply gradValue by weights1D[..] or, alternatively
 *   we could pass a 'weight' value also in GetSample. Currently do not know which recursion is more efficient.
 */

template< unsigned int SpaceDimension, int NumWeightsPerDimension, typename OutputImagePointerType, typename InputValueType, typename weightsPointerType, int gridOffsetTable0 = 1 >
class RecursiveSamplingImplementation_MultiplyJacobianWithValue {
public:

  /** Helper constant variable. */
  //itkStaticConstMacro( HelperConstVariable, unsigned int, ( SpaceDimension - 1 ) * ( NumWeightsPerDimension ) );

  /** MultiplyJacobianWithValue recursive implementation. */
  static void MultiplyJacobianWithValue(OutputImagePointerType image, const OffsetValueType * stepsTable , weightsPointerType weights, int * startEnds, InputValueType value )
  {
    const OffsetValueType bot                  = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( (int) SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex +NumWeightsPerDimension;
    }
    
    OutputImagePointerType     image_orig = image;
    for( int k = startIndex; k < endIndex; ++k )
    {
      if( gridOffsetTable0 == USE_STEPS ) {
        image = image_orig + stepsTable[ k ];
      }
      RecursiveSamplingImplementation_MultiplyJacobianWithValue< SpaceDimension-1, NumWeightsPerDimension, OutputImagePointerType, InputValueType, weightsPointerType, gridOffsetTable0 >
        ::MultiplyJacobianWithValue( image, stepsTable, weights, startEnds, value * weights[ k ] );

      if( gridOffsetTable0 != USE_STEPS )  {
        image += bot;
      }

    } //end loop over k

  } // end MultiplyJacobianWithValue()


};


/** \class RecursiveSamplingImplementation_MultiplyJacobianWithValue
 *
 * \brief Define the end case for SpaceDimension = 0.
 */

template< int NumWeightsPerDimension, typename OutputImagePointerType, typename InputValueType, typename weightsPointerType, int gridOffsetTable0 >
class RecursiveSamplingImplementation_MultiplyJacobianWithValue< 0, NumWeightsPerDimension, OutputImagePointerType, InputValueType, weightsPointerType, gridOffsetTable0> {
public:

  /** MultiplyJacobianWithValue recursive implementation. */
  static void MultiplyJacobianWithValue(OutputImagePointerType image, const OffsetValueType * stepsTable, weightsPointerType weights, int * startEnds, InputValueType value ) {
    *image += value;
  }; // end MultiplyJacobianWithValue()
};

/** \class RecursiveSamplingImplementation_GetNonZeroIndices
 *
 * \brief Define general case
 * This class computes the nonzero Jacobian indices (which also are the nonzero Jacobian of SpatialJacobian and Jacobian of SpatialHessian indices)
 * This class is almost never needed. Typically use MultiplyJacobianWithValue as that implicitly computes these indices.
 *
 * Special remark:
 *  if GetJacobian is called with a vector OutputIndexPointerType, GetNonZeroIndices should be called with
 *  this vector dimension as first dimension.
 * e.g.:
 *    RecursiveSamplingImplementation_GetJacobian< vecptr< double *, vecLength>, SpatialDimension, NumWeightsPerDimension, double>
 *      ::getJacobian( jacobians, weights1D, value )
 *    // note the typical call has vecLength == SpatialDimension
 * has as matching call:
 *    scaledGridOffsetTable[ SpatialDimension ]
 *    for (i = 1; i < SpatialDimension; ++i ) {
 *       scaledGridOffsetTable[i] = gridOffsetTable[i]*vecLength;
 *    }
 *    temp_nzji = nzji;
 *    CurrentIndexArray[vecLength];
 *    for (int i = 0; i < vecLength; ++i ) {
 *      CurrentIndexArray[i] = CurrentIndex+i;
 *    }
 *    vec< int, vecLength> vecCurrentIndex( & CurrentIndexArray[0] )
 *    RecursiveSamplingImplementation_GetNonZeroIndices< vecptr< int *, vecLength>, SpatialDimension , NumWeightsPerDimension, vec< int, vecLength> >
 *      ::GetNonZeroIndices( temp_nzji, vecCurrentIndex, &scaledGridOffsetTable[0] );
 *
 * Note: the input argument nzji is incremented to the end.
 */

template< class OutputIndexPointerType, unsigned int SpaceDimension, int NumWeightsPerDimension, class InputValueType, int gridOffsetTable0 = 1 >
class RecursiveSamplingImplementation_GetNonZeroIndices {
public:
  typedef typename std::iterator_traits< OutputIndexPointerType >::value_type InputIndexValueType;
  //const unsigned int HelperConstVariable = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ) ;

  /** GetNonZeroIndices recursive implementation. */
  static inline void GetNonZeroIndices(
    OutputIndexPointerType & nzji,
    InputIndexValueType currentIndex,
    const OffsetValueType * stepsTable,
    const int * startEnds)
  {
    InputIndexValueType        tmp_currentIndex = currentIndex;
    const OffsetValueType bot              = SpaceDimension == 1 && gridOffsetTable0 != 0 ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex +NumWeightsPerDimension;
    }
    for( unsigned int k = startIndex; k < endIndex; ++k ) {
      if( gridOffsetTable0 == USE_STEPS ) {
        tmp_currentIndex = currentIndex + stepsTable[ k  ];
      };
      RecursiveSamplingImplementation_GetNonZeroIndices< OutputIndexPointerType, SpaceDimension - 1, NumWeightsPerDimension, InputValueType, gridOffsetTable0 >
        ::GetNonZeroIndices( nzji, tmp_currentIndex, stepsTable );

      if( gridOffsetTable0 != USE_STEPS ){
        tmp_currentIndex += bot;
      }
    } //end loop over k
  } // end GetNonZeroIndices()
};


/** \class RecursiveSamplingImplementation_GetNonZeroIndices
 *
 * \brief Define the end case for SpaceDimension = 0.
 */

template< typename OutputIndexPointerType, int NumWeightsPerDimension, typename InputValueType, int gridOffsetTable0 >
class RecursiveSamplingImplementation_GetNonZeroIndices< OutputIndexPointerType, 0, NumWeightsPerDimension, InputValueType, gridOffsetTable0 > {
  public: static inline void GetNonZeroIndices(
    OutputIndexPointerType & nzji,
    InputValueType currentIndex,
    const OffsetValueType * stepsTable,
    const int * startEnds)
  {
    *nzji = currentIndex;
    ++nzji;
  } // end GetNonZeroIndices()
};



/** \class RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices
 *
 * \brief Define general case
 * This class computes the nonzero Jacobian indices (which also are the nonzero Jacobian of SpatialJacobian and Jacobian of SpatialHessian indices)
 * This class is almost never needed. Typically use MultiplyJacobianWithValue as that implicitly computes these indices.
 *
 * Special remark:
 *  if GetJacobian is called with a vector OutputIndexPointerType, GetWeightsAndNonZeroIndices should be called with
 *  this vector dimension as first dimension.
 * e.g.:
 *    RecursiveSamplingImplementation_GetJacobian< vecptr< double *, vecLength>, SpatialDimension, NumWeightsPerDimension, double>
 *      ::getJacobian( jacobians, weights1D, value )
 *    // note the typical call has vecLength == SpatialDimension
 * has as matching call:
 *    scaledGridOffsetTable[ SpatialDimension ]
 *    for (i = 1; i < SpatialDimension; ++i ) {
 *       scaledGridOffsetTable[i] = stepsTable[i]*vecLength;
 *    }
 *    temp_nzji = nzji;
 *    CurrentIndexArray[vecLength];
 *    for (int i = 0; i < vecLength; ++i ) {
 *      CurrentIndexArray[i] = CurrentIndex+i;
 *    }
 *    vec< int, vecLength> vecCurrentIndex( & CurrentIndexArray[0] )
 *    RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< vecptr< int *, vecLength>, SpatialDimension , NumWeightsPerDimension, vec< int, vecLength> >
 *      ::GetWeightsAndNonZeroIndices( temp_nzji, vecCurrentIndex, &scaledGridOffsetTable[0] );
 *
 * Note: the input argument nzji is incremented to the end.
 */

template< typename OutputImagePointerType, typename OutputIndexPointerType, typename weightsPointerType, unsigned int SpaceDimension, int NumWeightsPerDimension, int gridOffsetTable0 = 1 >
class RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices {
public:
  typedef typename std::iterator_traits< OutputImagePointerType >::value_type InputImageValueType;
  typedef typename std::iterator_traits< OutputIndexPointerType >::value_type InputIndexValueType;
  /** GetWeightsAndNonZeroIndices recursive implementation. */
  static inline void GetWeightsAndNonZeroIndices(
    OutputImagePointerType & image,
    OutputIndexPointerType & nzji,
    InputImageValueType currentValue,      
    InputIndexValueType currentIndex,
    weightsPointerType weights,       
    const OffsetValueType * stepsTable,
    const int * startEnds)
  {
    InputImageValueType        tmp_currentValue = currentValue;     
    InputIndexValueType        tmp_currentIndex = currentIndex;
    // Get size of step in current dimension (when step is constant in each dimension)
    const OffsetValueType bot              = (SpaceDimension == 1 && gridOffsetTable0 != 0) ? gridOffsetTable0 : stepsTable[ SpaceDimension - 1 ];
    // Compute the range of coefficients to process now:
    int startIndex, endIndex;
    if (NumWeightsPerDimension==-1) {
        // use startEnds
        startIndex = startEnds[(SpaceDimension-1)*2]; 
        endIndex = startEnds[(SpaceDimension-1)*2+1];
    } else {
        // compile time known startIndex and endIndex; assume compiler optimizes for loop away (for the first few dims)
        startIndex = ( (int) SpaceDimension - 1 ) * ( NumWeightsPerDimension  ); 
        endIndex = startIndex +NumWeightsPerDimension;
    }
    // Iterate over this range (
    for( int k = startIndex; k < endIndex; ++k ) {
      if( gridOffsetTable0 == USE_STEPS ) {
        tmp_currentIndex = currentIndex + stepsTable[ k ]; // stepsTable specifies for each step individually
      };
      RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< OutputImagePointerType, OutputIndexPointerType, weightsPointerType, SpaceDimension - 1, NumWeightsPerDimension, gridOffsetTable0 >
        ::GetWeightsAndNonZeroIndices( image, nzji, currentValue * weights[ k ], tmp_currentIndex, weights, stepsTable, startEnds);

      if( gridOffsetTable0 != USE_STEPS ){
        tmp_currentIndex += bot;
      }
    } //end loop over k
  } // end GetWeightsAndNonZeroIndices()
};


/** \class RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices
 *
 * \brief Define the end case for SpaceDimension = 0.
 */

template< typename OutputImagePointerType, typename OutputIndexPointerType,  typename weightsPointerType, int NumWeightsPerDimension, int gridOffsetTable0 >
class RecursiveSamplingImplementation_GetWeightsAndNonZeroIndices< OutputImagePointerType, OutputIndexPointerType, weightsPointerType, 0, NumWeightsPerDimension, gridOffsetTable0 > {
  typedef typename std::iterator_traits< OutputImagePointerType >::value_type InputImageValueType;
  typedef typename std::iterator_traits< OutputIndexPointerType >::value_type InputIndexValueType;
  public: static inline void GetWeightsAndNonZeroIndices(
    OutputImagePointerType & image,
    OutputIndexPointerType & nzji,
    InputImageValueType currentValue,      
    InputIndexValueType currentIndex,
    weightsPointerType weights,       
    const OffsetValueType * stepsTable,
    const int * startEnds)
  {
    *image = currentValue;
    *nzji = currentIndex;
    ++image;
    ++nzji;
  } // end GetWeightsAndNonZeroIndices()
};










#endif // RECURSIVESAMPLINGIMPLEMENTATION