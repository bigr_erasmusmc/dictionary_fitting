function [S] = escapeTex(varargin)
% [S ] = escapeTex( S1, S2 , ..)
% Escapes char array S so it is to be displayed 'as is' when fed into an
% tex interpretter (like legend and title)
% eg: 'a_b' is escaped to 'a\_b'
%
% INPUTS:
%   S1, S2, ... :  Strings (character arrays)
%  
% OUTPUT: 
%   S: Escaped text. If one input is provided it is the escaped string. 
%      If multiple inputs are provided it is a cell array with the escaped
%      strings. 
%
% Created by Dirk Poot, Erasmus MC,
% Updated help : 21-4-2015

persistent regexpVer 
if isempty(regexpVer )
    regexpVer = strcmp(regexprep('_','\_','\\$1'),'\$1'); % never gives warning and can discriminate between al known versions.
end;

% escapes: '\^{}'
if regexpVer
    % MATLAB 7:
    % regexptranslate('escape', '\\')
    
    % replacements: in any '\{', '\}',  '\^' , '\%', '\&' ,'\\' replace the '\' by '\textbackslash{}'
    
    S=regexprep(varargin,{'([\\\{\}_\^%&])','\\\\','\\textbackslash\{\}\\textbackslash\{\}'},{'\\$1','\\textbackslash\{\}','\\\\'});
    
else
    % MATLAB 6:
    S=regexprep(varargin,'([\\\{\}_\^])','\$1','tokenize');
end;
if nargin==1
    S=S{1};
end;