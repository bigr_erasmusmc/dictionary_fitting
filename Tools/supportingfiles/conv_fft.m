function [C] = conv_fft(A, B, shape, method)
% [C] = conv_fft(A, B [, shape, method])
% convolution of 2 column vectors or a matrix and a vector
% Same output as the conv function, but using fft's when that's expected to 
% be faster. 
%
% Less accurate when the dynamical range of A and/or B is large but much faster
% when both A and B have more than about 100 elements.
% 
% INPUTS:
%  A : k x (1 or m) matrix
%  B : l x (1 or m) matrix
%  shape : 'full'  default : full convolution
%          'same'  C has size k x m, central part of convolution
%          'valid' central part of the convolution.
%  method : 0 : automatically determine using conv or fft based.
%           1 : always call conv
%           2 : always use fft based algorithm
%
% OUTPUTS:
%  C : except for rounoff errors same output as 
%       C(:,k) = conv( A(:,1 or k), B(:,1 or k) , shape )   
%          % where '1 or k' means: whichever applicable based on the size of A or B
%
% Created by Dirk Poot, Erasmus MC
% 23-7-2014: based on convn_fft

if nargin<3 || isempty(shape)
    shape = 'full';
end;
if nargin<4 
    method =0;
end;

%% do FFT based convolution for speed:
[szA, ncolA] = size(A);
[szB, ncolB] = size(B);
if method == 0 
    % Make automatic choice (as fast as possible)
    method = 1+ (( szA-30)*(szB-30)<14000);
end;

if method==1 
    % conv might be faster (or it does not matter much)
    if ncolA==1 && ncolB==1
        C = conv(A,B,shape);
    else
        if ncolA==ncolB || ncolA==1 || ncolB==1
            C = cell(1,max(ncolA,ncolB));
            for k=1:numel(C)
                C{k} = conv(A(:,1+(k-1)*(ncolA>1)),B(:,1+(k-1)*(ncolB>1)),shape);
            end;
            C = [C{:}];
        else
            error('Inconsistent number of columns. The number of columns of A and B is not equal and not 1.');
        end;
    end;
    return;
end;
if  strcmpi(shape,'full') && szA<szB
    [A,B,szA,szB] = deal(B,A,szB,szA);
end;
%%
nblks = @(sz) ceil( szA ./ (sz-szB+1) );
if method>szB
    blkSz = method;
    n=nblks(  blkSz ) ;
else
    comp_fft = @(sz) 2.7e3 + 20*sz + log2(sz).*sz; % approximate nr of clock cycles for a fft of sz (when sz is a good FFT size)
    % # blocks: there are sz-szB+1 elements of A processed per block.

    tstblkSz = goodFFTsizes( min(szA+szB-1,ceil(23.8*(szB+70))) ); % get some sizes that are good for FFT and within a factor 2 of the size that is optimal for an infinite A.
    tstn = nblks(  tstblkSz ) ;
    tstcomp_blk = comp_fft( tstblkSz ) .* tstn;
    [comp_blk, idx ] = min( tstcomp_blk);
    n     = tstn(idx);
    blkSz = tstblkSz(idx);
end;
%%
% switch lower(shape)
%     % timing derived below.
%     case 'full'
%         comp_conv = .5*(szA+40)*(szB+40)+1e5; % approximate nr of clock cycles.
%         % = comp_fft( sz ) * 
%     case 'same'
%         comp_conv = .44*(szA+70)*(szB-80)+1e5; % approximate nr of clock cycles.
%     case 'valid'
%         comp_conv = .7*max(0,szA-szB).*szB + 6e4; % approximate nr of clock cycles.
%     otherwise
%         error('invalid shape argument');
% end;

if 0
    %% timing of convolution :
    szA = round(exp(linspace(6,7.3,45)));
    szB = round(exp(linspace(6,12,420)));
    shape = 'full';
    tstmeth = [1 2 11664 12288 13122 13824 15552 16384 17496 18432 19683 20736 23328 24576 26244 27648 31104 32768 34992 36864 39366 41472 46656 49152 52488 55296 59049 62208 65536 69984 73728 ]
    
    tu = zeros(numel(szB),numel(szA), numel(tstmeth));
%     tuF = zeros(numel(szB),numel(szA));
    nlp1 = zeros(size(tu));
    for szAidx = 1:numel(szA);
        A = randn(szA(szAidx),1);
        for szBidx =1:numel(szB)
            B = randn(szB(szBidx),1);
            for curmethidx = 1:numel(tstmeth)
                curmeth = tstmeth(curmethidx);
                if curmeth==1
                    nlp = ceil( 1e8/(1e5+szA(szAidx)*szB(szBidx)) );
                else
                    % TODO use better test size for FFT
                    % Especially with small sizes (as it is much slower than normal conv).
                    nlp = ceil( 1e8/(1e5+szA(szAidx)*szB(szBidx)) );
                end;
                nlp1(szBidx,szAidx,curmethidx) = nlp;
                tic;
                for k=1:nlp
                    Cc = conv_fft(A,B,shape,curmeth);
                end;
                tu(szBidx,szAidx,curmethidx ) = toc/nlp;
            end;
%             tic;
%             for k=1:nlp
%                 Cf = conv_fft(A,B,shape,2);
%             end;
%             tuF(szBidx,szAidx) = toc/nlp;
%             if any( abs(Cc-Cf) > 100*eps * max(abs(Cc)))
%                 error('too large difference between conv and fft')
%             end;
        end;
    end;
    [szB,szA] = ndgrid(szB,szA);
    %%
    imagebrowse(tu<tuF)
    %%
    if strcmpi(shape , 'same')
        X = [ones(numel(szB),1)   max(0,szA(:)-szB(:)).*szB(:)];
    elseif strcmpi(shape , 'full')
        X = [ones(numel(szB),1) szB(:) szA(:) szB(:).*szA(:)];
    else
        X = [ones(numel(szB),1) szB(:) szB(:).^2 szA(:) szA(:).^2 szB(:).*szA(:)  max(0,szA(:)-szB(:)).*szB(:)];
        
    end;
%     X = [ones(numel(szB),1) szB(:)  szA(:) szA(:).^2 szB(:).*szA(:)];

%     
    clcfreq = 2.6e9;
    Y = [tu(:) tuF(:)]*clcfreq;
    tht = bsxfun(@times, X, nlp1(:)) \ bsxfun(@times, Y, nlp1(:));
    residnrm = sqrt(mean(  bsxfun(@times,  Y - (X*tht) ,  nlp1(:) ).^2 ) )
    tht(1,:),tht(2:end,:)
    figure(1);
    imagebrowse(cat(3,reshape(Y,[size(tu) size(Y,2)]),reshape(Y-X*tht,[size(tu) size(Y,2)]) ),[-1 1]*1e7);
    %% timing of FFT :
    szf = goodFFTsize( round(exp(linspace(3,12,20))) );
    tuf = zeros(numel(szf),1);
    nlpf = zeros(size(tuf));
    for szAidx = 1:numel(szf);
        A = randn(szf(szAidx),1);
        nlp = ceil( 1e9/(1e5+log2(szf(szAidx))*szf(szAidx)) );
        nlpf(szAidx) = nlp;
        tic;
        for k=1:nlp
            C = fft(A);
        end;
        tuf(szAidx) = toc/nlp;
    end;
    % %
    Xf = [ones(numel(szf),1) szf(:) log2(szf(:)).*szf(:)];
    clcfreqf = 2.6e9;
    thtf = bsxfun(@times, Xf, nlpf(:))\(tuf(:).*nlpf(:))*clcfreqf;
    thtf(1),thtf(2:end)
    figure(2)
    plot(log([tuf*clcfreq, abs(Xf*thtf)]))
    %% find good size:
    [szB,sz]=ndgrid(1:10000,unique(goodFFTsize(1:1000000)));
    comp_fft = @(sz) 2.7e3 + 20*sz + log2(sz).*sz;
    nblks = @(szB, sz) (1 ./ (sz-szB+1) );
    C= comp_fft(sz) .* nblks(szB,sz);
    C(sz<szB)=inf;
    [Cm, id]=min(C,[],2);
    %%
    plot(szB(:,1), sz(1,id)'./(szB(:,1)+70))
end;

%%
% expndsz = goodFFTsize(szA+szB-1);

FB = fft( B, blkSz );
Cisreal = isreal(A) && isreal(B);
if n == 1
    % shortcut when there is just 1 block:
    FA = fft( A, blkSz );
    if Cisreal
        C = ifft( FA.*FB, [], 'symmetric');
    else
        C = ifft( FA.*FB );
    end;
else
    stepA = blkSz - szB +1;
    C = zeros( (n-1)*stepA + blkSz , size(A,2) );

    for k = 0 : n-1
        start_a = k*stepA+1;
        end_a = min(start_a+stepA-1, szA);
        end_c = start_a+blkSz-1;
        
        FA = fft( A( start_a:end_a, : ) , blkSz, 1);
        if Cisreal
            blkC = ifft(FA.*FB,[],'symmetric');
        else
            blkC = ifft(FA.*FB);
        end;
        C( start_a:end_c ,:)  = C( start_a:end_c ,: ) + blkC;
    end;    
end;

switch lower(shape)
    case 'full'
        start_c = 1; 
        end_c = szA+szB-1;
    case 'same'
        start_c = floor(szB/2)+1;
        end_c = szA-1+start_c;
    case 'valid'
        start_c = szB;
        end_c = szA;
    otherwise
        error('invalid shape argument');
end;
C = C( start_c : end_c , : );
  


function test
%%
testargs = { randn(20,1), randn( 4,1),'full' ; % typical full
             randn(20,1)+1i*randn(20,1), randn(4,1),'full' ; % complex A
             randn(20,1), randn( 4,1)+1i*randn(4,1),'full' ; % complex B
             randn(20,1)+1i*randn(20,1), randn(4,1)+1i*randn(4,1),'full' ; % complex A & B
             randn( 4,1), randn(20,1),'full' ; % 'reverse' full
             randn(20,1), randn( 5,1),'full' ; % shortcut full
             randn(20,1), randn( 4,1),'same' ; % typical same, even size b
             randn(20,1), randn( 5,1),'same' ; % typical same, even size b
             randn(21,1), randn( 5,1),'same' ; % typical same, odd size b
             randn(20,1), randn( 5,1),'valid' ; % typical valid
             randn( 4,1), randn(20,1),'valid' ; % 'reverse' valid
             randn(100101,1), randn(9111,1),'same' ; % Large test in which FFT should be considerably faster. 
             randn(100102,1), randn(1444,1),'full' ; % gridsz>1
             };
Tconvn     = zeros(         1,size(testargs,1));
Tconvn_fft = zeros(         1,size(testargs,1));
for k = 1 : size(testargs,1)
    [A, B, shape] = deal( testargs{k,:});
    %% convolute filter with test image:
    tic;
    Co = conv( A, B, shape);
    Tconvn(k) = toc;
    tic
    [C] = conv_fft(A, B, shape);
    Tconvn_fft(k) = toc;
%     if ~isempty(C)
%         imagebrowse(cat(5,C-Co),[-1 1])
%     end;
    if ~isequal(size(C),size(Co)) || any(max(abs(C(:)-Co(:)))>(100+numel(A))*eps)
        error('error to large or different size');
    end;
end;
[Tconvn;Tconvn_fft]


function sz = goodFFTsizes( n )
% return a vector with good fft sizes between [n ... 2*n]
%
maxpow3 = min(10,floor( log(2*n)/log(3)));
tstpow3 = 0:maxpow3;
npow2 = ceil( log2(n)-log2(3)*tstpow3 );
sz = 3.^tstpow3.*2.^npow2;
