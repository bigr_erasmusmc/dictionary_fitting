function submitTasks( taskfiles , clusterconfig, varargin) 
% submitTasks( taskfiles , clusterconfig, options)
%
% Starts processing the tasks. 
% If no arguments are provided, all tasks without results are started. 
% If needed executables for the tasks are first compiled. 
% When there is an error during compilation, no tasks are submitted. 
% 
% INPUTS:
%  taskfiles : optional string or cell array of strings. Each string containing the 
%              full file name (including path) of a task file that should be executed.
%              default (taskfiles empty or not provided) : all task files in the 
%              taskPath specified in load_clusterconfig. 
% Options-value pairs (or struct):
%  maxmemOverride : (bytes) Hard override value for maximum memory that the tasks may use.
%              Use when the tasks initially used a wrong value. 
%              Use this option when needed, but it is reccommended to set an 
%              appropriate value when creating the tasks rather than using this override. 
%
% NOTE: This function blindly starts all tasks that have not yet completed. 
%       Hence the caller should make sure all tasks explicitly or implicitly (default)
%       provided actually need to be started (and are not currently running)/ 
%
% NOTE2: (5-3-2018, D.Poot) On the BIGR cluster: The default MATLAB version( v716) 
%       does not have the compiler (properly) installed. Use R2013b instead.
%       > module load matlab/R2013b
%
% 13-2-2018, D.Poot, Erasmus MC : created function

if nargin < 2 || isempty( clusterconfig ) 
    clusterconfig = load_clusterconfig;
end;
if clusterconfig.executionMode == 1 %=='none'
    return;
end;
opt.maxmemOverride = [];
if nargin>=3
    opt = parse_defaults_optionvaluepairs( opt, varargin{:});
end;

%% Parse taskfiles into cell array of task mat files:
if nargin<1 || isempty( taskfiles) 
    taskfilesdir = dir( fullfile( clusterconfig.taskPath , 'Task*.mat') );
    % order on date&time of file:
    d = zeros(size(taskfilesdir)); 
    for k = 1 : numel( d ) ; 
        d(k)= datenum(taskfilesdir(k).date);
    end;
    [dum, ord] = sort( d );
    taskfilesdir = taskfilesdir( ord );
    
    taskfiles = cell(size( taskfilesdir ) );
    for k= 1 : numel( taskfiles ) 
        taskfiles{k}= fullfile( clusterconfig.taskPath, taskfilesdir(k).name );
    end;
else
    if ischar( taskfiles )
        taskfiles = {taskfiles };
    end;
     
end;

% Search for tasks that are not completed yet (i.e. have no result file):
runtask = false( size( taskfiles ) );
for k = 1 : numel( taskfiles )
    taskFullName  = taskfiles{ k };
    [resultFullName] = ResultFullNameFromTaskFullName( taskFullName );
    if ~exist( resultFullName ,'file')
        runtask(k) = true;
    end;
end;
if nnz(runtask)<1
    % return if no tasks need to be started. 
    return;
end;
taskfilesRun = taskfiles( runtask ); % only keep task files that are not completed. 
clear taskfiles; 

% Extract function name from task files:
functionnames = cell(size(taskfilesRun));
for k = 1 : numel( taskfilesRun )
    [p, fn ] = fileparts( taskfilesRun{ k } ) ;
    % test: fn =  'Task test 23_234l23423.mat' 
    idx = strfind( fn ,' ');
    functionnames{ k } = fn(idx(1)+1:idx(2)-1);
end;
% find unique function names:
[unnames, ~, taskfileExecutableIdx ] = unique( functionnames );
% For each function name get the executable name (create executable when not available or code updated)
executablenames = cell( size( unnames ) );
for k= 1 : numel( unnames)
    executablenames{k} = make_run_job( unnames{k} );
end;

% Start the tasks
if clusterconfig.executionMode == 3 
    % Run local in parallel. WARNING, dont do many tasks!
    % on the cluster delete excessive tasks by
    % 
    for k = 1 : numel( taskfilesRun )
        if ispc
            cmd = sprintf( '"%s" "%s" & exit &', executablenames{ taskfileExecutableIdx(k) }, taskfilesRun{k} );
        else
            cmd = sprintf( '"%s" "%s" &', executablenames{ taskfileExecutableIdx(k) }, taskfilesRun{k} );
        end;
        system( cmd ); 
    end;
elseif clusterconfig.executionMode == 4 % SGE bigrsub
    
    if 1 
        % Use BIGRSUB with task list file:
        % identify already submitted/running jobs:
        runjobfilename = @(jobnrstr) fullfile( clusterconfig.taskPath, ['Job' jobnrstr '_Tasks.mat']);
        try 
        [qstat_status, qstatout] = system('qstat -xml');
        %% test qstat output string:
        %  qstatout = '<?xml version=''1.0''?><job_info  xmlns:xsd="http://gridscheduler.svn.sourceforge.net/viewvc/gridscheduler/trunk/source/dist/util/resources/schemas/qstat/qstat.xsd?revision=11">  <queue_info>    <job_list state="running">      <JB_job_number>1673008</JB_job_number>      <JAT_prio>0.55000</JAT_prio>      <JB_name>QRLOGIN</JB_name>      <JB_owner>dpoot</JB_owner>      <state>r</state>      <JAT_start_time>2019-03-08T10:53:27</JAT_start_time>      <queue_name>interactive@app001.cm.cluster</queue_name>      <slots>1</slots>    </job_list>  </queue_info>  <job_info>  </job_info></job_info>'
%         qstat = xml_parse( qstatout ); % 8-3-2019, D.Poot, replaced by different xml parser. Impacts interpretation code below. 
        qstat = xml2struct( qstatout ); qstat = qstat.job_info;
        loadedoldJobFilename = [];
        qstat_fields = {'queue_info', 'job_info'};
        for fieldidx = 1 : numel(qstat_fields )
            if ~isfield( qstat,  qstat_fields{fieldidx } ) ||  ~isfield(qstat.(qstat_fields{fieldidx }),'job_list')
%                 warning('qstat did not return info on active jobs. Assuming no jobs previously created by submitTasks are active.');
            else
                qstatfield = qstat.(qstat_fields{fieldidx }).job_list;
                if ~iscell( qstatfield )
                    qstatfield = {qstatfield};
                end;
                for k = 1: numel( qstatfield )
                    oldJobFilename = runjobfilename( qstatfield{k}.JB_job_number.Text );
                    if exist( oldJobFilename,'file');
                        if ~isequal( oldJobFilename, loadedoldJobFilename )% avoid reloading the same file many times:
                            oldJobTasks = load( oldJobFilename); loadedoldJobFilename= oldJobFilename;
                        end;
                        [tokens,ed] = regexp( qstatfield{k}.tasks.Text,{'^(\d*)$', '^(\d*)-(\d*):(\d*)$'} ,'tokens','match') ;
                        if ~isempty( tokens{1} )
                            % format : 'number'
                            tasknrs = str2double( tokens{1}{1} ); 
                        elseif ~isempty( tokens{2} )
                            % format :  'number-number:number'
                            tasknrs = str2double( tokens{2}{1}{1} ) : str2double( tokens{2}{1}{3} ) : str2double( tokens{2}{1}{2} );
                        else
                            warning([ 'Unexpected task description: "'  qstatfield{k}.tasks.Text '"' ]);
                            continue; 
                        end;
                        activeTaskFiles = oldJobTasks.taskfilesRun( tasknrs );
%                         disp( [fieldidx, k] )   % DEBUG PRINT
%                         disp( activeTaskFiles ) % DEBUG PRINT
                        alreadyActiveJobs = ismember(  taskfilesRun, activeTaskFiles );
                        if any( alreadyActiveJobs ) 
                            % delete task files that are already active (running or waiting in queue) from the list of task files that are currently started. 
                            taskfilesRun( alreadyActiveJobs  )=[];
                            taskfileExecutableIdx( alreadyActiveJobs  )=[];
                        end;
                    end;
                end;
            end;
        end;
        catch ME
            disp('Error when trying to identify currently running tasks. Assuming no tasks are running and hence submitting all non-completed tasks.');
            disp(ME);
        end;
        if numel( taskfilesRun )<1
            % return if no tasks need to be started. 
            disp('All task files are currently executing or waiting. Hence not submitting an job this time.');
            return;
        end;
        % load task specific options. Currently only use maximum memory and last queue. 
        if ~isempty( opt.maxmemOverride )
            maxmem = opt.maxmemOverride;
            k = numel( taskfilesRun );
                l = load( taskfilesRun{k} ,'taskopts');
                queue = l.taskopts.queue;
        else
            mem = zeros(1, numel( taskfilesRun ) ); 
            for k = 1 : numel( taskfilesRun )
                l = load( taskfilesRun{k} ,'taskopts');
                mem(k) = l.taskopts.memory;
                queue = l.taskopts.queue;
            end;
            maxmem = max([mem 5e9]);
        end;
        if maxmem<1e9
            memstr = sprintf('%3.1fM',maxmem/1e6);
        else
            memstr = sprintf('%4.1fG',maxmem/1e9);
        end;
        tasklistFN = fullfile( clusterconfig.taskPath, sprintf('tasklist_%s.txt',datestr(now,'yyyymmdd_HHMMSS')) );
        fid = fopen(tasklistFN,'w');
%         maxGroup = 100; % maximum group size of tasks started at once; Note maximum delay added is maxGroup/maxRate in seconds. 
%         maxRate = 10; % ( jobs/second ) maximum rate of starting of jobs to avoid overloading the file system with starting MATLAB or reading task & argument files. 
        for k = 1 : numel( taskfilesRun )
%             cmd = sprintf( 'sleep %f;module add mcr/R2013b;"%s" "%s"', mod(k-1, maxGroup)/maxRate, executablenames{ taskfileExecutableIdx(k) }, taskfilesRun{k} );
            cmd = sprintf( 'module add mcr/R2013b;"%s" "%s"', executablenames{ taskfileExecutableIdx(k) }, taskfilesRun{k} );
            fprintf(fid,'%s\n',cmd);
        end;
        fclose(fid);
        % Create a task array job that executes these tasks:
        submitcmd = sprintf( 'bigrsub -q %s -R %s -N %s -t "%s"', queue, memstr, unnames{1} , tasklistFN);
        disp( submitcmd );
        if ispc
            warning( 'cannot submit jobs to the sge cluster from a pc.'); 
        else
            [status, jobnrstr] = system( submitcmd) ;
            [stat2, jobstr2] = system([ 'qhold ' jobnrstr(1:end-1)] ); % immediately place hold
            save(runjobfilename(jobnrstr(1:end-1)), 'taskfilesRun' ); % jobnrstr contains line ending. 
            % slow release of tasks to avoid overloading the system and hence get failing tasks. 
            jobnr = jobnrstr(1:end-1);
            tasknrs = 1 : numel( taskfilesRun ) ;
            releasepause = 1; % seconds to pause before releasing next job. 
            progressbar('start',numel( taskfilesRun ) , 'releasing jobs to be started');
            for t = tasknrs
                cmd = ['qrls ' jobnr '.' num2str(t)];
                system( cmd );
                progresssbar(t);
                pause( releasepause ) 
            end;
            progressbar('ready');
            disp('All tasks released. Please check if they are running correctly (http://bigr-app002:18888/queues).');
        end;
    else
        error('incomplete code');
        % direct qsub using taskopts:
        for k = 1 : numel( taskfilesRun )
            l = load( taskfilesRun{k} ,'taskopts');
            cmd = sprintf( 'module add mcr/R2013b;"%s" "%s"', executablenames{ idxA(k) }, taskfilesRun{k} );
            submitcmd = sprintf( 'echo "%s"|qsub -q %s -R %s -N %s ', cmd , l.taskopts.queue, memstr ,functionnames{ k } );
        end;
    

    end;
elseif clusterconfig.executionMode == 2
    error('submitTasks should be called when executionMode==2 as that case is handled internally in run_on_cluster (It needs to be done there as the arguments are not stored to disk).');
else
    error('invalid executionMode configured. Update clusterconfig (see load_clusterconfig).');
end;