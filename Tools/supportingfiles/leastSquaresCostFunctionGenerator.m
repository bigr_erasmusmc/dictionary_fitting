function [fun, hessmulfun, make_par, extract_par] = leastSquaresCostFunctionGenerator( sizeX, A, At, Y, varargin)
% [fun, hessmulfun, make_par, extract_par] = leastSquaresCostFunctionGenerator( X, A, At, Y/funY, [options,...])
%
%  [f, g, h_info ] = fun( par ) 
%  Hx = hessmulfun( h_info, vx )
%  par = make_par( X [,Ypar] )
%  [X , Ypar, Y] = extract_par( par )
%
% Computes 
%  f( X ) =  .5* | Y - A * X |^2              [ + 0.5* X'*regulfun(X) ]
%         == .5* (Y - A * X)'*(Y - A * X)     [ + 0.5* dot(X,regulfun(X)) ]
%
%  with Y = funY(ypar) or the input argument Y. 
%
% fun is to be used as cost function for non-linear optimization routines.
%
% Obviously, for purely quadratic problems you can use a quadratic solver such as
% cgiterLS, or, when A can be given explicitly, with the simple matlab
% command: X = A\Y.
% The benefit of this function is when there are additional cost terms that
% should be combined. In that case this function allows easy conversion
% from such least squares formulation to a general cost function. 
% Also see leastSquaresCostFunction_nonlinear.m for nonlinear functions. 
%
%
% INPUTS:
%  A, At : functions taking 1 argument and computing A(b) == A*x, 
%           or At(c) == A'*y, respectively. (Make sure complex conjugate is
%           used for complex A)
%  Y   : measurements vector. The optimum of the cost function is at the
%        least squares solution of X with the data Y. 
%  funY : [Y, JY] = funY(ypar): function taking a single argument and using that to construct 'Y'
%       JY : jacobian of funY w.r.t. par, or jacobian information for funY_jacmul and funY_jacmult
%            JY(i,j) = derivative of element i of funY( ypar ) w.r.t. ypar(j)
%   
%   X  : parameter array (to be estimated)
%   Ypar : parameter array of funY (to be estimated)
%   vx : numel(par) x n matrix, containing the n vectors that should be multiplied with the hessian. 
%   
% options:
%  multiplerhs : default = true : multiple x can be used simultanuously in A, At, and if funY is provided in funY_jacmul, and funY_jacmult.
%                if false only 1 X is supported by A and At and they are called multiple times if needed. 
%  minus : Two argument function that subtracts the second argument from the first
%          Default : @(Y,AX) Y-AX; 
%  dot : single argument function that computes the dot product of the argument with itself. 
%          Default: @(residual) dot(residual(:), residual(:))
%          NOTE: minus and dot are introduced to allow custom 'Y' formats. 
%  regulfun : Additional quadratic term specifying a hermitian (symmetric) matrix multiplication
%             E.g. to specify Tikhonov regularization: regulfun = @(x) lambda.*x;
%             with some appropriate regularization strength lambda. 
%  funY_jacmul: Jx = funY_jacmul( JY, vx) : multiplies vx (numel(ypar) x n) with the jacobian. 
%  funY_jacmult: Jx = funY_jacmult( JY, vx) : multiplies vx (numel(Y) x n) with the transpose of the jacobian. 
%  sizeX : size of x. Should be present if funY is provided or reshape is true.  
%  sizeparY : size of Ypar. 
%  complexX : default = false; if true: X is complex (and par contains an entry for the real and imaginary part)
%  reshape : default = false; if true:  X is reshaped into sizeX before passing to A 
%                                       y is reshaped into size(Y) before passing to At
%                                       Ypar is reshaped into sizeparY before passing to funY
%                           Other parameters (par, vx) remain/are vectors/matrices
% OUTPUTS:
%  fun        : function taking parameters as input, returning up to 3 outputs: function value, gradient, hessian information. 
%  hessmulfun : function having 2 inputs (hessinfo, vx) and one output (Hx), which is the multiplication of the hessian with the vector(s) vX. 
%  make_par   : function taking one or 2 arguments (2 iff funY is provided) constructing the parameter represenation that can be used for an optimization routine. 
%  extract_par: function returning X and Ypar (and Y) from par; Ypar and Y should only be requested when funY is provided. 
%
%   f   : cost function value
%   g   : gradient of cost function value with respect to X
%   h_info : hessian info. 
%   par : real vector constructed from X (and Ypar) to allow optimization. 
%   
%
% Created by Dirk Poot, 24-7-2015, Erasmus MC, 

opt = struct;
opt.multipleX = true;
opt.minus = @(Y,AX) Y-AX;
opt.dot = @(A) dot(A(:),A(:));
opt.regulfun = [];
opt.funY_jacmul = @(J, X) J*X;
opt.funY_jacmult = @(J, X) J'*X;
opt.sizeX = sizeX;
opt.sizeparY = [];
opt.reshape = false;
opt.complexX = false; % if true, X argument of fun is real and X argument of A (and At) is complex and At should be complex conjugate transpose of A.  
if nargin>4
    opt = parse_defaults_optionvaluepairs(opt, varargin{:});
end;
if isa(Y,'function_handle')
    opt.funY = Y;  % function modifying Y. Y = funY( parY ) , where parY is the input argument Y. 
    opt.Y = [];
else
    opt.funY = [];
    opt.Y = Y;
end;
opt.lastXre = prod(opt.sizeX );
if opt.complexX
    opt.lastXim = 2*opt.lastXre;
else
    opt.lastXim = opt.lastXre;
end;
    

fun = @(X,Y) leastSquaresCostFunction( X, A, At, opt);
if opt.multipleX
    hessmulfun = @(hinfo, x) leastSquaresCostFunction_hessmul(hinfo, x, A, At, opt);
else % opt.multipleX == false
    % functions support only 1 rhs, so use wrapper function to call leastSquaresCostFunction_hessmul
    % multiple times if x has multiple columns: 
    hessmulfun = @(hinfo, x) leastSquaresCostFunction_hessmulSingleX(hinfo, x, A, At, opt);
end;
if isempty(opt.funY)
    make_par = @(X) X_make_par(X, opt);
    extract_par = @(par) X_extract_par(par, opt);
else
    make_par = @(X, Y) funY_make_par(X,Y,opt);
    extract_par = @(par) funY_extract_par(par, opt);
end;
end

function par = X_make_par(X, opt)
if opt.complexX
    par = cat(1,real(X(:)),imag(X(:)));
else
    par = X;
end;
end
function [X] = X_extract_par(par, opt)
    if opt.complexX
        X = complex( par(1:opt.lastXre ) , par(opt.lastXre+1:opt.lastXim ) );
    else
        X = par;
    end;
    X = reshape(X , opt.sizeX);
end
function par = funY_make_par(X,Y,opt)
if opt.complexX
    par = cat(1,real(X(:)),imag(X(:)),Y(:));
else
    par = cat(1,X(:),Y(:));
end;
end

function [X, Ypar, Y, funY_jacinfo] = funY_extract_par(par, opt)
    if opt.complexX
        X = complex( par(1:opt.lastXre ,:) , par(opt.lastXre+1:opt.lastXim ,:) );
    else
        X = par(1:opt.lastxRe ,:) ;
    end;
    X = reshape(X , opt.sizeX);
    Ypar = par( opt.lastXim+1:end,:);
    if ~isempty(opt.sizeparY)
        Ypar = reshape(Ypar, opt.sizeparY);
    end;
    if nargout>=3
        if nargout<4
            Y = opt.funY( Ypar );
        else
            [Y, funY_jacinfo] = opt.funY( Ypar );
        end;
    end;
end
    
function [f, g, h_info] = leastSquaresCostFunction( par, A, At, opt)
% f = .5 * (Y - A * X)'*(Y - A * X)    + .5* X'*R*X
% g = df/dX = A'*(Y - A * X)           +  R*X
% h = A'*A                             +  R 

% parse Xin, Yin to X, Y (and Ypar)
if isempty(opt.funY)
    X = X_extract_par(par, opt);
    Y = opt.Y;
else
    if nargout<=1
        [X, Ypar, Y] = funY_extract_par(par, opt);
    else
        [X, Ypar, Y, funY_jacinfo] = funY_extract_par(par, opt);
    end;
end;
res = opt.minus( Y, A( X ) );

f = .5 * opt.dot(res);
if ~isempty(opt.regulfun)
    regulX = opt.regulfun(X);
    f = f + .5 * dot(X(:),regulX(:));
end;
if nargout>1
    % see below for derivative derivation (especially the part including funY)
    h_info = struct;%opt.h_info; % initialize h_info structure;
    gX = - At( res );
    if ~isempty(opt.regulfun)
        gX = gX + reshape(regulX,size(gX));
    end;
    if isempty(opt.funY)
        if opt.complexX 
            g = [real(gX(:));imag(gX(:))];
        else
            g = gX(:);
        end;
    else
        % f = .5 * (fY(Ypar) - A * X)'*(fY(Ypar) - A * X)
        % df/dYpar = .5 * (fY(Ypar) - A * X)'*D(fY(Ypar),dYpar) +...
        %            (.5 * (D(fY(Ypar),dYpar))'*(fY(Ypar) - A * X)).'
        gYpar = real( opt.funY_jacmult( funY_jacinfo, res ) );
        if opt.complexX 
            g = [real(gX(:));imag(gX(:));gYpar(:)];
        else
            g = [real(gX(:));gYpar(:)];
        end;
        h_info.yfun_jacinfo = funY_jacinfo;
    end;
    h_info.sizeX = size(X);
    h_info.sizeY = size(Y);
    h_info.numXin = numel(par);
end;
end

function Hx = leastSquaresCostFunction_hessmulSingleX(h_info, x, A, At, opt)
if size(x,2)>1 && size(x,1)==h_info.numXin
    % split over x
    Hx = cell(1,size(x,2));
    for k=1:numel(Hx)
        Hx{k} = leastSquaresCostFunction_hessmul(h_info, x(:,k), A, At, opt) ;
    end;
    Hx = [Hx{:}];
else
    Hx = leastSquaresCostFunction_hessmul(h_info, x, A, At, opt) ;
end;
end

function Hx = leastSquaresCostFunction_hessmul(h_info, x, A, At, opt)
if isempty(opt.funY)
    nx = size(x,2);
    if opt.complexX
        x = complex( x(1:opt.lastXre ,: ) , x(opt.lastXre+1:opt.lastXim ,:) );
    end;
    if opt.reshape
        x = reshape(x , [h_info.sizeX nx]);
    end;
  
    Hx = reshape( At( A( x ) ), [], nx );
    if ~isempty(opt.regulfun)
        Hx = Hx + reshape( opt.regulfun( x )  , size(Hx));
    end;
    if opt.complexX
        Hx = [real(Hx);imag(Hx)];
    end;
else
    % Include hessian multiplication with funY
            % F(X, Ypar ) = .5 * sum_i | funY_i( Ypar ) - sum_j A_ij X_j |^2 
            %             = .5 * sum_i ( funY_i( Ypar ) - sum_j A_ij X_j )' * ( funY_i( Ypar ) - sum_j A_ij X_j )
            %
            %  D( F, Re(X_k) ) = .5 * sum_i ( funY_i( Ypar ) - sum_j A_ij X_j )' * (  - sum_j A_ij delta_jk )
            %                   +.5 * sum_i ( - sum_j A_ij delta_jk )' * ( funY_i( Ypar ) - sum_j A_ij X_j )
            %                  =-.5 * conj( sum_i (  A_ik )' * ( funY_i( Ypar ) - sum_j A_ij X_j ) )
            %                   -.5 *       sum_i (  A_ik )' * ( funY_i( Ypar ) - sum_j A_ij X_j )
            %                  = - Re(  sum_i (  A_ik  )' * ( funY_i( Ypar ) - sum_j A_ij X_j )  )
            %  D( F, Im(X_k) ) = .5 * sum_i ( funY_i( Ypar ) - sum_j A_ij X_j )' * (  - sum_j A_ij 1i delta_jk )
            %                   +.5 * sum_i ( - sum_j A_ij 1i delta_jk )' * ( funY_i( Ypar ) - sum_j A_ij X_j )
            %                  = -.5 * 1i *conj( sum_i (  A_ik )'  * ( funY_i( Ypar ) - sum_j A_ij X_j )  )
            %                    -.5 * -1i*      sum_i (  A_ik )'  * ( funY_i( Ypar ) - sum_j A_ij X_j )     
            %                  = - Im(  sum_i (  A_ik  )' * ( funY_i( Ypar ) - sum_j A_ij X_j ) )
            %  D( F, Ypar_k )  =  .5 * sum_i ( D[funY_i, Ypar_k]  )' * ( funY_i( Ypar ) - sum_j A_ij X_j )
            %                    +.5 * sum_i ( funY_i( Ypar ) - sum_j A_ij X_j )' * ( D[funY_i, Ypar_k] )
            %                  = Re(  sum_i ( D[funY_i, Ypar_k]  )' * ( funY_i( Ypar ) - sum_j A_ij X_j ) )
            %  D( F, Re(X_k), Re(X_l) ) = - Re(  sum_i (  A_ik  )' * ( - sum_j A_ij delta_jl )  )
            %                           =   Re(  sum_i (  A_ik  )' * ( A_il )  )
            %  D( F, Re(X_k), Im(X_l) ) = - Re(  sum_i (  A_ik  )' * ( - sum_j A_ij 1i delta_jl )  )
            %                           = - Im(  sum_i (  A_ik  )' * ( A_il )  )
            %  D( F, Im(X_k), Re(X_l) ) = - Im(  sum_i (  A_ik  )' * ( - sum_j A_ij delta_jl ) )
            %                           =   Im(  sum_i (  A_ik  )' * ( A_il ) )
            %  D( F, Im(X_k), Im(X_l) ) = - Im(  sum_i (  A_ik  )' * ( sum_j A_ij 1i delta_jl ) )
            %                           =   Re(  sum_i (  A_ik  )' * ( A_il ) )
            %  D( F, Re(X_k), Ypar_l) = - Re(  sum_i (  A_ik  )' * ( D[funY_i, Ypar_l] )  )
            %  D( F, Im(X_k), Ypar_l) = - Im(  sum_i (  A_ik  )' * ( D[funY_i, Ypar_l] ) )
            %  D( F, Ypar_k , Re(X_l) )  = Re(  sum_i ( D[funY_i, Ypar_k]  )' * (- sum_j A_ij delta_jl ) )
            %                           - Re(  sum_i ( D[funY_i, Ypar_k]  )' * ( A_il ) )  
            %  D( F, Ypar_k , Im(X_l) )  = Re(  sum_i ( D[funY_i, Ypar_k]  )' * (-  sum_j A_ij 1i delta_jl ) )
            %                             Im(  sum_i ( D[funY_i, Ypar_k]  )' * ( A_il ) )  
            %  D( F, Ypar_k, Ypar_l)  =  Re(  sum_i ( D[funY_i, Ypar_k, Ypar_l]  )' * ( funY_i( Ypar ) - sum_j A_ij X_j ) )
            %                          + Re(  sum_i ( D[funY_i, Ypar_k]  )' * ( D[funY_i, Ypar_l]  ) )
    % Note: ignoring second derivative of funY. 
    ncolx = size(x,2);
    reX = reshape(x(1:opt.lastXre ,:) , [h_info.sizeX ncolx]);
    imX = reshape(x(opt.lastXre+1:opt.lastXim ,:) , [h_info.sizeX ncolx]);
    ypar_x = x( opt.lastXim+1:end,:);
    
    Jy = reshape( opt.funY_jacmul( h_info.yfun_jacinfo, ypar_x ), h_info.sizeY);
    HreX   = real( At( A( reX ) ) ) - imag( At( A( imX ) ) ) - real( At( Jy ) ) ;
    HimX   = imag( At( A( reX ) ) ) + real( At( A( imX ) ) ) - imag( At( Jy ) ) ;
    Hyparx =-real( opt.funY_jacmult( h_info.yfun_jacinfo,  A( reX ) ) ) + imag( opt.funY_jacmult( h_info.yfun_jacinfo,  A( imX ) ) ) + real( opt.funY_jacmult( h_info.yfun_jacinfo, Jy ) ) ;
    
    Hx = [ reshape(HreX, opt.lastXre ,ncolx ) ;reshape( HimX, opt.lastXim-opt.lastXre ,ncolx ); reshape( Hyparx, size(ypar_x)) ];
    
end;
end


function test
%% construct real linear problem:
sizeX = [7 1 ];
Amat = randn(10,7);
A = @(X) Amat*X;
At= @(X) Amat'*X;
xGT = randn(sizeX);
Y = Amat*xGT;
xtst = randn(sizeX);
[fun, hessmulfun] = leastSquaresCostFunctionGenerator( sizeX, A, At, Y );
dummy = validateDerivativeAndHessian(fun, xtst, hessmulfun);

%% Construct complex problem:
sizeX = [7 1 ];
Amat = complex(randn(10,7),randn(10,7));
A = @(X) Amat*X;
At= @(X) Amat'*X;
xGT = randn(sizeX);
Y = A(xGT);
xtst = randn(sizeX)+1i*randn(sizeX);
% dummy = validateJacobian( @(X) deal_relaxed(A(X),[]), xtst,  @(J,X,flag) wrapJacobianMul(J, X, flag, @(J,X)A(X), @(J,X)At(X) ) );
% [fun, hessmulfun] = leastSquaresCostFunctionGenerator( sizeX, A, At, Y );
% dummy = validateDerivativeAndHessian(fun, xtst, hessmulfun);
[fun, hessmulfun] = leastSquaresCostFunctionGenerator( sizeX, A, At, Y ,'complexX',true);
dummy = validateDerivativeAndHessian(fun, cat(1,real(xtst),imag(xtst)), hessmulfun);
%% Construct complex problem with Yfun:
sizeX = [3 5 6 4];
A = @(X) fft(fft(fft(X,[],2),[],3),[],4);
At= @(X) ifft(ifft(ifft(X,[],2),[],3),[],4)*prod(sizeX(2:end));
xGT = complex(randn(sizeX), randn(sizeX)) ;
kspace = A(xGT);
xtst = randn(sizeX)+1i*randn(sizeX);

nshots = 13;
ntrains = 9;
views = ceil( [ size(kspace,3)*rand(1,nshots, ntrains);size(kspace,4)*rand(1,nshots, ntrains);rand(1,nshots, ntrains)-.2]);
[unv,b,c] = unique( views(1:2,:)','rows');
for cid = 1:numel(b)
    cdel = find(c==cid);
    views(3,cdel(2:end))=false;
end;
kcenter= size(kspace)/2;kcenter(1)=[];
delta = randn(4,ntrains);
[funY, jacmulfunY, jacmulfunYT] = makeDisplacementsShotsFunction( kspace, views, kcenter);

[fun, hessmulfun, make_par, extract_par] = leastSquaresCostFunctionGenerator( sizeX, A, At, kspace ,'complexX',true,'reshape',true);
par = make_par( xtst );
dummy = validateDerivativeAndHessian(fun, par, hessmulfun);

[fun, hessmulfun, make_par, extract_par] = leastSquaresCostFunctionGenerator( sizeX, A, At, funY ,'complexX',true,'reshape',true, 'funY_jacmul',jacmulfunY, 'funY_jacmult', jacmulfunYT,'multipleX',false,'sizeparY',size(delta));
par = make_par( xtst , delta);
dummy = validateDerivativeAndHessian(fun, par, hessmulfun);

%%
end