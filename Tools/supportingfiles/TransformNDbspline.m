function [out]  = TransformNDbspline( in, T, mapT, newSz, F, bsplineord, AR, clip_i, boundary_conditions)
% [out]  = TransformNDbspline( in, T, mapT, newSz, F, bsplineord, AR, clip_i)
%
% This is a low level performance routine for ND image interpolation
% It transforms 'in' to 'out'
% pseudocode of function:
%
% doAdjoint = all( newSz<0 );
% if doAdjoint
%   size_out = -newSz; and the adjoint is computed. 
% else
%   size_out = newSz; and the normal sampling is computed. 
%
% if prefilterAR
%   for all dimensions dim:
%      in = filter(filter...)
%
% for all 'columns' j of o
%   for all i in current column:
%       if doAffine
%           T_column(:,i) = T * [i j 1]
%       elseif isempty(mapT)
%           T_column(:,i) = T(:,i,j)
%       else
%           Tpos = mapT * [i j 1]
%           T_column(:,i) = linear interpolate T(:, Tpos) 
%
% 	in_j = column j of in  
% 	% filter in forward and backwards with AR model:
% 	inf_j = flip( filter(1,AR, flip(filter(1,ar, in_j) )))
% 	for k=1:newszT
% 		pos_k = T( k , j);   
% 		posi_k = floor( pos_k );
% 		posf_k = (pos_k - posi_k) * ( size(F,2)-bsplineord );
% 
% 		out(k,j) = F(:,posf_k)' * inf_j( posi_k + [0:size(F,1)-1] , : );
% 		% bsplineord-Bspline interpolation between columns of F
% 		% (implicitly) replace elements outside in with zero. 
% out = ipermute(out , dimorders(3,:) );
%
% INPUTS: NOTE: all indices below are zero based (as in c++) rather than 1 based (as usually in MATLAB)
%  in	: N - Dimensional single or double image; 
%  T    : a) 1+N - Dimensional sampling locations matrix with size(T)== [Ntransformdim size(in)]
%            => mapT is empty. 
%         b) 1+N - Dimensional sampling locations matrix with size(T,1)== [Ntransformdim]
%            Tfull( :, k) =  T( :, mapT(1, transformdim)+ k.*mapT(2, transformdim)   )
%            interpolating into T with bsplineord( end )
%         c) Ntransformdim * (Ntransformdim+1 ) Affine transformation matrix
%            Tfull(: , k) = T * [k';1];
%  mapT : empty or 2 x N mapping matrix; the columns specifying 'tag along' dimensions are ignored. 
%  newSzT : N element vector specifying the new size in the transform dimensions.
%           NaN elements indicate dimension is 'tagged along', no prefiltering/interpolation 
%           is performed in these dimensions and the output size is equal to
%           the input size. Currently all 'tag along' dimensions should be consequitive
%           (and for speed preferably the first dimensions)
%  F    : filterbank, as for example designed by designLPfilter(_v2)
%  bsplineord : (0,1,2) b-spline interpolation order for interpolation between 
%               the columns of F.
%               if T is sampling array, last element specifies interpolation of T. 
%  AR   : Autoregressive pre-filter model. Empty => no prefiltering.
%         For bspline interpolation (i.e. isempty(F) ), the following prefiltering 
%         should be applied (by this function or in a preprocessing step by filterND_c)
%         bsplineord = 0 : no prefiltering for neirest neighbor interpolation
%         bsplineord = 1 : no prefiltering needed for linear interpolation
%         bsplineord = 2 : AR = ?
%         bsplineord = 3 : AR = [5 -(sqrt(3)-2)]; % first element is 'run-out-length'
%  clip_o : Ntransformdim element cell matrix with clipping half spaces of 'o'-image (=output if normal; input if adjoint)
%           clip_i/o should be a ndims(T) element cell array, with each 
%           element clip_o{k}  (k=1..ndims(in)) a ( nplanes(k)  x  (N-k+2) )  double matrix, 
%           where the subset of all allowable coordinate vectors xk (xk == 0.. size_o(k)-1) is given by  
%           clip_o{k}*[xk;-1] <=0 (if clip_o{k} = [A b] =>  A*xk <= b, which is 
%           the standard linear program form in MATLAB). Note xk is containing 
%           the coordinate for all dimensions >=k. So with 
%           in( x(1), :, x(2), ... x(n) ) , xk = x(k:N). This nested list of constraints
%           allows for optimized iterator performance as everything excluded at a 
%           high dimension is not traversed at lower dimensions. 
%           The 'tag along' dimensions are ignored; not using clip_o{ tag_dim }
%           and not using clip_o{ k }(:, tagdim ). 
%  boundary condition : 0 = zero boundary conditions [default]
%                       1 = mirror boundary conditions
%
% OUTPUTS:
%  out    : N-Dimensional transformed 'in'
%  doutdT : N-Dimensional derivative of out w.r.t. to T. 
%
% Created by Dirk Poot, Erasmus MC
error('This function is implemented as mex file');
end

function test()
%%
%% Test_transformNDbspline
%% setup test problem:
imin = zeros(30,50);imin(3,3)=1;imin(6,6)=2;imin( 4,7)=3;
imagebrowse(imin)
szout = [10 1000];
Taff = [1 0 0;0 .05 0];
boundary_condition = 1;
%% Affine transform as input
out1 = TransformNDbspline(imin, Taff,[], szout,[],2, [],[],boundary_condition);
out1r = TransformNDbspline_ReferenceImplementation(imin, Taff,[], szout,[],2, [],[],boundary_condition);
imagebrowse(cat(3,out1,out1r,out1-out1r))
%% Full transform field:
[xi,yi] = ndgrid(0:size(imin,1)-1,0:size(imin,2)-1);
[xo,yo] = ndgrid(0:szout(1)-1,0:szout(2)-1);
Tmap = reshape(Taff * [xo(:) yo(:) ones(numel(xo),1)]', [2 size(xo)]);
out2 = TransformNDbspline(imin, Tmap,[], szout,[],2, [],[],boundary_condition);
out2r = TransformNDbspline_ReferenceImplementation(imin, Tmap,[], szout,[],2, [],[],boundary_condition);
imagebrowse(cat(3,out2,out2r,out2-out2r))
%% transform field with Tmap:
[xs,ys] = ndgrid(-4:6.456:szout(1)+7,-5.5:18.34:szout(2)+20);
mapT = [-xs(1,1)/(xs(2,1)-xs(1,1)) -ys(1,1)/(ys(1,2)-ys(1,1));1/(xs(2,1)-xs(1,1)) 1./(ys(1,2)-ys(1,1))];
Tmaps = reshape(Taff * [xs(:) ys(:) ones(numel(xs),1)]', [2 size(xs)]);
out3 = TransformNDbspline(imin, Tmaps,mapT, szout,[],[2 1], [],[],boundary_condition);
out3r = TransformNDbspline_ReferenceImplementation(imin, Tmaps, mapT, szout,[],[2 1], [],[],boundary_condition);
imagebrowse(cat(3,out3,out3r,out3-out3r))
%% different bspline order:
[xs,ys] = ndgrid(-118:6.456:szout(1)+132,-146.5:18.34:szout(2)+181);
mapT = [-xs(1,1)/(xs(2,1)-xs(1,1)) -ys(1,1)/(ys(1,2)-ys(1,1));1/(xs(2,1)-xs(1,1)) 1./(ys(1,2)-ys(1,1))];
Tmaps = reshape(Taff * [xs(:) ys(:) ones(numel(xs),1)]', [2 size(xs)]);
out4 = TransformNDbspline(imin, Tmaps,mapT, szout,[],[3 2], [],[],boundary_condition);
out4r = TransformNDbspline_ReferenceImplementation(imin, Tmaps,mapT, szout,[],[3 2], [],[],boundary_condition);
imagebrowse(cat(3,out4,out4r,out4-out4r))
%% check ar prefiltering:
ar = [1 -(sqrt(3)-2)];
out5 = TransformNDbspline(imin, Taff,[], szout,[],3, ar,[],boundary_condition);
out5r = TransformNDbspline_ReferenceImplementation(imin, Taff,[], szout,[],3, ar,[],boundary_condition);
imagebrowse(cat(3,out5,out5r,out5-out5r))
%% test clipping:
arL = [1 -.8]; % long AR filter, to more clearly see the effect of clipping:
% clip_o = {

%% test filterbank:
x = sinc(0:.1:4);
filtbnk = filterbank([x(end:-1:2) x], 8, 10,[],2);
out6 = TransformNDbspline(imin, Tmap,[], szout,filtbnk,2, [],[]);
out6r = TransformNDbspline_ReferenceImplementation(imin, Tmap,[], szout,filtbnk,2, [],[]);
imagebrowse(cat(3,out6,out6r,out6-out6r))

%% interpn reference : 
outs = interpn(xi,yi,imin, permute(Tmap(1,:,:),[2 3 1]), permute(Tmap(2,:,:),[2 3 1]),'spline');
outl = interpn(xi,yi,imin, permute(Tmap(1,:,:),[2 3 1]), permute(Tmap(2,:,:),[2 3 1]),'linear');
%%
out  = cat(4,out1 ,out2 ,out3 ,out4 ,out5, out6, outs,outl);
outr = cat(4,out1r,out2r,out3r,out4r,out5r,out6r,outs,outl);
imagebrowse(cat(3,out,outr,out-outr))

%% Adjoint, setup test problem:
iminA = randn(21,19);
szInA = size(iminA);
szOutA = [11 13];
TaffA = [1.245 .1 0;-.12 1.45 0];
boundary_condition = 0;

%% Affine transform as input
numTests = 100;
JacFun1   = @(imI) TransformNDbspline                        (imI, TaffA,[], szOutA,[], 3, [],[], boundary_condition) ;
JAdjFun1  = @(imO) TransformNDbspline                        (imO, TaffA,[], -szInA,[], 3, [],[], boundary_condition) ;
JacFun1r  = @(imI) TransformNDbspline_ReferenceImplementation(imI, TaffA,[], szOutA,[], 3, [],[], boundary_condition) ;
JAdjFun1r = @(imO) TransformNDbspline_ReferenceImplementation(imO, TaffA,[], -szInA,[], 3, [],[], boundary_condition) ;
dummy = validateAdjoint( JacFun1,  JAdjFun1,  szInA, numTests);
dummyr = validateAdjoint( JacFun1r, JAdjFun1r, szInA, numTests);
% imagebrowse(cat(3,out1,out1r,out1-out1r))
% dummy = validateJacobian( @(T) TransformNDbspline_ReferenceImplementation(imIinA, T,[], szoutA,[],3, [],[]), TaffA);

%% Full transform field:
[xi,yi] = ndgrid(0:size(iminA,1)-1,0:size(iminA,2)-1);
[xo,yo] = ndgrid(0:szOutA(1)-1,0:szOutA(2)-1);
TmapA = reshape(TaffA * [xo(:) yo(:) ones(numel(xo),1)]', [2 size(xo)]);

JacFun2   = @(imI) TransformNDbspline(                        imI, TmapA,[], szOutA,[], 2, [],[], boundary_condition) ;
JAdjFun2  = @(imI) TransformNDbspline(                        imI, TmapA,[], -szInA,[], 2, [],[], boundary_condition) ;
JacFun2r  = @(imI) TransformNDbspline_ReferenceImplementation(imI, TmapA,[], szOutA,[], 2, [],[], boundary_condition) ;
JAdjFun2r = @(imO) TransformNDbspline_ReferenceImplementation(imO, TmapA,[], -szInA,[], 2, [],[], boundary_condition) ;
validateAdjoint( JacFun2,  JAdjFun2,  szInA, numTests);
validateAdjoint( JacFun2r, JAdjFun2r, szInA, numTests);

%% test jacobian wrt. Tmap. 
JacTestFun2  = @(Tmap) TransformNDbspline(iminA, Tmap,[], szOutA,[], 3, [],[],boundary_condition) ;
JacTestFun2r  = @(Tmap) TransformNDbspline_ReferenceImplementation(iminA, Tmap,[], szOutA,[], 3, [],[],boundary_condition) ;
[outimg, jac] = JacTestFun2( TmapA );
[outimgr, jacr] = JacTestFun2r( TmapA );
delta = .01;
deltaT = eye(size(TmapA,1))*delta;
numJac = jac;
for k=1:size(deltaT,2)
   Thi = bsxfun(@plus, TmapA, deltaT(:,k));
   Tlo = bsxfun(@plus, TmapA,-deltaT(:,k));
   numJac(k,:,:) = reshape(JacTestFun2( Thi ) - JacTestFun2( Tlo ) ,[1,size(outimg)])/(2*delta);
   numJacr(:,:,k) = reshape(JacTestFun2r( Thi ) - JacTestFun2r( Tlo ) ,1,[])/(2*delta);
end;
numJacrp = reshape( permute( numJacr, [3 1 2]), [size(TmapA,1), size(outimgr) ]);
jacrp = reshape( permute( jacr, [3 1 2]), [size(TmapA,1), size(outimgr) ]);
%%
imagebrowse(reshape( cat(4,jac, jacrp, numJac, numJacrp, jac-numJac, jac-numJacrp),[szOutA size(deltaT,2) 6]))
% dummy = validateJacobian( @(T) TransformNDbspline_ReferenceImplementation(iminA, T,[], szOutA,[],2, [],[]), TmapA);

%% Test Tag along dimension 1:
szOutAt = szOutA;szOutAt(1)=nan; 
szInAt = szInA;  szInAt(1) = nan;
TaffAt = TaffA(2:end,2:end);
JacFun1  = @(imI) TransformNDbspline(imI, TaffAt,[], szOutAt,[], 1, [],[], boundary_condition) ;
JAdjFun1 = @(imO) TransformNDbspline(imO, TaffAt,[], -szInAt,[], 1, [],[], boundary_condition) ;
JacFun1r  = @(imI) TransformNDbspline_ReferenceImplementation(imI, TaffAt,[], szOutAt,[], 1, [],[], boundary_condition) ;
JAdjFun1r = @(imO) TransformNDbspline_ReferenceImplementation(imO, TaffAt,[], -szInAt,[], 1, [],[], boundary_condition) ;

dummy = validateAdjoint( JacFun1,  JAdjFun1,  szInA, numTests);
dummyr = validateAdjoint( JacFun1r, JAdjFun1r, szInA, numTests);
% imagebrowse(cat(3,dummy.Areduced, dummy.ATreduced, dummyr.Areduced, dummyr.ATreduced)); % not identical due to different random subselection in validateJacobian.
imagebrowse( cat(4, JacFun1( iminA ), JacFun1r( iminA ) ) )

%% test 3D
filtbnk = [];
im3D = randn(20,20,20);
Taff3D = [.21 0 0 4 ;0 .23 0 4; 0 0 .24 4];
szout3D = [21 22 23];
bsplineord = 2;
tic
out3D1 = TransformNDbspline(im3D, Taff3D,[], szout3D, filtbnk, bsplineord, [],[], boundary_condition);
tu = toc, tic;
out3D1r = TransformNDbspline_ReferenceImplementation(im3D, Taff3D,[], szout3D, filtbnk, bsplineord, [],[], boundary_condition);
tur = toc
%%
CPUclockRate = 2.67e9; % in Hz. 
nprocdims = ndims(im3D);
disp(['CPUclocks per sample (mex, filterbank)       : ' num2str( tu/numel(out3D1r)*CPUclockRate)]);
disp(['CPUclocks per sample (reference, filterbank) : ' num2str( tur/numel(out3D1r)*CPUclockRate)]);

[flops, addressing, memory, str]  = TransformNDbspline_ComputationCost( im3D, Taff3D,[], szout3D,filtbnk,bsplineord, [],[]);
disp(str) 
imagebrowse(cat(4, out3D1, out3D1r, out3D1-out3D1r));
%% large image (speed test)

im3DL = randn(200,201,202);
Taff3DL = [.71 0 0 6 ;0 .73 0 6; 0 0 .74 6]+.01*randn(3,4);
szout3DL = [210 220 230];
bsplineord = 2;
tic
out3D1L = TransformNDbspline(im3DL, Taff3DL,[], szout3DL,filtbnk,bsplineord, [],[], boundary_condition);
tuL = toc,

tic
out3D1L2 = TransformNDbspline(im3DL, Taff3DL,[], szout3DL,[], 3 , [],[], boundary_condition);
tuL2 = toc,

disp(['CPUclocks per sample (mex, filterbank, large image): ' num2str( tuL/numel(out3D1L)*CPUclockRate)]);
disp(['CPUclocks per sample (mex, bspline, large image)   : ' num2str( tuL2/numel(out3D1L2)*CPUclockRate)]);
[flops, addressing, memory, str]  = TransformNDbspline_ComputationCost( im3DL, Taff3DL,[], szout3DL,[], 3 , [],[]);
disp(str);
imagebrowse(cat(4, out3D1L,out3D1L2) );

%% vector image test

bsplineord          = 2;
boundary_condition  = 1;    % 0 = zero, 1 = mirror

% define full grid
Natoms      = 100;
t = 1:5:100;
A = 1;
T2 = logspace( log10(20), log10(150), Natoms );
s = bsxfun(@times, exp( -(T2.^-1)'*t ), permute(A, [1 3 2]))';

% define sample points
T = zeros( [1 1 size(s,2)] );
T(1:Natoms) = 1:Natoms;

% interpolate
[s_int, ds_int] = TransformNDbspline( s, T, [], [nan size(T,3)], [], bsplineord, [], [], boundary_condition);
[sr_int, dsr_int] = TransformNDbspline_ReferenceImplementation( s, T, [], [nan size(T,3)], [], bsplineord, [], [], boundary_condition);

% plot
figure;
imagebrowse( cat(3, s_int, sr_int, s_int - sr_int), [], 'labels', {'time', 'T2', 'diff'})
figure;
imagebrowse( cat( 4, ds_int, permute( dsr_int, [1 3 2] ),  ds_int - permute( dsr_int, [1 3 2] ) ), [], 'labels', {'time', 'par', 'T2', 'diff'})
set(gcf,'name',['Derivative']);

%% large dictionary test
% there was a indexing error in C code for large dictionaries

bsplineord          = 2;
boundary_condition  = 1;    % 0 = zero, 1 = mirror

dict_comp    = rand( [500 5 5 5 500 5] );
%dict_comp    = rand( [500 27 20 20 50 5] );

Tsample = zeros( ndims( dict_comp ) - 1, 1 );
for k = 1:ndims( dict_comp )-1
    Tsample(k) = randi( size( dict_comp, k + 1 ) );
end

func_PD_spline_c = @(theta) spline_optimization_function( dict_comp , theta(1,:), theta(2,:), theta(3:end,:), [], [], [], bsplineord, [], [], boundary_condition, true);
tic
[S1_c, dS1_c] = func_PD_spline_c( [1; 0; Tsample] );
[S2_c, dS2_c] = func_PD_spline_c( [1; 0; Tsample] );
[S3_c, dS3_c] = func_PD_spline_c( [1; 0; Tsample] );
toc

func_PD_spline_M = @(theta) spline_optimization_function( dict_comp , theta(1,:), theta(2,:), theta(3:end,:), [], [], [], bsplineord, [], [], boundary_condition, false);
tic
[S1_M, dS1_M] = func_PD_spline_M( [1; 0; Tsample] );
[S2_M, dS2_M] = func_PD_spline_M( [1; 0; Tsample] );
[S3_M, dS3_M] = func_PD_spline_M( [1; 0; Tsample] );
toc

norm(S1_c-S1_M)
norm(S2_c-S2_M)
norm(S3_c-S3_M)
norm(dS1_c(:)-dS1_M(:))
norm(dS2_c(:)-dS2_M(:))
norm(dS3_c(:)-dS3_M(:))

%% speed comparison

Ntests = 500;
dict_dim = size( dict_comp );
Natom = prod( dict_dim(2:end) );
theta_test = cell( numel( dict_dim(2:end) ),1 );
[theta_test{:}] = ind2sub( dict_dim(2:end), randi(Natom, Ntests,1) );
theta_test = cat(2, theta_test{:} ).';

tic
for k=1:Ntests
    [Stmp, dStmp] = func_PD_spline_c([ 1; 0; theta_test(:,k)]);
end
toc

tic
[Stmp, dStmp] = func_PD_spline_c([ [1; 0]*ones(1,size(theta_test,2)); theta_test]);
t1mex = toc

tic
for k=1:Ntests
    [Stmp, dStmp] = func_PD_spline_M([ 1; 0; theta_test(:,k)]);
end
toc

profile on
tic
for k=1:Ntests
    [Stmp, dStmp] = func_PD_spline_c([ 1; 0; theta_test(:,k)]);
end
toc

tic
for k=1:Ntests
    [Stmp, dStmp] = func_PD_spline_M([ 1; 0; theta_test(:,k)]);
end
toc
profile viewer

disp('clocks per read: ' )
t1mex*2.26e9/( size( dict_comp,1) * 3^(ndims(dict_comp)-1) * Ntests )