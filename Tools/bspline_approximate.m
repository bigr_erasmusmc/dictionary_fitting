function [coeffs, map, Amats] = bspline_approximate( image, coeff_size , bsplineorder )
% [coeffs, map,  Amats] = bspline_approximate( image, coeff_size , bsplineorder )
%
% Creates a bspline coefficients field with size coeff_size that best approximates image. 
% Re-create the approximation:
% approx_image = bspline_interpolate_regular( coeffs, map, size(image), bsplineorder, []);
%
% INPUTS
% image : N dimensional image
% coeff_size : N element row vector with size of the bspline coefficient field.
%              nan values specifies no-transform dimensions.
% bsplineorder : scalar bspline interpolation order (1, 2, or 3)
%
% OUTPUTS:
% coeffs : N dimensional coefficient array
% map    : coordinate transformation map. 
% Amats  : N element cell array with for each dimension in the approximation the 
%          weight matrices. size( Amats{ dim } ) = [size(coeffs,dim)  size(image, dim)]
%          E.g. if only appoximating in dimension 1:
%          image( :, : ) =approx=  A{1} * coeffs(:,:);
%
% Created by Dirk Poot, Erasmus MC, 29-10-2015

Imsz = size( image );
Csz = coeff_size;
if ~isequal(size(Imsz),size(Csz)) || any(Csz<=bsplineorder) || any(Csz~=round(Csz) & ~isnan(Csz))
    error('coeff_size should be a vector with integers > bsplineorder');
end;
procdims = ~isnan(Csz);
Csz(isnan(Csz))= Imsz(isnan(Csz));

map = [zeros(size(Imsz));
       (Csz-bsplineorder-.001)./(Imsz-1) ];  
map(:,~procdims) = nan;
clip = [];
Amats = cell(1,size(map,2));
curImsz = Imsz;
for dim = find(procdims)
    mapdim = [nan(2,1) map(:,dim)];
    tstim = eye(Csz(dim));
    A = bspline_interpolate_regular( tstim, mapdim, [Csz(dim) Imsz(dim)], bsplineorder, clip)';
    Amats{dim} = A;
    permord = [dim 1:dim-1 dim+1:numel(Imsz)];
    image = permute( image, permord );
    curImsz(dim) = Csz(dim);
    if 0
        % good version, but somehow claims to much memory (for large images); MATLAB R2011b
        image = ipermute( reshape( A\image(:,:), curImsz(permord)), permord);
    else
        % numerically slightly worse version that does not claim unnececary memory 
        image = ipermute( reshape(  ((A'*A)\A') * image(:,:), curImsz(permord)), permord);
    end;
end;
coeffs = image;