function outp = validateDerivativeAndHessian(fun, x, hessmulfun, preconditioner, bandw)
% validateDerivativeAndHessian(fun, x [, hessmulfun [, preconditioner, bandwidth]])
%
% This function validates the function 'fun', which typically is function you want to pass
% to an optimization routine. 
% fun should compute 3 outputs:
%   [f, g, H] = fun( x ) 
% where 
%   f : function value in x; should be a scalar value.
%   g : df/dx ;  gradient in x
%   H : d2f/dxdx ; hessian in x, or hessian info that can be passed to hessmulfun
%
% This function compares the analytic gradient and hessian to a numerical estimate. 
% The numerical estimates are computed by the 'gradest' and 'jacobianest' function from 
% the 'DERIVESTsuite' by John D'Errico.
% http://www.mathworks.com/matlabcentral/fileexchange/13490-automatic-numerical-differentiation
% 
% NOTES:
%  - For large problems a reduced test is performed for efficiency reasons.
%    Since I use random projections of the gradient and hessian it very likely 
%    that programming errors in the gradient and/or hessian are detected.
%  - In order to fully trust the validation, pick several x (at random) that cover all (special) cases of your function (if it has them). 
%
% INPUTS:
% fun : function-handle of a (anonymous) function accepting one input argument
% x   : real or complex array with the point at which fun is computed. Note
%       that complex x are only supported when hessmulfun is provided as a
%       general hessian cannot be given for complex x. isreal(x) should be
%       false to test complex valued functions!
% hessmulfun : Optional argument; function that is can multiply one or more vectors with the hessian:
%       [Hy] = hessmulfun(H, y) : 
%         This allows you to avoid computing and/or storing the full
%         hessian of the function. Additionally, this allows you to accept 
%         complex x. 
%         Implicitly, when x is complex, x is assumed to be a vector with
%         2*numel(x) elements: xr  = [real(x);imag(x)]
%          then   hessmulfun(H, y)  should compute:
%           let  [ Hrr, Hri; Hir  Hii] = hessian of fun xr. Then 
%           Hy = complex( Hrr * real(y) + Hri * imag(y) , Hir * real(y) + Hii* imag(y) )
%         Note that then for any complex vectors Z, Y :   Z' * H * Y = real( Z' * Hy )
%
% [R, mulfun/pvec] = preconditioner(H, bandwidth, DM, DG);
%         optional function that computes a preconditioner R for M = DM*H*DM + DG
%         Note: this function only tests DM = eye and DG = zero.
%         if the second output is a function, it should be able to multiply 1 vector (with numel(x) elements) with R
%         (This is for using my hack into the MATLAB optimization toolbox that allows such function.)
%         The condition number of H and H preconditioned with R is compared. The latter should be lower (obviously). 
%
% Created by Dirk Poot, Erasmus MC

% set defaults:
if nargin<3
    hessmulfun = [];
end;
if nargin<4
    preconditioner = [];
end;

% call function:
[f1] = fun(x);
if numel(f1)~=1 || ~isnumeric(f1) ||~isreal(f1)
    error('Every optimization function should return a non complex scalar as first output');
end;
[f2,g2] = fun(x);
[f,g, H] = fun(x);
outp.f_1outp = f1;
outp.f_2outp = f2;
outp.f_3outp = f;
outp.g_2outp = g2;
outp.g_3outp = g;
outp.H = H;
goodresults = true;

if ~isequal(f1,f2,f) || ~isequal(g2,g)
    warning('Function value or gradient not equal if the number of requested outputs changes. This should not happen.'); % (When the difference is just roundoff errors it might occaisionally be acceptable).
    goodresults = false;
end;
if ~isempty( preconditioner )
    if nargin<5
        % fmin_fast preconditioner creation function
%         [R, mulfun] = preconditioner( H , x);
% TODO: update to new fmin_fast preconditioner interface:
%   precon_info = fun_makeprecon( x, f, g, H_info , old_precon_info);
% Does not provide 'mulfun'
        [R, mulfun] = preconditioner( H , x);
    else
        % fminunc type preconditioner creation function
        DM =  speye(numel(x));
        DG = sparse(numel(x),numel(x));
        [R, mulfun] = preconditioner(H, bandw, DM, DG );
    end;
    eR = zeros(numel(x));
    for k=1:numel(x); 
        xk = zeros(numel(x),1);
        xk(k)=1; 
        eR(:,k) = mulfun(xk, R);
    end;
    sqrteR = sqrtm(eR);
    outp.R = eR;
    outp.sqrtmR = sqrteR;
end;
% Using gradest from the 'DERIVESTsuite' by John D'Errico
% http://www.mathworks.com/matlabcentral/fileexchange/13490-automatic-numerical-differentiation
maxnumx_grad = 100;
reducedtest_grad = 50;
maxnumx_hess = 100;
reducedtest_hess = 50;
if numel(x) > maxnumx_grad 
    redproj_grad = randn(numel(x), reducedtest_grad);
    if ~isreal(x)
        makecomplex = @(x) complex(x(1:end/2,:,:,:),x(end/2+1:end,:,:,:));
        % test [real imag complex]
        rng = floor((0:3)*(reducedtest_grad-1)/3)+1;
        redproj_grad(:, rng(2):rng(3)-1 )=0;
        redproj_grad(:, rng(2):rng(4))=redproj_grad(:, rng(2):rng(4)) + 1i*randn(numel(x),rng(4)+1-rng(2));
        [gest , err] = gradest(@(xr) fun( x + reshape(redproj_grad*makecomplex(xr),size(x))), zeros(reducedtest_grad*2,1) );
        gest = makecomplex(gest(:));
        err = abs(makecomplex(err(:)));
    else
        [gest , err] = gradest(@(xr) fun( x + reshape(redproj_grad*xr,size(x))), zeros(reducedtest_grad,1) );
    end;
    g_red = (g(:)'*redproj_grad)';
    outp.g_project = redproj_grad;
else
    % full gradient
    g_red = g(:);
    [gest , err] = gradest(fun, x);
end;
gerr = (g_red-gest(:));
outp.gest = gest;
outp.gest_numericalerror = err;
if any( abs(gerr)./err(:) >2  &  abs(gerr)./abs(g_red+gest(:))>1e-5 )
    disp('[ gradient,   numerical_gradient,  difference_in_gradient    absolute_difference_in_gradient/numerical_error]: ')
    fprintf( '   %15f  ,  %15f  , %15f  ,    %15f\n',[g_red gest(:)  gerr  abs(gerr)./err(:)]');
    warning('error in gradient computation seems to be too large: ');
    goodresults = false;
end;
if numel(x)>maxnumx_hess 
    redproj_hess = randn(numel(x), reducedtest_hess);
    if ~isreal(x)
        % test [real imag complex]
        rng = floor((0:3)*(reducedtest_hess-1)/3)+1;
        redproj_hess(:, rng(2):rng(3)-1)=0;
        redproj_hess(:, rng(2):rng(4))=redproj_hess(:, rng(2):rng(4)) + 1i*randn(numel(x),rng(4)+1-rng(2));
    end;
    outp.H_project = redproj_hess;
end;
if ~isempty(hessmulfun)
    outp.Hinfo = H;
    if numel(x)>maxnumx_hess 
        H = hessmulfun(H, redproj_hess);
    else
        if isreal(x)
            H = hessmulfun(H, eye(numel(x)));
        else
            H = hessmulfun(H, [eye(numel(x)) 1i*eye(numel(x))]);
        end;
    end;
    Hred = H;
    outp.H = H;
else
    if numel(x)>maxnumx_hess 
        Hred = H*redproj_hess;
    else
        Hred = H;
    end;
end;
% Using jacobianest from the 'DERIVESTsuite' by John D'Errico
% http://www.mathworks.com/matlabcentral/fileexchange/13490-automatic-numerical-differentiation
if numel(x)>maxnumx_hess 
    % evaluate reduced hessian:
    [Hest , errHest] = jacobianest( @(xr) reshape(shuffleoutputs(fun, 2, 2, {x+reshape(redproj_hess*xr,size(x))}),[],1) , zeros(reducedtest_hess,1));
    Hest = conj(Hest ); % some diference in definition wrt jacobianest for complex x. 
else
    % evaluate full hessian
    if ~isreal(x)
        % jacobianest not correct for complex derivatives:
        makecomplex = @(x) complex(x(1:end/2,:,:,:),x(end/2+1:end,:,:,:));
        makereal =  @(x) [real(x);imag(x)];
        [Hest , errHest] = jacobianest( @(xx) makereal( reshape(shuffleoutputs(fun, 2, 2, {reshape(makecomplex(xx),size(x))}),[],1)) , makereal(x(:)));
        Hest = makecomplex( Hest );
        errHest = abs(makecomplex(errHest));
    else
        [Hest , errHest] = jacobianest( @(x) reshape(shuffleoutputs(fun, 2, 2, {x}),[],1) , x);
    end;
end;
outp.Hest = Hest;
outp.Hest_numericalerror = errHest;

if ~isempty( preconditioner )
    if numel(x)>maxnumx_hess && ~isempty(hessmulfun)
        warning('error probably follows; Cannot yet evaluate preconditioner performance with reduced hessian.');
    end;
    Hresid_a = sqrteR * H * sqrteR; 
    Hresid_n = sqrteR * Hest * sqrteR; 
    outp.condH = cond(H);
    outp.condHest = cond(Hest);
    outp.condRH = cond(Hresid_a);
    outp.condRHest = cond(Hresid_n);
    if outp.condH<outp.condRH
        warning('Condition number of preconditioned hessian is larger than original hessian => Bad preconditioner');
        goodresults = false;
    end;
    if outp.condRH>50
        warning(['Large condition number of preconditioned hessian. Condition number : ' num2str(outp.condRH) '\nIf the eigenvalues are evenly spread (not tested), many (>30) PCG iterations are needed for decent convergence (1%)']);
        goodresults = false;
    end;
end;

Herr = (Hred-Hest);
if any( abs(Herr(:))./(errHest(:)+1e-4*max(errHest(:))) >2 & abs(Herr(:))./abs(Hred(:)+Hest(:)+1e-6*max(Hred(:)))>1e-4)
    warning('error in hessian computation seems to be too large.');
    goodresults = false;
end;
outp.functionCorrect = goodresults;
if goodresults
    disp('The gradient and hessian appear to be computed correctly');
end;