function [curcoord, fval] = findlocalminima(mat, neigboursonly, cyclic, interpolate)
% [coords, val] = findlocalminima(mat , neigboursonly , cyclic, interpolate)
% computes the positions that are strict local minima of the n-dimensional
% matrix mat. 
% INPUTS:
% mat : N-dimensional matrix of which the minima are located
% neighboursonly : Default = 0; 
%       Specify 1 to only compare to the direct neigbours (4 in 2-D
%       matrices), by default all positions with an infinity norm<=1 from
%       each point are used (so all points with no more than 1 step in all
%       directions)  
% cyclic         : Default = 0;
%       Specify 1 or ones(size(size(mat))) to treat the matrix mat as
%       cyclic in all dimensions, or specify 1/0 for each dimension separately.
%       Specifying 1 (for the first dimension) means e.g. that mat(1,k) is a local
%       minimum only when mat(1,k)<mat(end,k) (in all dimensions)
% interpolate    : Default = 0;
%       Specify 1 to compute interpolated minima. A 2nd order polynomial (parabola)
%       is fitted to each local minimum (using the full 3^N neighbourhood), and the
%       minimum of that parabola is returned
%
% OUTPUTS:
% coords : matrix with in each row the location of a (interpolated) minimum of mat.
%          So mat(coords(k,1), ... coords(k,N)) is a local minimum. 
% val    : Column vector with the value of the matrix in the minima; might be
%          especially usefull when combined with the interpolate option. When the
%          output argument val is requested, the minima are sorted with respect to
%          val. When only 1 output argument is requested the results are unsorted!
%
% Created by Dirk Poot, University of Antwerp.

% original creation date unknown, added credits: 23-7-2008
% 24-7-2008: removed bug; used indloc instead of ind at last line.
% 7-8-2009 : added interpolate option.

if nargin<2 || isempty(neigboursonly)
    neigboursonly = false;
end;
if nargin<3 || isempty(cyclic)
    cyclic = 0;
end;
if nargin<4 || isempty(interpolate)
    interpolate = false;
end;
smat = size(mat);
if numel(smat)==2 && smat(2)==1
    smat = smat(1);
end;
if any(smat==1)
    error('All dimensions should have at least 2 elements (column vectors allowed)');
end;
if numel(cyclic)==1
    cyclic = cyclic*ones(1,numel(smat));
end;

ndims = numel(smat);
minim = true(size(mat)); % all positions that are local minima. Elements not local minima are set to false in the loop below. 
indices = cell(ndims,1);
indices(:) = {':'};
for dim = 1 : ndims
    % local minima can only be in points where the derivative changes sign from negative to positive:
    der = diff(mat,1,dim);
    curind = indices;
    curind(dim) = {1:smat(dim)-1};
    minim(curind{:}) = minim(curind{:}) & der>0;
    curind(dim) = {2:smat(dim)};
    minim(curind{:}) = minim(curind{:}) & der<=0;
    if cyclic(dim)
        % if cyclic extend the 'diff' and update the derivative sign
        % changes accross the edge. 
        curinde = curind;
        curind(dim) = {1};
        curinde(dim) = {smat(dim)};
        der = mat(curind{:})-mat(curinde{:});
        minim(curind{:}) = minim(curind{:}) & der <0;
        minim(curinde{:}) = minim(curinde{:}) & der >0;
    end;
    clear der
end;
% now locate the local minima in minim array:
curcoord_linidx = find(minim);
ind = cell(1,ndims);
[ind{:}] = ind2sub(smat,curcoord_linidx);
curcoord = horzcat(ind{:});

if ~neigboursonly && numel(smat)>1
    % prune list of local minima based on non-direct neighbors:
    clear minim 
    % all direct neighbours are tested against, now do test all others;
    steps = zeros(1,0);
    for dim=1:ndims
        steps = [kron([-1;0;1],ones(size(steps,1),1)) kron(ones(3,1),steps)];
    end;
    steps(sum(abs(steps),2)<=1,:)=[];
    cumsz = cumprod([1 smat(1:end-1)])';

    ind = curcoord-1;
    clear curcoord
    % process each step, remove indices that are not lowest.
    for m=1:size(steps,1);
        indloc = ind;
        indtst = ind + steps(m*ones(size(ind,1),1),:);
        for k_dim = find(cyclic)
            indtst(:,k_dim) = mod(indtst(:,k_dim),smat(k_dim));
        end;
        notdel = ~(any(indtst<0,2) | any(indtst>=smat(ones(size(ind,1),1),:),2));
        dosel = ~all(notdel);
        if dosel
            repr = find(notdel);
            indloc = indloc(repr,:);
            indtst = indtst(repr,:);
        end;
        notkeep = mat(indloc*cumsz+1)>=mat(indtst*cumsz+1);
        if any(notkeep)
            if dosel
                ind(repr(notkeep),:) =[];
            else
                ind(notkeep,:) =[];
            end;
        end;
    end;
    curcoord = ind+1;
end;

if interpolate
    % Interpolate second order polynomial 
    % in neighborhood around the identified local minima points:
    deltax = cell( 1, ndims );
    delta_step = cell( 1, ndims );
    curcoordT = curcoord';
    cumsz = cumprod([1 smat(1:end-1)]);
    ind = cumsz*curcoordT - sum(cumsz(2:end)); % ind = row vector with linear index of all local minima. 
    % make x = ndims element cell aray with row vector of coordinate indices of local minima in each dimenion. 
    x = mat2cell(curcoordT, ones(1,size(curcoordT,1)) , size(curcoordT,2));
    selindxx =zeros(0,1);
    for dim = 1 : ndims
        deltax{ dim } = [-1 0 1]'* ones(1,numel(x{dim}));
        if any( x{ dim } == 1)
            if cyclic(dim)
                deltax{dim}(1,x{dim}==1) = smat(dim)-1;
            else
                deltax{dim}(:,x{dim}==1) = deltax{dim}(:,x{dim}==1)+1;
                warning('findlocalminima:InterpolateOnBorder','speculative interpolation on border.');
%                 error('cannot yet interpolate on border');
            end;
        end;
        if any(x{dim}(:)==smat(dim))
            if cyclic(dim)
                deltax{dim}(3,x{dim}==smat(dim)) = -smat(dim)+1;
            else
                deltax{dim}(:,x{dim}==smat(dim)) = deltax{dim}(:,x{dim}==smat(dim))-1;
                warning('findlocalminima:InterpolateOnBorder', 'speculative interpolation on border.');
%                 error('cannot yet interpolate on border');
            end;
        end;
        delta_step{dim} = deltax{dim}*cumsz(dim);
        selindxx = [repmat( selindxx, 1, size(deltax{dim},1) );kron( ( 1 : size(deltax{dim},1) ) + (dim-1)*size(deltax{dim},1) ,ones(1,size(selindxx,2)))];
        % NOTE: (dim-1)*size(deltax{dim},1) counts number of rows in deltax in all previous dimensions 
    end;
    delta_step = vertcat(delta_step{:});
    delta_step = reshape( sum( reshape( delta_step(selindxx,:), [size(selindxx,1) size(selindxx,2) size(delta_step,2)]), 1), [size(selindxx,2) size(delta_step,2) ] );
    lb = cell( 1, ndims);ub = lb;
    for dim = 1 : ndims
        lb{dim} = deltax{dim}(1,:);
        ub{dim} = deltax{dim}(end,:);
    end;
    lb = vertcat(lb{:});
    ub = vertcat(ub{:});
    selmat = mat( ind( ones(1,size(delta_step,1) ) ,: ) + delta_step ); % select neigborhood around all local minima.
    % selmat = num_neighbors x num_local_minima array with all selected neighborhoods. 
    
    [indx,cnt] = symmetricTensorElements(2,ndims);
    % X * tht = value at all 3^ndims neigborhood points. We are looking for tht
    % 2D case: fun(x,y) = a + b x + c y + d x^2 + e x y + f y^2 with tht = [a b c d e f]
    % general ordering of tht: [constant  linear_terms  quadratic_terms]
    % 
    stepx = selindxx'-3*ones(3^ndims,1)*(0:ndims-1)-2; % 3^ndims == number of voxels in neighborhood. 
    X = [ones(3^ndims,1) stepx stepx(:,indx(:,1)).*stepx(:,indx(:,2))];
    tht = X\selmat;
    
    % now solve general 2nd order polynomial:
    %  arg min_x  (grad*2) * x  + x' * hess * x
    %  ==> (grad*-2) + 2 *hess*x ==0 => x = hess\grad; (assuming hess is positive definite)
    grad = tht(2:ndims+1,:)*(-.5); % linear terms. (b,c in 2D example above)
    hesssel = diag(1./cnt')*tht(ndims+2:end,:); % quadratic terms. (d, e,f, in 2D example above) 
    opt = zeros(ndims,size(tht,2));
    hess = zeros(ndims,ndims);
    for k = 1 : size(tht,2)
        hess(indx*[1;ndims]-ndims)=hesssel(:, k ); % place quadratic terms in hessian
        hess(indx*[ndims;1]-ndims)=hesssel(:, k );
        opt(:, k ) = hess \grad(:, k );
        
        % Apply constraints:
        dorep  = true; fix = false;
        while dorep 
            fix_lb = opt(:,k)< lb(:,k);
            fix_ub = opt(:,k)> ub(:,k);
            if any(fix_lb) || any(fix_ub) % test if new constraints are added 
                if any(fix_lb)
                    opt(fix_lb,k) = lb(fix_lb,k);
                end;
                if any(fix_ub)
                    opt(fix_ub,k) = ub(fix_ub,k);
                end;
                fix = fix | fix_lb | fix_ub;
                if all(fix) 
                    dorep = false;
                else
                    opt(~fix, k ) = hess(~fix,~fix) \grad(~fix, k );
                end;
            else
                dorep = false;
            end;
        end;
    end;
%     if any( abs(opt(:)) > 1 )
%         % might be a warning instead?
%         % We probably want to solve this by testing if this actually is another local
%         % minimum we found. Not implemented yet.
%         error('interpolated minimum outside of interpolation region.');
%     end;
    curcoord = (curcoordT+opt)';
    if nargout>1
        X = [ones(1,size(opt,2)); opt; opt(indx(:,1),:).*opt(indx(:,2),:)];
        fval = sum(X.*tht,1)';
    end;
elseif nargout>1
    % not interpolate, fval requested
    cumsz = cumprod([1 smat(1:end-1)]);
    ind = curcoord*cumsz' - sum(cumsz(2:end));
    fval = mat(ind);
end;
if nargout>1
    % sort:
    [fval, ind] = sort(fval);
    curcoord = curcoord(ind,:);
end;

function test
%% Test findlocalminima
[x,y] = ndgrid(0:20,0:2:54);
if 0 
else
    [xr] = [x(:) y(:)]*rotationMatrix(pi/5);
    x = reshape( xr(:,1), size(x));
    y = reshape( xr(:,2), size(y));
end;
f = cos(x) + cos(y);
[curcoord1, fval1] = findlocalminima(f, 1, 0, 0);
[curcoord2, fval2] = findlocalminima(f, 1, 0, 1);
imagebrowse( f );
hold on;
plot( [curcoord1(:,2) curcoord2(:,2)], [curcoord1(:,1) curcoord2(:,1)], '*');
hold off
