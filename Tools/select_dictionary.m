function [path_dict, info] = select_dictionary( basepath, extensive, path_img )
%path_dict = select_dictionary( basepath, extensive, path_img )

if nargin < 2
    if nargout > 1
        extensive = true;
    else
        extensive = false;
    end        
end
if nargin < 3
    path_img = [];
end

dict_list = struct('acq', [], 'date', [], 'path', [] );
info_list = struct('Nspins', [], 'edgeAtoms', [], 'p', [], 'Nimg', [] );
dict_idx = 1;

list_date = dir( [ basepath '*-*-*' ] );

for idate = 1:numel( list_date )
    list_test = dir( [ basepath list_date(idate).name '/' 'dict*' ] );
    for itest = 1:numel( list_test )
        acq_name = dir( [ basepath list_date(idate).name '/' list_test(itest).name ] );
        iacq = find( [acq_name.isdir] & ~( strcmp( {acq_name.name}, '.' ) + strcmp( {acq_name.name}, '..' ) ) );
        filename = [ basepath list_date(idate).name '/' list_test(itest).name '/' acq_name(iacq).name '/info_dictionary.mat' ];
        if exist( filename, 'file' )
                        
            dict_list( dict_idx ).acq  = acq_name(iacq).name;
            dict_list( dict_idx ).date = list_date(idate).name;
            dict_list( dict_idx ).path = [fileparts( filename ) '/'];            
            if extensive
                data_dict = load( filename, 'xyz', 'edgeAtoms', 'p', 'Nimg', 'pulseseq' );
                
                info_list( dict_idx ).Nspins    = size( data_dict.xyz, 1 );
                info_list( dict_idx ).edgeAtoms = data_dict.edgeAtoms;
                info_list( dict_idx ).par_disc  = cellfun( @numel, data_dict.p );
                info_list( dict_idx ).Nimg      = data_dict.Nimg;
                info_list( dict_idx ).param     = data_dict.pulseseq.param;
            end
            dict_idx = dict_idx + 1;
        end
    end
end

for idict = 1:dict_idx-1
    if ~extensive
        fprintf( '%2d: %s %s \n', idict, dict_list( idict ).date, dict_list( idict ).acq );
    else
        fprintf( '%2d: %s %14s, disc: %s, %7d spin, %1d edge, %4d imgs,  \n', idict,...
            dict_list( idict ).date, dict_list( idict ).acq, sprintf( '% 4d', info_list( idict ).par_disc' ), info_list( idict ).Nspins, info_list( idict ).edgeAtoms,...
            info_list( idict ).Nimg );
    end
end

fprintf( '\n' );
if isempty( path_img )
    idx_choice  = input(['CHOOSE DICTIONARY INDEX: '] );
    path_dict   = dict_list( idx_choice ).path;
    info        = info_list( idx_choice );
else
    path_dict   = cell( size( path_img ) );
    info        = cell( size( path_img ) );
    for k = 1:numel( path_dict )
        str         = fprintf( 'CHOOSE DICTIONARY INDEX FOR: %s', path_img{k} );
        idx_choice  = input( '\n' );
        path_dict{k}= dict_list( idx_choice ).path;
        info{k}     = info_list( idx_choice );
    end
end