function [f, CRLB_f, I_f, J_f , f_bias] = CramerRaoLowerBound_of_function(fun, par, CRLB_par, varargin)
%  [f, CRLB, I, J, bias] = CramerRaoLowerBound_of_function(fun, par, CRLB_par, options+values)
% 
% Computes the Cramer Rao Lower bound of the variance of fun( par )
% when the CRLB of par is given (explicitly or implicitly).
% Also see bias_correct_function which does almost the same. 
%
% INPUTS:
%  fun : function that converts or extracts part of par. Same format as for
%        fit_MRI (though different meaning as for these fun the output is a 
%        parameter of interest while for fit_MRI the output is a predicted signal intensity).
%        For example: DT_fa
% fun should accept a single argument and have outputs as fun in 
%         fit_MRI (for non scalar functions):
%             [v, g, h, hess_IJ] = fun( theta(:,:) )
%             size(v,2) = size(theta(:,:),2)
%             g  : (only used for CRLB_value)
%             size(h) = [size(v,1) size(h_IJ,1) size(theta(:,:),2)]  
%         For scalar functions (e.g. DT_fa) the outputs may be: 
%             [v, g, h, hess_IJ] = fun( theta(:,:) )
%             numel(v) = size(theta(:,:),2)
%             g  : (only used for CRLB_value)
%             size(h) = [size(h_IJ,1) size(theta(:,:),2)]
%  par : Parameters as estimated by fit_MRI (maximum likelihood)
%        size(par) = [nparameters spatial_size] (or masked)
%  CRLB_par : Cramer Rao Lower Bound as evaluated by CramerRaoLowerBound_MRI
%options:
%  CRLB_I, CRLB_J : Optional: second and third output of CramerRaoLowerBound_MRI.
%        Default assumes the output order of CramerRaoLowerBound_MRI, but these arguments
%        allow specification of a different order. 
%  useSecondDerivative : boolean, default = false; if true use the second derivative of 
%        fun to do a biascorrection on f.
%
% OUTPUTS (similar as CramerRaoLowerBound_MRI):
%  f     : fun( par ) 
%  CRLB_fun : Cramer Rao lowerbound of fun( par ) 
%  I,J   : indexing parameters. 
%             CRLB_matrix( I_f(k), J_f(k) ) = CRLB_fun( k, voxel_index) ;
%  bias : Only returned when useSecondDerivative==true
%         The bias in fun( par ) when par has covariance as specified in CRLB_par. 
%         Technically, the evaluation assumes the same as the CRLB evaluation and hence 'bias' means:
%           When evaluating CramerRaoLowerBound_of_function with ground truth values (par and CRLB_par)
%           you should add this bias to fun( par_groundtruth ) 
%           to get the expected value of fun( par_estimated ). 
%         While formally an (additional) approximation, you typically can subtract this
%         bias from fun( par_estimated ) to get a less biassed value. 
%         
% 6-4-2017: Created by Dirk Poot, Erasmus MC


% See SVN: fit_MRI/CramerRaoLowerBound.tex for derivation. 
%
% with f( theta ) =approx= f( E(theta) ) + df * (theta-E(theta)) + .5*(theta-E(theta))'*d2f *(theta-E(theta))
opts.CRLB_I = [];
opts.CRLB_J = [];
opts.useSecondDerivative = false;

if nargin>3
    opts = parse_defaults_optionvaluepairs( opts, varargin{:} );
end;

npar = size(par,1);
if isempty( opts.CRLB_I )
    [opts.CRLB_I, opts.CRLB_J] = find( triu( ones( npar ) ) );
end;
if size(CRLB_par(:,:),2) ~= size( par(:,:),2) || numel(opts.CRLB_I)~=size(CRLB_par,1)
    error('CRLB_par does not match to par');
end;

if opts.useSecondDerivative
    [f, df, hf,  fun_hess_IJ] = fun( par );
else
    [f,df] = fun( par );
end;
if size(f,1)==1 && isequal(size(df),size(par))
    % scalar function may put derivative into first dimension
    df = permute( df(:,:), [3 2 1] );
    if opts.useSecondDerivative
        % should be consistent and also put second derivative in first dimension:
        hf = permute( hf(:,:), [3 2 1]);
    end;
end;
nf  = size(f,1);
[I_f, J_f] = find( triu( ones( nf ) ) );

multiplicity = 1+(opts.CRLB_I~=opts.CRLB_J); % diagonal elements should be counted once, off-diagonal elements twice. 
CRLB_f = sum( bsxfun(@times, df( I_f , :, opts.CRLB_I).* df(J_f, : , opts.CRLB_J) , permute(bsxfun(@times, multiplicity, CRLB_par(:,:)),[3 2 1])) ,3);

if opts.useSecondDerivative
    % do a bias correction on f:
    [L, CRLB_rows] = ismember( sort(fun_hess_IJ,2), sort([opts.CRLB_I opts.CRLB_J],2) ,'rows');
    if ~all(L)
        error('The provided CRLB is too incomplete; bias correction of the function value cannot be performed.');
    end;
    halfmultiplicity = 0.5* multiplicity( CRLB_rows );
    f_bias = sum( bsxfun(@times, hf, permute( bsxfun(@times, halfmultiplicity, CRLB_par( CRLB_rows, :)),[3 2 1])) ,3); 
    
    % second order correction on CRLB_f (non-trivial derivation; see SVN: fit_MRI/CramerRaoLowerBound.tex
    covtht = zeros(npar);covthtidx = covtht;
    for k = 1 : numel(opts.CRLB_I)
        covthtidx( opts.CRLB_I(k), opts.CRLB_J(k) ) = k;
        covthtidx( opts.CRLB_J(k), opts.CRLB_I(k) ) = k;
    end;
    hessmat = zeros(npar);    hessmatidx = hessmat;
    for k= 1 : size( fun_hess_IJ, 1 )
        hessmatidx( fun_hess_IJ(k,1), fun_hess_IJ(k,2) ) = k;
        hessmatidx( fun_hess_IJ(k,2), fun_hess_IJ(k,1) ) = k;
    end;
    hessmask = hessmatidx~=0;
    hessmatidx = hessmatidx(hessmask);
    for voxidx = 1 : size(CRLB_f,2)
        B = cell(nf,1);
        covtht(:) = CRLB_par( covthtidx, voxidx );
        for paridx = 1 : nf
            hessmat(hessmask) = hf( paridx, voxidx , hessmatidx);
            B{paridx} = covtht * hessmat;
        end;
        for k=1: numel(I_f)
            CRLB_f(k, voxidx) = CRLB_f(k, voxidx) + .5*sum(sum( B{I_f(k)}'.* B{J_f(k)} ));
        end;
    end;
elseif nargout>=5
    error('only when using second derivative of warpfunction, the bias can be computed');
end;

function test()
% In these tests: manually compare if the observed covariance corresponds (well enough) to the CRLB. 
% This to ensure that the computations in CramerRaoLowerBound_of_function are done right. 
%% simulate diffusion weighted data:
g = randn(30,3);
Gmat = [ones(size(g,1),1) bsxfun(@times,-[1 2 2 1 2 1],g(:,[1 2 3 2 3 3]).*g(:,[1 1 1 2 2 3]))];
fun = @(tht) pred_Exponential_MultiCompartiment( tht, {Gmat} );
tht_GT = [3 1.3 .1 .23  .8 .21 .6]';
FA_GT = DT_fa( tht_GT );
sigma = 1;
S_GT = fun( tht_GT);
num_tests  = 10000; 
imageType = 'real';
if isequal(imageType,'real')
    S = bsxfun(@plus, S_GT , sigma* randn(size(S_GT,1),num_tests ));
else
    S = AddRiceNoise( S_GT *ones(1,num_tests), sigma);
end;    
%% fit model
npar = size(tht_GT,1);
tht_ML = fit_MRI(fun, S, tht_GT ,'noiseLevel',sigma,'imageType',imageType);
%%
[CRLB_GT , CRLB_I, CRLB_J] = CramerRaoLowerBound_MRI( tht_GT, fun , sigma,imageType);
[CRLB_par ] = CramerRaoLowerBound_MRI( tht_ML, fun , sigma,imageType);
%% scalar function
FA = DT_fa( tht_ML );
warpfun = @(tht) DT_fa(tht);
[fGT, CRLB_fGT, I_fGT, J_fGT ] = CramerRaoLowerBound_of_function(warpfun, tht_GT, CRLB_GT );
[fGT2, CRLB_fGT2, I_fGT2, J_fGT2 , biasGT2] = CramerRaoLowerBound_of_function(warpfun, tht_GT, CRLB_GT ,'useSecondDerivative',true);
[f, CRLB_f, I_f, J_f ] = CramerRaoLowerBound_of_function(warpfun, tht_ML, CRLB_par );
[f2, CRLB_f2, I_f2, J_f2 , bias2] = CramerRaoLowerBound_of_function(warpfun, tht_ML, CRLB_par ,'useSecondDerivative',true);
cmat = cov( tht_ML' )
CRLB_GTmat = zeros( npar ); CRLB_GTmat( CRLB_I+ (CRLB_J-1)*npar ) = CRLB_GT;CRLB_GTmat( CRLB_J+ (CRLB_I-1)*npar ) = CRLB_GT;
R = chol( CRLB_GTmat  );
cov_difference = inv(R')*cmat*inv(R)- eye(npar)
disp(['FA ground truth (separate) : ' num2str(FA_GT)]);
disp(['FA ground truth (function) : ' num2str(fGT)]);
disp(['FA ground truth (function2): ' num2str(fGT2)]);
disp(['FA ground truth+bias       : ' num2str(FA_GT+biasGT2)]);
disp(['mean FA  (separate)        : ' num2str(mean(FA)) ' +/- ' num2str(std(FA)/sqrt(numel(FA))) ]);
disp(['mean FA  (function)        : ' num2str(mean(f)) ' +/- ' num2str(std(f)/sqrt(numel(f))) ]);
disp(['mean FA  (function2)       : ' num2str(mean(f2)) ' +/- ' num2str(std(f2)/sqrt(numel(f2)) )]);
disp(['mean (FA-bias)             : ' num2str(mean(f2-bias2)) ' +/- ' num2str(std(f2-bias2)/sqrt(numel(f2-bias2))) ]);
disp(' ');
disp(['sqrt CRLB(FA ground truth)      : ' num2str( sqrt( CRLB_fGT))]);
disp(['sqrt CRLB(FA ground truth)  (2) : ' num2str( sqrt( CRLB_fGT2))]);
disp(['sqrt mean CRLB(FA est)          : ' num2str( sqrt( mean(CRLB_f)))]);
disp(['sqrt mean CRLB(FA est)      (2) : ' num2str( sqrt( mean(CRLB_f2)))]);
disp(['std (FA est)                    : ' num2str( std( f ) ) ]);
disp(['std (FA est - bias)             : ' num2str( std( f2-bias2 ))]);
%% nonscalar function
if 1
    % select eigenvalues:
    warpfun = @(tht) shuffleoutputs( @DT_eig, 4, [1 3], {tht} ,[],{[],[1 3 2]}); % DT_eig does not have second derivative (it is complex enough as it is ). 
else
    % select eigenvectors:
    warpfun = @(tht) shuffleoutputs( @DT_eig, 4, [2 4], {tht} ,[],{[],[1 2 4 3]},{[9 size(tht,2)],[9 size(tht,2) size(tht,1)]}); % DT_eig does not have second derivative (it is complex enough as it is ). 
end;
[EGT, CRLB_EGT, I_EGT, J_EGT ] = CramerRaoLowerBound_of_function(warpfun, tht_GT, CRLB_GT );
[E, CRLB_E, I_E, J_E ] = CramerRaoLowerBound_of_function(warpfun, tht_ML, CRLB_par );

[EGT mean(E,2) std(E,[],2)]
nE = size(E,1);
CRLB_Emat = zeros( nE );CRLB_Emat( I_EGT + (J_EGT-1)*nE) = CRLB_EGT;CRLB_Emat( J_EGT + (I_EGT-1)*nE) = CRLB_EGT;
CRLB_Emat
covE = cov(E')
% R = chol( CRLB_Emat  );
% cov_difference = inv(R')*covE*inv(R)- eye(nE)
CRLB_Emat-covE