function [RFpulses] = pulses_inversion( delay, Tspoil, Aspoil )
% [RFpulses] = pulses_inversion( delay )
%
% INPUTS:
%   delay : default = 0; delay after 180 degree inversion pulse (hard pulse)
%   Tspoil: default = [] ms, if nonempty: time of gradient spoiler
%   Aspoil: default = [0;0;0] rad/unit space, amplitude of gradient spoiler
%                      pulse
%
% Generates an adiabatic inversion pulse.
%
% Created by Dirk Poot, Erasmus MC
% 30-1-2013
% Adjusted by Willem van Valenberg (2018) TU Delft to include spoiler

if nargin<1
    delay = 0;
end;

if nargin < 2 || isempty(Tspoil)
    Tspoil = []; %ms
end
if nargin < 3 || isempty(Aspoil)
    Aspoil = zeros(3,0); %rad per unit space
end
RFpulses = diag([-1 1 -1]);

if isempty( Aspoil )
    actionid = [6 0];
    deltaT   = [0 delay];
else
    actionid = [6 8 0];
    deltaT   = [0 Tspoil delay-Tspoil];
end
RFpulses = MRI_pulses( deltaT, actionid, RFpulses, nan, Aspoil );