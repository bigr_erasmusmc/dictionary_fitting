function [points, faces, cub_weights, diagnostics] = sphere_best(n, distnrm, symmetry, OptimalExternPointSet)
% [points, faces, cubature_weights] = sphere_best(n, distnrm, symmetry);
% Construct a distribution over the sphere of n points. 
% distnrm = -2  : default, electrostatic repulsion, minimize electrostatic
%                 potential
% distnrm = 1   : maximize sum of absolute distances 
% distnrm = inf : maximize the minimum distance.
%
% symmetry = 0 : default, no symmetry enforced.
% symmetry = 1 : point are symmetric around 0, n should be even.
%                points(1:end/2,:)=-points(end/2+1:end,:)
%
% points = n x 3 matrix with [x,y,z] coordinates of the points.
% faces = m x 3 matrix with a good set of triangular faces to mesh the
%         approximate sphere. Each row contains the (integer) index of the
%         3 points of that triangle.
% cubature_weights = n x 1 column vector with the 'optimal' weights needed
%         for numerical integration with the set of points. Sums to 1.
%         optimal is defined as highest order spherical harmonic rejection
%         that is possible and difference between cubature_weights as low
%         as possible.
% 
% Display the sphere, with random interpolated colors:
%  patch('Faces',faces,'Vertices',points,'FaceVertexCData',rand(size(points)),'FaceColor','interp')
%
% ALTERNATIVE CALLING:  
% [points, cubature_weights, faces] = sphere_best(n, 'cubature' or 'cubatureE', symmetry);
% NOTE different ordering of output arguments!! 
% With these arguments, this routine computes the best cubature points and
% weights with approximately n sampling points. Use symmetry to indicate that the
% function you will sample is symmetric. Setting this allows this function
% to return half of a symmetric point set (usually substantially better, when
% applicable). Searches all currently computed results (any distnrm) for a
% pointset that provides the best (currently available) cubature with
% approximately n points.   
% 
% Created by Dirk Poot, University of Antwerp.
% 24-4-2008

persistent precomputed
persistent saveToFilename
% Options:
dosave = true;
if isempty(saveToFilename)
    saveToFilename = which('sphere_best.mat');
    if isempty(saveToFilename)
        saveToFilename = 'sphere_best.mat';
    end;
end;
storeAsPoints = false; % toggle storage to points (compatible) and compacted (small).
                       % Results are only actually stored when a new
                       % parameter set is computed.
debugplot = false;
% ndims = 3;
ntries = 50;

% Check input and apply defaults:
if nargin<2 || isempty(distnrm)
    distnrm = -2;
end;
if nargin<3
    symmetry = false;
else
    symmetry = symmetry~=0;
end;

findbestcubature = strcmp(distnrm,{'cubature','cubatureE'})*[1;2];
if numel(n)~=1 || n<4 + 2*(symmetry && findbestcubature==0)% at least 6 points when symmetric.
    error(' Number of points too low (>=4 required; >=6 for symmetric) ');
end;
if symmetry==1 && round(n/2)*2~=n && (findbestcubature==0)
    error('number of points should be even when symmetry is requested');
end;

% load old precomputed results
precomputed = loadfile(precomputed,saveToFilename);

if isequal(distnrm,'displayPrecomputedResults');
    % UNDOCUMENTED: display the previously computed results
    disp([num2str(size(precomputed.params,1)) ' precomputed distributions available']);
    plot3(precomputed.params(:,1),precomputed.params(:,2),precomputed.params(:,3),'+');
    xlabel('number of points');
    ylabel('norm');
    zlabel('symmetry');
    return;
end;
%%
nPrecomp = size(precomputed.params,1);
if findbestcubature~=0
    if ~isfield(precomputed,'cubaturetetval')
        precomputed.cubaturetetval = zeros(0,2);
    end;
    cs_c = size(precomputed.cubaturetetval,1);
    cs_r = size(precomputed.params,1);
    if cs_c<cs_r
        precomputed.cubaturetetval( cs_r ,:) = 0; % expand array with zeros.
        progressbar('start',[cs_c  cs_r],'Computing cubature weights');
        for idx = cs_c+1 : cs_r
        	tn = precomputed.params(idx,1);
            tsym = precomputed.params(idx,3);
            points = unpackpar(precomputed.angles{idx}, tn, tsym);
            [tstcub_weights, tstrejord] = sphere_cubature_weigths(points, tn, tsym);
            precomputed.cubaturetetval(idx,:) = [1./sum(tstcub_weights.^2)  tstrejord];
            progressbar(idx);
        end;
        progressbar('ready');
        precomputed = saveToFile(precomputed, saveToFilename);
    end;
    
    sz = precomputed.params(:,1);
    
    issym = precomputed.params(:,3)== 1;
%     addord = zeros(size(sz));
%     addord(issym) = (floor(floor(.5*(-3 + sqrt(1+4*sz(issym) )))/2)*2+1);
%     addord(~issym) = (-1 + floor(sqrt(sz(~issym))));
%     
    ordsc = 1; % weight of extra order spherical harmonic rejection.
    selectSpecifiedNstrength = 0 + (findbestcubature==2)*2; % Extra strongly prefer the number of elements specified.
    crit = precomputed.cubaturetetval*[1;ordsc] - 2 * max(0,sz-n) - selectSpecifiedNstrength * abs(sz-n);
    if symmetry  % function for which the cubature is to be computed is symmetric.
        crit( issym ) = precomputed.cubaturetetval(issym,:)*[.5;ordsc] - 2 * max(0,sz(issym)/2-n)  - selectSpecifiedNstrength * abs(sz(issym)/2-n);;
    end;
    % plot(sz(~issym),crit(~issym),'*',sz(issym)/2,crit(issym),'*')
    [dummy, bestidx] = max(crit);
% 
%     [sz,sind]=sort(sz);
%     fsel = fsel(sind);
%     bestnrm = 0;
%     idx = numel(sz);
%     while bestnrm < sz(idx) && idx>=1
%         tn = precomputed.params(fsel(idx),1);
%         tsym = precomputed.params(fsel(idx),3);
%         points = unpackpar(precomputed.angles{fsel(idx)}, tn, tsym);
%         [tstcub_weights, tstrejord] = sphere_cubature_weigths(points, tn, tsym);
%         if symmetry && tsym
%             tstcub_weights = tstcub_weights(1:end/2)*2;
%         end;
%         tstnrm = 1./sum(tstcub_weights.^2) + ordsc .* tstrejord;
%         if tstnrm > bestnrm
%             bestidx = idx;
%             bestcub_weights = tstcub_weights;
%             bestnrm = tstnrm;
%         end;
%         idx = idx-1;
%     end;
    tn = precomputed.params(bestidx,1);
    tsym = precomputed.params(bestidx,3);
    points = unpackpar(precomputed.angles{bestidx}, tn, tsym);
   
    dotesselate = nargout>=3;
    if nargout>=2
        cub_weights =sphere_cubature_weigths(points, tn, tsym);
    else
        cub_weights = [];
    end;
    if symmetry && tsym
        points = points(1:end/2,:);
        cub_weights = cub_weights(1:end/2)*2;
        if dotesselate 
            error('cannot tesselate half of symmetric cubature optimized sphere');
        end;
    end;
else
% Find current request in old results:
reqopts = [n,distnrm,symmetry];
foundIndx = find(all(reqopts(ones(nPrecomp,1),:)==precomputed.params,2));
if isempty(foundIndx)
    % New request, compute anew.
    
    noisemagn = sqrt((4*pi / n)/pi); % compute radius of circle with 1/n * area unit sphere.
    
    disp('Point distribution with the requested parameters not found in precomputed results.')
    useExternOptimizedSet = nargin>=4 && size(OptimalExternPointSet,1)==n && numel(OptimalExternPointSet)/n==3; 
    if useExternOptimizedSet
        % test optimality:
        if symmetry && any(any(OptimalExternPointSet(1:n/2,:)~=-OptimalExternPointSet(n/2+1:n,:)))
            error('Symmetry not correctly present in external point set');
        end;
        angls = packpar(OptimalExternPointSet,n, symmetry);
        [f,g] = dstcomputeAngl(angls,n,distnrm,symmetry);
        ng = sqrt(mean(g(:).^2));
        disp(['function value: ' num2str(f) ', 2-norm of gradient :' num2str(ng)]);
        useExternOptimizedSet = ng<.01*f;
    end;
	if ~useExternOptimizedSet
    
    suggest_alternatives(precomputed, reqopts);
    % initialize:
%     [I,J]=find(ones(n)-tril(ones(n))); % only select the combinations where I<J;
%     accumind = [kron(ones(size(I,1)*2,1),(1:ndims)') kron([I;J],ones(ndims,1))];
    points = randn(n,3);
    fopt = zeros(ntries,1);
    exflag = zeros(ntries,1);
    pointsC = cell(ntries,1);
    uselbfgs =  exist('lbfgsb', 'file')==3; % old LBFGS version
    if exist('fminlbfgs', 'file')==2
        uselbfgs = 2;
    end;
        uselbfgs = 0;
    opt = optimset('display','off','DerivativeCheck','off','Diagnostics','off','GradObj','on','Largescale','off','MaxIter',10*n);
    if uselbfgs    
        opt.HessUpdate = 'lbfgs';
        opt.GradConstr = false;
    else
        warning('sphere_best:NoLbfgsb','the lbfgsb library is not found, using slower fminunc instead');
        opt = optimset(opt, 'Largescale','on','Hessian','on','HessMult',@(hessinfo, Y) dstcomputeAngl_hessmul(Y, hessinfo, n, symmetry),'Algorithm','Interior-Point','SubproblemAlgorithm','cg','display','iter','Preconditioner',@dstcomputeAngl_precond);
    end;
    % compute:
    progressbar('start',ntries,'optimizing points on sphere','EstTimeLeft','on');
    for t = 1:ntries
        initPnts = points +  noisemagn * randn(n,3);
        init = packpar(initPnts,n, symmetry);

%         tic        
        if uselbfgs==1
            [angls,fopt(t)] = lbfgsb(init,-inf(size(init)),inf(size(init)),'feval_lbfgsb',{@dstcomputeAngl,n,distnrm,symmetry},[],'factr',1e-6,'maxiter',max(100,round(2*sqrt(n))));
        elseif uselbfgs==2
            [angls,fopt(t),exflag(t),outp(t)]=fminlbfgs(@(x) dstcomputeAngl(x,n,distnrm,symmetry),init ,opt);%outp just for debug.
        else
            nneighb = 40; % number of neighbors for hessian evaluation
            [angls,fopt(t),exflag(t),outp(t)]=fminunc(@(x) dstcomputeAngl(x,n,distnrm,symmetry,nneighb),init ,opt);%outp just for debug.
        end;
%         toc   
%         fopt(t)
%         exflag
%         outp
        [points]=unpackpar(angls, n, symmetry);
        if storeAsPoints
            pointsC{t} = points;
        else
            pointsC{t} = angls;
        end;
        progressbar(t);
    end;
	progressbar('ready');
    if debugplot
        plot(log(fopt))
    end;
    [dummy,minpos] = min(fopt); 
    
    % improve best found, since it still might have a large gradient.
    if uselbfgs==1
        [points, fopt_final] = lbfgsb(pointsC{minpos},-inf(size(pointsC{minpos})),inf(size(pointsC{minpos})),'feval_lbfgsb',{@dstcomputeAngl,n,distnrm,symmetry},[],'factr',1000*eps,'maxiter',100+round(1.2*n));
    elseif uselbfgs==2
        [points, fopt_final,exflag_final,outp_final]=fminlbfgs(@(x) dstcomputeAngl(x,n,distnrm,symmetry), pointsC{minpos} ,opt);%outp just for debug.
    else
        if n<1000
            nneighb = [];
        else
            nneighb = 200;
        end;
        opt.MaxPCGIter = 30;
        opt.TolFun = 1e-9;
        [points, fopt_final,exflag_final,outp_final]=fminunc(@(x) dstcomputeAngl(x,n,distnrm,symmetry,nneighb),pointsC{minpos} ,opt);%outp just for debug.
    end;
%     points = pointsC{minpos};
    else % do useExternOptimizedSet
        disp('Using externally optimized set and adding that to the precomputed results.');
        if storeAsPoints
            points = useExternOptimizedSet;
        else
            points = angls;
        end;
	end;
    if dosave
        disp(['Updating "' saveToFilename '", YOU should make sure no one else updates this file during my update.']);
        precomputed = loadfile(precomputed,saveToFilename);
%         if ~isequal(dir(saveToFilename),precomputed.loadedversion)
%             disp('file modified since last loading, loading most current version');
%             load(saveToFilename,'precomputed');
%             precomputed.loadedversion = dir(saveToFilename); % assume storeAsPoints is correctly set.
%         end;
    end;
    
    storeind = size(precomputed.params,1)+1;
    if storeAsPoints
        precomputed.points{storeind} = points;
    else
        precomputed.angles{storeind} = points;
        points = unpackpar(points, n, symmetry);
    end;
    precomputed.params(storeind,:) = reqopts;
    if dosave
        precomputed = saveToFile(precomputed, saveToFilename);
        disp('Update completed.');
    end;
else
    if numel(foundIndx)~=1
        error('Precomputed results invallid');
    end;
    if storeAsPoints
        points = precomputed.points{foundIndx};
    else
        points = unpackpar(precomputed.angles{foundIndx}, n, symmetry);
    end;
end;
    dotesselate = nargout>=2;
end;
%% find tesselation:
if dotesselate
    [faces,congraph] = tesselate(points, n);
end;
% patch('Faces',faces,'Vertices',points,'FaceVertexCData',rand(size(faces,1),3),'FaceColor','flat','FaceAlpha',.5)
%% Cubature weights
if nargout>=3 && (findbestcubature~=0)
    [cub_weights] = sphere_cubature_weigths(points,n, symmetry);
end;
%% diagnostics
if nargout>=4
%     [I,J]=find(ones(n)-tril(ones(n))); % only select the combinations where I<J;
%     accumind = [kron(ones(size(I,1)*2,1),(1:ndims)') kron([I;J],ones(ndims,1))];
%     [f,g] = dstcomputeAngl(packpar(points,n, symmetry),n,distnrm,symmetry,I,J,accumind);
    [f,g] = dstcomputeAngl(packpar(points,n, symmetry),n,distnrm,symmetry);
    diagnostics.funval  = f;
    diagnostics.fungrad = g;
%     diagnostics.connections = connects2;
    diagnostics.connectiongraph = congraph;
end;
%% Re-arange outputs
if findbestcubature~=0
    if dotesselate
        [cub_weights,faces] = deal(faces,cub_weights);
    else
        faces = cub_weights;
    end;
end;

function [sumdist, ddistdPoint] = dstcomputePoint(points)
rawdsts = points(:,I)-points(:,J);
dst2 = sum((rawdsts).^2);
dstp = dst2.^(distnrm/2);
sumdist = sum(dstp);
ddistdpntIJ = rawdsts .* ((distnrm .* ones(ndims,1)) * (dstp./dst2));
ddistdPoint = accumarray(accumind,[ddistdpntIJ(:); -ddistdpntIJ(:)],[ndims, n]);

% function [fval,grad] = dstcomputeAngl_lbfgsb(angls, inp2)
% persistent oldresults
% if isempty(oldresults) || ~isequal(oldresults.angls,angls)
%     oldresults.angls = angls;
%     [oldresults.sumdist, oldresults.ddistdAngl] = dstcomputeAngl(angls,inp2{:});
% end;
% if retfun
%     fvalorgrad = oldresults.sumdist;
% else
%     fvalorgrad = oldresults.ddistdAngl;
% end;
% [fval,grad] = dstcomputeAngl(angls,inp2{:});

% function [sumdist, ddistdAngl] = dstcomputeAngl(angls,n,distnrm,symmetry,I,J,accumind)
function [sumdist, ddistdAngl, hessinfo] = dstcomputeAngl(angls,n,distnrm,symmetry, nneighb)
% angls = [theta phi]
% x = sin(phi) cos(theta)
% y = sin(phi) sin(theta)
% z = cos(phi)

[points,csangl,snangl]=unpackpar(angls,n, symmetry);
points = points';
if nargout==1
    [sumdist]=sumDst_deriv_c( points, distnrm);
else
    if nargout>2
        [sumdist,ddistdPoint, hessinfo] = sumDst_deriv_c_v2(points,distnrm,nneighb);
        hessinfo.points = points;
        hessinfo.cosangle = csangl;
        hessinfo.sinangle = snangl;
        hessinfo.dEdPoint = ddistdPoint';
    else
        [sumdist,ddistdPoint]=sumDst_deriv_c(points,distnrm);
    end;
    % chain rule, compute d(dist)/d(angl) = d(dist)/d(point_i) * d(point_i)/d(angl)
    ddistdAngl = [sum([-snangl(:,2).*snangl(:,1) snangl(:,2).*csangl(:,1)]'.*ddistdPoint(1:2,:));
                  sum([csangl(:,2).*csangl(:,1) csangl(:,2).*snangl(:,1) -snangl(:,2)]'.*ddistdPoint)];
    if symmetry==1
        % reflect through 0, point -> -point; 
        ddistdAngl = ddistdAngl(:,1:n/2)+ddistdAngl(:,n/2+1:n);
    end;          
    ddistdAngl(1:3)=[];
end;

function [fullHx] = dstcomputeAngl_hessmul(x, hessinfo, n, symmetry)
if size(x,2)>1
    fullHx = cell(1,size(x,2));
    for k=1:size(x,2)
        fullHx{k} = dstcomputeAngl_hessmul(x(:,k), hessinfo, n, symmetry);
    end;
    fullHx =[fullHx{:}];
    return;
end;
x2 = reshape([0; 0; 0; x],2,n/(symmetry+1))';
ss = hessinfo.sinangle(:,2).*hessinfo.sinangle(:,1);
cc = hessinfo.cosangle(:,2).*hessinfo.cosangle(:,1);
sc = hessinfo.sinangle(:,2).*hessinfo.cosangle(:,1);
cs = hessinfo.cosangle(:,2).*hessinfo.sinangle(:,1);
s2 = hessinfo.sinangle(:,2);
c2 = hessinfo.cosangle(:,2);
Px = [-ss.*x2(:,1)+cc.*x2(:,2) ...
       sc.*x2(:,1)+cs.*x2(:,2) ...
                  -s2.*x2(:,2)]';
HPx = sumDst_mulhess_c_v2( Px, hessinfo );
HPx = HPx';
dEdt11 = -sc.*hessinfo.dEdPoint(:,1)-ss.*hessinfo.dEdPoint(:,2);
dEdt12 = -cs.*hessinfo.dEdPoint(:,1)+cc.*hessinfo.dEdPoint(:,2);
dEdt22 = -sc.*hessinfo.dEdPoint(:,1)-ss.*hessinfo.dEdPoint(:,2)-c2.*hessinfo.dEdPoint(:,3);

PHPx = [-ss.*HPx(:,1)+sc.*HPx(:,2) ...
        cc.*HPx(:,1)+cs.*HPx(:,2)-s2.*HPx(:,3)] ...
      +[dEdt11.*x2(:,1)+dEdt12.*x2(:,2) dEdt12.*x2(:,1)+dEdt22.*x2(:,2)];  
PHPx = PHPx';  
fullHx = PHPx(4:end)';

function [R,pR] = dstcomputeAngl_precond( hessinfo, uppderbandw , DM, DG)
%
% M = DM*H*DM + DG  (DM & DG diagonal matrices)
% R = chol(M) (approximately)
%
% Created by Dirk Poot, Erasmus MC

% for each point (2x2 block on diagonal of H): 
% dpdT = [-ss cc;sc cs; 0 -s2]
% Hp = hessinfo.hessdiag([1 2 3;2 4 5;3 5 6])
% dEdt = [dEdt11 dEdt12;dEdt12 dEdt22]
% H = dpdT' *Hp * dpdT + dEdt

ss = hessinfo.sinangle(:,2).*hessinfo.sinangle(:,1);
cc = hessinfo.cosangle(:,2).*hessinfo.cosangle(:,1);
sc = hessinfo.sinangle(:,2).*hessinfo.cosangle(:,1);
cs = hessinfo.cosangle(:,2).*hessinfo.sinangle(:,1);
s2 = hessinfo.sinangle(:,2);
c2 = hessinfo.cosangle(:,2);
dEdt11 = -sc.*hessinfo.dEdPoint(:,1)-ss.*hessinfo.dEdPoint(:,2);
dEdt12 = -cs.*hessinfo.dEdPoint(:,1)+cc.*hessinfo.dEdPoint(:,2);
dEdt22 = -sc.*hessinfo.dEdPoint(:,1)-ss.*hessinfo.dEdPoint(:,2)-c2.*hessinfo.dEdPoint(:,3);

hd = hessinfo.hessdiag';
Hp_dpdT = [sum(hd(:,[1 2  ]).*[-ss sc    ],2) sum(hd(:,[2 4  ]).*[-ss sc    ],2) sum(hd(:,[3 5  ]).*[-ss sc    ],2) ...
           sum(hd(:,[1 2 3]).*[ cc cs -s2],2) sum(hd(:,[2 4 5]).*[ cc cs -s2],2) sum(hd(:,[3 5 6]).*[ cc cs -s2],2)];

Hv = ( [sum(Hp_dpdT(:,[1 2  ]).*[-ss sc    ],2) sum(Hp_dpdT(:,[4 5  ]).*[-ss sc    ],2) ...
        sum(Hp_dpdT(:,[1 2 3]).*[ cc cs -s2],2) sum(Hp_dpdT(:,[4 5 6]).*[ cc cs -s2],2)] ...
      +[dEdt11 dEdt12 dEdt12 dEdt22] );
dDM = full(diag(DM));
dDM1 = [0;0;dDM(2:2:end)];
dDM2 = [0;dDM(1:2:end)];
dDG = full(diag(DG));
Mv = (Hv .* [dDM1.^2 dDM1.*dDM2 dDM1.*dDM2 dDM2.^2]);
Mv(:,[1 4]) = Mv(:,[1 4]) + reshape([0;0;0;dDG],2,[])';
poseigad = .5*(sqrt(Mv(:,1).^2 -2*Mv(:,1).*Mv(:,4) + Mv(:,4).^2 + 4 .* Mv(:,2).^2)-Mv(:,1)-Mv(:,4));
posdefad = max(max(max(-Mv(:,1),-Mv(:,4)), poseigad)+1,0);
Mv(:,[1 4]) = Mv(:,[1 4]) + posdefad(:,[1 1]);
n = 2*size(Mv,1);
M = sparse([1:2:n;2:2:n;1:2:n;2:2:n], [1:2:n;1:2:n;2:2:n;2:2:n;],Mv',n,n);
M = M(4:end,4:end);
[R,dum] = chol(M);
pR = 1:n-3;
% rawdsts = points(:,I)-points(:,J);
% dst2 = sum((rawdsts).^2);
% dstp = dst2.^(distnrm/2);
% sumdist = sum(dstp);
% if nargout>1
%     if 0
%         % direct gradient from angles
%         p = [I;J];
%         q = [J;I];
%         dsc =  (distnrm * dstp./dst2);
%         ddistdPhiIJ = (snangl(p,2).*csangl(q,2)-csangl(p,2).*snangl(q,2).*(csangl(p,1).*csangl(q,1)+snangl(p,1).*snangl(q,1)) )' .*dsc;
%         ddistdThtIJ = (points(1,q).*points(2,p)-points(1,p).*points(2,q)).*dsc;
%     error('code not ready');
%         ddistdAngl = accumarray(accumind,[ddistdpntIJ(:) -ddistdpntIJ(:)],[ndims, n]);
%     else
%         % first compute point gradients 
%         % d(sumd, pnti) = d( sum((pnti-pntj).^2)^(nrm/2), pnti) 
%         %   = (nrm/2) * sum((pnti-pntj).^2)^((nrm/2)-1) * d(sum((pnti-pntj).^2), pnti) 
%         %   = (nrm/2) * sum((pnti-pntj).^2)^((nrm/2)-1) * (2 * (pnti-pntj) ) 
%         %   = nrm * dstp/dst2 * (pnti-pntj)
%         ddistdpntIJ = rawdsts .* ((distnrm .* [1;1;1]) * (dstp./dst2));
%         ddistdPoint = accumarray(accumind,[ddistdpntIJ(:); -ddistdpntIJ(:)],[3, n]);
%         if 1
%             % test new code:
% %             [sumdist2,ddistdPoint2]=sumDst_deriv_old(points',distnrm);
%             [sumdist3,ddistdPoint3]=sumDst_deriv_c(points,distnrm);
% %             sumd4 = 0;
% %             ddistdP4 = zeros(size(points));
% %             for k1=1:size(points,2)-1
% %                 for k2=k1+1:size(points,2)
% %                     rwdst = points(:,k1)-points(:,k2);
% %                     srwdst = sum(rwdst.^2);
% %                     scrwdst = srwdst.^(distnrm/2);
% %                     sumd4 = sumd4 + scrwdst;
% %                     f = (distnrm * scrwdst/srwdst) * rwdst;
% %                     ddistdP4(:,k1) = ddistdP4(:,k1)+f;
% %                     ddistdP4(:,k2) = ddistdP4(:,k2)-f;
% %                 end;
% %             end;
%             fprintf('.');
%             if abs(sumdist3-sumdist)>size(points,2)*eps(sumdist) || any(any(ddistdPoint3'-ddistdPoint'>100*eps*max(ddistdPoint(:))))  %|| ~isequal(sumdist2,sumdist3) || ~isequal(ddistdPoint2',ddistdPoint3)
%                %|| abs(sumdist3-sumd4)>size(points,2)*eps(sumdist) || any(any(ddistdPoint3'-ddistdP4'>100*eps*max(ddistdPoint(:)))) 
%                 error('new code incorrect');
%             end;
%         end;
%         % chain rule, compute d(dist)/d(angl) = d(dist)/d(point_i) * d(point_i)/d(angl)
%         ddistdAngl = [sum([-snangl(:,2).*snangl(:,1) snangl(:,2).*csangl(:,1)]'.*ddistdPoint(1:2,:));
%                       sum([csangl(:,2).*csangl(:,1) csangl(:,2).*snangl(:,1) -snangl(:,2)]'.*ddistdPoint)];
%         if symmetry==1
%             % reflect through 0, point -> -point; 
%             ddistdAngl = ddistdAngl(:,1:n/2)+ddistdAngl(:,n/2+1:n);
%         end;          
%         ddistdAngl(1:3)=[];
%     end;
% end;

function precomputed = loadfile(precomputed,saveToFilename)
if isempty(precomputed) || ~isequal(dir(saveToFilename),precomputed.loadedversion)
    % load default/known results
    if exist(saveToFilename,'file')
        load(saveToFilename,'precomputed');
        precomputed.loadedversion = dir(saveToFilename);
    else
        disp(['File with precomputed best sphere distributions, "' saveToFilename '", not found, creating new.']);
    end;
    if isempty(precomputed)
        precomputed = struct('points',cell(0,1),'params',zeros(0,3));
        precomputed.loadedversion = dir(saveToFilename);
    end;
    % Convert old results when needed:
%     if storeAsPoints 
%         if isfield(precomputed,'angles') 
%             if numel(precomputed.angls)==size(precomputed.params,1)
%                 disp('converting storage to points');
%                 for k=1:size(precomputed.params,1)
%                     precomputed.points{k} = unpackpar(precomputed.angles{k}, precomputed.params(k,1), precomputed.params(k,3));
%                 end;
%             else
%                 error('wrong precomputed structure');
%             end;
%             precomputed = rmfield(precomputed,'angles');
%         end;
%     else
%         if isfield(precomputed,'points') 
%             if numel(precomputed.points)==size(precomputed.params,1)
%                 disp('converting storage to compacted angles');
%                 for k=1:size(precomputed.params,1)
%                     precomputed.angles{k} = packpar(precomputed.points{k}, precomputed.params(k,1), precomputed.params(k,3));
%                 end;
%             else
%                 error('wrong precomputed structure');
%             end;
%             precomputed = rmfield(precomputed,'points');
%         end;
%     end;
end;
function [precomputed] = saveToFile(precomputed,saveToFilename)
if ~isequal(dir(saveToFilename),precomputed.loadedversion)
    warning(['file "' saveToFilename '" modified before writing the new version, previous modifications are erased.']);
end;
save(saveToFilename,'precomputed');
precomputed.loadedversion = dir(saveToFilename);

function [init]=packpar(initPnts,n, symmetry)
% Should be matched with unpackpar, use storeAsPoints=true to convert stored
% results (verify!!!) before modifiing these functions.
if symmetry==1
    initPnts = initPnts(1:n/2,:);
end;
if initPnts(2,1)<0
    initPnts(:,1) = -initPnts(:,1);
end;
init = atan2([initPnts(:,2) sqrt(initPnts(:,[1 2]).^2 * [1;1])],initPnts(:,[1 3]))';
init([1 2 3]) = [];

        
function [points,csangl,snangl]=unpackpar(angls,n, symmetry)
% should be matched with packpar and stored restuls. Use storeAsPoints=true
% to convert stored results, verify data and code, before modifiing these functions.
angls = reshape([0 0 0 angls],2,n/(symmetry+1))';
csangl = cos(angls);
snangl = sin(angls);
if symmetry==1
    % reflect through 0, point -> -point; do by setting sin(phi) and
    % cos(phi) to -sin(phi) and -cos(phi) respectively.
    csangl = [csangl;csangl*[1 0;0 -1]];
    snangl = [snangl;snangl*[1 0;0 -1]];
end;
points = [snangl(:,2).*csangl(:,1) snangl(:,2).*snangl(:,1) csangl(:,2)];


function suggest_alternatives(precomputed, reqopts)
% suggest alternative point distributions that are already computed.
symstr = {', no symmetry',', symmetric around 0'};
disp(['Starting optimization, which will take a while. Parameters: numpoints = ' num2str(reqopts(1)) ', norm = ' num2str(reqopts(2)) symstr{reqopts(3)+1}]);
% Suggest alternatives, max 8?, usually less:
nPrecomp = size(precomputed.params,1);
sameInAspect = reqopts(ones(nPrecomp,1),:) == precomputed.params;
selectsuggest = [];
for k = 0:6
    if k==0
        sameN = (1:size(precomputed.params,1))';
    elseif k<=3
        sameN = find(sameInAspect(:,k));
    else
        sameaspect = [1 2;1 3;2 3];
        sameN = find(all(sameInAspect(:,sameaspect(k-3,:)),2));
    end;
    if numel(sameN)>1
        % find closest in other parameters.
        others = 1:3;
        if k<=3
            others(others==k)=[];
        else
            others(others==sameaspect(k-3,1) | others==sameaspect(k-3,2))=[];
        end;
        difference = reqopts(ones(size(sameN)),others)-precomputed.params(sameN,others);
        dist = (difference).^2*ones(numel(others),1);
        [sdist,sdistind]= sort(dist);
        sameN = sameN(sdistind);
        difference = difference(sdistind,:);
        % find all that closest in some direction:
        pp=1;
        while pp < length(sameN)
            curdiff = difference(pp,:);
            keep = difference * curdiff' <= curdiff*curdiff';
            sdist = sdist(keep);
            difference = difference(keep ,:);
            sameN = sameN(keep );
            pp = sum(keep(1:pp))+1;
        end;
        % keep only overall closest in each direction.
        pp=1;
        while pp < length(sameN)
            curdiff = difference(pp,:);
            keep = difference * curdiff' < curdiff*curdiff';
            keep( sdist == min(sdist(~keep))) = true; % keep closest.
            sdist = sdist(keep);
            difference = difference(keep ,:);
            sameN = sameN(keep );
            pp = sum(keep(1:pp))+1;
        end;
    end;
    selectsuggest = [selectsuggest;sameN];%#ok: Dont know beforehand how large it will be.
end;
selectsuggest = sort(selectsuggest);
selectsuggest(diff(selectsuggest)==0)=[];
if ~isempty(selectsuggest)
    disp('Alternatively you could break (CTRL+BREAK) the current optimization and use an alternative,');
    disp('for example one from the following closest sets [numpoints, norm, symmetry]:');
    disp(num2str(precomputed.params(selectsuggest,:)));
end;

function [faces,congraph] = tesselate(points,n)
% tesselate the set of points, specifically for uniform point
% distributions, not a general tesselation routine.
if n>200
    % use complicated routine for large point sets (faster and requires
    % less memory)
    n=size(points,1);
    % grid the points on a cube with size gridsz:
    gridsz = ceil(sqrt(n/6 * 1.6)); % only few with more than 1 per gridpoint.
    [pointsm, inface] = to_grid_points(points, gridsz);
    grid = zeros(gridsz,gridsz,6); 
    cumgrsz = cumprod([1 gridsz gridsz]');
    indx = 1+[pointsm inface-1]*cumgrsz;
    grid(indx)=(1:n);
    
    % find the grid points where more than 1 point is located
    doaslists = find(grid(indx)~=(1:n)');
    lists = cell(10,1);
    nlists = 0;
    maxlistsz = 0;
    % and create lists of them
    for k=doaslists'
        if grid(indx(k))<=n
            nlists = nlists+1;
            lists{nlists} = [grid(indx(k)) k];
            grid(indx(k)) = n + nlists;
            maxlistsz = max(2,maxlistsz);
        else
            lists{grid(indx(k))-n} = [lists{grid(indx(k))-n} k];
            maxlistsz = max(numel(lists{grid(indx(k))-n}),maxlistsz);
        end;
    end;
%     imagebrowse(grid);
%     nlists
    % awfully complicated way of finding how we have to stitch the individual
    % squares of the cube:
    tpnt = [[1 1;1 -1;-1 1;-1 -1] .3*ones(4,1)];
    adj = [[-1 1;1 1; 1 1; -1 1] zeros(4,1)];
    tpnt = [tpnt;tpnt(:,[2 3 1]);tpnt(:,[3 1 2])];
    adj = [adj;adj(:,[2 3 1]);adj(:,[3 1 2])];
    tpnt = [tpnt+.01*adj;tpnt-.01*adj];
    tpnt  = tpnt ./repmat(sqrt(sum(tpnt.^2,2)),[1 3]); % normalize;
    [tsm, tfc] = to_grid_points(tpnt, 5);
    
    regionsz = 3;
    gridexpnd = zeros(gridsz,regionsz,6,4);
    % now copied (and rotate/flip) the data with which each square is
    % expanded (regionsz rows/columns at each side); with the data from its 
    % attaching square:
    for k=1:6
        ingrd = find(tfc==k);
        opnt = mod(ingrd-1+size(tpnt,1)/2,size(tpnt,1))+1;
        for k2=1:4
            if k2==1
                % right
                oright = find(tsm(ingrd,2)==4);
                ingrdp2 = tsm(ingrd(oright),1);
            elseif k2==4
                % left
                oright = find(tsm(ingrd,2)==0);
                ingrdp2 = tsm(ingrd(oright),1);
            elseif k2==2
                % top
                oright = find(tsm(ingrd,1)==0);
                ingrdp2 = tsm(ingrd(oright),2);
            else
                % bottom
                oright = find(tsm(ingrd,1)==4);
                ingrdp2 = tsm(ingrd(oright),2);
            end;
            
            if tsm(opnt(oright),2)==0
                ogrdp2 = tsm(opnt(oright),1);
                gridexpnd(:,:,k,k2) = grid(:,1:regionsz,tfc(opnt(oright)));
            elseif tsm(opnt(oright),2)==4
                ogrdp2 = tsm(opnt(oright),1);
                gridexpnd(:,:,k,k2) = grid(:,end:-1:end-regionsz+1,tfc(opnt(oright)));
            elseif tsm(opnt(oright),1)==0
                ogrdp2 = tsm(opnt(oright),2);
                gridexpnd(:,:,k,k2) = grid(1:regionsz,:,tfc(opnt(oright)))';
            elseif tsm(opnt(oright),1)==4
                ogrdp2 = tsm(opnt(oright),2);
                gridexpnd(:,:,k,k2) = grid(end:-1:end-regionsz+1,:,tfc(opnt(oright)))';
            else
                error('wrong matching');
            end;
            % flip when upside down:
            if ogrdp2~=ingrdp2
                gridexpnd(:,:,k,k2) = gridexpnd(end:-1:1,:,k,k2);
            end;
        end;
    end;
    gridright  = gridexpnd(:,:,:,1);
    gridleft   = gridexpnd(:,end:-1:1,:,4);
    gridtop    = permute(gridexpnd(:,end:-1:1,:,2),[2 1 3]);
    gridbottom = permute(gridexpnd(:,:,:,3),[2 1 3]);
    if 0 
        %% verify matching.
        topsel = gridtop(end,4:40,:) & grid(1,4:40,:);sum(topsel,2)
        pntA=zeros(0,3);pntB = pntA;
        for k=1:6; 
            pntA(end+(1:sum(topsel(:,:,k))),:) = points(gridtop(end,find(topsel(:,:,k))+3,k),:); 
            pntB(end+(1:sum(topsel(:,:,k))),:) = points(grid(1,find(topsel(:,:,k))+3,k),:);
        end;
        pointDist(pntA,pntB)
        
        topsel = gridright(4:40,1,:) & grid(4:40,end,:);sum(topsel,1)
        pntA=zeros(0,3);pntB = pntA;
        for k=1:6; 
            pntA(end+(1:sum(topsel(:,:,k))),:) = points(gridright(find(topsel(:,:,k))+3,1,k),:); 
            pntB(end+(1:sum(topsel(:,:,k))),:) = points(grid(find(topsel(:,:,k))+3,end,k),:);
        end;
        pointDist(pntA,pntB)
        
        topsel = gridbottom(1,4:40,:) & grid(end,4:40,:);sum(topsel,2)
        pntA=zeros(0,3);pntB = pntA;
        for k=1:6; 
            pntA(end+(1:sum(topsel(:,:,k))),:) = points(gridbottom(1,find(topsel(:,:,k))+3,k),:); 
            pntB(end+(1:sum(topsel(:,:,k))),:) = points(grid(end,find(topsel(:,:,k))+3,k),:);
        end;
        pointDist(pntA,pntB)
        %%
    end;
    
    % Now actually expand the squares:
    grid = [zeros(regionsz,regionsz,6)   gridtop    zeros(regionsz,regionsz,6);
                 gridleft                  grid            gridright;
            zeros(regionsz,regionsz,6)  gridbottom  zeros(regionsz,regionsz,6)];
	grsz = size(grid);
    cumgrsz = cumprod([1 grsz(1:2)]');
    
    % find the neighbouring region, from which all closest neigbours (the
    % ones used for the edges) should come from:
    [regionx,regiony] = meshgrid((-regionsz:regionsz),(-regionsz:regionsz));
    dsq = regionx.^2+regiony.^2;
    [regiony,regionx] = find(dsq>0& dsq<=10);
    region = [regiony-1 regionx-1];
    others = zeros(n, size(region,1));
    
    % use the region to find the closest points (or lists of points)
    for k=1:size(region,1)
        indx2 = 1+[pointsm + region(k*ones(n,1),:) inface-1]*cumgrsz;
        others(:,k) = grid(indx2);
    end;
    % 'defragment' the others matrix: (put zeros at the end and store the
    % last used position)
    others = sort(others',1,'descend'); %#ok:wrong mlint message.
    othrcnt = sum(others~=0,1);
    
    % append the 'others' in each list to the others matrix of each point
    % in the list: 
    for k=1:nlists
        lk = lists{k};
        for k2=1:numel(lk)
           others(othrcnt(lk(k2))+(1:numel(lk)-1),lk(k2)) = lk(lk~=lk(k2));
        end;
        othrcnt(lk) = othrcnt(lk) + numel(lk)-1;
    end;
    
    % expand the lists in the others matrix:
    for k=find(others(1,:)>n)
        k2= 1;
        while others(k2,k)>n
            lk = lists{others(k2,k)-n};
            others([k2 othrcnt(k)+(1:numel(lk)-1)],k) = lk;
            othrcnt(k) = othrcnt(k) + numel(lk)-1;
            k2 = k2+1;
        end;
    end;
    
    % find others:
    [I,J] = find(others);
    s = inf(size(others));
    % compute distance between point and 'others'
    s(I+(J-1)*size(s,1)) = pointDist(points(J,:),points(others(I+(J-1)*size(s,1)),:));
    % sort distances:
    [s,sind] = sort(s);
    % and the sind should contain the points-index (instead of the index into the unsorted s):
    sind = reshape(others(sind + ones(size(sind,1),1)*(0:size(sind,1):numel(sind)-1)),size(sind));
    % we don't include the points themself, so set hasself to 0
    hasself = 0;
    if 0
        % old large set code; full eval.
        s    = zeros(9,n);
        sind = zeros(9,n);
        st = 1;
        while st<n
            ed = min(st+ceil(3000/sqrt(n)),n);
            [stmp, sindtmp] = sort(pointDist(points,points(st:ed,:),1)); 
            s(:,st:ed) = stmp(1:size(s,1),:);
            sind(:,st:ed) = sindtmp(1:size(sind,1),:);
            st = ed+1;
        end;
    end;
else
    [s,sind] =sort(pointDist(points,points,1)); 
    hasself = 1;
end;
% npvrtx=4;
% if n<=5 || (n==8 && symmetry==1)
%     npvrtx=3;
% end;
npvrtx=3;
I=ones(npvrtx,1)*(1:size(points,1));
J=sind(hasself+(1:npvrtx),:);
doad = true;
k=npvrtx+hasself;
while doad && k<n
    k=k+1;
    ad = s(npvrtx+hasself,:)*1.13 > s(k,:);
    I = [I(:);find(ad)'];
    J = [J(:);sind(k,ad)'];
    doad = any(ad);
end;
%CD = mat2cell(permute(cat(3,points(I(:),:),points(J(:),:)),[3 1 2]),2,numel(I),[1 1 1]);plot3(CD{:})
connects = unique(sort([I(:) J(:)],2),'rows');
[connects2,conind] = sortrows([connects;connects(:,[2 1])]);
alterind(conind) = (1:size(connects2,1))';
alterind = alterind([size(connects,1)+1:size(connects2,1) 1:size(connects,1)]');
alterind = alterind(conind);
congraph = zeros(size(connects2,1),1);
%% order the connections of each point. 
% Any incomming connection to a point is connected to the next outgoing direction
% (sort of the angles).
ke = 1;
ax1 = ones(size(points,1),1)*[1 0 0];
adj = abs(points(:,1))>.9; 
ax1( adj ,: ) = ones(nnz(adj),1)*[0 1 0]; % make sure basis not (almost) singular.
ax1 = ax1 - points .* (sum(points.*ax1,2)*[1 1 1]); % ortogonalize.
Fax1 = ax1.*((1./sqrt(sum(ax1.^2,2)))*[1 1 1]); % normalize
Fax2 = cross(points, Fax1, 2); % orthonormal axis for projection with correct sense
while ke<size(connects2,1)
    ks = ke;
    while ke<=size(connects2,1) && connects2(ke,1)==connects2(ks,1)
        ke=ke+1;
    end;
    ax1 = Fax1(connects2(ks,1),:)';
    ax2 = Fax2(connects2(ks,1),:)';
    endpnts = points(connects2(ks:ke-1,2),:);
    [angls,anglsind] = sort(atan2(endpnts*ax1,endpnts*ax2)); % angls not needed
    congraph(ks-1+anglsind) = alterind(ks-1+anglsind([2:end 1]));
end;
if any(diff(sort(congraph))~=1)
    error('invalid graph constructed');
end;
%%
usedcon = false(size(congraph));
strtind = 1;
triangles = zeros(3,10);
ntriangles = 0;
nused = 0;
while nused<numel(usedcon)
    while usedcon(strtind)
        strtind = strtind +1;
    end;
    region = strtind;
    while congraph(region(end))~=strtind
        region(end+1)=congraph(region(end));
    end;
    if any(usedcon(region))
        error('should not use edges more than once');
    end;
    usedcon(region) = true;
    nused = nused+numel(region);
    if ntriangles +3>size(triangles,2)
        triangles(:,ntriangles*2+10)=0; % expand;
    end;
    if numel(region)==3
        ntriangles = ntriangles+1;
        triangles(:,ntriangles) = region;
    elseif numel(region)==4
        % compute the length of the diagonals and cut along the shortest
        pnts = points(connects2(region,1),:);
        diags = sum(([1 0 -1 0;0 1 0 -1]*pnts).^2,2);
        if [1 -1]*diags>0
            region = region([2:end 1]); % rotate region so the cut is along 1 3 diagonal
        end;
        triangles(:,ntriangles+(1:2)) = region([1 3 4;3 1 2])';
        ntriangles = ntriangles +2;
    elseif numel(region)==5
        % pentagon largest figure where center cannot be further from all
        % edgepoints, think optimal solution should not have it, but
        % maybe...
        
        % Compute length of all 5 diagonals and cut along the shortest set
        pnts = points(connects2(region,1),:);
        diags = sqrt(sum(([-1 0 0 1 0;1 0 -1 0 0;0 0 1 0 -1; 0 -1 0 0 1;0 1 0 -1 0]*pnts).^2,2));
        dst = diags+diags([2:end 1]);
        [dummy,shrtest] = min(dst(1:5));    
        rotate = mod(0:2:8,5);
        region = region([1+rotate(shrtest):end 1:rotate(shrtest)]);% rotate region so the cut is along 1 3 and 1 4 diagonals
        triangles(:,ntriangles+(1:3)) = region([1 2 3;1 3 4;1 4 5])';
        ntriangles = ntriangles+3;
    else
        error(['Cannot triangulate region with ' num2str(numel(region)) ' points']);
    end;
end;
faces = reshape(connects2(triangles(:,1:ntriangles),1),3,ntriangles)';

function [pointsm, inface] = to_grid_points(points, gridsz)
n = size(points,1);
[dummy, inface] = max(abs(points),[],2);
pointsm = points(mod(inface*[1 1 1]+ones(n,1)*[-1 0 1],3)*n+(1:n)'*[1 1 1]);
inface(pointsm(:,1)<0) = inface(pointsm(:,1)<0) +3;
pointsm = floor(pointsm(:,2:3)./pointsm(:,[1 1])*(gridsz/2-.0001)+gridsz/2);
    
