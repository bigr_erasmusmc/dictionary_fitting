function [x, f, exflag, G, H_info, outp] = fmin_fast( fun, x, options , constraints)
% [x, f, exflag, G, H, outp] = fmin_fast( fun, xinit, options , constraints)
% Fast non linear optimization routine.
% Uses a trust region + dogleg method. The dogleg step is truncated to the
% trust region radius.
% 
% INPUTS:
%  fun : function that should be minimized. Called as:
%         [fx, Gx, Hx ] = fun( x )
%       where fx is the function value at the location x 
%             Gx is the derivative of fun at x
%             Hx is the hessian at x, or hessian information when HessMult is
%                 provided
%  xinit: vector, matrix or ND array with initial position.
%  options: scalar structure with options; obtain the default values with 
%           options = fmin_fast();
%    Some of the valid options:
%   HessMult : hessian multiplication function
%               HessMult( Hx, y ) should multipy the vector y with the
%               hessian evaluated at x
%   Preconditioner : preconditioner creation function
%               precon_info = fun_makeprecon( x, f, g, H_info , old_precon_info);
%               By providing the old preconditioner info hessian approximations can be shared/updated over 
%               iterations. For some preconditioners function value or gradient might be usefull. 
%               Though often not needed, this information might be needed/usefull and it can be provided for free.
%               (Note fminunc needs [R, perm] = preconditioner( Hinfo, pcoptions, DM, DG, ... )
%                whose interface is incompatible (and too limited in my opinion) ).
%   Preconditioner_initialInfo = []; : initial preconditioner information used by preconditioner. 
%   Preconditioner_Multiply : function that multiplies with the
%               preconditioner.
%               d = precon_fun( precon_info, y );
%               approximates  inv(Hx) * y
%                
% OUTPUTS:
%  x      : Optimal location of fun that I found
%  f      : optimal value of fun at x
%  exflag : exit flag; 
%            0 : run out of iterations; not converged.
%            1 : norm of gradient satisfies convergence tolerance (norm(G) < abs_tol_G)
%            2 : norm(step in x) <  abs_tol_step_x
%            3 : converged. Function was reasonably approximated by a
%                quadratic function and the improvement in f was small.
%  G      : Gradient at x (should be close to 0)
%  H_info : Hessian (information) at x
%  outp   : extra information about the optimization; 
%            iterations  : maximum number of iterations or function calls
%                          reached.
%            funcCount   : number of function evaluations
%            cgiterations: Total number of conjugate gradient iterations,
%                          which equals the number of hessian
%                          multiplications that are performed.
%            stepstaken  : number of steps that are accepted. (Any
%                          difference with iterations indicates
%                          'wasted' computations.)
%            preconditionerPrepare : number of times the preconditioner is
%                          constructed.
% 
% Created by Dirk Poot, Erasmus MC, 21-11-2012


if nargin<1
    %return default options:
    options.HessMult = @(H, x) H*x;
    options.Preconditioner = @make_default_preconditioner; % make preconditioner
    options.Preconditioner_initialInfo = [];
    options.Preconditioner_Multiply = @mul_default_preconditioner;
    options.TR_scale_invalid_f = .1;
    options.TR_scale_good_step = 2;
    options.TR_scale_bad_step  = .25;
    options.TR_scale_verybad_step = .1;
    options.InitialTrustRegionRadius = [];
    options.pcg_options = pcg_dogleg();
    options.maxIter = 1000;
    options.abs_tol_G = .0001;
    options.abs_tol_x = .001;
    options.abs_tol_fval = 0.001; % Iterations stop if expected improvement in function value of next iteration is below this value. (I only try to estimate the next iteration improvement; so typically this value should be about an order of magnitude below the required tolerance w.r.t. the global minimum function value)
    options.MaxFunEvals = 100;
    options.dynamicGradientTrustRegion = false; % default to preserve behaviour. dynamicGradientTrustRegion performs a few extra evaluations on the gradient per iteration to update the trust region radius based on the expected gradient error at the end of the quadratic subproblem.
    options.lineSearchMethod = 0; % 0 = no line search ; just iterate with updated trust region if step is not good. 
                                  %(1 : Newton Rapson like; not implemented)
                                  % 2 = secant line search  
    options.lineSearch_maxIter = 10; 
    options.lineSearch_gradientToleranceRelative = .1 ; % fraction of gradient magnitude in search direction. When the new point has gradient magnitude less than lineSearch_gradientToleranceRelative * gradient magnitude of start point the line search stops. This is a fast stopping criterion (early in the compution). 
    options.lineSearch_minFvalGain = 1.5; % minimum expected improvement in function value needed for the line search to continue. Continue if  (Fstart-Fcurrent)*lineSearch_minFvalGain > (Fstart - Fexpect) , where Fstart = function value of start of line search, Fcurrent = function value of current best point in line search, Fexpect = predicted (/lower bound of) function value of the suggested next point. 
    x=options;
    return;
end;

size_x = size(x);

[f, G, H_info ] = fun( x );numFunEvals = 1;
H_updated = true;
fun_hessmul    = options.HessMult;
fun_makeprecon = options.Preconditioner;
precon_info    = options.Preconditioner_initialInfo;
fun_mulprecon  = options.Preconditioner_Multiply;
pcg_opts = options.pcg_options;
pcg_opts.maxR = options.InitialTrustRegionRadius;
abs_tol_G          = options.abs_tol_G;
abs_tol_step_x     = options.abs_tol_x;
max_numFunEvals    = options.MaxFunEvals;
preconditionerPrepare =0;
cgiterations = 0;
numstepsaccepted = 0;
posdef = 1;
exflag = 0;

for iter = 1 : options.maxIter
    % checks value and gradient:
    if ~isfinite(f) || any(~isfinite(G(:)))
        error('Function value and gradient should be finite.');
    end
    
    % Check convergence:
    G_norm = norm( G(:) );
    if posdef && (G_norm < abs_tol_G )
        % norm of gradient satisfies convergence tolerance.
        exflag = 1;
        break;
    elseif iter>1
        if ( pcg_exflag <=1 ) && ... %(x_step_norm < pcg_opts.maxR_frac_continue * pcg_opts.maxR) && ...
           (quadratic_approx_ratio > .25) && ...
           (-delta_f < abs_tol_G * (1+abs( f )) )
            exflag = 3;
            break;
        elseif (x_step_norm < abs_tol_step_x ) && ( pcg_exflag <=1 ) % if pcg_exflag>1 then trust region boundary is the cause of the small step.
            exflag = 2;
            break;
        elseif (numFunEvals > max_numFunEvals)
            exflag = 0;
            break;
        end;
       
    end
    
    % Compute Newton step, limitted to within trust region:
    if H_updated 
        precon_info = fun_makeprecon( x, f, G,  H_info , precon_info);
        H_updated = false; % preconditioner and hessian are now consistent
        preconditionerPrepare = preconditionerPrepare +1;
    end;
    [x_step, delta_f_predicted, pcg_exflag, k, G_end] = pcg_dogleg( fun_hessmul , H_info, fun_mulprecon, precon_info, G(:), pcg_opts);
    cgiterations = cgiterations + k;
    posdef = (pcg_exflag ~= 4);
    
    x_step_norm = norm(x_step);
    switch options.lineSearchMethod 
        case 0  % no line searching; assume we 'll update trust region if the step is not correct. 
            x_new = x + reshape(x_step, size_x);

            [f_new,G_new ,H_info_new ] = fun( x_new );numFunEvals = numFunEvals + 1;

        case 2  % perform secant line search. If quadratic approximation was OK, this completes without any real computation work:
            [alpha, x_new, f_new, G_new, H_info_new, LSnumFunEvals ] = secantLineSearch( fun, x, f, G, x_step, options );
            x_step_norm = x_step_norm * alpha; 
            numFunEvals  = numFunEvals + LSnumFunEvals;
            if alpha~=1 % pcg_dogleg prediction is for alpha==1
                % For the trust region update: pretend that the found step was proposed by pcg_dogleg. (Is that a good strategy?)
                %
                % TODO: most of the (expensive as is vector-vector) computations in this step are more or less done in pcg_dogleg and/or secantLineSearch; 
                %
                % quadratic approximation: fq( alpha ) = f + g0 * alpha + .5*h0 * alpha^2;
                % fq( 0 ) = fval;
                % D[ fq, alpha ] @alpha==0  = g0
                % D[ fq, alpha ] @alpha==1  = g1 == g0 + h0
                % 
                % quadratic approximation: fq( x_step ) = f +  x_step' * G+ .5*x_step'*H * x_step;
                % D[ fq, x_step ] @ x_step = 0 = G;
                % D[ fq, x_step ] @ x_step     = G + H* x_step == G_end;
                % D[ fq, x_step ] @ alpha*x_step = G + H* x_step * alpha;
                g0 = dot( G(:), x_step(:));
                g1 = dot( G_end(:), x_step(:));
                h0 = g1-g0 ; % hessian in step direction. 
                delta_f_predicted = g0* alpha + .5 * h0 * alpha^2;
                G_end = (G_end- G(:))*alpha + G(:);
                x_step = x_step * alpha;
            end;
        otherwise 
            error('invalid lineSearchMethod ');
    end;
    delta_f = f_new - f;
    
    if 0 % debug
        %% plot function and quadratic approximation in step direction:
        ss = linspace(0,1,100);ft=ss;fp=ft;g = x_step(:)'*G(:);H=x_step(:)'*fun_hessmul(H_info,x_step);for k=1:numel(ss);ft(k) = fun(x + ss(k)*reshape(x_step, size_x));fp(k) = ft(1)+ss(k)*g+.5*ss(k).^2*H;end;plot(ss,[ft' fp'] ); legend('function','quadratic prediction')
    end;
    if ~isfinite(f_new)
        if isempty( pcg_opts.maxR) % trust region radius not specified. 
            pcg_opts.maxR = inf; 
        end;
        pcg_opts.maxR = min( x_step_norm, pcg_opts.maxR) * options.TR_scale_invalid_f;
        quadratic_approx_ratio = 0;
    elseif options.dynamicGradientTrustRegion
        
        
        %% estimate trust region step size (relative to current step size)
        
        % for which gradient error is about 50% of G_new if we accept the step, or G if we dont. 
        quadratic_approx_ratio = delta_f/delta_f_predicted;
        Gdiffnorm = norm(G_end-G_new(:));
        if f_new < f
            nrmrat = .5*norm( G_new(:) )/ (Gdiffnorm);
        else
            nrmrat = .5*G_norm / Gdiffnorm;
        end;
        if nrmrat > 1
            adev = log( nrmrat ) + 1 ./ nrmrat.^.6;  % approximate solution for >1
        else
            adev = nrmrat + .5671*(sqrt(nrmrat)-nrmrat)-.2912*(nrmrat.^2-nrmrat); % approximate solution for <1, a bit too high for nrmrat very close to zero
        end;
        newTRR = x_step_norm * adev;   
        
        % approximate 
        
        
        fprintf('%3d : f=%7g, |G|= %7g, |step|= %7g, |R|=%7g, k=%d, exflag=%d\n', iter, f, G_norm, x_step_norm, newTRR, k, pcg_exflag ) % debug display. 
        
        pcg_opts.maxR = newTRR;   
    else
        % 'OLD'/standard trust region update:
        quadratic_approx_ratio = delta_f/delta_f_predicted;
        if isempty(pcg_opts.maxR) % first iteration when trust region radius not specified
            if quadratic_approx_ratio<.75
                % quadratic approximation not good
                
                % fit f + alpha * g + alhpa^2 * h + |g|*(exp( b* alpha) -1 -b*a -.5*b^2*a^2)
                % for fun( x + alpha * x_step )
                % on function value, gradient and hessian of fun( x ) 
                % and function value of fun( x + 1*x_step )
                g_f = G(:)'*x_step(:);
                h_f = -g_f;

                % solve b from f_new-(f+g_f+h_f/2) = |g|*(exp(b)-1-b- .5* b^2 )
                lhs = (f_new - (f + g_f + .5*h_f) ) /(-g_f);
                
                if 0 
%%
                    b_0 = log( lhs+3.5 );
                    lhs_1 = lhs + b_0 + .5* b_0^2;
                    b_1 = log( lhs_1+1 );
                    alpha_t = linspace(0,1,1000)';
                    f_t = zeros(size(alpha_t));
                    for k=1:numel(alpha_t); 
                        f_t(k) = fun( x+ alpha_t(k)*reshape(x_step, size_x));
                    end;
                    pred_q = f+alpha_t*g_f+.5*h_f*alpha_t.^2;
                    pred_e = pred_q + (-g_f)*(exp(alpha_t*b_1)-1-b_1*alpha_t-.5*b_1.^2.*alpha_t.^2);
                    plot(alpha_t,[f_t pred_q pred_e])
                end;
                % find trust region radius for which ratio is approx .9
                % (based on approximation function)
                % => find point on which (exp( b* alpha) -1 -b*a -.5*b^2*a^2)
                %    = -.1 * delta_f_predicted
                
                % approximated alpha. Approximation formula obtained with
                % Mathematica (approx_find_initial_trust_region_radius.nb)
                h = log(abs(lhs));
                alpha = .79/(h+1.6*exp(-.42*h));
                pcg_opts.maxR = x_step_norm * alpha;
            elseif quadratic_approx_ratio < 2
                % initial step was good or a bit better than expected
                if quadratic_approx_ratio<1.1 && quadratic_approx_ratio>.9
                    % very good approximation 
                    pcg_opts.maxR = 4 * x_step_norm;
                else
                    pcg_opts.maxR = 1.5 * x_step_norm;
                end;
            else % improvement much larger than expected: quadratic approximation not good
                % set trust region smaller than current step:
                pcg_opts.maxR = .7 * x_step_norm;
            end;
        elseif (quadratic_approx_ratio >= .75) && (x_step_norm>=.7*pcg_opts.maxR)
            % Good step: increase trust region radius
            pcg_opts.maxR = pcg_opts.maxR * options.TR_scale_good_step;
            if abs( quadratic_approx_ratio-1) < .001 && (pcg_exflag==2 || pcg_exflag==3)
                % Problem is (almost) exactly quadratic because:
                %  the function value is as predicted (quadratic_approx_ratio is 1 +- .001)
                %  and PCG stopped due to the trust region boundary. 
                Gdiffnorm = norm(G_end-G_new(:))/G_norm;
                if Gdiffnorm <.01
                    % and gradient is as predicted by PCG. 
                    
                    % Hence increase trust region size to allow to step to 1.5 
                    %  times the minimum in this search direction. 
                    
                    % quadratic function:
                    % f( alpha * x_step ) = f + g * alpha + .5 * h * alpha^2
                    % f( 0 ) = f
                    % f( x_step ) = f_new == f + g  + .5 * h  
                    % [ D( f )/D(alpha) | alpha == 0]  = g + h * 0 == G(:)'*x_step  
                    % => h = 2*(f_new - f - g)
                    % a = arg min_alpha f( alpha * x_step ) =
                    %   => g + h* alpha ==0 = > alpha = - g/h
                    g = real( dot( G(:),x_step ) );
                    
                    a = -g / (2* (f_new - f - g));
                    a_tomin = 1.5* a - 1; % we want to be able to step from x to 1.5 * the distance to the minimum in direction x_step. However we already took a step of 1. 
                    
                    % However, assume deviation of gradient is growing exponentially:
                    % D[ fun( x ) , x ] @ x=alpha*x_step 
                    %       = D[ f( x ) , x] + (exp( alpha )-1-(exp(1)-2)*alpha) * (G_new(:)-G_end(:))
                    % @ alpha == 0 and 1 this exactly matches to the observed. 
                    % Now constrain step such that gradient difference is equal to .5 of the norm of G_new. 
                    % => norm( G_new(:)-G_end(:) ) * (exp( alpha )-1-(exp(1)-2)*alpha) <= .5*norm( G_new(:) )
                    % => (exp( alpha )-1-(exp(1)-2)*alpha) <=.5*norm( G_new(:) )/ norm( G_new(:)-G_end(:) ) 
                    % => alpha = ?  (very difficult expression. Try to approximate; right hand side of above equation is x : )
                    % x>=1 : invfun = @(x) log(x)+1./x.^.6; alph=linspace(1,20,100); plot(alph,invfun( exp(alph)-1-(exp(1)-2)*alph ) - alph) % shows that for alph>1 error is <.03 which is good enough. 
                    % x<=1 : invfun = @(x) x + .5671*(sqrt(x)-x)-.2912*(x.^2-x); alph=linspace(0,1,100); plot(alph,invfun( exp(alph)-1-(exp(1)-2)*alph ) - alph) % shows that for alph>1 error is <.03 which is good enough. 
                    %        X=bsxfun(@minus, [(alph').^.50 alph'.^2],alph');tht = X\(q-alph)'; plot(alph, [(q-alph)'-X*tht X]),ylim([-.01 .01]),tht   
                    nrmrat = .5*norm( G_new(:) )/ (Gdiffnorm* G_norm);
                    adev = log( nrmrat)+1./nrmrat.^.6;
                    pcg_opts.maxR = pcg_opts.maxR * max(1, min( a_tomin , adev)/options.TR_scale_good_step );
                end;
            end;
        elseif (quadratic_approx_ratio < -1)
            pcg_opts.maxR = min( pcg_opts.maxR , x_step_norm) * options.TR_scale_verybad_step;
        elseif (quadratic_approx_ratio < .25)
            pcg_opts.maxR = pcg_opts.maxR * options.TR_scale_bad_step;
        end;
    end;
    if f_new < f
        % accept step 
        x = x_new;
        f = f_new;
        G = G_new;
        H_info = H_info_new;
        H_updated =true;
        clear f_new G_new H_info_new;
        numstepsaccepted = numstepsaccepted +1;
    end;
end;
    

if nargout>=6
    outp.iterations   = iter;
    outp.funcCount    = numFunEvals;
    outp.cgiterations = cgiterations;
    outp.stepstaken   = numstepsaccepted;
    outp.preconditionerPrepare = preconditionerPrepare;
end;
end

function [alpha, x, f, G, Hinfo, numFunEvals] = secantLineSearch( fun, x, f, G, x_step, options )
    % 5-10-2018: copied from conjgrad_nonlin and subsequently modified to fit the use here. 

    % secant line search on the gradient:
    %   f( alpha ) = fun( x + alpha * x_step ),  f' is derivative of f w.r.t. alpha
    %   linear interpolation on interval x_L, x_R (left, right); try to null y
    %      y(x) = (f'( x_L ) - f'( x_R ))/( x_L - x_R ) * (x - x_L) + f'( x_L )
    %       NOTE: ( y( x_L ) = f'(x_L), y( x_R ) = f'( x_R ) 
    %   => solve y==0 for x  (which is the new x):
    %      0 = (f'( x_L ) - f'( x_R ))/( x_L - x_R ) * (x - x_L) + f'( x_L )
    %      => -x * (f'( x_L ) - f'( x_R ))/( x_L - x_R ) = -x_L * (f'( x_L ) - f'( x_R ))/( x_L - x_R )  + f'( x_L )
    %      => x = x_L - f'(x_L)*( x_L - x_R )/(f'( x_L ) - f'( x_R ))
    %           = ( x_L* (f'( x_L ) - f'( x_R )) - f'(x_L)*( x_L - x_R )) / (f'( x_L ) - f'( x_R ))
    %           = ( x_L* f'( x_L ) - x_L *f'( x_R ) - x_L * f'(x_L) +  x_R f'(x_L) ) / (f'( x_L ) - f'( x_R ))
    %           = ( x_R f'(x_L) - x_L *f'( x_R )  ) / (f'( x_L ) - f'( x_R ))

    % additional constraints/considerations:
    % - we want to get and keep y==0 bracketed, which means that the lowest of 
    %   x_m1 and x_m2 should have f' <0 and the highest of x_m1 and x_m2 should have f'>0.
    %   as long as we did not bracket the minimum, we extend the interval by at least a factor 2. 
    % - we want to get the lowest f, so if f indicates that the function increased between
    %   x_m1 and x_m2, then we take that as evidence that f' is positive at the end (and similar for decreasing => negative at start of interval)
    %   since interpolation will not work in that case, we use bisection. 
    % - we really want to as few function evaluations as possible. Hence each 
    %   additional function evaluation should be expected to improve fval by at least
    %   absFunTol (i.e. expected_f_new < f_current_best - abs_tol_fval) 
    %   and at least lineSearch_minFvalGain (i.e. max(0,f-f_current_best)*lineSearch_minFvalGain < f - expected_f_new )
    % - The provided x_step is to the minimum of the quadratic approximation in the x_step direction or at the 
    %   border of the current trust region. Hence we should first evaluate that point. If it brackets the zero gradient
    %   thats convenient and we see if we need to do an update. If not we should consider if we extend the step. As we 
    %   have done an extra evaluation of fun, its OK to step beyond the original trust region. 

    gd = dot( G(:), x_step );
    pL = struct('alpha',0 ,'f',f ,'df',gd,'x',x, 'G', G,'Hinfo',[]);
    x_step = reshape( x_step, size(x));
    numFunEvals = 0; 
    function p = get_pnt( alpha )
        p = pL;
        p.alpha = alpha;
        p.x = x + x_step * alpha;
        [p.f, p.G, p.Hinfo] = fun(  p.x );numFunEvals = numFunEvals + 1; 
        p.df = dot( p.G(:), x_step(:) ) ;
        
    end
    pR = pL;
    pBest = pL;
    alpha_new = 1;  
    isbracketed = false; % as long as isbracketed==false : alpha_new > pR.alpha
                         % if isbracketed == true : pL.alpha < alpha_new < pR.alpha
    for j = 1 : options.lineSearch_maxIter
        % make a next step:
        pN = get_pnt( alpha_new );
        
        acceptpnt = pN.f < pBest.f;
        if acceptpnt
            pBest = pN;
        end;
        % save memory:
        pN.G = []; pN.Hinfo = [];pN.x = [];
        if acceptpnt && abs(pN.df) <  options.lineSearch_gradientToleranceRelative * abs(gd)
            break; % new gradient zero to within tolerance; so we converged. 
        end;
        % Remark: 
        % j==1 : We only get here if the quadratic approximation that is 
        %        used to compute x_step is not correctly predicting the minimum. 
        % j>1  : We only get here if the newly proposed alpha is not close to the
        %        minimum in the line search direction. 
        if ~isbracketed
            % Check if with the test point the minimum is bracketed:
            % if function value increased => there is a positive gradient (and hence a zero gradient) somewhere in the interval. 
            if pN.f >= pR.f || pN.df > 0
                isbracketed = true;
                [pN, pR]=deal(pR, pN); % make order correct. 
            end
        end;
        
        % Now for all cases that we separate in the if structure below:
        %  - create alpha_new and f_new_predicted
        %  - update pL or pR with pN and clear pN. 
        if ~isbracketed
            % here ordered on alpha: pL, pR, pN
            % select R and N as new L and R:
            pL = pR; pR = pN; clear pN;
            % in decreasing order of importance:
            % new alpha should be at least twice the last evaluated alpha
            % new alpha should not extrapolate by more than a factor 4
            % new alpha is secant solution of last two evaluations (L and R)
            alpha_new = max( 2*pR.alpha, min( 4*(pR.alpha-pL.alpha), (pR.alpha * pL.df - pL.alpha * pR.df)/(pL.df - pR.df)));
            % for b  == alpha_new - pR.alpha
            % fR + Integrate[ (dfL - dfR )/(xL - xR)*(x - xL) + dfL, {x, xR, xR + b}]
            
%             f_new_predicted = pR.f + (alpha_new- pR.alpha)*(alpha_new *(pL.df - pR.df) + 2* pR.df * pL.alpha - (pL.df+pR.df)*pR.alpha )/(2*(pL.alpha - pR.alpha));
            b = alpha_new - pR.alpha;
            f_new_predicted = pR.f + b * pR.df + b^2 *(pR.df-pL.df)/(2*(pR.alpha-pL.alpha));
        else % isbracketed
            % minimum already bracketed; pL.alpha <= pN.alpha <= pR.alpha 
            inFirstInterval = pL.df<0 && (pN.df>0 || pN.f>pL.f); % can we actually include pnt(idnew).f>pnt(id1).f here? It implies that the function had positive gradient at least somewhere in that interval. Note that equal f also happens in the initial bracketing phase where second interval should be selected (as alpha_new = 0)
            inSecondInterval = pN.df<0 && pR.df>0  ; 
            if inFirstInterval || inSecondInterval
                if inFirstInterval 
                    % zero gradient is within first part, replace pR

                    % make sure that we average at least an interval length reduction factor of 0.7 .
                    low_lim_alpha = pL.alpha + .3*(pN.alpha-pL.alpha).^2/(pR.alpha-pL.alpha); %allow substantial reduction if previous step succesfully reduced interval size ( pN-pL is small compared to pR-pL), if that was not the case ( pN-pL is similar to pR-pL) make sure it reduces to most a factor .7 in the next step. 
                    upp_lim_alpha = pL.alpha + min( .7*(pR.alpha-pL.alpha), .95*(pN.alpha-pL.alpha));% make sure the interval is at least reduced by a factor .7 over two iterations and there always is some minimal reduction.
                    
                    % Update pR
                    pR = pN; clear pN;
                else % inSecondInterval
                    % zero in second part, replace pL:

                    % make sure that we average at least an interval length reduction factor of 0.7 .
                    upp_lim_alpha = pR.alpha - .3*(pN.alpha-pR.alpha).^2/(pR.alpha-pL.alpha); %allow substantial reduction if previous step succesfully reduced interval size, if that was not the case make sure it reduces to most a factor .7 in the next step. 
                    low_lim_alpha = pR.alpha - min(.7*(pR.alpha-pL.alpha), .95*(pR.alpha-pN.alpha));% make sure the interval is at least reduced by a factor .7 over two iterations and there always is some minimal reduction.
                    
                    % update pL:
                    pL = pN; clear pN;
                end;
                % solve third order polynomial [1  x  x.^2  x.^3]*[d c b a]' from f and df at current interval. 
                X_alpha_fit_3rd =[  1 pL.alpha   pL.alpha.^2   pL.alpha.^3;
                                    0       1     2*pL.alpha   3*pL.alpha.^2;
                                    1 pR.alpha   pR.alpha.^2   pR.alpha.^3;
                                    0       1     2*pR.alpha   3*pR.alpha.^2];
                alpha_fit_3rd = X_alpha_fit_3rd\[pL.f;pL.df;pR.f;pR.df];
                if 0 
                    %% Show fit:
                    alpha_plot = linspace(pL.alpha, pR.alpha,100)';
                    lh_alphaplot = plot(alpha_plot, [ones(numel(alpha_plot),1) alpha_plot alpha_plot.^2 alpha_plot.^3]*alpha_fit_3rd);
                    xlabel('alpha');ylabel('function value');
                    %% show actual values of nonlinear part only. TODO: add quadratic part: (requires evaluation of f, so may take long and require substantial memory)
                    alpha_plot_fval = linspace( pL.alpha, pR.alpha,10)';
                    fval_alpha_plot = zeros(size(alpha_plot_fval));
                    fval_alpha_plot([1 end]) = [pL.f pR.f];
                    for dummy_it = 2 : numel(fval_alpha_plot)-1
                        fval_alpha_plot( dummy_it ) = fun( x + x_step * alpha_plot_fval(dummy_it )  );
                    end;
                    hold on
                    fv_alphaplot = plot( alpha_plot_fval, fval_alpha_plot );
                    set(fv_alphaplot,'color',get(lh_alphaplot,'color'),'LineStyle','none','Marker','*')
                end;
                % take derivative wrt x and solve for zero of this derivative
                dalpha_fit_3rd = alpha_fit_3rd(2:4).*[1 2 3]'; % dt = [ c b a]  in   a x^2 + b x + c = 0
                root_alpha_fit = dalpha_fit_3rd(2)^2-4*dalpha_fit_3rd(1)*dalpha_fit_3rd(3); % b^2 - 4 a c 
                alpha_newcandidates = (-dalpha_fit_3rd(2) + [-1 1]*sqrt( root_alpha_fit ) ) / (2 * dalpha_fit_3rd(3) ); 
                delanew = imag(alpha_newcandidates)~=0 | alpha_newcandidates<pL.alpha | alpha_newcandidates>pR.alpha; % delete roots that are out of bound.
                alpha_newcandidates(delanew) = [];
                if numel(alpha_newcandidates)>1
                    alpha_newcandidates = min(alpha_newcandidates); % If both roots are in the interval, we want the smallest alpha as that is the minimum (derivative at start of interval is negative). 
                elseif isempty(alpha_newcandidates)
                    % find alpha where df == 0, assuming linear change of df between pL and pR (one of these is the last computed pN). 
                    alpha_newcandidates = (pR.alpha * pL.df - pL.alpha * pR.df)/(pL.df - pR.df) ;
                end;
                % store anew within the bounds:
                alpha_new = max( low_lim_alpha, min(upp_lim_alpha,  alpha_newcandidates )); 
                % predict new f with this alpha:
                f_new_predicted = [ 1 alpha_new   alpha_new.^2   alpha_new.^3 ] * alpha_fit_3rd;
            else
                % non usual bracketing of minimum (gradients of end points have wrong sign); So base decision primary on function values:
                if pL.df>0
                    error('should not happen');
                end;
                if pL.f < pN.f
                    % minimum in first interval, replace the right point
                    pR = pN; clear pN;
                elseif pN.f < pR.f && pN.df<0
                    % minimum in second interval, replace first point:
                    pL = pN; clear pN;
                else
                    % strange case; not entirely sure if this can happen absolutely never. 
                    % What is true if this case is selected:
                    %    isbracketed == true
                    %    pL.df>=0 || (pN.df<=0 && pN.f<=pL.f) )
                    %    pN.df>=0 || pR.df<=0  
                    %    pL.df<=0
                    %    pN.f >= pL.f
                    %    pN.f >= pR.f || pN.df>=0
                    error('what is happening? unforseen case that probably should not happen');
                end;  
                l = pR.alpha-pL.alpha;
                low_lim_alpha = pL.alpha + .3*l;% make sure the interval is at least reduced by a factor .7
                upp_lim_alpha = pL.alpha + .7*l;% make sure the interval is at least reduced by a factor .7
                % parabolic approximation using f of point 1 and 2, and df of point 1
                %  poly = a + b * delta_alpha + c * delta_alpha^2
                poly = [1 0 0;1 l l.^2;0 1 0] \ [pL.f;pR.f;pL.df];
                % solve d(poly, alpha) ==0 => b + 2*c delta_alpha ==0 => 
                delta_alpha = poly(2)/(2*poly(3));
                alpha_new = max( low_lim_alpha, min(upp_lim_alpha,  pL.alpha + delta_alpha )); 
                f_new_predicted = [ 1 alpha_new-pL.alpha   (alpha_new-pL.alpha).^2 ] * poly;
            end;
        end; % end of isbracketed
        if f_new_predicted + options.abs_tol_fval >= pBest.f || max(0,f-pBest.f)*options.lineSearch_minFvalGain > f - f_new_predicted 
            break; % we stop the line search; there is to little expected benefit. 
        end;
    end; % end of for j
    if pBest.alpha == 0 
        warning('Line search did not find an acceptable update. Cannot continue.');
        g_n = 0; 
        return;
    end;
    if 0 
        %% display line-search problem (also helps to validate the functions
        alpha_tst = linspace(0,max(alpha,pntbest.alpha*1.2),100)';
        ftst = zeros(numel(alpha_tst),2);
        for i_tst = 1 :numel(alpha_tst)
            x_new =  x + alpha_tst(i_tst ) * reshape(d, szx);
            ftst(i_tst) = fun( x_new );
        end;
        f_pred_q = f_q + gd_q * alpha_tst + .5*alpha_tst.^2 * dHd_q;
        f_pred_n = f_n_start+ (gd-gd_q) * alpha_tst + .5*alpha_tst.^2 * dHd_n;
        f_new_tst = f_q + gd_q * pntbest.alpha+ .5*pntbest.alpha^2 * dHd_q + f_n ;
        plot(alpha_tst, [sum(ftst,2) f_pred_q+ftst(:,2) f_pred_q+f_pred_n],'-',pntbest.alpha, f_new_tst,'*');
        legend('observed','pred quadratic + observed nonlinear','prediction at start','chosen point')
    end;
alpha = pBest.alpha;
x = pBest.x;
f = pBest.f;
G = pBest.G;
Hinfo = pBest.Hinfo;
end