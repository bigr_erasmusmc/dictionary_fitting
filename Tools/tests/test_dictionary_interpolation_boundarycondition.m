% test_dictionary_interpolation_boundarycondition
%
% Test script that creates a 'dictionary' of a 1D function 'fun'. 
% An function interpolating this dictionary is created by make_spline_predict_function
% The interpolation is performed with the specified bsplineorder and boundaryCondition
% (See help prefilter_spline for details on that)
%
% As a result a figure with 4 subfigures is shown.
% It displays the original function as well as the dictionary interpolating approximation.
% Also the derivative w.r.t. the function argument as well as the dictionary interpolation approximation theroff. 
% For both cases also the difference (==interpolation error) is explicitly shown. 
% Note that the vertical range of this difference is automatically adjusted to the 
% expected range of the errors (of the default function)
% 
%
% 16-11-2018, D.Poot, Erasmus MC

%% Definition of the test; feel free to modify:
f = 1; 
fun = @(x) exp(1i*x*f);
dfundx = @(x) f*1i*exp(1i*x*f); % analytical derivative of fun. 

xstart = .05; 
xstep = .2;
xend = 4; 
xgrid = xstart: xstep: xend; % dictionary points
xtest = linspace( -1, 4, 500); % evaluation points

dictionary =  fun( xgrid ) ;
dDictionarydx = dfundx( xgrid );
bsplineorder = 2; 
boundaryCondition = 9;
expectedErrorScale = (.1*xstep)^bsplineorder;

%% Evaluation; (dont modify)
interpolfun = make_spline_predict_function( dictionary, bsplineorder, false, boundaryCondition , true);

genidx = @(x) [ones(1,numel(x));(x-xstart)/xstep+1];

Fx = permute( fun( xtest ) , [2 1]);
dFdx = permute( dfundx( xtest ) , [2 1]);
[Ix_ , dIx_ ] = interpolfun( genidx( xtest) ); 
Ix = permute( Ix_, [2 1]);
dIdx = permute( dIx_(:,:,2),[2 1])/xstep;


% Plot function:
subplot(2,2,1);
lh = plot( xtest, [real( [Fx Ix] ) imag( [Fx Ix] )], '-', xgrid, [real(dictionary)' imag(dictionary)'],'*' );
set( lh(5:6),'color',get(lh(3),'color'));
set( lh(3),'color',get(lh(1),'color'),'lineStyle',':');
set( lh(4),'color',get(lh(2),'color'),'lineStyle',':');
title('Real and imag of function and interpolant')
% plor error:
subplot(2,2,2);
lh2 = plot( xtest, [real( [Fx-Ix] ) imag( [Fx-Ix] )], '-', xgrid, real(dictionary*0),'*' );
set(lh2(2),'color',get(lh2(1),'color'),'lineStyle',':');
ylim([-1 1]*expectedErrorScale)
xlim([-.1 xend+.1]);
title('Real and imag of error in interpolant')

% Plot dfunctiondx:
subplot(2,2,3);
lh = plot( xtest, [real( [dFdx dIdx] ) imag( [dFdx dIdx] )], '-', xgrid, [real(dDictionarydx)' imag(dDictionarydx)'],'*' );
set( lh(5:6),'color',get(lh(3),'color'));
set( lh(3),'color',get(lh(1),'color'),'lineStyle',':');
set( lh(4),'color',get(lh(2),'color'),'lineStyle',':');
title('Real and imag of derivative function and interpolant')
% plor error in dfunction:
subplot(2,2,4);
lh2 = plot( xtest, [real( [dFdx-dIdx] ) imag( [dFdx-dIdx] )], '-', xgrid, real(dictionary*0),'*' );
set(lh2(2),'color',get(lh2(1),'color'),'lineStyle',':');
ylim([-.004 .004]*.5)
xlim([-.1 xend+.1]);
title('Real and imag of error in derivative interpolant')


if 0
    %% Test second derivative of third order bspline:
    bsplineorder = 3; 
    xval = floor(-bsplineorder/2) : ceil(bsplineorder/2);
    dstep = .00001; 
    [bsplinep , dbsplinedxp] = bsplinefunction( bsplineorder, xval +dstep )
    [bsplinem , dbsplinedxm] = bsplinefunction( bsplineorder, xval -dstep )
    d2dbsplinedx = (dbsplinedxp-dbsplinedxm)/(2*dstep)
end;