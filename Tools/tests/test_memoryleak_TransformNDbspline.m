Nvox    = 10;
bsplineord = 2
boundary_condition = 1
sz_dict = [16 7 13 6 8 35];

in      = randn( sz_dict );
Tin     = cellfun( @(n)randi(n, 1, Nvox)-1, num2cell( sz_dict(2:end) ), 'UniformOutput', false );
T       = permute( cat( 1, Tin{:} ), [1 3:ndims(in) + 1 2]); % adjust number of dimensions to match dictionary
newSz   = [nan ones(1,ndims(in)-2) Nvox];
[S, dSdT] = TransformNDbspline( in, T, [], newSz, [], bsplineord, [], [], boundary_condition);
memory
tic
for k = 1:1e6
[S, dSdT] = TransformNDbspline( in, T, [], newSz, [], bsplineord, [], [], boundary_condition);
end
toc
memory
%task manager memory:  (14.9-10.6)*1e9*1e6