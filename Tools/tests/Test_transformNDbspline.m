%% Test_transformNDbspline
imin = zeros(30,50);imin(3,3)=1;imin(6,6)=2;imin( 4,7)=3;
imin(2:10,10:30)=1;
figure(1);imagebrowse(imin)
% % affine transform:
boundarycondition = 0; 
szout = [10 1000];
Taff = [1 0 0;0 .05 0];
bspineord =3;
out1 = TransformNDbspline(imin, Taff,[], szout,[], bspineord, [],[], boundarycondition);
% % transform points
[xi,yi] = ndgrid(0:size(imin,1)-1,0:size(imin,2)-1);
[xo,yo] = ndgrid(0:szout(1)-1,0:szout(2)-1);
Tmap = reshape(Taff * [xo(:) yo(:) ones(numel(xo),1)]', [2 size(xo)]);
out2 = TransformNDbspline(imin, Tmap,[], szout,[],bspineord, [],[], boundarycondition);

% % mapping transformation field:
[xs,ys] = ndgrid(-4:6.456:szout(1)+7,-5.5:18.34:szout(2)+20);
mapT = [-xs(1,1)/(xs(2,1)-xs(1,1)) -ys(1,1)/(ys(1,2)-ys(1,1));1/(xs(2,1)-xs(1,1)) 1./(ys(1,2)-ys(1,1))];
Tmaps = reshape(Taff * [xs(:) ys(:) ones(numel(xs),1)]', [2 size(xs)]);
out3 = TransformNDbspline(imin, Tmaps,mapT, szout,[],[bspineord 1], [],[], boundarycondition);

% % check ar prefiltering:
ar = [1 -(sqrt(3)-2)]; arbspineorder = 3;
out4 = TransformNDbspline(imin, Taff,[], szout,[],arbspineorder, ar,[], boundarycondition);


% % Reference methods:
outs = interpn(xi,yi,imin, permute(Tmap(1,:,:),[2 3 1]), permute(Tmap(2,:,:),[2 3 1]),'spline');
outl = interpn(xi,yi,imin, permute(Tmap(1,:,:),[2 3 1]), permute(Tmap(2,:,:),[2 3 1]),'linear');
% %  Inspect results:
msg = [sprintf( 'Inspect if images are correct. All methods should apply the same deformation. Different methods concatenated in Dim 3:\n') ...
       sprintf( '1 : Affine transform, bspineorder = %d, no prefiltering \n', bspineord)   ...
       sprintf( '2 : Transform points, bspineorder = %d, no prefiltering \n', bspineord)   ...
       sprintf( '3 : Transform field with linear interpolation between gridpoints, image interpolation bspineorder = %d, no prefiltering \n', bspineord)   ...
       sprintf( '4 : Affine transform, bspineorder = %d, prefiltering \n', arbspineorder)   ...
       sprintf( '5 : interpn, spine interpolation \n')   ...
       sprintf( '6 : interpn, linear interpolation \n')   ...
       ];
figure(2);
imagebrowse(cat(3,out1,out2,out3,out4,outs,outl),[],'description',msg)
% 
% 
%%
% filtbnk = randn(5,8);
x = sinc(0:.1:4);
filtbankbsplineorder = 1; 
filtbnk = filterbank([x(end:-1:2) x], 8, 10,[],filtbankbsplineorder);
im3D = randn(100,100,100);
Taff = [.01 0 0 4 ;0 .01 0 4; 0 0 .01 4];
szout = 2*[101 101 101];
disp('Timing on problem:')
disp(['Filterbank length = ' num2str(size(filtbnk,1)) ' => ' ]);
disp(['   #filterbank interpolations/sample : ' num2str( (filtbankbsplineorder +1)* size(filtbnk,1)* numel(size(im3D)) )]);
disp(['   #image samples/sample             : ' num2str( size(filtbnk,1)^ numel(size(im3D)) )]);
disp(['input image size = [' num2str( size( im3D) ) ']']);
tic
out = TransformNDbspline(im3D, Taff,[], szout,filtbnk, filtbankbsplineorder, [],[], boundarycondition);
tu = toc;
clkpersample = tu/numel(out)*2.6e9;
disp(['output image size = [' num2str( size( out) ) '] with ' num2str( nnz(out) ) '/' num2str( prod( size(out))) ' nonzero (=computed) elements']);
disp([' Total time used = ' num2str( tu) ' sec']);
disp([' Clocks per output sample = ' num2str( clkpersample)]);