%% check for different sizes
checktiming  = true;
dotdim = 3;
% if ~checktiming 
%     szc = {[4 3],[12 3],[13 3],[14 3],[15 3],[16 3]};
% else
    szc = {[3 1],  [15 1],  [1 3],  [1 157], [1 1 3], [ 1 1 19], [1 3 2], [1 17 21], [4 3],  [10 3],...
           [11 3], [12 3],  [13 3], [14 3],  [15 3],  [ 3 1 19], [3 3 2], [3 17 21], [16 3],  ...
           [43 23 12], [1500 120]}; %, [150000 120]}; % last one takes a lot of time, only performance measurement
% end;
ABiscomplex = [false false;
               false true;
               true false;
               true true];
nloops = 10;
resultcheck = false(numel(szc),4);
if  checktiming  
    tused1 = zeros(size(resultcheck));
    tused2 = zeros(size(resultcheck));
end;
for szid = 1:numel(szc)
    sz = szc{szid};
    for cid = 1:size(ABiscomplex,1)
        if ABiscomplex(cid,1)
            a = randn(sz) + 1i*randn(sz);
        else
            a = randn(sz);
        end;
        if ABiscomplex(cid,2)
            b = randn(sz) + 1i*randn(sz);
        else
            b = randn(sz);
        end;

        args = {a,b,dotdim};
%%
        if checktiming 
            tic
            for k = 1: nloops
                c1 = dot(args{:});
            end;
            tused1(szid, cid) = toc;
            tic
            for k = 1 : nloops
                c2 = dot_c(args{:});
            end;
            tused2(szid, cid) = toc;
        else
            c1 = dot(args{:});
            c2 = dot_c(args{:});
        end;
%         if any( abs(c1-c2)>(2+sqrt(size(a,1)))*eps(max(abs(c1)))) 
%             error('difference to large');
%         end
        resultcheck(szid, cid) = ~any( reshape( abs(c1-c2)>(2+sqrt(size(a,dotdim)))*eps(max(abs(c1(:)))) ,[],1)) ;
    end;
end;
resultcheck
if checktiming 
    tused2./tused1
end;
    
%% check nan/ inf
cycle = [nan inf -inf 0 randn];
dotdim = 3;

[i1, i2] = ndgrid(1:numel(cycle),1:numel(cycle) );
a = cycle(i1);
b = cycle(i2);

args = {a,b,dotdim};
c1 = dot(args{:});
c2 = dot_c(args{:});
isequalwithequalnans(c1,c2)