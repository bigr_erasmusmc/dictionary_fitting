function [sCore,varargout] =  EM_tensor_kSVD( calmat , k, correctdatamask, maxiter) 
% [s,u1,u2,...] =  EM_tensor_kSVD( mat, k, correctdatamask, maxiter) 
% Simple implementation of the EM svd algorithm for missing data mentioned
% in Kurucz_KDDCup2007_Methods for large scale SVD with missing values.pdf
% 
% Combined with the higher order SVD algorithm 
% https://en.wikipedia.org/wiki/Higher-order_singular_value_decomposition
% reference: http://epubs.siam.org/doi/pdf/10.1137/S0895479896305696, 
% Lathauwer, A MULTILINEAR SINGULAR VALUE DECOMPOSITION, SIAM  21,4,2000, p1253-1278
%
% Iterates SVD and filling in missing data by current SVD truncated to k elements. 
%
% Computes the EM algorithm for the missing data svd.
% INPUTS:
%  mat : N dimensional array of which the higher order SVD should be computed
%  k   : truncation order in the iterations; default .3*min(size(mat))
%        vector: specify order for each dimension. 
%  correctdatamask: index or mask into mat that selects the correct values.
%        By default all nonzero, finite values in mat. 
%  maxiter: (maximum) number of EM iterations
%
% Based on EM_kSVD by Dirk Poot, Erasmus MC
% Created by Dirk Poot, 2-12-2016, Erasmus MC

if nargin<2 || isempty(k)
    k = max(1,round(min(size(calmat))*.3));
end;
if nargin<3 || isempty( correctdatamask )
    correctdatamask = calmat~=0 & isfinite(calmat); 
end;
if nargin<4 || isempty(maxiter)
    maxiter = 30;
end;
%%
if nnz( correctdatamask )==numel(correctdatamask)
    % Fully correct
    cur = calmat;
    maxiter = 1; % converged in 1 iteration. 
    doassigncorrect = false;
else
    % There are unsampled/incorrect positions:
    cur = zeros(size(calmat),class(calmat));
    doassigncorrect = true;
end;
ndim = ndims( cur ) ;
if numel(k)==1
    k = k(ones(1,ndim));
end;
lp = true;  % TODO: add check for convergence. 
iter = 0;
diags = cell(1,maxiter);
u = cell(1,ndim);
while lp && iter < maxiter
    iter = iter + 1;
    % update correct positions:
    if doassigncorrect
        cur(correctdatamask) = calmat(correctdatamask);
    end;
    % Do SVD in each dimension. Iteratively compressing 'cur' on the selected components:
    sel = cell(1,ndim);
    for dim = 1 : ndim
        curp = permute( cur, [dim 1:dim-1 dim+1:ndim]);
        [u{dim},s,v]=svd(curp(:,:),'econ');
        szcurp = size(curp);
        curp = reshape( u{dim}'*curp(:,:) , [size(u{dim},2) szcurp(2:end)] );
        cur = ipermute( curp, [dim 1:dim-1 dim+1:ndim]);
        sel{dim} = 1:k(dim);
    end;
    % Result of compression in all dimensions is core tensor:
    sCore = cur;
    % Unpack core tensor to full data matrix:
    cur = cur(sel{:});
    for dim = 1 : ndim
        curp = permute( cur, [dim 1:dim-1 dim+1:ndim]);
        szcurp = size(curp);
        curp = reshape( u{dim}(:,1:k(dim))*curp(:,:) , [size(u{dim},1) szcurp(2:end)] );
        cur = ipermute( curp, [dim 1:dim-1 dim+1:ndim]);
    end;    
%     diags{iter} = diag(s);
end;
varargout = u;