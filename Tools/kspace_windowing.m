function [ k_out ] = kspace_windowing( k_in, transform_dim, method, varargin )
% applies window to k-space to redice Gibbs ringing
%
% k_in          : input k-space
% transform dim : vector that specifies which dimensions to transform
% method        : species methods:
%                 'Tukey', requires 
%                   varargin{1} = kc, maximum unattenuated spatial frequency
%                   varargin{2} = w, roll-off distance

if ~isempty( transform_dim )
    t_dim = transform_dim ;
else
    t_dim = [1 2];
end

switch method
    case 'Tukey'
        kc = varargin{1};
        w  = varargin{2};
        k_out = k_in;
        for d = 1:numel( t_dim )
            N       = size( k_in, t_dim( d ) );
            k_pos   = (1-N)/2:1:(N-1)/2;
            
            window  = zeros( size( k_pos ) );
            
            idx     = abs( k_pos ) < kc(d) + w(d);
            window( idx ) = cos( pi * ( abs( k_pos( idx ) ) - kc(d) ) / ( 2*w(d) ) ).^2;
            
            idx     = abs( k_pos ) < kc(d);
            window( idx ) = 1;      
            
            shift = circshift( 1:ndims( k_in ), [0, t_dim( d ) - 2] );
            
            k_out = bsxfun( @times, k_out, permute( window, shift ) );
            
        end
    otherwise
        error('Unknown method');
end



end

