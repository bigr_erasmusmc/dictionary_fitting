function [prior] = parse_parameterPrior( prior ,opts )
% [prior_out] = parse_spatialRegularizer( prior_in )
% Helper function that parses parameterPrior argument of fit_MRI
% and creates a single parameter prior function.
%
% INPUTS:
%  prior_in : parameterPrior option of fit_MRI.  
%  opts : fit_MRI option structure; used for reading some options not related to the prior. 
% OUTPUTS:
%  prior_out : single prior function
%
% Created by Dirk Poot, Erasmus MC
% created: 27-9-2018, moved from fit_MRI and updated to allow multiple
%                     prior terms (which are summed up)
    
for el = 1: numel( prior )
    if strcmpi( prior(el).fun,'normal')
        neginvSigma = -inv( prior(el).sigma );
        parameterPrior_mu = prior(el).mu;
        [prior(el).hessian_I, prior(el).hessian_J] = find( triu( neginvSigma) ); 
        hessIJlin = prior(el).hessian_I + (prior(el).hessian_J-1) * size(neginvSigma,1);
        prior(el).fun = make1arg_anonfun(@lognormalVec, parameterPrior_mu, neginvSigma, hessIJlin);
    elseif strcmpi( prior(el).fun,'laplace') || strcmpi( prior(el).fun,'doubleExp')
        invSigma = prior(el).sigma;
        parameterPrior_mu = prior(el).mu;
        if ~iscell( invSigma )
            invSigma = {invSigma};
        end;
        if ~iscell(parameterPrior_mu)
            parameterPrior_mu = {parameterPrior_mu};
        end;
        if numel(parameterPrior_mu) ~=numel( invSigma)
            error('each covariance matrix should be accompanied by a mu for doubleExp prior');
        end;
        for k = 1 : numel( invSigma )
            invSigma{k} = inv( invSigma {k});
        end;
%         hessIJlin = opts.funHessian_I + (opts.funHessian_J-1) * size(neginvSigma,1);
        prior(el).fun = make1arg_anonfun(@logdoubleExpPrior, parameterPrior_mu, invSigma);
        [dum1,dum2,dum3, prior(el).hessian_I, prior(el).hessian_J] = prior(el).fun( rand(size(invSigma{1},1),1) );
    else
        error('unknown/unsupported prior distribution for the parameter vectors');
    end;
    
    if ~isempty( prior(el).fun )
        if isempty( prior(el).gradient_linindex)
            prior(el).gradient_linindex = (1:opts.numParam)'; 
        else
            prior(el).gradient_linindex = prior(el).gradient_linindex(:);
        end;
        if ~isequal( size( prior(el).hessian_I ), size( prior(el).hessian_J )) || any( prior(el).hessian_I(:)<0) || any( prior(el).hessian_I(:) > opts.numParam) ||  any( prior(el).hessian_J(:)<0) || any( prior(el).hessian_J(:)>opts.numParam )
            error('parameterPrior.hessian_I and parameterPrior.hessian_J should have equal size and be integer vectors between 1 and numParam');
        end;
        if isempty( prior(el).hessian_I)
            prior(el).hessian_I = opts.funHessian_I;
            prior(el).hessian_J = opts.funHessian_J;
        end;
        [test , prior(el).hessian_write_funhessindex] = ismember( ...
                sub2ind( opts.numParam*[1 1], prior(el).hessian_I, prior(el).hessian_J) , ...
                sub2ind( opts.numParam*[1 1], opts.funHessian_I  , opts.funHessian_J )) ;    
        if any(~test)
            error('All hessian elements computed by the parameterPrior function should be contained in the function hessian.');
        end;
    end;
end;

if numel( prior ) > 1
    prior = makeaddPrior( prior );
end;

    

function [priorout] = makeaddPrior( priors )
priorout = priors(1);

[priorout.gradient_linindex , dummy, gradorigidx] = unique( vertcat( priors.gradient_linindex ) );
[priorout.hessian_write_funhessindex , dummy, hessorigidx] = unique( vertcat( priors.hessian_write_funhessindex ) );
hessIJ = unique([ vertcat( priors.hessian_I ) vertcat( priors.hessian_J )],'rows');
priorout.hessian_I = hessIJ(:,1);
priorout.hessian_J = hessIJ(:,2);
ngrad = numel( priorout.gradient_linindex );
nhess = numel( priorout.hessian_write_funhessindex ) ; 
ngrad_done = 0;
nhess_done = 0;
for k = 1 : numel( priors ) 
    priors( k ).gradient_linindex = gradorigidx( 1 : numel(  priors( k ).gradient_linindex  ) );
    ngrad_done = ngrad_done + numel( priors( k ).gradient_linindex ); 

    priors( k ).hessian_write_funhessindex = hessorigidx( 1 : numel(  priors( k ).hessian_write_funhessindex  ) );
    nhess_done = nhess_done + numel( priors( k ).hessian_write_funhessindex ); 
end;
priorout.fun = makeaddPriorFunhandle( priors , ngrad, nhess);

function fun = makeaddPriorFunhandle( priors , ngrad, nhess)
fun = @(val) addPriorFun( val, priors , ngrad, nhess);
    
function [f,g,h] = addPriorFun( val, priors , ngrad, nhess)
f=0;
if nargout >= 2
    szval = size(val);
    g = zeros( [ngrad, szval(2:end)] );
    if nargout >= 3
        h = zeros( [nhess, szval(2:end)] );
    end;
end;
for k = 1 : numel(priors)
    if nargout<=1
        fk = priors(k).fun(val);
    else 
        if nargout<=2
            [fk, gk] = priors(k).fun(val);
        else
            [fk, gk, hk] = priors(k).fun(val);
            h( priors(k).hessian_write_funhessindex , :)  = h( priors(k).hessian_write_funhessindex , :)  + hk;
        end;
        g( priors(k).gradient_linindex , :) = g( priors(k).gradient_linindex , :) + gk;
    end;
    f = f + sum(fk);
end;

