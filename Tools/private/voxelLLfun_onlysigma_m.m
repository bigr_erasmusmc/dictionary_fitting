function [ll, grad, hessinfo] = voxelLLfun_onlysigma_m( par, fun, logpdffun, data,   explicitHessian, logprior, funHessian_I, funHessian_J)
% [ll, grad, hess] = voxelLLfun_onlysigma_m( par, fun, logpdffun, data , maxfunArgsOut, explicitHessian, logprior, npar_sigma)
% computes the likelihood of the MR data with likelihood function logpdffun.
% This is a specialization of voxelLLfun_sigma_m, assuming fun has no par.
% INPUTS:
% par           : a vector/matrix with the parameters for fun (each column of par specifies a voxel)
% fun( parfun )      : a function predicting the magnitudes of the MR images for each column of par
%                 (Each column specifies the parameters of one voxel)
%                 [A] = fun( par )
%                 parfun : (0 x numtr) empty array
%                 A  : ( numim  x  numtr  ) , where nelpp is the number of components per datapoint (typically 1, but might be 2 for complex data)
% logpdffun(data, A, sigma) : log(pdf(data, A, sigma)), log likelihood function, e.g. @(data, A, sigma) logricepdf(data, A, sigma, [false, true, true])
% data          : Measurements; (magnitude) MR values.
% maxfunArgsOut : maximum number of output arguments of fun
% explicitHessian : boolean: should explicit or compacted hessian be returned?
% logprior.fun   : if non empty: function that computes the log likelihood of the prior distribution of par.
%                               as well as the gradient and compactified hessian.
%
% OUTPUTS:
% ll : - log likelihood value
% grad: gradient of ll 
% hess: hessian of ll
%
% handles multiple data vectors simultanuously, each column of par should correspond to a column in data (and par after reshape).
%
% NOTE: update code in conjunction with voxelLLfun_m
%
% Created by Dirk Poot, Erasmus MC, 22-3-2011
szdta = size(data);
numtr = prod(szdta(2:end));

sigma = reshape(par,[],numtr);
npar_sigma = size(sigma,1);

[A ] = fun( zeros(0,numtr) );
lrargs = cell(1, max(1,nargout));
[lrargs{:}] = logpdffun(data(:,:), A , sigma, [false false true]); % derivative w.r.t. A and sigma.
ll = - sum( lrargs{1}(:) );
if ~isempty(logprior.fun)
    % Evaluate prior for the parameters:
    lpriorargs = cell(1, nargout);
    [lpriorargs{:}] = logprior.fun( par );
    ll = ll - sum(lpriorargs{1});
end;
if nargout>=2
    % also compute gradient:
    % d ll /d par_i =
    %  i<=nparrs     = sum_j  d ll / d lgpdf_j * d lgpdf_j/d f_j * d f_j / dpar_i
    %                  + d ll / d lgprior * d lgprior / d par_i
    %                =  -1 * sum( lrargs{2}(:,:,1) .* fargs{2}(:,:,i) ,1)'
    %                   -1 * lpriorargs{2}(:,i);
    %  i>nparrs      = sum_j  d ll / d lgpdf_j * d lgpdf_j/d dpar_i
    %                  + d ll / d lgprior * d lgprior / d par_i
    %                =  -1 * sum( lrargs{2}(:,:,i-nparrs) ,1)
    %                   -1 * lpriorargs{2}(:,i);
    numim = size(data,1);
    nparrs = 0;

    lrargs{2} = reshape( lrargs{2}, numim, numtr, npar_sigma);
    % compute gradient, see DifusionTensorLL_gradient.nb
    % evaluate dLLdA_i * dA_idpar
    % first part: derivative wrt to parr, where continued derivative is used
    % second part: derivative wrt to sigma. 
    grad = reshape(-sum(lrargs{2},1),numtr, npar_sigma)';
    
    if ~isempty(logprior.fun)
        grad(logprior.gradient_linindex,:) = grad(logprior.gradient_linindex,:) - lpriorargs{2};
    end;
        
    if nargout>=3
        % additionally compute hessian:
        % D2_ll /d par_i d par_j = 
        % i,j<=nparrs   = sum_k d ll / d lgpdf_k * d2 lgpdf_k/d f_k d f_k  d f_k / dpar_i  d f_k / dpar_j
        %                      +d ll / d lgpdf_k * d lgpdf_k/d f_k * d2 f_k / d par_i d par_j
        %                 + d ll / d lgprior * d2 lgprior / d par_i d par_j    
        %               = -1 * sum(lrargs{3}(:,:,1) .* fargs{2}(:,:,i) .* fargs{2}(:,:,j)) 
        %                 -1 * sum( lrargs{2}(:,:,1) .* fargs{3}(:,:,ij) )
        %                 -1 * lpriorargs{3}(:,ij)  
        % i<=nparrs, j>nparrs =  sum_k  d ll / d lgpdf_k * d2 lgpdf_k/d f_k d dpar_j * d f_k / d par_i
        %                        + d ll / d lgprior * d2 lgprior / d par_i d par_j   
        %               =  -1 * sum( lrargs{3}(:,:,1(j-nparrs)) .* fargs{2}(:,:,i) ,1)
        %                  -1 * lpriorargs{3}(:,ij)  
        % i,j > nparrs  =  sum_k  d ll / d lgpdf_k * d2 lgpdf_k/d par_i d dpar_j 
        %                        + d ll / d lgprior * d2 lgprior / d par_i d par_j   
        %               =  -1 * sum( lrargs{3}(:,:,(i-nparrs)(j-nparrs)) ,1)
        %                  -1 * lpriorargs{3}(:,ij)  
        npars = npar_sigma;
%         [I,J] = find(triu(ones(npars)));
        
        lrargs{3} = reshape(lrargs{3}, numim,  numtr, (npar_sigma+1).*(npar_sigma)/2);

        
%         hesscoeff = reshape( -sum( reshape( hessLng, numim, numtr*nI) ,1) , numtr, nI)';
        
        [Io, Jo] = find(triu(ones(npar_sigma)));
        
        red3 = (funHessian_I>nparrs) & (funHessian_J>nparrs); % dL /d sigma d sigma;    red3 could be simplified to I>nparrs, since I<=J
        hessLng3 = lrargs{3}(: , : , Io ) ;
        hesscoeff3 = reshape( -sum( hessLng3,1) , numtr, nnz(red3))';
        
        hesscoeff = zeros(numel(funHessian_I),numtr);
        hesscoeff(red3,:) = hesscoeff3;
        
        if ~isempty(logprior.fun)
            hesscoeff(logprior.hessian_write_funhessindex,:) = hesscoeff(logprior.hessian_write_funhessindex,:) - lpriorargs{3};
        end;
        

        if explicitHessian
            rev = funHessian_I~=funHessian_J;
            hessblk = [hesscoeff(:,:);hesscoeff(rev,:)];
            Ir = bsxfun(@plus, [funHessian_I;funHessian_J(rev)] , 0:npars:numel(par)-1);
            Jr = bsxfun(@plus, [funHessian_J;funHessian_I(rev)] , 0:npars:numel(par)-1);
            hessinfo = sparse(Ir,Jr, hessblk, numel(par), numel(par));
        else
            hessinfo.hesscoeff = hesscoeff;
            hessinfo.hessMulFun = @compactedHessianMul;
            hessinfo.makePreconditioner = @compactedHessianMakePreconditioner;            
        end;
    end;
end;
if ~isfinite(ll)
    ll = inf;
end;
