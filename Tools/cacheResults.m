function varargout = cacheResults( funname, args, taskPath )
% [out1, out2, ...] = cacheResults( funname, args [, taskPath] )
%
% Performs (using feval):
%  [out1, out2, ...] = funname( args{:} )  
% and caches the outputs to speedup subsequent calls with identical inputs. 
%
% Interface function to cache the results of some deterministic function that has long runtime.
%
% INPUTS;
%  funname: String with the name of the MATLAB function you want to run 
%           This function should be:
%           - Deterministic (return identical outputs when called multiple times with identical inputs)
%           - Have no side effects (including modification of the file system such as saving/modifying a file, as these 
%             operations cannot be applied multiple times with identical effect)
%           - Not use global variables or initialized persistent variables (as they might be different in subsequent calls)
%           - Have a significant runtime (with typical arguments); say >1 minute. 
%             (else the overhead of computing the hash and storing/reading the results is 
%              probably outweighing the benefits of not computing multiple times)
%           - If you have multiple calls that can be evaluated in parallel, consider using 
%             run_on_cluster to actually evaluate in parallel.
%  args  : cell array with arguments that are provided to funname
%          Specifically it evaluates (replacing funname with the content of the string funname):
%            [out1, out2, ..] = funname( args{:} )
%          Arguments should be numeric, cell, struct, or char. Custom classes and (anonymous) functions
%          are currently not supported. (hash cannot be computed) 
%          Note that the arguments are not stored to disk, hence from the result file it 
%          is impossible to re-create input arguments. (The hash allows only to verify that the same arguments are provided)
%  taskPath : optional foldername where the results are stored in a cache file. 
%             if non-empty taskPath should be a folder that exists. 
%             Default = taskpath as specified in load_clusterconfig.
%             Any call with identical args (and number of outputs) should also use the same taskPath as only 
%             that folder is checked for existence of the cache file. 
%
% OUTPUTS:
% [out1, out2, ...] : outputs of funname called with arguments args. Either computed now, or the 
%            read from a cache file created by a previous call. As the cache file name contains a hash of args
%            any difference in args results in a different cache file name (ignoring the extremely small chance of hash collisions)
%
% NOTE1 : Internally uses run_on_cluster. 
%   To be prepared for the situation in which you actually want to run these 
%   computations on the cluster (in parallel) by simply replacing 'cacheResults' with 'run_on_cluster'
%   consider the following relevant difference : 
%          In run_on_cluster the evaluation (typically) is delayed. Hence, the first call returns empty outputs.
%          ONLY USE isempty TO CHECK! (see run_on_cluster for further details)
% NOTE2 : If you update the code of the function funname, or anything called by that function,
%         delete all cache files (anywhere) with funname in their name to force updating of the results. 
%         A search for sprintf('"Result %s %d_????????????????????????????????.mat"', funname, nargout )
%         captures all cache files and probably results in few false positives. 
%
% 13-2-2018, D.Poot, Erasmus MC: Created first version
if nargin<3
    taskPath = [];
end;
varargout = cell( 1, nargout );
[varargout{:}] = run_on_cluster( funname, args , 'executionMode', 2, 'taskPath', taskPath);


function test
%%
scurr = rng ;
rng( 'default')
A= randn(123,234);
rng( scurr );
B = cacheResults( 'sin', {A} );
assert( isequal( B, sin( A ) ) )
