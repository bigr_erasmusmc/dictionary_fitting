
% Setup inversion recovery prepared FSE sequence:
ir = struct;
ir.TR = [3100 3200 3400 3800 4600];
ir.TI = [ 100  200  400  800 1600];
ir.TE = [  40   40   40   40   40];
ir.samplingduration = 2;
% p_init = cell(1,numel(ir.TR));
p_acq = cell(1,numel(ir.TR));
for k=1:numel(p_acq)
    p_acq{k} = [pulses_inversion( ir.TI(k) ) pulses_FSE( ir.TE(k), 6 ,[], ir.samplingduration, 0) pulses_delay(ir.TR(k) - ir.TI(k)- ir.TE(k))];
end;


%% Specify tissues:

  tissuenames = { 'WM', 'WM RF:95%', 'WM 10Hz', 'GM', 'CSF', 'FAT', 'LIVER', 'MUSCLE', 'KIDNEY', 'CARTILAGE'};   % tissue name (for display puroposes and possibly file name generation)
        T1          = [  617   617          617        1124  4166   343    576      1008      680       1024      ];   % ms
        T2          = [   78    78           78          95  1149    58     46        44       55         30      ];   % ms
        T2star      = [   59    62           62          83   800    40     34        30       40         25      ];   % ms
        %T2prime    = [   30    30           30          30   300   200     30        30                          ];   % ms
        T2prime      = 1./(1./T2star-1./T2);                                                                           % ms
        B0          = [   0      0       2*pi*0.01        0     0   220*2*pi/1000 0    0        0          0      ];   % rad/ms
        Mz0         = [   1      1           1            1     1     1      1         1        1          1      ]*1000; % a.u.
        B1scale     = [   1      0.95        1            1     1     1      1         1        1          1      ];   % (fraction)

Ntst = 10000;        
spins = MRI_spins(T1, T2, T2prime, B0, Mz0, Ntst, 1);

%% Run Bloch simulation:
Sacq = cell(size(p_acq));
Mfinal = cell(size(p_acq));
tic
for k= 1: numel(p_acq)
    [Sacq{k}, Mfinal{k}] = bloch_sim2( spins, p_acq{k} , []);
end;
toc
%% Sample continuously to inspect spin evolution:
k=1;
T = linspace(0,4000,6000);

p_inspect = addSampling( removeSampling( p_acq{k} ), T);
tic
[S_inspect, Mfinal] = bloch_sim2( spins, p_inspect, []);
toc;