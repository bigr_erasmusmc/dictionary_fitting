function [Msample, M] = bloch_sim2( spins, pulses, M, useMex)
% [Msample, Mfinal] = bloch_sim2( spins, pulses, Minitial, useMex);
% 
% Computational backend of a quite general Bloch simulator, assuming 'hard' RF
% pulses. Uses the spins and pulses as provided. These are (typically)
% setup by separate functions.
%
% 
% INPUTS:
%   spins  : an MRI_spins object that specifies the (tissue) properties of
%            the voxels. 
%   pulses : an MRI pulses object that specifies the pulse sequence that
%            will be applied.
%   Minitial : optional, default is rest state magnetisation.
%              Ns x 3 x Nv matrix with initial magnetisation state (see
%              MRI_spins for explanation of these sizes)
%              Should be a Mfinal output.
%   useMex : scalar boolean, default =true. Do you want to use the fast mex
%             implementation ?
%
% OUTPUTS:
%   Msample : Nv x 3 x Ts matrix with net magnetisation in each voxel 
%             at each sample time point.
%   Mfinal  : Ns x 3 x Nv matrix with all spins as present right after the
%             last action specified in pulses.  
%
% Created by Dirk Poot, Erasmus MC
% 29-1-2013

Nv = numel(spins.Mz0);
Ns = size(spins.B0,1);

if nargin<3||isempty(M)
    % Create initial magnetisation:
    M = bsxfun(@times, ones(Ns,1)* [0 0 1], reshape(spins.Mz0,[1 1 Nv]));
end;
if nargin<4
    useMex = true;
end;
if useMex
    % Compile mex file:
    % mex Bloch_sim_c.cpp -I".\Tools\supportingfiles\" -g -O -v
    
    % preparations for calling mex file:
    pulses = pulses.cleanup();
    
    dtime = pulses.deltaT;
    [deltatimes , b, timestepid ] = unique( dtime + 100*sum(dtime) ); % round dtime a bit to avoid new groups only due to roundoff errors in time computation.
    deltatimes = dtime(b)'; % non rounded delta time; should do mean over all in group

    relaxT2 = exp( bsxfun(@rdivide, -deltatimes, reshape( spins.T2 , 1, [])) );
    relaxT1 = expm1( bsxfun(@rdivide, -deltatimes, reshape( spins.T1 , 1, [])) );
    
    RFpulses = pulses.RFpulses;
    if ~( isempty(spins.RFscale) && all( (pulses.RFscalefactor==1) | isnan(pulses.RFscalefactor)))
        if isempty(spins.RFscale)
            spins.RFscale = ones(size(spins.T1));
        end;
        RFscale = bsxfun(@times, spins.RFscale , pulses.RFscalefactor ) ;
        if size(RFpulses,4)~=1
            error('RFpulses should be the same for each voxel when RF is scaled.');
        end;
%         tic
        RFscale( isnan( RFscale(:) ) ) = 1 ; % nan indicates adiabatic pulse that is invariant to RF scaling. 
        RFpulses = mpower_bsxfun( RFpulses, reshape(RFscale,[1 1 size(RFscale)]));
%         toc
%         tic
%         if size(RFscale,1)==1
%             RFscale = RFscale(ones(1,size(RFpulses,3)),:);
%         end;
%         RFpulses = RFpulses(:,:,:,ones(1,size(RFscale,2)));
%         for k = find( ~isnan( RFscale(:) ) & RFscale(:)~=1 )';
%             % matrix power:
%             RFpulses(:,:,k) = RFpulses(:,:,k)^RFscale(k);
%         end;
%         toc
%         isequal(RFpulses, RFpulses_new)
        if ~isreal( RFpulses )
            imp = imag(RFpulses);
            if max(abs(imp(:))) > 1e-6 % assume typical values are 1, so ignore only parts introduced by roundoff errors.
                error('imaginary components introduced by scaling RF power; this should not happen.');
            end;
            clear imp
            RFpulses = real(RFpulses);
        end;
    end;
    timestepidc = int32( timestepid - 1 );
    actionId = int32( pulses.actionid ); 
    xyz_is_trajectories_array = false; % check if xyz contains a diffusion trajectory array
    if isempty( spins.xyz ) || isempty( pulses.gradpulses ) % if there are no gradient pulses, we don't need to apply them
        if isempty( pulses.gradpulses )
            gradPulseIndex = [];
        else
            error('gradient pulses not empty, but no coordinates (or diffusion) provided. Cannot apply the gradient pulses.');
        end;
    else
        if ndims(spins.xyz) == 4
            % Xyz contains spin trajectories
            gradTimeIdx = find( bitand( pulses.actionid, 8 ) );
            gradPulseIndex = int32( (0:numel(gradTimeIdx)-1)' ); % each gradient pulse is unique.
            xyz_is_trajectories_array = true;
        elseif ~isempty( spins.diffusionTensors )
            % Setup gradient pulses for diffusing spins
            
            % first prepare random number generator:
            rngstate = rng;
            
            
            % x(i+1) = A* x(i) + B * noise
            % Solve A and B such that for each i : cov( r(i) ) ==S and cov( r(i+1)-r(i) ) = D * deltaT(i) 
            Npos = max([size(spins.diffusionTensors,3), size(spins.voxelSize, 3), size(spins.voxelCenter,2)]);
            rD = min( 1:Npos, size(spins.diffusionTensors,3)); if ~any(rD(end)==[1 Npos]), error('Required: one diffusion tensor for all, or one per voxel ');end;
            
            rS = min( 1:Npos, size(spins.voxelSize,3)); if ~any(rS(end)==[1 Npos]), error('Required: one voxel size for all, or one per voxel ');end;
            rC = min( 1:Npos, size(spins.voxelCenter,2)); if ~any(rC(end)==[1 Npos]), error('Required: one voxel center for all, or one per voxel ');end;

            tmp = [reshape(spins.diffusionTensors(:,:,rD),[],Npos);
                   reshape(spins.voxelSize(:,:,rS),[],Npos);
                   reshape(spins.voxelCenter(:,:,rC),[],Npos)]';
            [dummy, uniquespinpos, uniquerevpos] = unique( tmp , 'rows');
            rD = rD( uniquespinpos );
            rS = rS( uniquespinpos );
            rC = rC( uniquespinpos );
            Npos = numel(uniquespinpos);
            
            D = mat2cell(spins.diffusionTensors(:,:,rD),3, 3, ones(1,Npos));
            S = mat2cell(spins.voxelSize(:,:,rS),3,3,ones(1,Npos));
            sS = cell(1,Npos);
            for k=1:Npos
                sS{k} = chol(S{k})';
            end;
            voxelCenter = mat2cell( spins.voxelCenter(:, rC), 3, ones(1,Npos)); 

            
            gradTimeIdx = find( bitand( pulses.actionid, 8 ) );
                            
            gradPulseIndex = int32( bsxfun(@plus, (0:numel(gradTimeIdx)-1)'*Npos, uniquerevpos'-1 ) ); % each gradient pulse is unique.
            nexttimeidx = 1;
            deltaT = zeros(1, numel(gradTimeIdx));
            % Perform diffusion simulation. 
            % The spin positions are only needed at the time of the
            % gradient pulses. 
            for k=1:numel(gradTimeIdx)
                curtimeidx = gradTimeIdx(k );
                deltaT(k) = sum( pulses.deltaT( nexttimeidx:curtimeidx  ) );
                
                nexttimeidx = curtimeidx+1;
            end;
            
        else
            % constant spin positions:
            [uniquepulses , ignored_sel, gradPulseIndex ] = unique( pulses.gradpulses','rows' );
%             if size(spins.xyz,2)~=1
%                 error('currently all spins should have the same xyz positions');
%             end;
            gradPulseIndex = int32( bsxfun(@plus, (gradPulseIndex(:) -1)*size(spins.xyz,2) , 0:size(spins.xyz,2)-1 ) );
            clear ignored_sel 
        end;
    end;
    
    totalnumB0cache =  numel(deltatimes)*size(spins.B0,2) ;
    if ~isempty(gradPulseIndex)
        totalnumB0cache = totalnumB0cache  + max(gradPulseIndex(:))+1;
    end;
    if totalnumB0cache * size(spins.B0,1) > 5e7 
        % There are more than 5e7 complex double values in total (=800MB), split to avoid
        % out of memory issues. Still use a large amount of memory since there is
        % some (but rather minor) inefficiency in splitting. 
        numblks = ceil( min( size(spins.B0,1)/4 , totalnumB0cache * size(spins.B0,1) / 4e7 ) );
        spinstartend = 1+ round(linspace( 0,  size(spins.B0,1) , numblks+1)/2)*2; %blocksize should be even.
    else
        % we can do it in 1 go (fits in memory)
        spinstartend = [1 size(spins.B0,1)+1];
    end;
    clear totalnumcache
    nblocks = numel(spinstartend)-1;
    for blockid = 1:nblocks
        B0 = spins.B0(spinstartend(blockid):spinstartend(blockid+1)-1,:);
        B0c = exp(-1i* bsxfun(@times, reshape(B0, [size(B0,1) 1 size(B0,2)]), deltatimes') );
        B0w = spins.B0w(spinstartend(blockid):spinstartend(blockid+1)-1,:);
        
        if isempty(gradPulseIndex)
            gradPulses = [];
        elseif ~xyz_is_trajectories_array && isempty( spins.diffusionTensors )
            % no diffusion, just gradient pulses 
            xyz = spins.xyz( spinstartend(blockid):spinstartend(blockid+1)-1 , :, :); 
            gradPulses = reshape( reshape( xyz,[],3) * uniquepulses' , size(xyz,1), size(xyz,2) * size(uniquepulses,1) );
            gradPulses = exp(1i*gradPulses);
        elseif xyz_is_trajectories_array
            % calculate gradient pulses for spin positions
            num_spins = size(spins.xyz, 1);
            gradPulses = zeros(num_spins, numel(gradTimeIdx));
            gradPulseTimes = cumsum(dtime);
            gradPulseTimes = gradPulseTimes(gradTimeIdx);
            for k = 1 : numel(gradTimeIdx)
                t = round(gradPulseTimes(k)/spins.step_time);
                gradPulses(:, k) = exp(1i* (permute( spins.xyz(:,1,:,t),[1 3 2 4]) * pulses.gradpulses(:,k) )) ;
            end;
            clear num_timesteps num_spins total_time k s
        else
            % diffusion simulation:
            rng( spins.diffusion_rngstate )
            nblk = spinstartend(blockid+1)-spinstartend(blockid);
            gradPulses = zeros(nblk, Npos, numel(gradTimeIdx) ); 
            rnd = randn(3, size(spins.B0,1)); % create all random numbers to create a deterministic simulation.
            rnd = rnd(:,spinstartend(blockid):spinstartend(blockid+1)-1);
            x = cell(1,Npos);
            for v = 1 : Npos
                x{v} = sS{v} * rnd ;
            end;

            for k = 1 : numel(gradTimeIdx)
                rnd = randn(3, size(spins.B0,1)); % create all random numbers to create a deterministic simulation.
                rnd = rnd(:,spinstartend(blockid):spinstartend(blockid+1)-1);
                for v = 1 : Npos
                    DT = D{v} * (2*deltaT(k)); % covariance of step in x is D * deltaT*2 (due to definition of diffusion, eq3 in  http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2041910/)
                    % x_{k+1} = A * x_k + B * noise
                    % Solve A and B such that for each k : cov( x_k ) == S and cov( x_{k+1}-x_k ) = DT
                    % assume cov(x_k)==S and  cov( noise ) = I = eye(3), then 
                    %  cov( x_{k+1} ) = E( (A * x_k + B*noise) * (A * x_k + B*noise)' )
                    %                 = A * E( x_k * x_k' )* A'  + B *I * B' 
                    %                 = A * S * A' + B * B' == S
                    %  cov( x_{k+1}-x_k ) = E( (A * x_k + B*noise - x_k) * (A * x_k + B*noise - x_k)' )
                    %                     = E( ( (A-I) * x_k + B*noise ) * ( (A-I) * x_k + B*noise )' )
                    %                 = (A-I) * E( x_k * x_k' )* (A-I)'  + B * I * B' 
                    %                 = (A-I) * S * (A-I)' + B * B' == DT
                    %
                    % subtracting both gives:
                    %     A * S * A' - (A-I) * S * (A-I)' == S - DT
                    %     A * S * A' - A * S * A' + I *S * A' + A* S *I' - I* S *I' == S - DT
                    %      S * A' + A * S == 2*S - DT
                    % trying  A = I - .5* DT * inv(S)  and using S = S'
                    %     S * (I - .5* DT * inv(S))' + (I - .5* DT * inv(S)) * S 
                    %   = S  - S * .5*inv(S)'*DT' + S - .5 * DT  = 2*S - DT
                    %
                    % Then with this A, solve for B:
                    % B*B' = DT - (A-I) * S * (A-I)'
                    % get a B from right hand side with chol:

                    A = eye(3)-.5*DT/S{v};
        %             BB = S-A*S*A';  % or BB = D - (A-I)*S*(A-I)'
                    BB = DT - (A-eye(3))*S{v}*(A-eye(3))'; % or BB = S- A*S*A', but for D<<S that is numerically less accurate.
                    B = chol(BB)';
                    x{v} = A* x{v} + B * rnd ;
                    gradPulses( :, v, k ) = exp(1i* (pulses.gradpulses(:,k)'*bsxfun(@plus,x{v},voxelCenter{v}))' ); % TODO: maybe faster to multiply x and voxelCenter seperately with grad; or maybe include voxelCenter in x (but then rederive above equations for diffusion)
                end;
            end;
            clear x
            gradPulses = gradPulses(:,:);
            rng( rngstate ); % restore random number generator state.

        end;
        Mblock = M(spinstartend(blockid):spinstartend(blockid+1)-1,:,:);

    % Perform the Bloch simulation via MEX file
        [Msample_blk, Mblock] = Bloch_sim_c( spins.Mz0, relaxT2, relaxT1, B0c, RFpulses, gradPulses, gradPulseIndex ,B0w, timestepidc , actionId, Mblock);
        M( spinstartend(blockid):spinstartend(blockid+1)-1,:,:) = Mblock;
        clear B0c B0w gradPulses Mblock;
        if blockid==1
            Msample = Msample_blk;
        else
            Msample = Msample + Msample_blk;
        end;
    end;
    return;
end;
error('non mex version not implemented');

L = (1:Ns);
H = L+Ns;
B0_I = [L;L;H;H];
B0_J = [L;H;L;H];
B0_nn = 2*Ns;
deltaT_prevB0rot = nan;

t_prev = 0;
Tpidx  = 1;
Tsidx  = 1;
nextPulseTime = RFpulsetimes(Tpidx);
nextSampleTime = sampletimes(Tsidx);
Msample = zeros([Nv 3 Ts]);
if size(RFpulses,4)>1
    pulseit = 1: Nv;
else
    pulseit = ones(1,Nv);
end;
srttimes = sort([0 RFpulsetimes sampletimes]);
[dummy1, dummy2, timestepid] = unique( diff(srttimes) + 1000*srttimes(end) );
B0sparse = cell(1,numel(dummy1));

for tidx = 1 : Tp+Ts
    
    % bookkeeping of time and decide if an RF pulse is applied or we sample the current magnetisation:
    doPulse = nextPulseTime <= nextSampleTime;
    if doPulse
        % apply RF pulse
        t_new = nextPulseTime;
        Tpidx = Tpidx+1;
        if Tpidx <= Tp
            nextPulseTime = RFpulsetimes(Tpidx);
        else
            nextPulseTime = inf;
        end
    else
        % sample magnetisation
        t_new = nextSampleTime;
        Tsidx = Tsidx+1;
        if Tsidx <= Ts
            nextSampleTime = sampletimes(Tsidx);
        else
            nextSampleTime = inf;
        end
    end;
    
    % The time step we take:
    deltaT = t_new - t_prev; % DEBUG: show time step
    
    
    % T2 and T1 relaxation since previous time point:
    relaxT1 = exp( - deltaT ./ T1 );
    relaxT2 = exp( - deltaT ./ T2 );
    
    for it = 1 : numel(T1)
        M(:, 1:2, it) = M(:, 1:2, it) * relaxT2(it);
        M(:, 3  , it) = Mz0(it) + (M(:, 3 , it) - Mz0(it)) * relaxT1(it);
    end;
    
    % B0 rotation since previous time point: (simulates offresonance and T2* decay)
    Mxy = reshape(M(:,1:2,:), [2*Ns Nv]);
    if size(B0,2)>1
        deltaTidx = timestepid( tidx );
        if ~isempty(B0sparse{deltaTidx}) %deltaT_prevB0rot ==  deltaT
            % common case of same time step: avoid re-creating B0sparse
            % again when it will be the same anyway.
        else
            cB0 = cos( B0 * deltaT );
            sB0 = sin( B0 * deltaT );
            B0sparse{deltaTidx} = cell(1, size(B0,2));
            for B0idx = 1: size(B0,2)
                B0sparse{deltaTidx}{B0idx} = sparse(B0_I, B0_J, [cB0(:,B0idx)';sB0(:,B0idx)';-sB0(:,B0idx)';cB0(:,B0idx)'] , B0_nn , B0_nn);
            end;
            deltaT_prevB0rot =  deltaT;
        end;
        for B0idx = 1: size(B0,2)
            Mxy(:,B0idx) = B0sparse{deltaTidx}{B0idx} * Mxy(:,B0idx);
        end;
    else    
        % fast single sparse matrix multiplication
        if deltaT_prevB0rot ==  deltaT
            % common case of same time step: avoid re-creating B0sparse
            % again when it will be the same anyway.
        else
            cB0 = reshape( cos( B0 * deltaT ), [1 Ns]);
            sB0 = reshape( sin( B0 * deltaT ), [1 Ns]);
            B0sparse = sparse(B0_I, B0_J, [cB0;-sB0;sB0;cB0] , B0_nn , B0_nn);
            deltaT_prevB0rot =  deltaT;
        end;
        Mxy = B0sparse * Mxy;
    end;    
    M(:,1:2,:) = reshape(Mxy, [Ns 2 Nv]);
    
    if doPulse
        % apply RF pulse
%         RFpulses(:,:,Tpidx-1, pulseit(1) ) % DEBUG: show RF pulse
        for it = 1: numel(T1)
            M(:, : , it) = M(:, :, it) * RFpulses(:,:,Tpidx-1, pulseit(it) );
        end;
    else
        % Store magnetisation
        if size(B0w,2)>1
            for B0wit = 1 : size(B0w,2)
                Msample(B0wit,:,Tsidx-1) = B0w(:,B0wit)'*M(:,:,B0wit);
            end;
        else
            Msample_p = reshape( B0w'*M(:,:) ,[3 Nv]);
            Msample(:,:,Tsidx-1) = Msample_p';
        end;
    end;
%     B0w(:,1)'*M(:,:,1) % DEBUG: show net magnetisation of first voxel
    
    t_prev = t_new;
end;
