function varargout = run_on_cluster( funname, args , varargin)
% [out1, out2, ..] = run_on_cluster( funname, args , [options, value])
%
% Interface function to run some deterministic function delayed/in parallel on the cluster.
%
% INPUTS;
%  funname: String with the name of the MATLAB function you want to run on the cluster
%           This function should be:
%           - Deterministic (return identical outputs when called multiple times with identical inputs)
%           - Have no side effects (including modification of the file system such as saving/modifying a file, as these 
%             operations cannot be applied multiple times with identical effect)
%           - Not use global variables or initialized persistent variables (as the MATLAB 
%             context in which the evaluation occurs does not have those initialized)
%           - Have a significant runtime (with typical arguments); say >10 minutes. 
%             (else the overhead is outweighing the benefits of parallel evaluation)
%  args  : cell array with arguments that are provided to funname
%          Specifically it evaluates (replacing funname with the content of the string funname):
%            [out1, out2, ..] = funname( args{:} )
%          Arguments should be numeric, cell, struct, or char. Custom classes and (anonymous) functions
%          are currently not supported. (hash cannot be computed and correct and efficient storing is not guaranteed) 
% option value pairs:
%   memory : required memory in bytes (may be rounded to MB or GB)
%   queue  : name of queue used for submitting the task. 
%   waitForCompletion : default = false : create task file or load results 
%          if true AND the task file already exists: wait for completion of the task. 
%              If the task does not exist, then the task is created and started and then immediately run_on_cluster returns (without waiting for completion). 
%              So this option is usefull when you know that all tasks are created and running(/waiting to be started)
%              and you want to start collecting results before all tasks are completed. 
%          if 2  : always wait for completion; even if the task file does not exists it is not created, but I wait till the result file is present.
%              Usefull if you manually transfer files between PC and cluster. (try to avoid that; its a lot of hassle)
%          if -1 : Dont create a task file; only load result file if it exists. 
%
% OUTPUTS:
% [out1, out2, ...] : empty or outputs of funname called with arguments args.
%          As the evaluation is delayed, the first call returns empty outputs.
%          ONLY USE isempty TO CHECK! Future plans are to output an object that 
%          can be used as input of another run_on_cluster function evaluation. This object will then overload isempty to return true if the result is not complete. 
%          When the processing is complete, a new call with the same arguments
%          does return the values. So assuming the deterministic(!) function does not only 
%          return empty outputs, succesfull completion can be tested by checking if the outputs are nonempty. 
%          Note that errors thrown during execution are thrown also upon the 'function complete' call 
%          (instead of returning the not-available outputs). 
%
% See the test function at the bottom of this mfile for an working (test) example.
% See load_clusterconfig to configure how tasks are executed. 
% See submitTasks to do the actual submitting of the tasks when evaluating remotely. 
%
% How does it work? :
%  When first calling run_on_cluster with a specific set of arguments, it creates a task file (*1).
%  All returned outputs are empty and the function returns basically immediately 
%  (does not wait for the computations to start/finish; it only creates a task file specifying this call). 
%  Depending on the cluster configuration (see load_clusterconfig)
%  the task is immediately started, or you need to call 'submitTasks'
%  yourself on the cluster. The latter is needed when calling this function on 
%  a local (windows) PC that cannot directly submit jobs on the cluster.
%  submitTasks actually starts the separate jobs. (BIGR: call this on an appnode)
%  If needed submitTasks compiles an executable that runs the matlab function 
%  specified by funname. You should make sure the environment calling submitTasks
%  has all required code, and the correct version. Executables are rebuild
%  if the called function mfile changes. However, this by no means is a fool-proof method;
%  For to performance reasons no full dependency check is performed. 
%  As compilation is done through submitTasks, that is also the function in 
%  which you may observe compilation errors/issues. (on the computer evaluating submitTasks.)
%  When there is an error during compilation, no tasks are submitted. 
%
%  Later calls to run_on_cluster with exactly the same arguments will load 
%  the computed results and return that. 
%
%  An optimization is that large arguments (>storeThresholdBytes) are stored 
%  in separate files identified by a hash of the argument. 
%  This means that when arguments appear multiple times (over multiple calls)
%  they are stored only once. Thereby saving disk space and bandwidth. 
%  The user should remove these large arguments when they are no longer needed. 
%  The argument files are no longer needed as soon as there is no task file referencing them
%  anymore. 
%  Use run_on_cluster('removeOldLargeArgumentFiles') to remove the files that are no 
%  longer needed by the existing tasks. (Future tasks will re-create the same argument files if needed.)
%  Use run_on_cluster('removeResultFilesWithErrors') to remove all task results in which 
%  running of the task resulted in an error. (maybe out of memory, or bug in the code).
%
% (*1) : To make the name of the task file uniquely dependent on the arguments a hash is computed (using hash.m, currently (5-2018) implemented with MD5 hashing).
%  While many arguments produce the same hash (the hash is much smaller than the typical argument size)
%  it is exceedingly rare that different arguments produce the same hash. 
%  Specifically, if there are no bugs nor succesfull attempts to enhance collision rate,
%  the probabily of one or more collisions is approx 10^-21 when 10^9 tasks are created (512 bit hash).
%
% Uses:
%  load_clusterconfig : specifies the cluster setup
%  submitTasks : when starting from local; runs the jobs. 
%  ResultFullNameFromTaskFullName : Creates the result file name from a task file name. 
%  
%
% 13-2-2018, D.Poot, Erasmus MC: Created first version
% 1-5-2018, D.Poot, Erasmus MC: added 'local-blocking' option (used by cacheResults)

% Parse arguments to run_on_cluster:
opts = struct;
opts.memory = 1e9;  % task option; stored to task file. 
opts.queue = 'week';% task option; stored to task file. 
opts.waitForCompletion = false; % if true: when task file exists; wait for completion of the task. 
opts.executionMode = []; % if non empty overrides load_clusterconfig.
                         % Mainly usefull to select local-blocking running, regardless of load_clusterconfig settings. 
                         %  This can be used to only cache the result of a (long) computation. 
opts.taskPath = []; % if non empty an override for the task and result paths. May prevent automatically or easily starting jobs so mainly usefull 

if nargin>2
    opts = parse_defaults_optionvaluepairs( opts, varargin{:} );
end;
if nargin<2
    args = {};
end;
if isempty( opts.executionMode ) || ~isequal(opts.executionMode ,2 ) || isempty( opts.taskPath) 
    clusterconfig = load_clusterconfig;
else
    % very specific case used by cacheResults; we might want to run entirely without setting up clusterconfig. 
    clusterconfig  = struct;
end;
if ~isempty( opts.executionMode )
    clusterconfig.executionMode = opts.executionMode; % 'local-blocking'
end; 
if ~isempty( opts.taskPath )
    clusterconfig.taskPath = opts.taskPath; % override task path. 
end; 

if nargin==1 
    if isequal(funname, 'removeOldLargeArgumentFiles')
        removeOldLargeArgumentFiles( clusterconfig );
        return;
    elseif isequal(funname, 'removeResultFilesWithErrors')
        removeResultFilesWithErrors( clusterconfig );
        return;
    end;
end;

% Parse arguments of function:
arg_isstored = false( 1, numel(args ) );
if clusterconfig.executionMode ~= 2 % if local-blocking dont store arguments (so also dont check for that)
    argsToStore = cell( size(args));
    for k = 1 : numel( args )
        tmp = args{k};
        q = whos('tmp');
        tmp = [];
        arg_isstored( k ) = q.bytes >=  clusterconfig.storeThresholdBytes;
        if arg_isstored( k ) 
            argsToStore{k} = args{ k };
            fn = store_arg_filename( args{ k } ); % construct filename for large argument. It contains an hash, so can be assumed to be unique for every argument. 
            args{ k } = fn; % Note that fn contains the hash of the original argument (required for hash of arguments below). 
        end;
    end;
end;
numout = nargout;

% Hash arguments. Then use that to construct the task file name and the result file name. 
h = hash(  args  );
% NOTE : function name between spaces is used in submitTasks to retrieve funname. 
taskFullName = fullfile( clusterconfig.taskPath , sprintf('Task %s %d_%s.mat',funname, numout , h ) );
resultFullName = ResultFullNameFromTaskFullName( taskFullName );

% Wait for completion of task if requested. 
if opts.waitForCompletion==true && exist( taskFullName , 'file') 
    disp(['Waiting for completion of task : ' taskFullName ]);
    disp('Break (<Ctrl>+C) if task is not expected to finish');
    while exist( taskFullName , 'file')
        fprintf('.');
        pause(120);
    end;
    fprintf('.\n');
end;
if opts.waitForCompletion==2 && ~exist( resultFullName , 'file') 
    disp(['Waiting for completion of task : ' taskFullName ]);
    disp('Break (<Ctrl>+C) if task is not expected to finish');
    while ~exist( resultFullName , 'file')
        fprintf('.');
        pause(120);
    end;
    fprintf('.\n');
end;

% Check existence of result file name and differentiate action on that:
if exist( resultFullName , 'file') 
    if ~exist( taskFullName , 'file')
        % Result file exists. Task file is deleted. Task file is deleted after result is stored, so result is complete. 
        % Assume result is current so load it. 
        try 
            tmp = load( resultFullName );
        catch ME
            % error during reading of existing file. Usually a temporary error, so try again. 
            disp(['Error during reading of file : ' resultFullName ]);
            disp(['Pausing for 10 seconds before retrying. ']);
            pause(10)
            disp(['Trying again.']);
            tmp = load( resultFullName );
            disp(['Loading of this file succeeded.']);
            pause(1)
        end;
        if ~isempty(tmp.errors)
            msgstr= sprintf( 'The following error occured when running function ''%s'' from task file ''%s''', funname, taskFullName);
            disp(msgstr)
            tmp.errors = addCause( tmp.errors, MException('run_on_cluster:errorInTask', '%s', msgstr) );
            rethrow(tmp.errors); % Rethrow errors that happened during evaluation of function. 
        end
        varargout = tmp.out;
    else
        % Strange case; probably should not happen, but inform the user:
        warning('Run_on_cluster:Results', 'Result file exists, but task file not yet deleted. This means the results are currently being writted, or there was some error during storing of the results. If the task completed, delete the results file and try running the task again, or (if you trust the results to be completed and not corrupted) delete the task file.\nTask file  : %s\nResult file: %s', taskFullName ,resultFullName );
        varargout = cell( 1, numout ); % empty results to be returned as outputs. 
    end;
elseif opts.waitForCompletion==-1
    % result file does not exists, but we should not create task files or run anything. So return empty results and exit. 
    varargout = cell( 1, numout );
    
elseif clusterconfig.executionMode == 2 % if local-blocking run locally and store results.
    % This basically is a copy of run part in run_job_base.m
    % (though with substantial modifications for logistical reasons.)
    % KEEP FUNCTIONALLY IN SYNC WITH run_job_base.m!!
    errors = []; % used in save
    out = cell( 1, numout ); % empty results to be returned as outputs. 
    try 
        functionstarttime = clock; 
        fprintf('Calling %s at :  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)\n', funname, floor( functionstarttime([3 2 1 4 5 6])));
        [out{:}] = feval( funname, args{:} );
        functionendtime = clock; 
        fprintf('%s finished at:  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)\n', funname,  floor( functionendtime([3 2 1 4 5 6])));
    catch errors % used in save
        functionendtime = clock; 
        fprintf('%s finished with error at:  %d-%d-%d %d:%d:%d   (D-M-Y H:M:S)\n', funname,   floor( functionendtime([3 2 1 4 5 6])));
    end;
    save( resultFullName, 'out' ,'errors', 'functionstarttime', 'functionendtime');
    varargout = out;
    if ~isempty(errors)
        msgstr= sprintf( 'The following error occured when running function ''%s'' locally. Note that this error is cached in ''%s''.', funname, resultFullName);
        disp(msgstr)
        rethrow(errors); % Rethrow errors that happened during evaluation of function. 
    end
else % general call
    % Result file does not yet exists, so results not completed yet. 
    
    varargout = cell( 1, numout ); % empty results to be returned as outputs. 
    
    % Store the large arguments in separate files:
    for k = find( arg_isstored )
        argFullName = fullfile( clusterconfig.taskPath, args{k} );
        if exist( argFullName ,'file' )
            % file storing this argument (identical hash) already exists. Do not overwrite
        else
            % file with argument does not yet exists. Create:
            arg = argsToStore{k}; %#ok<NASGU>
            save( argFullName, 'arg');
        end;
    end;
    taskopts=struct;
    taskopts.memory = opts.memory ;
    taskopts.queue = opts.queue ;

    % If task file does not yet exists, store it: (As task file is the signalling file, it is stored last. )
    if ~exist( taskFullName , 'file')
        % store task input. 
        [dummy, resultName, ext] = fileparts( resultFullName );resultName = [resultName, ext];
        save( taskFullName ,'funname', 'numout','resultName','arg_isstored','args', 'taskopts');
    end;
    submitTasks(  taskFullName , clusterconfig );
end;

function [fn] =  store_arg_filename( arg )
% [fn] =  store_arg_filename( arg )
% Helper function to create an 'argument' file name for a given large argument.  
fn = sprintf('Arg_%s.mat', hash( arg ) );

function removeOldLargeArgumentFiles( clusterconfig  )
%%
% clusterconfig = load_clusterconfig;
taskfilesdir = dir( fullfile( clusterconfig.taskPath , 'Task*.mat') );
taskfiles = cell(size( taskfilesdir ) );
argfilesreferenced_c = cell(1,numel( taskfiles ) );
taskfilenr_c = cell(1,numel( taskfiles ) );
progressbar('start',numel( taskfiles ) ,'scanning task files','minTimeInterval', 2);
for k= 1 : numel( taskfiles ) 
    taskfiles{k}= fullfile( clusterconfig.taskPath, taskfilesdir(k).name );
    try
        tmp = load( taskfiles{k} );
    catch ME
        disp(['Error during loading of task file: ' taskfiles{k}]);
        pause(.1);
        if ~exist( taskfiles{k} ,'file')
            disp('This file does no longer exist. Probably the task just finished.');
        end;
        continue; 
    end;
    argfilesreferenced_c{k } = tmp.args( tmp.arg_isstored );
    taskfilenr_c{k} = k * ones( size( argfilesreferenced_c{k } ));
    progressbar(k);
end;
progressbar('ready');
argfilesreferenced = [ argfilesreferenced_c{:} ]; % make flat cell array. 
taskfilenr = [taskfilenr_c{:}]; % make flat cell array. 
argfilesdir = dir( fullfile( clusterconfig.taskPath , 'Arg_*.mat') );
argfiles = {argfilesdir.name};
if numel(argfilesreferenced)<1
    delargfiles = true(size(argfiles));
else
    notfoundargfiles = ~ismember( argfilesreferenced, argfiles);
    if any( notfoundargfiles )
        taskfilesmissingarg = unique( taskfilenr( notfoundargfiles)  );
        disp( sprintf( 'The following %d task files reference task files that are not in %s', numel(taskfilesmissingarg), clusterconfig.taskPath) );
        fprintf('%s\n', taskfiles{ taskfilesmissingarg } )
        [YN]= input('These task files cannot be runned at this moment.\nDo you want to delete these task files, including argument files not referenced by remaining tasks? (Y/N) ', 's' );
        if isequal(lower(YN),'y')
            disp('Deleting these task files.');
            for k = taskfilesmissingarg
                delete( taskfiles{ k }  );
            end;
            argfilesreferenced_c( taskfilesmissingarg ) =[];
            taskfilenr_c( taskfilesmissingarg ) =[];
            argfilesreferenced = [ argfilesreferenced_c{:} ]; % make flat cell array. 
            taskfilenr = [taskfilenr_c{:}]; % make flat cell array. 
        else
            notfoundargfilename = unique( argfilesreferenced( notfoundargfiles ) );
            disp(sprintf( 'Keeping task files. Before running these tasks, make sure the following %d argument files are in %s', numel(notfoundargfilename ), clusterconfig.taskPath ));
            fprintf('%s\n' , notfoundargfilename{:} );
        end;
    end;
    delargfiles = ~ismember( argfiles, argfilesreferenced);
end;
for k = find( delargfiles ) 
    delete( fullfile( clusterconfig.taskPath, argfilesdir(k).name ) );
end;

function removeResultFilesWithErrors( clusterconfig  )
%%
showerror = true; 
% clusterconfig = load_clusterconfig;
resultfilesdir = dir( fullfile( clusterconfig.taskPath , 'Result *.mat') );
resultfiles = cell(size( resultfilesdir ) );
progressbar('start',numel( resultfiles ) ,'Scanning result files for errors during evaluation.','minTimeInterval', 1);
for k= 1 : numel( resultfiles ) 
    resultfiles{k}= fullfile( clusterconfig.taskPath, resultfilesdir(k).name );
    tmp = load( resultfiles{k} ,'errors');
    if ~isempty( tmp.errors )
        if showerror
            %%
            disp(['error in : ' resultfiles{k}]);
            disp(['Error ''' tmp.errors.identifier  ''' with message: ' ]);
            disp(tmp.errors.message)
            for s = 1 : numel( tmp.errors.stack) 
                fprintf( 'on line %d of %s\n', tmp.errors.stack(s).line, tmp.errors.stack(s).file );
            end;
        else
            disp(['Deleting : ' resultfiles{k}]);
        end;
        delete( resultfiles{k} );
    end;
    progressbar(k);
end;
progressbar('ready')

function test()
%% Test functionality of 'running on the cluster'. 
% NOTE that the use of this function below (using sin) is completely irrelealistic. 
% This because sin is a very fast function.
% You should use run_on_cluster only for functions that have a runtime much larger
% than the overhead of storing, transfering, and loading the arguments and results. 
% i.e. I would say: dont use it on functions requiring less than 10 minutes runtime. 


%% small argument (included in task file:)
scurr = rng ;
rng( 'default')
randdata = randn(10);
rng( scurr );
a = run_on_cluster( 'sin', { randdata } );
if isempty(a) 
    disp('Computation not ready yet');
else
    disp('Computation ready. Is result equal to direct evaluation? (boolean):')
    isequal( a, sin( randdata )) % when evaluated, this should be true
end;

%% Large argument (stored separately:)
scurr = rng ;
rng( 'default')
randdataL = randn(1000);
rng( scurr );
b = run_on_cluster( 'sin', { randdataL } );
if isempty(b) 
    disp('Computation not ready yet');
else
    disp('Computation ready. Is result equal to direct evaluation? (boolean):')
    isequal( b, sin( randdataL ))
end;

%% test returning an error:
c = run_on_cluster( 'sin', {'Erroneaously an string argument.'} );
    
% %% Test passing anonymous function.
% TI = [100 200 400 800 2000 4000]';
% fun = @(tht) predict_IRT1( tht, TI );
% thtGT = [1000/200 1000/1000; 10 10; 10 20];
% Spred = fun( thtGT );
% sigma = 1; 
% scurr = rng ;
% rng( 'default')
% Sn = AddRiceNoise( Spred, sigma);
% rng( scurr );
% arg = {fun, Sn, 10*ones(3,2), 'noiseLevel', sigma };
% ThtEst1 = fit_MRI( arg{:} );
% ThtEst2 = run_on_cluster( 'fit_MRI' , arg ); % cannnot compute hash of anonymous function. 