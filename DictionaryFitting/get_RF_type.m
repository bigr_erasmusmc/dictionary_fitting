function [ RF_id, RF_fa, RF_phase ] = get_RF_type( acq )
% [ RF_id, RF_fa, RF_phase ] = get_RF_type( acq )
%
%   Determine properties from the RF pulses of a MRI_pulses object
%
%   INPUT:  acq,        MRI_pulses object with fields RFpulses and RFscalefactor
%   OUTPUT: (all N x 1)
%           RF_id,      index describing type of pulse: 
%                       1 = excitation
%                       2 = inversion
%                       3 = refocusing
%                       4 = fermi
%                       5 = spoiler
%           RF_fa       flip angle of pulse (rad)
%           RF_phase    phase of pulses (rad)
%
% Willem van Valenberg (2018) TU Delft

prec = 1e-4;

[ RF_fa, RF_phase ] = rot_mat2angles( acq.RFpulses );

idx_RFnan = isnan( acq.RFscalefactor );
idx_RFxy  = find( acq.RFpulses(3,3,:) == 1 );

idx_inv =  idx_RFnan & abs( RF_fa(:) - pi ) < prec; % inversion pulses
idx_ref = ~idx_RFnan & abs( RF_fa(:).*acq.RFscalefactor(:) - pi ) < prec; % refocusing pulses

RF_id                       = ones( size( RF_fa ) ); % excitation pulses
RF_id( :,:, idx_inv)        = 2;
RF_id( :,:, idx_ref)        = 3;

for k_rf = 1:numel( idx_RFxy )
    switch det( acq.RFpulses(3,3,idx_RFxy( k_rf )) )
        case {-1, 1}
            RF_id( :,:, idx_RFxy( k_rf ) )         = 4; % rotation pulse
        case 0
            RF_id( :,:, idx_RFxy( k_rf ) )         = 5; % spoiler pulse
        otherwise
            error( 'STRANGE RF PULSE' );
    end
end


end