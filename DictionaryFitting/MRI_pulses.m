classdef MRI_pulses
% MRI_pulses: Class that contains a pulse sequence
% This pulse sequence can be used in bloch_sim2.
% Use directly or through routines that already specify ready made pulse
% sequence parts. 
% Examples of such functions are: pulses_FISP, pulses_FSE, pulses_SPGR
%
% Created by Dirk Poot, TUDelft, 10-10-2013
    properties
        % General abbeviations:
        %  Nv = number of voxels
        %  Tp = number of time points at which RF pulses are applied
        %  Ts = number of time points at which net magnetisation is sampled.
        %  Tg = number of time points at which gradient is applied.
        %  Ns = number of spins per voxel
        
        % properties:
        
        % deltaT: Row vector with time between previous and current action
        % Be constent in the units, suggest ms.
        % 1 x (Tp+Ts+Tg) vector with time between previous and current action, (>=0)
        deltaT;   
                  
        % actionid: Type of action: 1=sample, 2=RFz, 4=RFx, 8=Gradient
        %  same size as deltaT. Bit wise set:
        % 0      (invalid action)
        %  +  1 : sample net magnetisation
        %  +  2 : apply RF pulse (around z axis, changes xz only)
        %  +  4 : apply RF pulse (around x axis, changes yz only)
        %  +  6 : apply general pulse, 3x3 matrix mixing all coordinates.
        %  +  8 : apply gradient pulse
        % Specified order of application (when multiple events are selected): 
        %       -  (update spins due to time after previous event) 
        %       -  apply RF pulse
        %       -  apply gradient pulse
        %       -  sample
        actionid; 
        
                  
        % RFpulses: 3 x 3 x Tp x (1 or Nv) spin rotation matrices for each of the RF pulses.
        %    Each 3 x 3 matrix RFpulses(:,:,i) should be a RF induced
        %    rotation matrix. However, note that this general formalism
        %    allows perfect (xy) spoiling to be included as a 'pulse': diag([0 0 1])
        RFpulses; 
        
        % RFscalefactor: Tp x (1 or Nv) matrix with the power that should be
        % applied to the corresponding RF pulses. So the actual pulse
        % applied is RFpulses(:,:,i)^(RFscalefactor(i) * RFscale(i) ) 
        % (where RFscale is the  MRI_spins RFscale factor.)
        % This is needed to define a unique root for large angle flips. 
        % if RFscalefactor(i) = nan : adiabatic pulse (or otherwise a pulse that is not 
        %                             affected by B1+ variations; e.g. since is abuses
        %                             RFpulses to do spoiling) 
        RFscalefactor;
        

        % gradpulses: 3 x Tg  gradient pulses applied along x, y, z coordinates
        %         rad/unit space (recommended spatial unit: mm)
        %         note that the values provided here are the
        %         time integral (over deltaT) of the applied spatial
        %         gradient (Tesla / mm) multiplied by the gyromagnetic
        %         ratio of the simulated spins (rad/T).  
        %   Although this does support spatial encoding, this property
        %   is mainly provided to include gradient spoiling and diffusion in the model.
        gradpulses; 
        
        % samplingduration: 1 x Ts,  vector with time duration (in unit time) of each 'sample' period
        %       Not used internally, but can be used to compute 'time
        %       efficiency' of sequences. Default = nan.
        %       The time efficiency of two pulse sequences p_1 and p_2 can be
        %       compared by computing:
        %       for each p_i : 
        %         a = bloch_sim2( spins, p_i , M_pseudoSteadyState);
        %         b = extract_gradient_of_a_wrt_parameters( a , ... )
        %         I = b'*b * sum(p_i.Tacq)/( size(a,1) * totalDuration(p_i) )
        %         CRLBperUnitTime = inv(I); % 'time efficiency' lower is better; in terms of 'variance' per unit time. 
        samplingduration; 
    end;
    methods
        function this = MRI_pulses(deltaT , actionid, RFpulses, RFscalefactor, gradpulses, samplingduration)
        % pulses = MRI_pulses(deltaT , actionid, RFpulses, RFscalefactor, gradpulses, samplingdurations)
        % Constructs an MRI_pulses object which can be used with bloch_sim2
        % INPUTS:
        %   deltaT  : 1 x (Tp+Ts+Tg)  
        %             vector with time between previous and current action, (>=0)
        %   actionid: Same size as deltaT. Bit wise set
        %             0      (invalid action/ no action )
        %             +  1 : sample net magnetisation
        %             +  2 : apply RF pulse (around z axis, changes xz only)
        %             +  4 : apply RF pulse (around x axis, changes yz only)
        %             +  6 : apply general pulse, 3x3 matrix mixing all coordinates.
        %                    The special cases +2 and +4 are automatically
        %                    identified, so always use +6 to specify RF rotations.
        %             +  8 : apply gradient pulse
        %           Specified order of application (when multiple events are selected): 
        %             0)  (update spins due to time after previous event) 
        %             1)  apply RF pulse
        %             2)  apply gradient pulse
        %             3)  sample     
        %   RFpulses: 3 x 3 x Tp x (1 or Nv)
        %             Each 3 x 3 matrix RFpulses(:,:,i) should be a RF induced
        %             rotation matrix. However, this general formalism also
        %             allows perfect (xy) spoiling to be included as a 'pulse': diag([0 0 1])
        %   RFscalefactor: Tp x (1 or Nv) matrix with the power that should be
        %             applied to the corresponding RF pulses. So the actual pulse
        %             applied is RFpulses(:,:,i)^(RFscalefactor(i) * RFscale(i) ) 
        %             (where RFscale is the  MRI_spins RFscale factor.)
        %             This is needed to define a unique root for large angle flips. 
        %       if RFscalefactor(i) = nan : adiabatic pulse (or otherwise a pulse that is not 
        %                      affected by B1+ variations; e.g. since is abuses
        %                      RFpulses to do spoiling) 
        %       default = 1
        %   gradpulses: optional argument,  3 x Tg   in rad/unit space (recommended spatial unit: mm)
        %               Specifies gradient pulses that are applied along x, y, z coordinates
        %               Although this does support spatial encoding, this property
        %               is mainly provided to include gradient spoiling in the model.
        %               The values provided are the time integral (over
        %               deltaT) of the applied spatial gradient (Tesla / mm) multiplied by the gyromagnetic ratio. 
        %                    
        % with the following abbeviations:
        %  Nv = number of voxels
        %  Tp = number of time points at which RF pulses are applied
        %  Ts = number of time points at which net magnetisation is sampled.
        %  Tg = number of time points at which gradient is applied.
        %  Ns = number of spins per voxel
        %
        % Created by Dirk Poot, TUDelft, 10-10-2013
            this.deltaT = deltaT;
            this.actionid = actionid;
            this.RFpulses = RFpulses;
            if nargin<4 || isempty(RFscalefactor)
                this.RFscalefactor = ones(size(RFpulses,3),size(RFpulses,4));
            else
                if size(RFscalefactor,1)~=size(RFpulses,3) ||size(RFscalefactor,2)~=size(RFpulses,4)
                    error('size of RFscalefactor inconsitent with RFpulses; see help for expected relation');
                end;
                this.RFscalefactor = RFscalefactor;
            end;
            this.gradpulses = [];
            if nargin>=5
                this.gradpulses = gradpulses;
            end;
            if nargin>=6
                this.samplingduration = samplingduration;
                if nnz(bitand( actionid , 1 ) ) ~= size(this.samplingduration,2)
                    error('The number of elements in sampling duration does not match the specified number of samples.');
                end;
            else
                this.samplingduration = nan(1, nnz(bitand( actionid , 1 ) )); 
            end;
            if nnz(bitand( actionid , 8 ) ) ~=size(this.gradpulses,2)
                error('the number of gradient pulses provided does not match the number of gradient pulses used');
            end;
        end;
        function this = horzcat( varargin )
            deltaTc = cell(1,nargin);
            actionidc = cell(1,nargin);
            RFpulsesc = cell(1,nargin);
            RFscalefactorc= cell(1,nargin);
            gradpulsesc = cell(1,nargin);
            samplingdurationc = cell(1,nargin);
            for k = 1 : nargin
                if isempty(varargin{k})
                    continue;
                end;
                if ~isa(varargin{k},'MRI_pulses')
                    error('MRI_pulses can only be concatenated with other MRI_pulses.');
                end;
                deltaTc{k} = varargin{k}.deltaT;
                actionidc{k} = varargin{k}.actionid;
                RFpulsesc{k} = varargin{k}.RFpulses;
                RFscalefactorc{k} = varargin{k}.RFscalefactor;
                gradpulsesc{k} = varargin{k}.gradpulses;
                samplingdurationc{k} = varargin{k}.samplingduration;
            end;
            this = MRI_pulses( [deltaTc{:}], [actionidc{:}], cat(3, RFpulsesc{:}),vertcat(RFscalefactorc{:}),[gradpulsesc{:}], [samplingdurationc{:}]);
        end;    
        function this = removeSampling(this, startT, endT)
            % Removes all sampling from this pulse sequence.
            % Call as:
            %    pulses = pulses.removeSampling();         % removes all
            %    pulses = pulses.removeSampling(tillTime); % removes all before and including tillTime
            %    pulses = pulses.removeSampling(fromTime, tillTime); % removes all betweem fromTime and tillTime
            % This might be usefull if this is a preparation pulse.
            if nargin==1
                this.actionid = bitand(this.actionid, 14);
                this.samplingduration = nan(1,0);
            else
                if nargin==2
                    endT = startT; startT = -inf;
                end;
                time = cumsum(this.deltaT);
                selrange = time>startT & time<endT;
                this.samplingduration( selrange( bitand(this.actionid, 1)) )=[];  % create a logical vector with true in the positions where the sampling should be deleted and delete those.
                this.actionid(selrange) = bitand(this.actionid(selrange), 14);
            end;
        end;
        function this = addSampling( this, samptime , samplingduration)
            % pulseq = pulseq.addSampling( sampletimes )
            % Adds the sampling of the (net) spin state at sampletimes.
            % Preserves the original sampling; if you want to overwrite the
            % old sampling, call removeSampling first.
            if nargin<3
                samplingduration = nan(1,numel(samptime));
            end;
            noldT = numel(this.deltaT);
            [ newTime, order ] = sort( [0 cumsum( this.deltaT )  samptime(:)'] );
            this.deltaT = diff(newTime);
            if order(1)~=1
                % all deltaT and samptime should be >=0
                error('inconsistency in ordered actions; all deltaT and sampltime should be >=0');
            end;
            order(1)=[]; % remove first element.
            newactionid = zeros(size(this.deltaT));
            sel = order<= noldT+1 ;
            newactionid( sel ) = this.actionid; % assuming valid sequence identical to:  this.actionid( order( sel ) -1 ); 
            newactionid(~sel ) = 1;
            this.actionid = newactionid;
            sampleidx = logical( bitand( this.actionid, 1) );
            temp_samplingduration = nan(1,nnz(sampleidx ));
            temp_samplingduration( sel( sampleidx) ) = this.samplingduration;
            temp_samplingduration(~sel( sampleidx) ) = samplingduration;
            this.samplingduration = temp_samplingduration;
        end;
        function [T, samplingduration] = samplingtimes( this )
            allT = cumsum( this.deltaT );
            T = allT( logical( bitand( this.actionid ,1) ) );
            samplingduration = this.samplingduration;
            if numel(T)~=numel(samplingduration)
                error('error (ASSERT) in MRI_pulses, each sample should have a samplingduration');
            end;
        end;
        function this = cleanup(this)
            % this.cleanup()
            % cleans the pulse sequence.
            % removes all zero actions, simplifies the RF pulse actions and checks for negative steps.
            
            this.actionid =  uint8(this.actionid);
            
            % simplify RF pulses (may remove them entirely if identity)
            RFind = find(bitand( this.actionid,6)~=0);
            if numel(RFind)~=size(this.RFpulses,3)
                error('The number of RF pulses provided does not match the number of RF pulses applied. This should not happen');
            end;
            trySimplifyRF = bitand( this.actionid(RFind),6)==6 ;
            map2ind = RFind( trySimplifyRF );
            testRotY = permute( this.RFpulses(:,:,trySimplifyRF)  , [3 1 2]);
            isRotY = testRotY(:,1,1)==testRotY(:,3,3) &  testRotY(:,1,3)==-testRotY(:,3,1) & ~any( testRotY(:,[2 4 6 8]),2) & testRotY(:,2,2)==1;
            isRotX = testRotY(:,2,2)==testRotY(:,3,3) &  testRotY(:,2,3)==-testRotY(:,3,2) & ~any( testRotY(:,[2 3 4 7]),2) & testRotY(:,1,1)==1;
            this.actionid( map2ind( isRotY ) ) = bitxor( this.actionid( map2ind( isRotY ) ), 4 ); % don't need doRFy
            this.actionid( map2ind( isRotX ) ) = bitxor( this.actionid( map2ind( isRotX ) ), 2 ); % don't need doRFx
            if any(isRotY & isRotX )
                % need to remove the RF pulse since the RF action is
                % removed. Expected to be uncommon (only for RF identity),
                % so do expensive removal within an if.
                map2ind_2 = find(trySimplifyRF);
                this.RFpulses(:,:,map2ind_2( isRotY &isRotX)) = [];
                this.RFscalefactor(map2ind_2( isRotY &isRotX),:)=[];
            end;
            
            % check number of gradients and remove zero gradients:
            gradpulseind = find(bitand(this.actionid,8)~=0);
            if numel(gradpulseind)~=size(this.gradpulses,2)
                error('The number of applied gradient pulses not equal to number of provided gradient pulses. This should not happen.');
            end;
            zerograd = ~any(this.gradpulses,1);
            if any(zerograd)
                % remove zero gradient pulses from actions and gradpulses
                % array:
                this.actionid( gradpulseind( zerograd ) ) = bitxor(this.actionid( gradpulseind( zerograd ) ) ,8);
                this.gradpulses = this.gradpulses(:,~zerograd);
            end;
            sampleid = logical(bitand(this.actionid,1));
            if nnz(sampleid)~=numel(this.samplingduration)
                error('error (ASSERT) in MRI_pulses, each sample should have a samplingduration');
            end;
            if any( (this.actionid( sampleid ) ~=1) & isfinite( this.samplingduration )) || ...
               any( this.deltaT(      sampleid    ) *2 < this.samplingduration ) || ...
               any( this.deltaT( find(sampleid(1:end-1))+1 ) *2 < this.samplingduration(1:end-sampleid(end)) )  % if sampling is last action, we don't check if it has suffiicent time after sampling (creates out of bound error if we would)
                warning('MRI_pulses:insuffucientTime','The time around at least one sample is insufficient for the specified samplingduration.'); 
            end;
            % remove empty actions
            del = (this.actionid ==0);
            del(end) = false; % dont remove an empty closing action as it indicates the end of this pulse sequence.
            for k = find(del)
                this.deltaT(k+1) = this.deltaT(k+1)+ this.deltaT(k);
            end;
            this.actionid = this.actionid(~del);
            this.deltaT = this.deltaT(~del);
            if any(this.deltaT<0)
                error('There is negative time specified between some actions. That is invalid.');
            end;
        end;
        function this = fuseGradients(this)
            if ~isdeployed
                warning( 'Fusing gradients with other actions. Only possible if gradient timing is not important' )
            end
            idx_grad = cumsum( bitand( this.actionid, 8 ) > 0 ); % give each applied gradient an index
            %idx_only_grad = find( this.actionid == 8 ) ; 
            
            idx_fuse = find( this.actionid(2:end) == 8  & this.actionid(1:end-1) ~= 1 ) + 1; % combine with previous action unless sampling
            for k = numel( idx_fuse ) : -1 : 1
                if bitand( this.actionid( idx_fuse( k ) - 1 ), 8 ) % if previous step already has gradient
                    this.gradpulses( :, idx_grad( idx_fuse( k ) - 1 ) ) = sum( this.gradpulses( :, idx_grad( idx_fuse( k ) - 1 : idx_fuse( k ) ) ), 2 );
                    this.gradpulses( :, idx_grad( idx_fuse( k ) ) ) = [];
                else
                    this.actionid( idx_fuse( k ) - 1 ) = bitor( this.actionid( idx_fuse( k ) - 1 ), 8 );
                end
                
                this.deltaT( idx_fuse( k ) + 1 ) = sum( this.deltaT( :, idx_fuse( k ) : idx_fuse( k ) + 1 ) );
                
                % remove current action
                this.actionid( idx_fuse( k ) ) = [];
                this.deltaT( idx_fuse( k ) ) = [];
            end
            
        end
        function [T] = totalDuration( this )
            % T = MRI_pulses.totalDuration
            % Returns the total duration of the pulse sequence.

            T = sum(this.deltaT);
        end;
        function [] = plot( this )
            % MRI_pulses.plot   OR   plot( pulses )
            % Plots the pulse sequence into the current axis.
            time = cumsum( this.deltaT );
            Tsamp = time( logical( bitand( this.actionid, 1 ) ) );
            TRF   = time( logical( bitand( this.actionid, 6 ) ) );
            Tgrad = time( logical( bitand( this.actionid, 8 ) ) );
            tmpRFscale = this.RFscalefactor; tmpRFscale(isnan(tmpRFscale))=1; % nan indicates adiabatic pulse that is not scaled by varying RF power.
            RFangle = acosd( max(-1+1e-6,min(1-1e-6, this.RFpulses(3,3,:) ) ) ).*permute(tmpRFscale,[3 4 1 2]);
            RFphase = angle( complex( this.RFpulses(1,3,:), this.RFpulses(2,3,:) )) * 180/pi;
            linehandles = plot( Tsamp, zeros(size(Tsamp)),'*',TRF,[ RFangle(:) RFphase(:)],'+',Tgrad,2*ones(size(Tgrad)),'.');
            legend(linehandles,'samples','RF angle (deg)','RF phase (deg)','gradient');
            xlabel('time')            
        end;
    end;
end