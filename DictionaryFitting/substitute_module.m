function [ p ] = substitute_module( p, mod, idx_mod )
% [ p ] =  substitute_pulse( p, rf_pulses, idx_rf_pulse, Gslice, Gprephase, Grephase, Gend )
%
% substitutes hard pulses in MRI_pulses objects by pulse envelopes with
% slice selective gradient
%
% NOTE that the sampled pulses have nonzero pulse duration in contrast to
% instantanteous pulses. This has consequences:
%  1. first event after RF pulse has minimal spacing of T/2
%  2. final event has its duration decreased by T/2
% this keeps TR and TE consistent with the hard pulse implementation
%
% INPUTS:
% p_in          : MRI_pulses object with (#FA) hard pulses
% rf_pulses     : cell array of rf pulses,
%               where rf_pulses{i} = [
%                   deltaT(i,:);    discretization time [ms]
%                   rf_amp(i,:)]    RF amplitude [rad]
%               note that pulse should be normalized to 1 deg rotation
% idx_rf_pulse  : (#FA or 1 x 1) array that gives the sampled rf_pulse
%               for each hard RF pulse
%                => hard_pulse(i) substituted by rf_pulses{ idx_rf_pulse(i) }
%               idx_rf_pulse(i) == 0 => don't substitute pulse i
% Gslice         : cell array of grad pulses used for slice selection.
%                 the gradient magnitude (rad/unit space) is divided over
%                 the pulse discretization
%                 Default = [0; 0; 8*pi]
% idx_grad_pulse: (#FA or 1 x 1) indices of used grad pulse:
%                   => hard_pulse(i) is played with grad_pulses{ abs( idx_grad_pulse(i) ) }
%                   Default = 0 (i.e. no slice selection gradient)
% frac_rewinder : fraction of the gradient pulse used for rewinding
%                   Default = 0 (i.e. no rewinder => balanced)
%
% Created by Willem van Valenberg (2018) TUDelft

%gamma   = 4.2577478;    % kHz/Gauss, gyromagnetic ratio

%actionid_rf     = find( bitand( p.actionid , 6) );
idx_rf          = cumsum( bitand( p.actionid , 6) ~= 0 );
idx_grad        = cumsum( bitand( p.actionid , 8) ~= 0 );

Tinit = totalDuration( p );
pinit = p;

% substitute into pulse sequence
for k = numel( idx_mod ): -1 : 1
        
    if idx_mod(k) > 0
        
        t_mod   = mod( idx_mod( k ) ).deltaT;
        rf_mod  = mod( idx_mod( k ) ).rf;
        g       = mod( idx_mod( k ) ).g;
        
        pos_rf  = find( rf_mod );
        pos_g   = find( sum( g, 1 ) );
        
        add_actionid = zeros( size( t_mod ) );
        
        if any( pos_rf ) % check if RF pulse included
            
            if  bitand( p.actionid(k) , 6) == 0
                error( ['NO RF PULSE PRESENT IN PULSESEQ AT INDEX ' num2str( k ) ] );
            end
            
            T     = sum( t_mod ); % module duration
            if isfield( mod( idx_mod( k ) ), 'eventT' )
                T_pre = mod( idx_mod( k ) ).eventT;
                T_post = T - mod( idx_mod( k ) ).eventT;
            else
                T_pre = T/2;
                T_post = T/2;
            end             
            
            % reduce interval before RF pulse
            if k == 1
                action_pre = numel( p.deltaT ); % remove from last interval to keep TR invariant
            else
                action_pre = k;
            end
            dt_pre  = p.deltaT( action_pre );
            if dt_pre < T_pre
                T_pre = T_pre - dt_pre; 
                p.deltaT( action_pre ) = 0;
                if p.actionid( action_pre - 1 ) == 0 && p.deltaT( action_pre - 1 ) > T_pre
                    p.deltaT( action_pre-1 ) = p.deltaT( action_pre-1 ) - T_pre;
                else
                    error( ['At index ' num2str( action_pre ) ' interval (' num2str( dt_pre ) ') too short for RF pulse (' num2str( T / 2 ) ')'] );
                end
            else
                p.deltaT( action_pre ) = dt_pre - T_pre;
            end
            
            % reduce interval after RF pulse
            dt_post = p.deltaT( k + 1 );
            if dt_post < T_post                
                error( ['At index ' num2str( k ) ' interval (' num2str( dt_post ) ') too short for RF pulse (' num2str( T / 2 ) ')'] );

            else
                p.deltaT( k + 1 ) = dt_post - T_post;
            end
            
            if isnan( p.RFscalefactor( idx_rf(k) ) );
                warning( ['Pulse ' num2str(idx_rf(k)) ' loses NaN scaling due to substitution'] );
            end
            
            [R_fa, R_phase] = rot_mat2angles( p.RFpulses(:,:,idx_rf(k)) );
            rf_deg  = (180/pi) * R_fa * exp( 1i* R_phase ) * rf_mod( pos_rf ) * p.RFscalefactor( idx_rf(k) );
            [add_RFpulses, add_RFscale]     = makeRFpulses( rf_deg );
            
            add_deltaT              = t_mod;
            if k > 1
            add_deltaT( 1 )         = add_deltaT( 1 ) + p.deltaT( action_pre ); % keep wait before original pulse
            end
            add_actionid( pos_rf )  = add_actionid( pos_rf ) + 6;
            add_actionid( pos_g )   = add_actionid(  pos_g ) + 8;
            add_grad                = g( :, pos_g );
            if  bitand( p.actionid(k) , 1) % sample at end if included
                add_actionid( end ) = add_actionid( end ) + 1; 
            end
            
            p.RFpulses   = cat( 3,   p.RFpulses(:,:, 1:idx_rf(k) - 1),...
                add_RFpulses,...
                p.RFpulses(:,:, idx_rf(k) + 1 : end ) );
            
            p.RFscalefactor    = [ p.RFscalefactor(1:idx_rf(k) - 1);...
                add_RFscale;...
                p.RFscalefactor(idx_rf(k) + 1 : end ) ];
            
        else % readout/gradient/empty events
            add_deltaT              = p.deltaT( k );
            add_actionid(:)         = p.actionid( k );
            
            if p.actionid(k) == 9
                error( 'only gradient and sampling should not happen' );
            end
            if p.actionid(k) == 8  % keep original gradient for spoiling
                add_grad            = [];
            elseif any( g )
                if p.actionid(k) == 1 % do sampling before gradient
                    add_deltaT          = [ p.deltaT( k ) 0 ];
                    add_actionid        = [ 1 8 ];
                    %add_grad            = [p.gradpulses( :, idx_grad( k ) ) g];
                    add_grad            = g;
                else % empty action
                    add_actionid        = 8;
                    add_grad            = g;
                end
            end
            
        end
        
        p.deltaT     = [     p.deltaT( 1:k-1 ),...
            add_deltaT,...
            p.deltaT( k + 1 : end ) ];
        
        p.actionid   = [     p.actionid( 1:k-1 ),...
            add_actionid,...
            p.actionid( k + 1 : end ) ];
        
        p.gradpulses = cat( 2, p.gradpulses( :, 1:idx_grad( k ) ),...
            add_grad,...
            p.gradpulses( :, idx_grad( k ) + 1:end ) );
                
    end
end

if totalDuration( p ) - Tinit > 1e-6
    error( 'pulse timing adjusted by module substitution' );    
end

end


function test

%%
R2       = p.RFpulses(:,:,k);
R_phase2 = angle( R2(3,2) - R2(2,3) + ( R2(1,3) - R2(3,1) ) * 1i );  % phase (rad) of RF pulse in xy-plane
R_fa2    = acos( ( sum( diag( R2 ) ) - 1 ) / 2 );                 % FA (rad) of RF pulse

end



