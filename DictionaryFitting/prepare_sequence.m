function [acq_out, rec_phase] = prepare_sequence( acq_in, prep, RFcycle, modfiles, mod_pos, doFuseGradients )
% acq_out = prepare_sequence( acq_in, prep, RFcycle, modfiles, mod_pos, doFuseGradients )
% 
%   INPUT
%
%     acq_in,     Px1 cell vector with MRI_pulses objects
%     prep,       multiple options:
%                     - cell vector of MRI_pulses of size acq_in, specifying the
%                     preparation for each acquisition 
%                     - cell vector of scalars, specifying the minimal preperation
%                     duration (ms) in which an acquisition is repeated without
%                     sampling
%                     - empty (default), no preperation part            
%     RFcycle,    phase increment in degrees between subsequent pulse sequences
%     modfile,    module to be implemented in pulse sequence, by either: 
%                     - struct containing fields: 
%                         deltaT, wait time [ms] before action (Nt x 1)
%                         rf,     complex RF amplitude [rad] scaled to 1 deg rotation (Nt x 1) 
%                         g,      gradient wavorms [rad/cm] (Nt x 1)
%                     - path to TOPPE file ('[..].mod')
%     mod_pos,    list that determines for each action of acq_in the index of
%                 the module it is replaced by (0 = don't replace)
%     doFuseGradient, specifies if the gradient actions are combined with
%                     before RF actions, only applicably if gradient timing is not important
% 
%     if any input is not a cell or size = 1, the given input is repeated for each acquisition
%
%
%   OUTPUT
%
%       acq_out,    Px1 cell vector with MRI_pulses object with RF cycling
%                   and module substitution
%       rec_phase,  Px1 cell vector with the phase [rad] of the receiver
%                   that should cancel out the RF cycling
% 
% Willem van Valenberg (2019) TU Delft

if nargin < 2 
    prep = {[]};
end
if nargin < 3
    RFcycle = {0};
end
if nargin < 4
    modfiles = {[]};
end
if nargin < 5
    mod_pos = {[]};
end
if nargin < 6 || isempty( doFuseGradients )
    doFuseGradients = false;
end

if ~iscell( prep )
    prep = repmat( {prep}, size( acq_in ) ) ;
end
if ~iscell( RFcycle )
    if isempty( RFcycle )
        RFcycle = {0};
    else
        RFcycle = num2cell(RFcycle);
    end
end
if ~iscell( modfiles )
    modfiles = repmat( {modfiles}, size( acq_in ) ) ;
end
if ~iscell( mod_pos )
    mod_pos = repmat( {mod_pos}, size( acq_in ) ) ;    
end

if numel( prep ) == 1
    prep = repmat( prep, size( acq_in ) );
end
if numel( RFcycle ) == 1
    RFcycle = repmat( RFcycle, size( acq_in ) );
end
if numel( modfiles ) == 1
    modfiles = repmat( modfiles, size( acq_in ) );
end
if numel( mod_pos ) == 1
    mod_pos = repmat( mod_pos, size( acq_in ) );
end

%% setup sequences

acq_out     = cell( size( acq_in ) );
rec_phase   = cell( size( acq_in ) );

for k_acq = 1 : numel( acq_in )
    
    % preparation
    if ~isempty( prep{ k_acq } )
        if isnumeric( prep{ k_acq } )
            npreplps        = ceil( prep{ k_acq } / totalDuration( acq_in{k_acq} ) );
            if npreplps > 0
                prep{ k_acq }   = removeSampling( [acq_in{ k_acq*ones(1,npreplps) }] );
            else
                prep{ k_acq } = [];
            end
        else
            if any( mod_pos{k_acq} )
                error( 'module substitution not implemented for arbitrary preperation' );
            end
        end
        acq_out{ k_acq } = [ prep{ k_acq } acq_in{k_acq} ];
    else
        acq_out{ k_acq } = acq_in{k_acq};
        npreplps         = 0;
    end            
        
    % RF cycling
    rec_phase{ k_acq } = zeros( nnz( bitand( acq_out{ k_acq }.actionid, 1 ) ), 1 );
    if ~isempty( RFcycle{ k_acq } ) 
        if RFcycle{ k_acq } ~= 0
            [ acq_out{ k_acq }, rec_phase{ k_acq } ] = include_RFcycle( acq_out{ k_acq }, RFcycle{ k_acq }*pi/180 );
        end        
    end
    
    % module substitution
    if ~isempty( modfiles{k_acq} )
        mod_pos_all = repmat( mod_pos{k_acq}, [1 npreplps + 1] );
        if isstruct( modfiles{k_acq} )
            acq_out{ k_acq } = substitute_module(       acq_out{ k_acq }, modfiles{k_acq}, mod_pos_all );
        else
            acq_out{ k_acq } = substitute_pulse_toppe(  acq_out{ k_acq }, modfiles{k_acq}, mod_pos_all );
        end
    end
    
    if doFuseGradients
        acq_out{ k_acq } = fuseGradients( acq_out{ k_acq } );
    end    
end

end