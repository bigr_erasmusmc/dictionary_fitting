function [RFpulses, RFscale] = makeRFpulses( flipangles_deg , isadiabatic)
% [RFpulses, RFscale] = makeRFpulses( flipangles_deg , isadiabatic)
%
% Creates the RFpulses matrices and RFscale factors for MRI_pulses from
% flipangles specified in degrees. 
%
% INPUTS:
%   flipangles_deg : a vector with complex valued flip angles. 
%                    The phase (angle) specifies the angle around which the
%                    RF pulse is applied and the magnitude specifies the
%                    degrees over which the spins are rotated. 
%   isadiabatic : default = false; optional logical scalar or vector of size(flipangles_deg).  
%                 If true the corresponding pulse is assumed to be adiabatic 
%                 (and hence completely insensitive to RF scaling that simulates
%                 B1+ inhomogeneity). This option is binary, to simulate
%                 approximately adiabatic pulses (etc), create a compound
%                 pulse that simulates it. 
% OUTPUTS:
%   RFpulses : 3 x 3 x numel(flipangles_deg) matrix with rotation matrices
%              that can be provided as input to MRI_pulses
%   RFscale : column vector with the scale factor that should be applied. 
%
%Created by Dirk Poot, Erasmus MC and TUdelft, 21-4-2015

flipangles = flipangles_deg / 180 *pi; %convert to radian
absflipangles = abs(flipangles);
RFscale = max(1,ceil( absflipangles/2)); % scale flipangle by integer, keep maximum angle to <2 rad ( = 114.6 degrees ) 
                                         % While 2 rad is arbitrary, limit angle should be below 180 degrees to have a unique matrix root/logaritm
                                         % as that is needed to properly apply the RF scaling (which is a matrix power of RFpulses).
                                         % Preference for (low) integer flip angle scaling as RF-scale often is exactly 1, which 
                                         % then causes integer matrix powers of RFpulses to be computed and these are faster 
                                         % and nummerically more accurate than non-integer powers. 
absflipangles = absflipangles./RFscale;
if nargin>=2 && any(isadiabatic)
    if numel(isadiabatic)==1
        isadiabatic = repmat(isadiabatic,size(RFscale));
    end;
    RFscale(isadiabatic) = nan;
    absflipangles(isadiabatic) = abs(flipangles(isadiabatic));
end;
angflipangles = angle(flipangles);

Rxzm = zeros(9,numel(absflipangles));
Rxzm([1 3 7 9],:) = real([1;1i;-1i;1]*exp(1i*absflipangles(:)')); % get sin and cos efficiently
Rxzm(5,:)=1; 
RFpulses = reshape(Rxzm,[3 3 numel(absflipangles)]);

hasangle = abs(angflipangles) > 1e-10 ;
if any(hasangle)
    hasangle = find(hasangle);
    Rxym = zeros(9,numel(hasangle));
    Rxym([1 2 4 5],:) = real([1;1i;-1i;1]*exp(1i*angflipangles(hasangle))); % get sin and cos efficiently
    Rxym(9,:)=1;  
    Rxym = reshape(Rxym,[3 3 numel(hasangle)]);

    for k=1:numel(hasangle)
    %     Rxy = rotationMatrix( [a(k); 0;0]);
    %     Rxz = rotationMatrix( [0;m(k);0]);
    %     if ~isequal(Rxy, Rxym(:,:,k)) || ~isequal(Rxz,Rxzm(:,:,k))
    %         error('unexpected difference');
    %     end;
        Rxy = Rxym(:,:,k);
        Rxz = RFpulses(:,:,hasangle(k));
        RFpulses(:,:,hasangle(k)) = Rxy'*Rxz*Rxy;
    end;
end;
RFscale = RFscale(:);