function dictionary_grid_estimate_recursive( filename, nprocs, procid )
%  dictionary_grid_estimate_recursive.M estimates the number of steps in
%  each parameter dimension for a given interpolation error.
%
%  as shown in: 
%   An Efficient Method for Multi-Parameter Mapping in Quantitative MRI
%   using B-Spline Interpolation.
%
%   van Valenberg W, Klein S, Vos FM, Koolstra K, van Vliet LJ, Poot DHJ. 
%   
%     Copyright (C) 2019 Willem van Valenberg
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or (at
%     your option) any later version.
% 
%     This program is distributed in the hope that it will be useful, but
%     WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see
%     <https://www.gnu.org/licenses/>.

if isdeployed
    nprocs = str2double(nprocs);
    procid = str2double(procid);
    
    load( filename );
    
else 
    
    load( filename );
        
end

%%
Nspins   = size( xyz, 1 );

tag_par         = strcmp( 'none', {pulseseq.param(:).scale} );
fix_par         = strcmp( 'val', {pulseseq.param(:).scale} );
transform_par   = find( ~tag_par & ~fix_par );

Npar            = numel( transform_par );
par_local       = procid : nprocs: Npar;
maxiter         = floor( log2(max_steps) ) + 1;

% p_val0          = cell( 1, numel( pulseseq.param ) );
% [p_val0{:}]     = pulseseq.param.range;
% [p_val0{fix_par}]= pulseseq.param( fix_par ).val;

p_val0          = {pulseseq.param.range};
for k = find( fix_par )
    p_val0{ k } =  pulseseq.param( k ).val;
end
p_val0(tag_par) = cellfun( @mean, p_val0(tag_par), 'UniformOutput', false );

gridsize        = zeros( 1, numel( par_local ) );
spline_error    = cell( numel( par_local ), maxiter-1 );

voxelblock      = max( 1, floor( maxSpin/Nspins ) );
Nslice          = numel( RFslice );

% setup pulse sequence
if ~isfield( pulseseq, 'RFcycle' )
    if isfield( pulseseq, 'spoiling' ) %backwards compatible
        if pulseseq.spoiling == 100
            pulseseq.RFcycle = 117;
        else
            pulseseq.RFcycle = 0;
        end
    else
        pulseseq.RFcycle = 0;
    end
end
if isfield( pulseseq, 'sub_mod' )
    [acq_sim, rec_phase] = prepare_sequence( pulseseq.acq, minpreptime, pulseseq.RFcycle, pulseseq.sub_mod, pulseseq.sub_idx );
else
    [acq_sim, rec_phase] = prepare_sequence( pulseseq.acq, minpreptime, pulseseq.RFcycle );
end

rec_correction = reshape( exp( -1i*cat( 1, rec_phase{:} ) ), [], 1 );

%%

for kp = transform_par( par_local )
    
    grid_file  = [ fileparts( filename ) '/grids/' pulseseq.param( kp ).name ];
    
    % determine midpoints intervals
    lb = pulseseq.param( kp ).range(1);
    ub = pulseseq.param( kp ).range(2);
    
    S_grid = [];
    for iter = 0:maxiter-1
        
        N           = 2^iter+1; % number of points within range (without boundary)
        gridpoints  = -edgeAtoms : N - 1 + edgeAtoms;
        
        p_val       = p_val0;
        switch pulseseq.param( kp ).scale
            case 'linear'
                c = ( ub - lb ) / ( N - 1 );
                p_val{kp} = lb + c* gridpoints;
            case 'log'
                c = ( ub / lb ) ^ ( 1 / (N - 1) );
                p_val{kp} = lb * c.^gridpoints;
            otherwise
                error('Unknown scale');
        end
        
        p_dim       = cellfun( @numel, p_val ); % dimensions of grid this iteration
        pidx_todo  = cellfun( @(p) 1:numel(p), p_val, 'UniformOutput', false ); % indices of atoms to be calculated this iteration
        if iter > 0
            pidx_todo{kp} = ( 2 - mod(edgeAtoms, 2) : 2 : numel( p_val{kp} ) ); % do not recalculate atoms
        end
        
        pidx_done                           = cellfun( @(p) 1:numel(p), p_val, 'UniformOutput', false );
        pidx_done{ kp }( pidx_todo{ kp } )  = []; % remove indices calculated this iterarion
        pidx_done_grid                      = pidx_done;
        pidx_done_grid{ kp }                = 1 + ceil( edgeAtoms / 2 ) : size( S_grid, 1 + kp ) - ceil( edgeAtoms / 2 ); % remove outer edges that are not part of new grid
        pidx_todo_int                       = pidx_todo; % positions in interior S_new calculated this iteration
        pidx_todo_int{ kp }                 = 2 + edgeAtoms : 2 : p_dim( kp ) - edgeAtoms;
        
        if exist( [ grid_file '_iter' num2str( iter ) '.mat'], 'file' )
            if verbmode
                fprintf('%s, ITERATION %d: GRID EXISTS. LOADING... \n', pulseseq.param( kp ).name, iter )
            end
            load( [ grid_file '_iter' num2str( iter ) '.mat'] );
        else
            %if doCalculate( iter + 1 )
            if verbmode
                fprintf('%s, ITERATION %d: TOTAL INTERIOR POINTS: %d, CALCULATING VALUES: %s %s \n', pulseseq.param( kp ).name, iter, N, num2str( p_val{kp}( pidx_todo{kp} ), '% 5.2f' ),  pulseseq.param( kp ).units )
            end
            
            pidx_grid      = cell( 1, numel( pidx_todo ) );
            [pidx_grid{:}] = ndgrid( pidx_todo{:} );
            
            Nvox = prod( [cellfun( @numel, pidx_todo ) ] );
            
            M = zeros( Nimg, 3, Nvox );
            for k_a = 1:voxelblock:Nvox
                
                idx_vox = k_a:min( k_a + voxelblock - 1, Nvox );
                
                pidx_list      = cellfun( @(p) p( idx_vox ).', pidx_grid, 'UniformOutput', false );
                p_list         = cellfun( @(p, pidx) reshape( p(pidx), 1, [] ), p_val, pidx_list, 'UniformOutput', false  );
                
                spins           = MRI_spins( p_list{ 1:4 }, [], Nspins, xyz, p_list{ 6 } );
                % evaluate signals
                switch spinDistribution
                    case {0, 1, 4} % Cauchy distribution over whole slice
                        spins           = shuffleSpins( spins, spin_idx );
                    case 2 % Cauchy distribution same in each slice position
                        [B0w, B0]       = T2prime_spins( p_list{ 3 }, Nspins/Nslice, p_list{ 4 } );
                        spins.B0        = repmat( B0, Nslice, 1 );
                        spins.B0w       = repmat( B0w, Nslice, 1 );
                    otherwise
                        error('UNKNOWN SPIN DISTRIBUTION' );
                end
                
                % set slice profile
                spins = setSliceprofile( spins, RFslice );
                
                % do Bloch simulations
                if 1
                    Mslice = cell(numel( acq_sim ), 1);
                    for k_acq = 1 : numel( acq_sim )
                        Mslice{k_acq} = bloch_sim2( spins, acq_sim{k_acq}, [] );
                    end;
                else
                    if isfield( pulseseq, 'sub_mod' )
                        Mslice = simulate_signal( spins, pulseseq, minpreptime, pulseseq.sub_mod, pulseseq.sub_idx );
                    else
                        Mslice = simulate_signal( spins, pulseseq, minpreptime );
                    end
                end
                Mslice = vertcat( Mslice{:} );
                
                % sum over slice
                for k_slice = 1:numel( idx_vox )
                    M(:,:, idx_vox(k_slice) ) = mean( Mslice(:,:, (k_slice-1)*Nslice + ( 1:Nslice ) ), 3 );
                end
            end
            
            %convert transverse magnetization to complex number
            S_eval = permute( bsxfun( @times, M(:,1,:) + 1i*M(:,2,:), rec_correction ), [1 3 2] );
            
            % combine with previous calculated atoms
            S_new   = zeros( [Nimg, p_dim] );
            if ~isempty( S_grid )
                S_new( :, pidx_done{:} )   = S_grid( :, pidx_done_grid{:} );
            end
            S_new( :, pidx_todo{:} )   = reshape( S_eval, [ Nimg, cellfun( @numel, pidx_todo ) ] );
            
            save( [ grid_file '_iter' num2str( iter ) '.mat'], 'S_new' );
        end
        
        %% calculate spline interpolated signal at halfpoints
        if iter > 0
            dim_dict    = size( S_grid );
            dim_par     = dim_dict( 2:end );
            dim_fix     = setdiff( 1:numel(dim_dict), kp + 1 );
            
            if bsplineord > 1 % prefilter dictionary
                % define spline
                xval = floor(-bsplineord/2) : ceil(bsplineord/2);
                [bspline, dbsplinedx] = bsplinefunction(bsplineord, xval);
                
                % create matrix for deconvolution
                conv_matrix = toeplitz( [ bspline( ceil(numel(xval)/2):end ) zeros(1, dim_par( kp ) - ceil(numel(xval)/2))], [bspline(ceil(numel(xval)/2):end) zeros(1, dim_par( kp ) - ceil(numel(xval)/2))]);
                
                switch boundary_condition
                    case 0 % zero extend
                        % dont need to update conv_matrix
                    case 1 % mirror boundary conditions
                        for x = 1 : dim_par( kp )
                            for yidx = 1 : numel( xval );
                                y  = x + xval( yidx );
                                if y < 1
                                    y = -y+1;
                                    conv_matrix(x,y) = conv_matrix(x,y) + bspline(yidx);
                                elseif y > dim_par( kp )
                                    y = 2*dim_par( kp )-y+1;
                                    conv_matrix(x,y) = conv_matrix(x,y) + bspline(yidx);
                                end;
                            end;
                        end;
                    case 2 % periodic domain:
                        nneg = sum(xval <0);
                        addd = toeplitz([bspline(1) zeros(1,nneg-1)], bspline(1:nneg));
                        conv_matrix( 1 : size(addd,1), end-size(addd,2)+1 : end ) = conv_matrix( 1 : size(addd,1), end-size(addd,2)+1 : end ) + addd;
                        conv_matrix( end-size(addd,2)+1 : end, 1 : size(addd,1) ) = conv_matrix( end-size(addd,2)+1 : end, 1 : size(addd,1) ) + addd';
                    case {4 5 6} % match derivative of one but last point.
                        % NOTE: not interpolating at last element of dictionary, but improving smoothness.
                        % Numerical derivative to match at dictionary(2) and dictionary(end-1)
                        % obs = [dictionary(3)-dictionary(1);dictionary(2:end-1);dictionary(end)-dictionary(end-1)]
                        %     == [-.5 0 .5 0 ... ;
                        %          0 1 0 0 ...
                        %          ...continue diagonal...
                        %          ... 0 -.5 0 .5]  * dictionary.
                        %             if bsplineorder~=2
                        %                 error('currently  boundary_condition == 2 only supported for  bsplineorder==2' );
                        %             end;
                        ddict = [-.5 0 .5];
                        if bsplineord==3
                            % set second derivative:
                            ddict = [1 -2 1];
                            dbsplinedx = [0 -1 2 -1 0];
                        end;
                        idx1 = xval+1+1;
                        idx1(idx1<=0) = idx1(idx1<=0) + size( conv_matrix,2);
                        conv_matrix(1,idx1) = -dbsplinedx;
                        idxe = xval-1+size( conv_matrix,2);
                        idxe(idxe>size( conv_matrix,2)) = idxe(idxe>size( conv_matrix,2)) - size( conv_matrix,2);
                        conv_matrix(end,idxe) = -dbsplinedx;
                        dictToObs = eye(size(conv_matrix));
                        dictToObs(1,1:3) = ddict;
                        dictToObs(end,end-2:end) = ddict;
                        conv_matrix = dictToObs\conv_matrix ;
                    otherwise
                        error('unsupported value for boundary_condition provided.');
                end;
                
                deconv_matrix = inv( conv_matrix );
                
                lin_idx             = 1:prod( dim_dict( dim_fix ) );
                idx_sub             = cell( numel(dim_dict), 1);
                [idx_sub{dim_fix}]  = ind2sub( dim_dict( dim_fix ), repmat( lin_idx, dim_par( kp ), 1 ) );
                idx_sub{kp+1}       = repmat( [1:dim_par( kp )]', 1, numel(lin_idx) );
                idx_ind             = sub2ind( dim_dict, idx_sub{:} );
                
                S_grid( idx_ind )   = deconv_matrix * S_grid( idx_ind );
            end
            
            % do interpolation
            sample_points       = permute( edgeAtoms + .5 : size( S_grid, kp + 1 ) - edgeAtoms - 1.5, [1 3 2] );
            
            dim_samples         = dim_dict;
            dim_samples(kp+1)   = numel( sample_points );
            
            %S_spline    = zeros( Nimg, numel( sample_points ), 2^( numel( p_val0 ) - 1 ) );
            S_spline    = zeros( dim_samples );
            
            T_samp      = cell( numel( p_val0 ) + 1, 1);
            T_samp(:)   = {':'};
            for k = 1:2^( numel( transform_par ) - 1 ) % iterate over different edges
                [T_samp{ dim_fix( 2:end )  }] = ind2sub( dim_dict( dim_fix( 2:end ) ), k );
                S_single_edge = permute( S_grid( T_samp{:} ), [1 kp + 1 , dim_fix( 2:end ) ] );
                if boundary_condition < 4
                    S_spline( T_samp{:} ) = TransformNDbspline( S_single_edge, sample_points, [], [nan, numel( sample_points )] , [], bsplineord, [], [], boundary_condition );
                else
                    S_spline( T_samp{:} ) = TransformNDbspline( S_single_edge, sample_points, [], [nan, numel( sample_points )] , [], bsplineord, [], [], boundary_condition-4 );
                end
            end
            
            % determine error
            S_calc    = S_new(:, pidx_todo_int{:});
            
            if use_T1T2_constrain % only evaluate error at T1 >= T2
                p_val_calc      = cellfun( @( v,i ) v(i), p_val, pidx_todo_int, 'UniformOutput', false );
                p_val_calc_grid = cell( 1, Npar, 1 );
                [p_val_calc_grid{:}] = ndgrid( p_val_calc{:} );
                idx_error = p_val_calc_grid{1} >= p_val_calc_grid{2}; % only calculate error over T1>=T2
            else
                idx_error = true( dim_samples( 2:end ) );
            end
            
            S_error   = sqrt( sum( abs( S_calc( :, idx_error )  - S_spline( :, idx_error ) ).^2 , 1 )/Nimg );
            if useRelError
                S_error = S_error ./ sqrt( sum( abs( S_calc( :, idx_error )  ).^2 , 1 )/Nimg );
            end
            
            if verbmode
                fprintf('MAXIMAL ERROR AT MIDPOINTS WITH %d ATOMS: %f \n', numel( sample_points )+1, max( S_error(:) ) );
            end
            
            spline_error{ kp, iter } = S_error;
            
            if 1 % show where largest error is:
                
                %S_error   = ( sum( abs( S_calc( :, idx_error )  - S_spline( :, idx_error ) ).^2 , 1) ./ sum( abs( S_calc( :, idx_error )  ).^2 , 1) ).^.5;
                
                num_printed = min( 32, numel( S_error ) );
                
                [val_max, i_max] = sort( S_error(:), 'descend' );
                i_pos = cell( ndims( idx_error ), 1 );
                [i_pos{:}] = ind2sub( size( idx_error ), i_max(end) );
                
                fprintf('ITER %d, MEAN ERROR: %e, MIN ERROR: %e, POSITION: %s \n', iter-1, mean( S_error(:) ), min( S_error(:) ), num2str( cat( 2, i_pos{2:end} ) ) );
                for iprint = 1:num_printed
                    [i_pos{:}] = ind2sub( size( idx_error ), i_max(iprint) );
                    fprintf('     MAX ERROR %d: %e, POSITION: %s \n', iprint , val_max( iprint ), num2str( cat( 2, i_pos{:} ) ) );
                end
            end
        end
        
        S_grid = S_new;
    end
    if verbmode
        fprintf('\n');
    end
end

%error_file = [ fileparts( filename ) '/errors/s' num2str( bsplineord ) '_b' num2str( boundary_condition ) ] ;
error_file = sprintf( '%s/errors/s%d_b%d', fileparts( filename ), bsplineord, boundary_condition );
if use_T1T2_constrain
    error_file = [error_file '_T1T2c']
end
if useRelError
    error_file = [error_file '_rel']
else
    error_file = [error_file '_abs']
end

if verbmode
    fprintf('SAVING RESULTS TO: %s \n', error_file );
end
mkdir( fileparts( error_file ) );
save( error_file, 'gridsize', 'spline_error', 'boundary_condition', 'bsplineord', 'edgeAtoms', 'max_steps', 'thresh_error', 'useRelError');

end

