function [pulses] = pulses_MRF( TR, FA, TE, TI, gradpulses, readduration )
% [RFpulses, RFtimes, sampletimes] = pulses_MRF_FISP( TR, FA, TE, TI, spoiling_gradient, readduration)
%
% INPUTS:
%  TR               : (1 or N) x 1 vector with repetition time of the pulses
%  FA               : (1 or N) x 1 vector with flip angle(s), specified in degrees. 
%  TE               : (1 or N) x 1 vector with time after each RF pulse at which the magnetization is sampled. 
%                       Default: TR*.4
%                       if echoTime == 'empty' : no sampling is performed. 
%  TI               : scalar with the time between the inversion prepulse and
%                       the first excitation pulse 
%                       Default: 0
%                       if TI < 0, no inversion prepulse
%  spoiling_gradient: (1 or N) x 3 vector with the gradient pulse used to
%                       spoil the magnetization ( in rad/unit space)
%                       Default: [0 0 8*pi] 
%  readduration     : default = 1, duration of sampling (for time efficiency
%                       computations). Time duration of sampling the k-space. 
%
% Created by Willem van Valenberg, TUDelft
% 02-02-2018

N = max( numel( TR ), numel( FA ) );

if numel( FA ) == 1
    FA = FA( ones( N, 1 ) );
end
if numel( TR ) == 1
    TR = TR( ones( N, 1 ) );
end
if nargin>=3 && ~isempty(TE)
    if numel( TE ) == 1
        TE = TE( ones( N, 1 ) );
    end
else
    TE = .4* TR;
end
if ischar(TE) && isequal(TE,'empty')
    TE = [];
end;
if any(TE<=0)||any(TE>=TR)
    error('echoTime should be between 0 and TR');
end;
if nargin>=5 && ~isempty(gradpulses)
    if size( gradpulses, 1 ) == 1
        gradpulses = repmat( gradpulses, N, 1 );
    end
else
        gradpulses = repmat( [0 0 8*pi], N, 1 );
end
if nargin<6 || isempty(readduration)
    readduration = 1;
else
    readduration = reshape( readduration(:)*ones(1,N) , 1, []);
end;

[ RFpulses, RFscale ] = makeRFpulses( FA.' );

RFpulses( (RFpulses+17)-17 ==0 ) = 0; % remove some roundoff errors

if nnz( gradpulses ) > 0 % spoiled sequence

    Tspoil      = TR - TE;
    actionid    = [ repmat( [ 6 1 8 ], 1, N ) 0 ] ; % RF + sampling + spoiling
    deltaT      = [ reshape( [ [ 0; Tspoil(1:end-1)/2 ], TE, Tspoil/2 ].', 1, [] ) Tspoil( end )/2 ];

    if nargin>=4 && ~isempty( TI ) % add adiabatic prepulse 
        actionid    = [ 6 8 0 actionid ];
        deltaT      = [ 0 0 TI deltaT ];
        RFpulses    = cat( 3, diag([-1 1 -1]), RFpulses );
        RFscale     = [nan; RFscale];
        gradpulses  = [0 0 8*pi ; gradpulses ];
    end    
else % balanced sequence
    actionid    = [ repmat( [ 6 1 ], 1, N ) 0 ]; % RF + sampling
    deltaT      = [ 0 reshape( [ TE, TR - TE ].', 1, [] ) ];
    gradpulses  = [];

    if nargin>=4 && ~isempty( TI ) % add adiabatic prepulse
        actionid    = [ 6 0 actionid ];
        deltaT      = [ 0 TI deltaT ];
        RFpulses    = cat( 3, diag([-1 1 -1]), RFpulses );
        RFscale     = [nan; RFscale];
    end    
end

pulses = MRI_pulses( deltaT, actionid, RFpulses, RFscale,  gradpulses.', readduration );
