%  CMD_DICTIONARY_GENERATION.M used to setup the parameters for generating
%  the signal dictionary and the command files for the cluster  
%   
%     Copyright (C) 2018 Willem van Valenberg
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or (at
%     your option) any later version.
% 
%     This program is distributed in the hope that it will be useful, but
%     WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see
%     <https://www.gnu.org/licenses/>.    

addpath(...
    genpath('../Mapping'),... 
    genpath('../Tools') );

if isunix
    mcc('-m','dictionary_generation.m','-R','-nojvm','-R','-singleCompThread','-R','-nosplash', '-a', 'MRI_pulses')
    mcc('-m','dictionary_grid_estimate_recursive.m','-R','-nojvm','-R','-singleCompThread','-R','-nosplash', '-a', 'MRI_pulses')
    mcc('-m','dictionary_reduction.m','-R','-nojvm','-R','-singleCompThread','-R','-nosplash')
end

%% SETUP DICTIONARY GENERATION

edgeAtoms       = 1;

% SETUP DEFAULT DISCRETIZATION GRID
param = struct('name',{},'units',{},'range',{},'steps',{},'scale',{}, 'val', {} );
% name : parameter name
% range = [lb ub] : lower and upper bound of discretization
% steps : minimal number of steps between upper and lower bound
% scale : scaling of steps, either 'linear' or 'log'
% val   : give specific discretization points. If val is nonempty:
%           - steps = numel( val )
%           - scale = 'val'
%           - range = [ min(val) max(val) ]

param(1).name  = 'T1';
param(1).units = 'ms';
param(1).range = [5 6000]; %ms
param(1).steps = 5;
param(1).scale = 'log';
param(1).val   = [];

param(2).name  = 'T2';
param(2).units = 'ms';
param(2).range = [5 2000]; %ms
param(2).steps = 4;
param(2).scale = 'log';
param(2).val   = [];

param(3).name  = 'T2prime';
param(3).units = 'ms';
param(3).range = [10 1000]; %ms
param(3).steps = 3;
param(3).scale = 'log';
param(3).val   = 1000;

param(4).name  = 'B0';
param(4).units = 'rad/ms';
param(4).range = [-100 100]*2*pi/1000; %rad/ms
param(4).steps = 16;
param(4).scale = 'linear';
param(4).val   = 0;

param(5).name  = 'PD';
param(5).name  = 'Mz0';
param(5).units = 'a.u.';
param(5).range = [500 1500]; %a.u
param(5).steps = 1;
param(5).scale = 'none';
param(5).val   = [];

param(6).name  = 'B1+';
param(6).units = 'a.u.';
param(6).range = [.5 1.5]; %a.u
param(6).steps = 3;
param(6).scale = 'linear';
param(6).val   = [];

param(7).name  = 'phi0';
param(7).units = 'rad';
param(7).range = [-pi pi]; %rad
param(7).steps = 1;
param(7).scale = 'none';
param(7).val   = [];

% CREATE DISCRETIZATION GRID
p_disc = cell( numel(param), 1);
for k2 = 1:numel(param)
    
    if ~isempty( param(k2).val )
        param(k2).val   = sort( param(k2).val(:), 'ascend' ).';
        param(k2).steps = numel( param(k2).val );
        param(k2).scale = 'val';
        
        p_disc{k2} = param(k2).val;
        warning(['CUSTOM DISCRETIZATION ' param(k2).name ' PARAMETER']);
    else
        lb =  param(k2).range(1);
        ub =  param(k2).range(2);
        N  =  param(k2).steps;
        
        switch param(k2).scale
            case 'linear'
                c = ( ub - lb ) / ( N - 1 );
                p_disc{k2} = lb + c*( -edgeAtoms : N-1+edgeAtoms);
            case 'log'
                c = ( ub / lb ) ^ ( 1 / (N - 1) );
                p_disc{k2} = lb * c.^( -edgeAtoms : N-1+edgeAtoms);
            case 'none'
                p_disc{k2} = (ub+lb)/2; %take mean
            otherwise
                error('unknown scale for steps');
        end
    end
end

%% LOAD PULSESEQ

info_scan               = struct;
info_scan.t_prep        = 5000;
info_scan.readout.N     = [ 128 128 1 ];
info_scan.readout.L     = [ 22 22 1 ];
info_scan.tipdown.dz    = .5;

dz_mult = 2; % z positions sampled from dz_mult*slicewidth cm range
vox_size= [ info_scan.readout.L(1:2)./info_scan.readout.N(1:2), dz_mult*info_scan.tipdown.dz ];

pulseseq_all        = load_pulse_sequences( info_scan );

for k = 1:numel( pulseseq_all )
    fprintf( 1, ' %2d : %s\n', k, pulseseq_all(k).name );
end
pulseseq_idx        = input( 'CHOOSE PULSE SEQUENCE: ' );
fprintf( 1, '\n');
pulseseq0            = pulseseq_all( pulseseq_idx );
pulseseq0.param      = param;

%% ADD SLICE SELECTIVE GRADIENT
pulseseq = pulseseq0;

path_rf  = [ '../Data/'];
Nspinsperslice      = 1e3 + 6;
spinDistribution    = 1;

pulseseq.mod_samples.slice_profile     = 7;

pulseseq.acq = cellfun( @cleanup, pulseseq.acq, 'UniformOutput', false );

[ pulseseq.sub_idx, pulseseq.sub_mod, slice_width ] = setup_pulse_substitution( pulseseq.acq, path_rf, pulseseq.mod_samples );

if any( [slice_width(:)] < vox_size(3) )
    warning( 'not all reduced pulses optimized for slicewidth: %f cm', vox_size(3) )
end

RFslice     = 1;
xyz         = permute( bsxfun( @times, rand( Nspinsperslice, 3 ) - .5, vox_size ), [1 3 2] ) ;

fprintf( 'spin positions:\n x in +-%f cm\n y in +-%f cm\n z in +-%f cm\n', vox_size/2 )

spin_idx    = randperm( Nspinsperslice*numel( RFslice ) );

%% estimate number of steps in each dimension

max_steps           = 2^4 + 1;
thresh_error        = 10^-10;
bsplineord          = 2;
boundary_condition  = 5;
verbmode            = true;
fuseGradients       = false;
useRelError         = false;
use_T1T2_constrain  = 1;

transform_par   = find( strcmp( 'none', {pulseseq.param(:).scale} ) + strcmp( 'val', {pulseseq.param(:).scale} ) == 0 );
nprocs          = numel( transform_par );
maxSpin         = 2*10^6;
procid          = 1;
comment         = '';

filename = ['../output/grid_tests/' pulseseq.name comment '/info' ]

if exist( [filename '.mat'], 'file' )
    str = input( ['FILE ' filename ' ALREADY EXISTS. OVERWRITE? (y/n) \n'], 's');
else
    str = 'y';
end

if strcmp( str, 'y')
    mkdir( [ fileparts( filename ) '/grids' ] );
    mkdir( [ fileparts( filename ) '/errors' ] )
    mkdir( [ fileparts( filename ) '/figures' ] )
    
    Nimg     = sum( cellfun( @(X) nnz( X.actionid == 1 ), pulseseq.acq ) );
    minpreptime = info_scan.t_prep;
    save( filename, 'pulseseq', 'comment', 'minpreptime', 'xyz', 'spinDistribution', 'edgeAtoms', 'max_steps', 'maxSpin', 'thresh_error', 'bsplineord', 'boundary_condition', 'Nimg', 'verbmode', 'RFslice', 'spin_idx', 'fuseGradients', 'useRelError', 'use_T1T2_constrain');
end

%% SEND RESOLUTION ESTIMATION COMMAND TO CLUSTER

if isunix
    req_memory = 3; %G, 4 for MRF, 3 for normal
    proc_id     = [];
    
    cmdfile = ['/scratch/wvanvalenberg/output/cmdfiles/grid_res_' pulseseq.name comment '.txt'];
    [~, cmdname] = fileparts( cmdfile );
    log_file  = ['/scratch/wvanvalenberg/output/log_grid_estimate/' cmdname '/'];
    
    if exist( cmdname, 'file' )
        cmd_idx = input(['COMMAND ' cmdname ' ALREADY EXISTS. OVERWRITE? (y/n) \n'], 's');
    else
        cmd_idx = 'y';
    end
    
    % create tasklist
    if any( strcmp( cmd_idx, {'Y', 'y'} ) )
        cmdID = fopen( cmdfile,'w');
        for k2 = 1:nprocs
            fprintf(cmdID,'module add mcr; ./dictionary_grid_estimate_recursive %s %d %d \n', filename, nprocs, k2);
        end
        fclose(cmdID);
    else
        error('RENAME DICTIONARY COMMAND \n');
    end    
    
    if ~isempty( proc_id )
        system( sprintf( 'bigrsub -q week@node%03d.cm.cluster -R %dG -l %s "./dictionary_grid_estimate_recursive %s %d %d" \n', ...
            proc_id, req_memory, log_file, filename, nprocs, k2 ) );
    else
        system( sprintf( 'bigrsub -q week -R %dG -t "%s" -l %s \n', ...
            req_memory, cmdfile, log_file ) );
    end
else
    dictionary_grid_estimate_recursive( filename, 1, 1 )
end

%% CREATE DICTIONARY GENERATION COMMAND

description     = '';

nprocs          = 1;
procid          = 1:nprocs;
sim_prec        = 'single'; % precision of signals
transform_par   = strcmp( 'none', {param(:).scale} ) + strcmp( 'val', {param(:).scale} ) == 0;
Natoms          = prod( [ param.steps ] + 2 * transform_par * edgeAtoms );
Nimg            = sum( cellfun( @(X) nnz( X.actionid == 1 ), pulseseq.acq ) );

maxSpin         = 5*10^5;
maxSize         = 10^6;
blocksize       = min( 1000, ceil( Natoms / nprocs ) );
blocksperproc   = ceil( Natoms / ( blocksize * nprocs) );
req_memory      = Nimg*Natoms*8*10^(-9);

% find free subfolder
namebase = datestr(now,29);
idxfolder = 1;
destfolder = ['../output/' namebase '/dict' num2str(idxfolder) '/'];
while exist( destfolder, 'dir')
    idxfolder = idxfolder + 1;
    destfolder = ['../output/' namebase '/dict' num2str(idxfolder) '/'];
end

fprintf(['OUTPUT IN: ' destfolder '\n \n']);

%% SEND DICTIONARY GENERATION COMMAND TO CLUSTER
cmdname = 'dictionarygeneration.txt';

mkdir(destfolder);
save( [destfolder 'disc_settings'], 'blocksize', 'RFslice', 'spinDistribution', 'spin_idx', 'xyz', 'Nimg', 'sim_prec', 'param', 'edgeAtoms', 'p_disc', 'pulseseq', 'minpreptime', 'maxSpin', 'description' );

if isunix
    log_file  = ['/scratch/wvanvalenberg/output/log_dictionary_generation/' namebase '/test' num2str(idxfolder) '/' ];
    req_memory = 3; %G
    proc_id     = []; % send to specific node
    
    sumID = fopen([destfolder 'summary_dictionary_generation.txt'], 'w');
    fprintf(sumID, summarytext);
    fclose(sumID);
        
    if exist( cmdname, 'file' )
        cmd_idx = input(['COMMAND ' cmdname ' ALREADY EXISTS. OVERWRITE? (y/n) \n'], 's');
    else
        cmd_idx = 'y';
    end
    
    % create tasklist
    if any( strcmp( cmd_idx, {'Y', 'y'} ) )
        cmdID = fopen( cmdname,'w');
        for k2 = 1:nprocs
            fprintf(cmdID,'module add mcr; ./dictionary_generation %s %d %d \n', destfolder, nprocs, k2);
        end
        fclose(cmdID);
    else
        error('RENAME DICTIONARY COMMAND \n');
    end
        
    if ~isempty( proc_id )
        system( sprintf( 'bigrsub -q week@node%03d.cm.cluster -R %dG -t "%s" -l %s \n', ...
            proc_id, req_memory, cmdname, log_file ) );
    else
        system( sprintf( 'bigrsub -q week -R %dG -t "%s" -l %s \n', ...
            req_memory, cmdname, log_file ) );
    end
else
    if input( 'run locally?\n' )
        dictionary_generation(destfolder, 1, 1);
    end    
end

%% DICTIONARY TIME DIMENSION REDUCTION WITH SVD
cmdname = 'cmdfiles/dictionary_reduction.txt';
task_name = 'dict_red';
path_dict = [destfolder pulseseq.name '/'];

Ncoef               = 30;
meas_sel            = [];   % determine which measurements to include in fitting

if ~isempty( Ncoef )
    task_name = [task_name '_svd' num2str( Ncoef ) ]
end;

if isunix;
    log_file  = ['../../output/log_dictionary_reduction/' ];
    req_memory = 5; %G
    proc_id     = [];
    if isunix
        if ~isempty( proc_id )
            system( sprintf( 'bigrsub -q week@node%03d.cm.cluster -N %s -R %dG -l %s "./dictionary_reduction %s %f %f" \n', ...
                proc_id, task_name, req_memory, log_file, path_dict, Ncoef, meas_sel  ) );
        else
            system( sprintf( 'bigrsub -q week -N %s -R %dG -l %s "./dictionary_reduction %s %f %f" \n', ...
                task_name, req_memory, log_file, path_dict, Ncoef, meas_sel ) );
        end
    end
else
    dictionary_reduction( path_dict, Ncoef, meas_sel )
end

