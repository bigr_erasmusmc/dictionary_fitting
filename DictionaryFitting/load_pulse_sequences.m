function [ pulseseq ] = load_pulse_sequences( in )
%  load_pulse_sequences.m used to setup the parameters for generating
%  the signal dictionary and the command files for the cluster  
%   
%[ pulseseq ] = load_pulse_sequences( in )
%   loads a list of pulse sequences with the following fields:
% name : string with name of the acquisition
% description : a (long) description of the acquisition sequence
% prop : structure with properties of this acquisition.
% prep : Preparation pulses to reach a (pseudo) steady state.
%        Assumed to consume negligable time in actual acquisitions
% acq  : Actual acquisition block. This part is timed to obtain the
%        acquisition's time efficiency
%        prep and acq are equal size cell arrays, with each acq having a
%        corresponding prep.
% npreplps : number of preperation runs
% RFcycle : increment of RF phase [deg] in consequetive excitation
%           pulses (used for spoiling)
% predfun : empty or 2 element cell array, first element: intensity prediction
%   function with 1 argument (paramter vector), 2nd element: cell array
%   with property names from parnames in the order that predfun uses them.
%
% NOTE: time unit is ms.
%       spatial unit is 'voxel' (that is a distance 1 means 1 voxel displacement); Relevant for gradient (spoiling).
%
%     Copyright (C) 2019 Willem van Valenberg
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or (at
%     your option) any later version.
% 
%     This program is distributed in the hope that it will be useful, but
%     WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see
%     <https://www.gnu.org/licenses/>.

pulseseq = struct('name',{},'description',{},'prop',{},'prep',{},'acq',{}, 'npreplps', {}, 'RFcycle', {} ); % each element a cell array with MRI_pulses objects.

cur_id = 0; % id of acquisition that is currently being setup.

if isstruct( in ) % info_scan file from TOPPE
    minpreptime = in.t_prep;
    voxsize     = in.readout.L ./ in.readout.N;
    if voxsize(3) > in.tipdown.dz
        voxsize(3) = in.tipdown.dz;
    end
else
    minpreptime = in;
    voxsize = [1 1 1];
end

if nargin < 1 || isempty( in )
    minpreptime = (-log(.01))*5000; % make sure maximum residual non-steady state magnetization is < 1%. (assuming that T1 > T2)
end

MRFsettingsfolder = '../Data/MRF/';

%% MR fingerprinting, bSSFP type ( LUMC EXPERIMENT - 3T - 12/02/2018 )
cur_id = cur_id+1;

MRFsettingsfiles =  fullfile( MRFsettingsfolder, 'bSSFP_Ma2013.mat');

MRFsett             = load( MRFsettingsfiles, 'bSSFP' );

MRF.N               = numel( MRFsett.bSSFP.fa );
MRF.N               = 500;
MRF.FA              = double( MRFsett.bSSFP.fa(1:MRF.N ) );
MRF.TR              = double( MRFsett.bSSFP.tr(1:MRF.N ) );
MRF.phase           = double( MRFsett.bSSFP.ph(1:MRF.N ) );

MRF.TE              = 2.5; %ms
MRF.TI              = 20; %ms
MRF.Ginv            = 4*pi*[1; 1; 0]./voxsize'; % rad/unit space (voxel for TOPPE);
MRF.Ginv(3)         = max( MRF.Ginv );

MRF.Tread           = 1; %ms
MRF.delay           = 5000; %ms, wait time between acqu

[MRFpulses] = [ pulses_inversion( MRF.TI, MRF.TI - 5, MRF.Ginv )...
    pulses_MRF( MRF.TR, MRF.FA.*exp(-1i*MRF.phase), MRF.TE, [], [0 0 0], MRF.Tread ),...
    pulses_spoiler( MRF.Ginv, MRF.delay/2 ) pulses_delay( MRF.delay/2 ) ];
p_acq = {MRFpulses};

pulseseq(cur_id).name = 'MRF_balanced' ;
pulseseq(cur_id).description = 'Magnetic resonance fingerprinting acquisition. bSSFP (balanced) train in which the magnitude of the RF pulses as well as the TR is varied (Ma2013).';
pulseseq(cur_id).prop = MRF;
% pulseseq(cur_id).prep = p_init;
pulseseq(cur_id).acq = p_acq;
pulseseq(cur_id).prep = cell(size(p_acq));
pulseseq(cur_id).npreplps = ceil( minpreptime/p_acq{1}.totalDuration ) ; % number of preperation runs

clear fid MRF  MRFsettingsfiles MRFpulses

%% MR fingerprinting, FISP type ( LUMC EXPERIMENT - 3T - 12/02/2018 )
cur_id = cur_id+1;

MRFsettingsfiles =  fullfile( MRFsettingsfolder, 'FISP_Jiang2015_1000flips.mat');

MRFsett             = load( MRFsettingsfiles, 'FISP' );
MRF.N               = 1000;
MRF.FA              = double( MRFsett.FISP.fa( 1:MRF.N ) );
MRF.TR              = double( MRFsett.FISP.tr( 1:MRF.N ) );
MRF.phase           = double( MRFsett.FISP.ph( 1:MRF.N ) );

MRF.TE              = 2.5; %ms, LUMC acquisition
MRF.TE              = 3.5; %ms

MRF.TI              = 20; %ms, LUMC acquisition
MRF.Tread           = 1; %ms
MRF.delay           = 5000; %ms, wait time between acqu (LUMC)

MRF.Ginv            = [2*pi; 2*pi; 8*pi]./voxsize'; % rad/unit space;
MRF.Ginv(3)         = max( MRF.Ginv );
MRF.Gspoil          = 2*pi*[0 1 4]./voxsize; % 8 pi rad dephasing per unit space in the slice direction

pulseseq(cur_id).RFcycle = 0; %original paper does not have RF spoiling

[MRFpulses] = [ pulses_inversion( MRF.TI, MRF.TI - 3, MRF.Ginv )...
    pulses_MRF( MRF.TR, MRF.FA.*exp(-1i*MRF.phase), MRF.TE, [], MRF.Gspoil, MRF.Tread ),...
    pulses_spoiler( MRF.Ginv, MRF.delay/2 ) pulses_delay( MRF.delay/2 ) ];
p_acq = {MRFpulses};

pulseseq(cur_id).name = 'MRF_spoiled' ;
pulseseq(cur_id).description = 'Magnetic resonance fingerprinting acquisition. FISP (spoiled) train in which the magnitude of the RF pulses as well as the TR is varied (Ma2013).';
pulseseq(cur_id).prop = MRF;
pulseseq(cur_id).acq = p_acq;
pulseseq(cur_id).prep = {[]};

clear fid  MRFsettingsfiles MRFpulses
