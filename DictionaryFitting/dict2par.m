function [theta, Dtheta] = dict2par( PD, T, param, par_int, boundary_condition, estT2star, imageType )
%   maps dictionary grid positions and PD scaling to parameter values
%   INPUT:  PD:     complex scaling factor between data and dictionary atom
%                   (1xN)
%           T:      matrix containing P zero-based grid indices of N objects (PxN),
%           param:  structure with properties of Q parameters described by
%                   fields:
%                   - steps, number of discretized values
%                   - range, lower and upper bound [2x1]
%                   - scale, scaling of parameter: 'linear' or 'log'
%           par_int: indices of the parameters of T in structure param,
%                   default = find( [param.steps] > 1 )
%           boundary_condition: how to handle values outside range:
%                   - if 0, extrapolate values
%                   - if 1, mirror grid values to range (default)
%           estT2star:  boolean indicating if T2* is estimated from T2 and
%                       T2'
%
%   OUTPUT: theta:  parameter values (Q + estT2star x N)

if nargin < 4 || isempty( par_int )
    par_int = find( [param.steps] > 1 );
end
if nargin < 5 || isempty( boundary_condition )
    boundary_condition = 1;
end
if nargin < 6 || isempty( estT2star )
    estT2star = false;
end
if nargin < 7 || isempty( imageType )
    imageType = 'complex';
end
if size( PD, 1 ) == 2
    PD = PD( 1, : ) + 1i*PD( 2, : );
end

Pin  = size( T, 1 ) + 1 + strcmp( imageType, 'complex' ); % number of input parameters
Pout = numel( param ) + estT2star; % number of output parameters
N    = size( T, 2 ); % number of voxels;

theta = zeros( Pout, N );
idx_fix =  ~cellfun( @isempty, {param.val} );
if any( idx_fix )
    theta( idx_fix, : ) = repmat( [param( idx_fix ).val]', [1 N] );
end

[theta( par_int, : ), Dtheta0] = grid2par( T, param, par_int, boundary_condition );

idxAbs = find( strcmp( {param.name}, 'PD' ) + strcmp( {param.name}, 'Mz0' ) );
if ~isempty( idxAbs )
    %theta( idxAbs, : ) = abs( PD ) * mean( param( idxAbs ).range );
    theta( idxAbs, : ) = abs( PD );
else
    error( 'PD NOT INCLUDED IN PARAMETERS' );
end

idxAngle = find( strcmp( {param.name}, 'phi0' ) );
if ~isempty( idxAngle )
    theta( idxAngle, : ) = angle( PD );
else
    error( 'ANGLE NOT INCLUDED IN PARAMETERS' );
end

if nargout > 1 % determine derivative
    Dtheta = zeros( Pout, N, Pin );
    
    Npd = 1 + strcmp( imageType, 'complex' );
    
    for k = 1:size( T, 2 )
        Dtheta( par_int, k, Npd + (1:numel(par_int) ) ) =  diag( Dtheta0( :, k ) );
    end
    Dtheta( idxAbs, :, 1 )      =  real( PD )./theta( idxAbs, : );
    Dtheta( idxAngle, :, 1 )    =  -imag( PD )./theta( idxAbs, : ).^2;
    
    if strcmp( imageType, 'complex' )
        Dtheta( idxAbs, :, 2 )      =  imag( PD )./theta( idxAbs, : );
        Dtheta( idxAngle, :, 2 )    =  real( PD )./theta( idxAbs, : ).^2;
    end
end

if estT2star
    idxT2   = find( strcmp( {param.name}, 'T2' ) );
    idxT2p  = find( strcmp( {param.name}, 'T2prime' ) );
    idxT2s  = Pout;
    theta( idxT2s, : ) = ( theta( idxT2, : ).^-1 +  theta( idxT2p, : ).^-1 ).^-1;
    if nargout > 1
        Dtheta( idxT2s, :, Npd+idxT2 )    = Dtheta( idxT2,  :, Npd+idxT2  ).*( 1 + theta( idxT2,  : )./theta( idxT2p, : ) ).^-2;
        Dtheta( idxT2s, :, Npd+idxT2p )   = Dtheta( idxT2p, :, Npd+idxT2p ).*( 1 + theta( idxT2p, : )./theta( idxT2,  : ) ).^-2;
    end
end

end

