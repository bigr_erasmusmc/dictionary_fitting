function [ Sout ] = simulate_signal( spins, pulseseq, minpreptime, modfiles, mod_pos, sampling, Minit, doFuseGradients )
%[ Sout ] = simulate_signal( spins, pulseseq, minpreptime, modfiles, mod_pos, sampling, Minit, fuseGradients )

if nargin < 3
    minpreptime = [];
end
if nargin < 7
    Minit = [];
end
if ~iscell( spins )
    spins = {spins};
end

Sout = cell( numel( pulseseq ), numel( spins ) );

%% setup sequences

for k_seq = 1 : numel(pulseseq)
    
    if ~isfield( pulseseq(k_seq), 'RFcycle' )
        if isfield( pulseseq(k_seq), 'spoiling' ) %backwards compatible
            if pulseseq(k_seq).spoiling == 100
                pulseseq(k_seq).RFcycle = 117;
            else
                pulseseq(k_seq).RFcycle = 0;
            end
        else
            pulseseq(k_seq).RFcycle = 0;
        end
    end
    if isfield( pulseseq(k_seq), 'sub_mod' )
        acq_sim = prepare_sequence( pulseseq(k_seq).acq, minpreptime, pulseseq(k_seq).RFcycle, pulseseq(k_seq).sub_mod, pulseseq(k_seq).sub_idx );
    else
        acq_sim = prepare_sequence( pulseseq(k_seq).acq, minpreptime, pulseseq(k_seq).RFcycle );
    end
    
    for spinid = 1 : numel(spins)
        Sacq = cell( size( acq_sim ) );
        for k_acq = 1 : numel( acq_sim )
            Sacq{k_acq} = bloch_sim2( spins{spinid}, acq_sim{k_acq}, Minit);
        end;
        Sout{ k_seq, spinid } = vertcat(Sacq{:});
    end    
end

end

