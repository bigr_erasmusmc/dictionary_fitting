function dictionary_generation(destfolder, nprocs, procid)
%  dictionary_generation.m calculates a signal dictionary for a given
%  parameter discretization
%
%  as used in: 
%   An Efficient Method for Multi-Parameter Mapping in Quantitative MRI
%   using B-Spline Interpolation.
%
%   van Valenberg W, Klein S, Vos FM, Koolstra K, van Vliet LJ, Poot DHJ. 
%   
%     Copyright (C) 2019 Willem van Valenberg
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or (at
%     your option) any later version.
% 
%     This program is distributed in the hope that it will be useful, but
%     WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see
%     <https://www.gnu.org/licenses/>.
fprintf('STARTED \n')

if isdeployed
    nprocs = str2double(nprocs);
    procid = str2double(procid);    
end

load([destfolder 'disc_settings.mat'], 'blocksize', 'RFslice', 'xyz','spin_idx', 'param', 'edgeAtoms','sim_prec', 'p_disc', 'pulseseq', 'minpreptime', 'Nimg', 'maxSpin', 'spinDistribution');

%% PARAMETER MAPPING FOR EACH SEQUENCE
Nslice          = numel( RFslice );
Nspins          = size(xyz, 1);

pidx            = cell(numel(param),1);
voxelblock      = max( 1, floor( maxSpin/Nspins ) );

tic
for k_seq = 1:numel(pulseseq)
    
    fprintf('SIMULATING %s, USING BLOCKSIZE %d and %d SPINS ',pulseseq(k_seq).name, blocksize, Nspins);
    
    mkdir([destfolder pulseseq(k_seq).name ]);
    mkdir([destfolder pulseseq(k_seq).name '/dictionaries']);
    
    p = p_disc( :, k_seq );
    dim_grid =  cellfun(@numel, p).';
    nparam = prod( dim_grid );
    nblocks = ceil( nparam/blocksize );
    
    % setup pulse sequence
    if ~isfield( pulseseq(k_seq), 'RFcycle' )
        if isfield( pulseseq(k_seq), 'spoiling' ) %backwards compatible
            if pulseseq(k_seq).spoiling == 100
                pulseseq(k_seq).RFcycle = 117;
            else
                pulseseq(k_seq).RFcycle = 0;
            end            
        else
            pulseseq(k_seq).RFcycle = 0;
        end
    end
    if isfield( pulseseq(k_seq), 'sub_mod' )
        [acq_sim, rec_phase] = prepare_sequence( pulseseq(k_seq).acq, minpreptime, pulseseq(k_seq).RFcycle, pulseseq(k_seq).sub_mod, pulseseq(k_seq).sub_idx );
    else
        [acq_sim, rec_phase] = prepare_sequence( pulseseq(k_seq).acq, minpreptime, pulseseq(k_seq).RFcycle );
    end  
    
    rec_correction = reshape( exp( -1i*cat( 1, rec_phase{:} ) ), [], 1 );
    
    if procid == 1
        fprintf(['\n' 'SAVING FITTING INFO TO ' destfolder pulseseq(k_seq).name '/info_dictionary' '\n']);
        save([destfolder pulseseq(k_seq).name '/info_dictionary'], 'pulseseq', 'sim_prec', 'blocksize', 'p', 'xyz', 'Nimg', 'edgeAtoms');
    end
    
    local_block_idx = procid : nprocs : nblocks;
    fprintf('%d LOCAL BLOCKs, PROGRESS: ', numel(local_block_idx) );
    
    for k2 = local_block_idx % start_pos(2):end_simul
        fprintf('%d ',k2);
        filename = [destfolder pulseseq(k_seq).name '/dictionaries/dictionary' num2str(k2) '.mat'];
%         
%         if exist( filename, 'file' )
%             fprintf('\n FILE %s ALREADY EXISTS. SKIPPING \n',filename);
%         else
            
            atomidx = ( k2 - 1 ) * blocksize + 1:min( k2*blocksize, nparam );
            Nvox = numel( atomidx );
            
            M = zeros( Nimg, 3, Nvox );
            S = zeros( size(M,1), size(M,3) );
            
            for k_a = 1:voxelblock:Nvox
                
                idx_vox = k_a : min( k_a + voxelblock - 1, Nvox );
                
                %select parameters
                [pidx{:}]   = ind2sub( dim_grid, atomidx( idx_vox ) );
                p_block     = cellfun( @(P,I) P(I), p, pidx, 'UniformOutput', false );
                %p_all       = cat( 1, p_block{:} );
                
                %% CREATE SPINS
                spins = MRI_spins( p_block{ 1:5 }, Nspins, xyz, p_block{ 6 } ); %note conversion to T2star
                
                % set slice profile
                switch spinDistribution
                    case {0, 1, 4} % Cauchy distribution over whole slice
                        spins           = shuffleSpins( spins, spin_idx );
                    case 2 % Cauchy distribution same in each slice position
                        [B0w, B0]       = T2prime_spins( p_block{ 3 }, Nspins/Nslice, p_block{ 4 } );
                        spins.B0        = repmat( B0, Nslice, 1 );
                        spins.B0w       = repmat( B0w, Nslice, 1 );
                    otherwise
                        error('UNKNOWN SPIN DISTRIBUTION' );
                end
                
                spins = setSliceprofile( spins, RFslice );
                
                %% BLOCH SIMULATIONS
                
                % do Bloch simulations
                if 1
                    Mslice = cell(numel( acq_sim ), 1);
                    for k_acq = 1 : numel( acq_sim )
                        Mslice{k_acq} = bloch_sim2( spins, acq_sim{k_acq}, [] );
                    end;
                else
                    if isfield( pulseseq, 'sub_mod' )
                        Mslice = simulate_signal( spins, pulseseq, minpreptime, pulseseq.sub_mod, pulseseq.sub_idx );
                    else
                        Mslice = simulate_signal( spins, pulseseq, minpreptime );
                    end
                end
                Mslice = vertcat( Mslice{:} );
                
                for k_slice = 1:numel( idx_vox )
                    M(:,:, idx_vox(k_slice) ) = mean( Mslice(:,:, (k_slice-1)*Nslice + ( 1:Nslice ) ), 3 );
                end
            end
            
            %convert transverse magnetization to complex number
            S(:,:) = bsxfun( @times, M(:,1,:) + 1i*M(:,2,:), rec_correction );
            %S(:,:) = M(:,1,:) + 1i*M(:,2,:); 
            
            if strcmp( sim_prec, 'single' )
                S = single( S );
            end
            
            fprintf(['\n' 'SAVING DICTIONARY TO ' filename '\n']);
            save(filename, 'S');
            fprintf('PROGRESS: %d/%d, time: %f min\n\n', find( k2 == local_block_idx ), numel(local_block_idx), toc/60 );
        %end
    end
    
    fprintf('\n');
end

toc

end