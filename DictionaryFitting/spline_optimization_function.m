function [ Sout, dSout ] = spline_optimization_function(in, PD_real, PD_imag, Tin, mapT, newSz, F, bsplineord, AR, clip_i, boundary_condition, useMex)
% wrapper for the B-spline interpolation function

if nargin < 12
    useMex = false;
end

PD_in = PD_real + 1i*PD_imag;

%[ S0, dS0 ] = wrapperspline( in, Tin, mapT, newSz, F, bsplineord, AR, clip_i, boundary_condition);

if isempty(newSz)
    % first dimension in signal, last voxels, include scalar dimensions
    % to match size in.
    newSz = [nan ones(1,ndims(in)-2) size(Tin,2)];
end

T = permute( Tin ,[1 3:ndims(in) + 1 2]); % adjust number of dimensions to match dictionary

if useMex     
    if bsplineord > 0
        [S, dSdT] = TransformNDbspline( in, T, mapT, newSz, F, bsplineord, AR, clip_i, boundary_condition);
        dSdT = permute( dSdT, [1 ndims(in) + 1 2 3:ndims(in)] ); % difference in output size between c and m codes
    else
        S = TransformNDbspline( in, T, mapT, newSz, F, bsplineord, AR, clip_i, boundary_condition);
        dSdT = nan(1);
    end    
else % use MATLAB reference implementation
    if bsplineord > 0
        [S, dSdT] = TransformNDbspline_ReferenceImplementation( in, T, mapT, newSz, F, bsplineord, AR, clip_i, boundary_condition);
    else
        S = TransformNDbspline_ReferenceImplementation( in, T, mapT, newSz, F, bsplineord, AR, clip_i, boundary_condition);
        dSdT = nan(1);
    end
end

S = permute( S, [1 ndims(S) 2:ndims(S)-1]); % remove empty dimensions

Sout  = bsxfun(@times, PD_in, S);
dSout = cat(3, S, 1i*S, bsxfun(@times, PD_in, dSdT) );

end

