%  CMD_DICTIONARY_FITTING_INTERPOLATION.M used to setup the parameters for
%  dictionary fitting through interpolation and the command files for the cluster  
%   
%     Copyright (C) 2019 Willem van Valenberg
% 
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or (at
%     your option) any later version.
% 
%     This program is distributed in the hope that it will be useful, but
%     WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%     General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see
%     <https://www.gnu.org/licenses/>.    

addpath( genpath('../Tools') );

if isunix 
    mcc('-m','dictionary_fitting_interpolation.m','-R','-nojvm','-R','-singleCompThread','-R','-nosplash')
end

%% SETUP DICTIONARY FITTING 

path_dict           = select_dictionary( '../output/', true );

bsplineord          = 2;                % order of bspline
boundary_condition  = 5;                % 0 = zero, 1 = mirror
maxSize             = 10^4;             % maximal number of elements in memory during dictionary matching
file_dictmatch      = [];
useMex              = true;
dictReduced         = false;
nSVD                = 30;
fitConstraints      = 1;
saveSignal          = true;
p_prior             = [];

dict_segment        = {':', ':', ':', ':', ':', ':', ':'}; %T1, T2, T2', B0, PD, B1, phi0
progressbarGUI      = 'yes';

% CHOOSE MEASUREMENT DATA
base_data   = '../Data/';
%path_img   = [base_data 'phantom/phantom_3T_spoiled1000_spiral1.mat'];
%path_img   = [base_data 'phantom/phantom_3T_spoiled1000_spiral48.mat'];
%path_img   = [base_data 'volunteer/invivo_3T_spoiled1000_spiral48_nocpr.mat'];
path_img   = [ base_data 'testsets/testset_FISP.mat' ];

[~, img_name]           = fileparts( path_img );
name_mask_fit           = []; %'mask_map_full';
name_mask_background    = []; %'mask_background'; % give mask op background where the noiselvl is selected based on median std over all images

path_out = [path_dict img_name ];
acq_name = img_name;

fit_opts                        = struct; % clear options.
fit_opts.linearParameters       = [1 2];
fit_opts.optimizer              = 3;
fit_opts.noiseLevel             = 1e-2; %1e-8;
fit_opts.tolFun                 = 1e-5; %1e-5;
fit_opts.blockSize              = 1e0;
fit_opts.maxIter                = 1e2; %2e2; %
fit_opts.imageType              = 'complex';
fit_opts.hessian                = 'off';
fit_opts.profile                = 0;

if ~isempty( name_mask_fit )
    fit_name = sprintf( '%s_s%d_b%d',name_mask_fit, bsplineord, boundary_condition );
else
    fit_name = sprintf( 's%d_b%d', bsplineord, boundary_condition );
end
if fitConstraints
    fit_name = [fit_name '_con_I' num2str( fit_opts.maxIter ) ];
else
    fit_name = [fit_name '_unc_I' num2str( fit_opts.maxIter ) ];
end
if dictReduced
    if isempty( nSVD )
        fit_name = [fit_name '_red' ];
    else
        fit_name = [fit_name '_svd' num2str( nSVD ) ];
    end
end

idx_fix = find( cellfun( @(X) ~strcmp( X, ':'), dict_segment ) );
for k = 1:numel( idx_fix )
    fit_name = [fit_name '_p' num2str( idx_fix(k) ) 'fix' num2str( dict_segment{idx_fix(k)} )];
end

[~, name_img] = fileparts(path_img);

mkdir( path_out )
file_fitsettings = [path_out '/fit_settings_' fit_name ];

if ~exist( [ path_dict 'info_dictionary.mat' ], 'file' );
    error('DICTIONARY DOES NOT EXIST');
end

if ~isempty( name_mask_fit )
    path_mask_fit = select_mask( path_img, name_mask_fit );
else
    path_mask_fit = [];
end
if ~isempty( name_mask_background )
    path_mask_background = select_mask( path_img, name_mask_background );
else
    path_mask_background = [];
end

% SAVE FITTING SETTINGS
save( file_fitsettings, 'path_img', 'path_dict', 'path_mask_fit', 'path_mask_background', 'fit_opts', 'bsplineord', 'boundary_condition', 'maxSize', 'dict_segment', 'fit_name', 'dictReduced', 'useMex', 'file_dictmatch', 'p_prior', 'fitConstraints', 'saveSignal', 'nSVD')

%% SUBMIT JOB TO CLUSTER

name_test   = [acq_name '_' fit_name]
log_file    = ['../../output/log_dictionary_interpolation/' datestr( date, 'yyyy-mm-dd' ) '/' name_test ];
req_memory  = 20; %G
proc_id     = [];
if isunix
    if ~isempty( proc_id )
        system( sprintf( 'bigrsub -q day@node%03d.cm.cluster -N %s -R %dG -l %s "./dictionary_fitting_interpolation %s" \n', ...
        proc_id, ['dfit_' name_test], req_memory, log_file, file_fitsettings ) );
    else
        system( sprintf( 'bigrsub -q day -N %s -R %dG -l %s "./dictionary_fitting_interpolation %s" \n', ...
        ['dfit_' name_test], req_memory, log_file, file_fitsettings ) );
    end    
else
    dictionary_fitting_interpolation( file_fitsettings )    
end