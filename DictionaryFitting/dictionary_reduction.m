function dictionary_reduction( path_dict, varargin )
%% SETUP DICTIONARY + INTERPOLATION FUNCTION

%parnames = {'T1','T2','T2prime','B0','PD','B1','DT','T2star'};

Ncoef       = [];
meas_sel    = [];

if isdeployed
    progressbarGUI = 'no';
else
    progressbarGUI  = 'yes';
end
if nargin > 1
    if isdeployed        
        Ncoef = str2double(varargin{1})
        if numel( varargin ) > 1
            meas_sel = str2double(varargin{2})
        end
    else
        Ncoef = varargin{1}
        if numel( varargin ) > 1
            meas_sel = varargin{2}
        end
    end
end

%%

fprintf('LOADING DICTIONARY INFORMATION FROM: \n %s \n', [path_dict 'info_dictionary.mat'])
if exist([path_dict 'info_dictionary.mat'], 'file')
    % load dictionary
    load([path_dict 'info_dictionary']);
    par_int = find( cellfun( @numel, p ) > 1 ).';
else
    error('FILE NOT FOUND');
end

if ~isempty( meas_sel )
    if Nimg < max( meas_sel )
        error('TOO FEW IMAGES FOR SELECTION');
    end
    if numel( meas_sel ) < Nimg
    warning( 'not all images used for projection' );
end
else
    meas_sel = 1:Nimg;
end

p = reshape( p, 1, [] );

dim_p     = cellfun( @numel, p );
Natoms    = prod( dim_p( par_int ) );
Nblocks   = ceil( Natoms / blocksize );

dim_dict  = [ numel( meas_sel ),  dim_p( par_int ) ];
pidx      = cell( numel(dim_p), 1 );
[pidx{:}] = ind2sub( dim_p, 1:Natoms );
pidx = cellfun( @uint16, pidx, 'UniformOutput', false);

%% LOAD DICTIONARIES

calc_cov = true;

if exist( [path_dict 'dict_covariance_matrix.mat'], 'file')
    calc_cov = false;
    if ~isdeployed
        str = input( 'DICTIONARY COVARIANCE MATRIX ALREADY EXISTS. REMOVE? (y/n) \n', 's');
        if strcmp( str, 'y' );
            delete( [path_dict 'dict_covariance_matrix.mat'] );
            calc_cov = true;
        end
    end
end

if calc_cov
    fprintf( 'DICTIONARY COVARIANCE MATRIX NOT FOUND. CALCULATING. \n');
    
    covariance_matrix   = zeros( Nimg );
    
    progressbar('start', [], 'Loading dictionaries. Please wait...', 'hasGUI', progressbarGUI, 'EstTimeLeft', 'on');
    for k = 1 : Nblocks
        if exist([path_dict 'dictionaries/dictionary' num2str(k) '.mat'], 'file')
            dict_data = load([path_dict 'dictionaries/dictionary' num2str(k)], 'S');
            S = double( dict_data.S );
            
            if any( isnan( S(:) ) )
                S( isnan( S(:) )  ) =0;
            end
            
            covariance_matrix = covariance_matrix + S  * S';
            
        else
            warning(['dictionary' num2str(k) ' does not exist']);
        end
        
        if strcmp( progressbarGUI, 'yes' )
            progressbar(k / Nblocks);
        else
            if mod(k,100) == 0
                fprintf(1, '.');
            end
        end
    end
    progressbar('ready');
    fprintf( 'CREATED COVARIANCE MATRIX. NOW EIGENVALUE DECOMPOSITION.\n');
    [V, D] = eig( covariance_matrix( meas_sel, meas_sel ) );
    fprintf( 'SAVING COVARIANCE MATRIX AND EIGENVALUE DECOMPOSITION TO: %s \n', [path_dict 'dict_covariance_matrix']);
    
    save( [path_dict 'dict_covariance_matrix'], 'covariance_matrix', 'V', 'D', 'meas_sel' );
else
    load( [path_dict 'dict_covariance_matrix'], 'V', 'D' );
end

%%
[sigmas, idx] = sort( real( sqrt( diag( D ) ) ), 'descend' );
energy =  cumsum( sigmas.^2 )/sum( sigmas.^2 );
Ncoef0 = find( energy > .99, 1, 'first' )
if isempty( Ncoef )
    Ncoef = max( Ncoef0, 2*(nnz( dim_p > 1 ) + 2) )
end

if ~isdeployed
    
    line_width = 3;
    
    figure
    subplot( 3,1,1 )
    semilogy( sigmas, 'LineWidth', line_width );
    set( gca, 'XLim', [0, numel(sigmas)] )
    title( 'singular values' );
    grid on
    subplot( 3,1,2 )  
    semilogy( energy, 'LineWidth', line_width );
    title( 'cumulative energy' );
    grid on
    
    subplot( 3,1,3 )
    plot( abs( V( :, idx(1:Ncoef0) ) ) ); 
    title( 'singular vectors representing 99% of the energy' );
    hold on    
    plot( abs( V( :, idx(Ncoef0+1:Ncoef) ) ), '--' );
    hold off
    
end

%%
if ~isempty( Ncoef );
    
    if Ncoef < Nimg
        
        path_dict_red = [path_dict 'dictionaries_svd' num2str(Ncoef) '/'];        
        mkdir( path_dict_red )
        %mkdir( [path_dict 'reduced_dictionaries'] )        
        
        fprintf( 'PROJECTING DICTIONARY TO FIRST %d SINGULAR VECTORS.\n', Ncoef);
        
        [~, idx] = sort( real( sqrt( diag( D ) ) ), 'descend' );
        
        proj   = V( :, idx(1:Ncoef) )';        
        
        progressbar('start', [], 'Loading dictionaries. Please wait...', 'hasGUI', progressbarGUI, 'EstTimeLeft', 'on');
        for k = 1 : Nblocks
            path_dict_block = [path_dict 'dictionaries/dictionary' num2str(k) '.mat'];
            if exist( path_dict_block, 'file' )
                dict_data   = load( path_dict_block, 'S');
                S           = proj * dict_data.S( meas_sel , : );
                if isa(dict_data.S, 'single')
                    S = single( S );
                end
                save( [path_dict_red 'dictionary' num2str(k)], 'S');
            else
                warning(['dictionary' num2str(k) ' does not exist']);
            end
            
            if strcmp( progressbarGUI, 'yes' )
                progressbar(k / Nblocks);
            else
                if mod(k,100) == 0
                    fprintf(1, '.');
                end
            end
        end
        progressbar('ready');
        save( [path_dict 'dict_covariance_matrix'], 'Ncoef', '-append' );

        fprintf( 'DONE.\n');
        
    else
        error('ReductionFactor should reduce the number of points')
    end     
end

end

