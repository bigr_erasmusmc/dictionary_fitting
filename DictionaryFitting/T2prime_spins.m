function [M0, B0] = T2prime_spins( T2prime, nsamp , B0c, M0c, samplingpattern)
% [M, B] = T2prime_spins( T2prime, nsamp [,B0, M0] );
% Creates a vector with spin magnitudes and offresonance 
% frequencies that is well suited for bloch simulations involving T2prime.
% The B weighted by M gives a Cauchy distributed set of offresonances,
% which gives exponential decay as observed for T2'.
%
% INPUTS:
%  T2prime : row vector with T2prime time of the simulated voxel(s), in ms
%  nsamp  : number of spins with which the T2prime effect is simulated
%           Need many, default = 1001;
%           In the demo experiment below this leads to a nsamp =[1000 10000]
%           induced standard deviation in the predicted signals of:
%              [.0046 .00083] * Mz0  for samplingpattern=1
%              [.0015 .00016] * Mz0  for samplingpattern=2
%              [.0172 .00605] * Mz0  for samplingpattern=3
%              [.0047 .00100] * Mz0  for samplingpattern=4
%  B0     : row vector with base off-resonance frequency in rad/ms
%  M0     : row vector with resting state Z-magnetisation of the voxel
%  samplingpattern : scaling integer, default = 2
%           1 : uniform M
%           2 : Chebychev nodes (=better than uniform for T2*)
%           3 : random ( M= uniform and B0 is a realization of a Cauchy
%               distribution; note that the mean and (thus also) variance
%               are undefined for the Cauchy distribution) 
%           4 : random ( M = weighted )
% 
% OUTPUTS:
%  M      : fractional resting state Z-magnetisation for each spin
%  B      : Off resonance frequency for each spin.
%
% DEMO:
%  nspins = 10000;
%  TE = 50; T2prime = 20; T2 = 1000;
%  Tsamp = 0.25:.5:200; Tsamp( abs(Tsamp-TE/2)<=TE*.05 ) =[];
%  p = addSampling( removeSampling( pulses_FSE(TE,1,1,0) ), Tsamp );
%  s = cell(1,5);
%  s(:) = {MRI_spins( 3000, T2, T2prime, 0, 1, nspins, 1 )};
%  [s{1}.B0w, s{1}.B0] = T2prime_spins( T2prime, nspins, [], [], 1);
%  [s{2}.B0w, s{2}.B0] = T2prime_spins( T2prime, nspins, [], [], 2);
%  [s{3}.B0w, s{3}.B0] = T2prime_spins( T2prime, nspins, [], [], 3);
%  [s{4}.B0w, s{4}.B0] = T2prime_spins( T2prime, nspins, [], [], 4);
%  [s{5}.B0w, s{5}.B0] = T2prime_spins( T2prime, nspins*10, [], [], 3);
%   s{5}.xyz = randn(size(s{5}.B0w,1),1,3);
%  pred = cell(1,numel(s)+1);
%  for k=1:numel(s)
%    pred{k} = bloch_sim2( s{k}, p );
%  end;
%  pred{end} = exp(- min(abs(Tsamp), abs(Tsamp - TE ))/T2prime )'.*exp( - Tsamp'./T2);
%  allpred = [pred{:}];
%  plot(Tsamp, allpred)
%  residu = bsxfun(@minus, [-allpred(:,[2:3:end-1])], allpred(:,end));
%  residu(23:27,:)=[];
%  std(residu)
%
% Use separate bloch simulation for each spin. 
% Then get the combined voxel value by summing the magnetisation vectors.
%
% Created by Dirk Poot, Erasmus MC,
% 25-1-2013

if nargin<2 || isempty(nsamp)
    nsamp = 1001;
end;
if nargin<5 || isempty(samplingpattern)
    samplingpattern = 2;
end;
% intra-voxel dephasing (T2*), simulated by a Cauchy distributed set of offresonance spins. 
% By default we use $N=1001$ spins to obtain high accuracy. The offresonance frequencies are 
% obtained by sampling the inverse Cauchy cumulative distribution function at the Chebychev 
% nodes ($x_k=\cos( \pi (2k  +1) / (2 N) )$, $k=0,\ldots,N-1$) with a weight corresponding
% to the node distance ($w_k = \cos( \pi k / N ))-\cos( \pi (k+1) / N ))$). Thus the
% offresonance frequencies are $f_k = 1/T_2* \tan(x_k*\pi/2)$ (rad/ms) when T2* is specified in ms.

centraldiff = false;
if samplingpattern==1
    cxsmpls = 1-2*(.5:nsamp)'/nsamp;
    cxsmpls_halfw = 1-2*(1:nsamp-1)'/nsamp; % -1 and 1 are included automatically.
elseif samplingpattern==2
    xsmpls = (.5:nsamp)'/nsamp;
    xsmpls_halfw = (1:nsamp-1)'/nsamp;
    cxsmpls = cos(pi*xsmpls);
    cxsmpls_halfw = cos(pi*xsmpls_halfw);  % -1 and 1 are included automatically.
elseif samplingpattern==3
    cxsmpls =  1-2*rand(nsamp, numel(T2prime) );
    cxsmpls_halfw = 1-2*(1:nsamp-1)'/nsamp;  %cxsmpls(1:nsamp-1)'/nsamp; % zero and 1 are included automatically.
elseif samplingpattern==4
    cxsmpls =  1-2*sort(rand(nsamp, numel(T2prime) ));
    cxsmpls_halfw = cxsmpls;  %cxsmpls(1:nsamp-1)'/nsamp; % zero and 1 are included automatically.
    centraldiff = true;
else
    error('invalid samplingpattern');
end;
if nargin<4 || isempty(M0c)
    M0c = ones(size(T2prime));
end;
cumpdf = [0; -1/pi*atan(tan( cxsmpls_halfw * pi/2))+.5; 1];
if centraldiff
    wpdf = .5*(cumpdf(3:end,:)-cumpdf(1:end-2,:));
    wpdf([1 end]) = wpdf([1 end]) + .5*[cumpdf(2) ; 1-cumpdf(end-1)];
else
    wpdf = diff(cumpdf);
end;
M0 = bsxfun(@times, M0c       , wpdf         ); 
B0 = bsxfun(@times, 1./T2prime , tan( cxsmpls * pi/2) );
if nargin>=3 && ~isempty(B0c) && ~all(B0c==0)
    B0 = bsxfun(@plus, B0 , B0c ) ; 
end;